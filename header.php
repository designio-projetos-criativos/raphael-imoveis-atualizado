<div class="auto fix">
	<h1 class="logoTopo fix fl hide-mobile"><a href="index.php"><img src="img_index/raphael-imoveis.png" class="fl" alt=""></a></h1>
	<a href="https://www.facebook.com/pages/Raphael-Im%C3%B3veis/110853852397561?fref=ts" target="_blank" class="btn-fb fr hide-mobile"></a>
	<ul id="mainMenu" class="fl hide-mobile">
		<li class="fl">
			<a href="historico.php" title="historico">Histórico</a>
			
		</li>
		<li class="fl separador"></li>
		<li class="fl">
			<a href="#">Aluguéis</a>
			<ul class="subMenu subMenuAlugueis">
				<li><a href="pesquisa.php?busca=L">Listar todos os imóveis</a></li>
				<li><a href="aval_imo.php">Avaliação do imóvel</a></li>
				<li><a href="devo_imo.php">Devolução do imóveis</a></li>
				<li><a href="docs.php">Doc. para locação</a></li>
				<li><a href="garantias.php">Garantias exigidas</a></li>
				<!-- <li><a href="taxas.php">Taxas/Proprietários</a></li> -->
				<li><a href="/usuario/">2º Via de boletos</a></li>
			</ul>
		</li>
		<li class="fl separador"></li>
		<li class="fl">
			<a href="#">Vendas</a>
			<ul class="subMenu subMenuVendas">
				<li><a href="pesquisa.php?busca=V">Listar todos os imóveis</a></li>
				<li><a href="aval_imov.php">Avaliação do imóvel</a></li>
				<li><a href="responsa.php">Responsabilidade</a></li>				
			</ul>
		</li>
		<li class="fl separador"></li>
		<li class="fl">
			<a href="juridico.php">Jurídico </a>
			<ul class="subMenu subMenuVendas">
				<li><a href="juridico.php">Advogados coligados</a></li>
				<li><a href="lei_inq.php">Lei do inquilinato</a></li>
				<li><a href="lei_cond.php">Lei dos condomínios</a></li>				
			</ul>
		</li>
		<li class="fl separador"></li>
		<li class="fl">
			<a href="contato.php">Contato </a>
			<ul class="subMenu subMenuVendas">
				<li><a href="contato.php">Fale conosco</a></li>
				<li><a href="curriculo.php">Envie seu currículo</a></li>							
			</ul>
		</li>
		<li class="fl separador"></li>
		<li class="fl">
			<a href="servicos.php">Serviços</a>
			<ul class="subMenu subMenuVendas">
				<li><a href="servicos.php">Obras</a></li>
			</ul>
		</li>
		<li class="fl separador"></li>
		<li class="fl">
			<a href="adm_cond.php">Condomínios</a>
			<ul class="subMenu subMenuCond">
				<li><a href="adm_cond.php">Proposta administração</a></li>
				<li><a href="condominios.php">Nossos serviços</a></li>							
				<li><a href="/usuario/">2ª via de boleto</a></li>							
			</ul>
		</li>
	</ul>

	<div class="menuMob hide-desktop">
		<a href="<?=$caminho?>"><img src="img_responsa/logo_resp.png" alt=""></a>
		<span class="js-span">
			
		</span>
		<ul class="js-menu menu ">
				<li><a href="historico.php">histórico</a></li>
				<li class="js-menu-alugueis">aluguéis
					<ul>
						<li><a href="busca.php?busca=L">Pesquisa imóveis</a></li>
						<li><a href="pesquisa.php?busca=L">Listar todos imóveis</a></li>
						<li><a href="aval_imo.php">Avaliação de imóvel</a></li>
						<li><a href="devo_imob.php">Devoluções</a></li>
						<li><a href="docs.php">Doc. para locação</a></li>
						<li><a href="garantias.php">Garantias exigidas</a></li>
						<li><a href="/usuario/">2° via boleto</a></li>
					</ul>
				</li>
				<li class="js-menu-vendas">vendas
					<ul>
						<li><a href="busca.php?busca=V">Pesquisa imóveis</a></li>
						<li><a href="pesquisa.php?busca=V">Listar todos imóveis</a></li>
						<li><a href="aval_imov.php">Avaliação de imóvel</a></li>
						<li><a href="responsa.php">Responsabilidade</a></li>
						
					</ul>
				<li class="js-menu-juridico">jurídico
					<ul>
						<li><a href="juridico.php">Advogados coligados</a></li>
						<li><a href="lei_inq.php">Lei do inquilinato</a></li>
						<li><a href="lei_cond.php">Lei dos condomínios</a></li>
					</ul>
				</li>
				<li class="js-menu-contato">contato
					<ul>
						<li><a href="contato.php">Fale conosco</a></li>
						<li><a href="curriculo.php">Envie seu currículo</a></li>
					</ul>
				</li>
				<li class="js-menu-servicos">serviços
					<ul>
						<li><a href="servicos.php">Obras</a></li>
						
					</ul>


				</li>
				<li class="js-menu-condominios">condomínios
					<ul>
						<li><a href="adm_cond.php">Propostas Administração</a></li>
						<li><a href="condominios.php">Nossos Serviços</a></li>
						<li><a href="/usuario/">2° via de boleto</a></li>
					</ul>

				</li>
			</ul>


		

		<!-- <select id="js-mobile-menu">
				<option value="<?=$caminho?>">Home</option>
				<option value="<?=$caminho?>sobre">Sobre</option>
				<option value="<?=$caminho?>produtos">Produtos</option>
				<option value="<?=$caminho?>servicos">Serviços</option>
				<option value="<?=$caminho?>ppci">PPCI</option>
				<option value="<?=$caminho?>contato">Contato</option>
			</select> -->
	</div>