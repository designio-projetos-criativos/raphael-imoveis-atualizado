<?php require "head.php";?>
</head>
<body id="internas" class="corretores">
<? require "header.php"; ?>
<div class="content">
	<div class="centro636">
		<h1 class="bordLaranja">Responsabilidade</h1>
		<h2 class="seo">Competência, Honestidade e Profissionalismo</h2>
		<table cellpadding="0" cellspacing="0" class="table_familia">
			<tbody>
				<tr>
					<th colspan="2">Compre e Venda Imóveis com Segurança</th>
				</tr>
				<tr> 
					<td><img src="familia4.jpg" alt=""/></td>
					<td>
						<p>A compra de um imóvel é sempre a realização de um sonho.
						Na maioria das vezes, representa o emprego de valores economizados
						ao longo de uma vida inteira. Para o investidor, adquirir imóveis
						também significa planejamento e garantia de uma aplicação segura.
						Exatamente por ser uma área que exige dedicação e responsabilidade,
						o cliente precisa contar com uma empresa séria, com tradição e
						credibilidade no ramo imobiliário. Na Raphael Imóveis, desde 1991,
						você encontra tudo isso. </p>
						<img src="ponto.gif" alt=""/>
						<p>Queremos assessorá-lo nessa hora tão especial e, para isso, oferecemos
						um atendimento personalizado, com o suporte de um departamento jurídico,
						próprio e especializado em Direito Imobiliário.</p>
					</td>
				</tr>
				
			</tbody>
		</table>
	</div>
</div>
</div>
<?php require "footer.php"; ?>
</body>
</html>
