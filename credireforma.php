<?php require "head.php"; ?>
</head>
<body id="internas" class="servicos">
<h1 class="seo">Serviços</h1>
<?php require "header.php"; ?>
<div class="content">
	<div class="centro636">
		<h2 class="bordLaranja">Credireforma</h2>
		<div class="just">
			<table cellpadding="0" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td align="left">
						<img src="img/credi.jpg" width="150" height="109" alt="credireforma"/>
						</td>
						<td style="padding:0 0 0 10px;">
							<p>
								<strong>Primando pela qualidade dos imóveis, a Raphael criou o CrediReforma, um benefício para 
								os proprietários que desejam valorizar ainda mais o seu patrimônio. 
								Assim, caso o seu imóvel necessitar de benfeitorias e você não estiver preparado para 
								esse investimento, a Raphael financia a obra.</strong>
							</p>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="center">
				<br />
				<p><img src="img/logo.jpg" alt="" /></p>
				<p><strong><em>"Seu imóvel aqui, você mais tranqüilo aí"</em></strong></p>
			</div>
			<div class="caixaverde">
				Informe-se pelo Telefone (53)3225.1100
			</div>
		</div>
	</div>	
</div>
</div>
<?php require "footer.php"; ?>
</body>
</html>

