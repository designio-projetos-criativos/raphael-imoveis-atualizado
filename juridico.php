<?php require "head.php";?>
</head>
<body id="internas" class="historico">

<?php require "header.php"; ?>
<div class="content">
	<div class="centro636" style="width:565px;">
		<h1 class="bordLaranja">Advogados Coligados</h1>
		<div class="adv" style="display:block;text-align:center;padding:0 0 15px 0;">
			<img class="img_adv" src="img/img_morales.jpg" alt="Morales | Advogados coligados" style="display:inline;text-align:center;" height="100" width="129"/>
		</div>
		<div class="juriCol01">
			<div class="box_direitoimobi">
				<p class="hide-mobile">
					<strong>DIREITO IMOBILIÁRIO</strong><br/>
					Advogados coligados à Raphael Imóveis<br/>
					Rua Santa Cruz, 1992 - <strong>Tel.</strong>: 3225-1100
				</p>
				<h3 class="hide-desktop">direito imobiliário</h3>
				<ul class="listStyle01" style="margin:0 0 15px 0;">
					<li>Ação de retomada de imóvel</li>
					<li>Ação de despejo por falta de pagamento</li>
					<li>Cobrança de alugueis e taxas atrasadas</li>
					<li>Contestações</li>
					<li>Recursos em geral</li>
				</ul>
				<p class="hide-mobile" style="margin:0 0 30px 0;"><strong>Consultas com hora marcada</strong></p>
				<h3 class="hide-desktop">Consultas com hora marcada</h3>
				<p class="adv_p">A <strong>Raphael Imóveis</strong> mantêm em seu quadro funcional, para lhe dar suporte e tranqualidade em suas transações imobiliárias, um DEPARTAMENTO JURÍDICO próprio e especializado em Direito Imobiliário.</p>
			</div>
		</div>
		<div class="juriCol02">
			<div class="box_advogados">
				<strong class="tit01">Advogados</strong>
				<ul class="listStyle01 hide-mobile">
					<li>Felipe Duarte da Costa<br/><span class="f10">OAB/RS 79.340</span></li>
					<li>Mariana Magalhães Monteiro<br/><span class="f10">OAB/RS 80.717</span></li>
				</ul>
				<p class="hide-desktop">Felipe Duarte da Costa - OAB/RS 79.340</p>
				<p class="hide-desktop">Mariana Magalhães Monteiro - OAB/RS 80.717</p>

				<p class="hide-desktop">Advogados coligados à Raphael Imóveis</p>
				<p class="hide-desktop">Rua Santa Cruz, 1992 - Tel.: 3225-1100</p>
			</div>

			<div class="box_causas">
				<strong class="tit01">Causas</strong>
				<ul class="listStyle01">
					<li>Cíveis em geral</li>
					<li>Locações</li>
					<li>Separações</li>
					<li>Inventarios</li>
					<li>Previdenciário</li>
					<li>Trabalhista (patronal)</li>
					<li>Cobranças</li>
				</ul>
			</div>

		</div>
		<br class="clr" />
	</div>
</div>
</div>
<?php require "footer.php"; ?>
</body>
</html>
