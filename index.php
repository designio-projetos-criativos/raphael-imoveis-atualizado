﻿<?php
	include "head.php";
?>
<title>.: Raphael Imóveis :.</title>
</head>

<body>
<?php // include "floater.php";?>

<?php
	include "header.php";
?>


	<div id="slider" class="fl clr">
		<ul id="sliderHome">
			<!-- <li><a href="http://www.raphaelimoveis.com.br/contato.php"><img src="slider/residencial-das-flores-raphael-imoveis.jpg" alt="Residencial das Flores"></a></li> -->
			<!-- <li><img src="slider/recesso-2016.png" alt="Recesso 2016"></li> -->
			<li><a href="http://www.raphaelimoveis.com.br/contato.php"><img src="slider/banner-hola-studio-pelotas.jpg" alt="Hola Studio Pelotas"></a></li>
			<li><a href="http://www.raphaelimoveis.com.br/contato.php"><img src="slider/banner-san-francisco-residence-pelotas.jpg" alt="San Francisco Residence Pelotas"></a></li>
			<li><a href="http://www.raphaelimoveis.com.br/contato.php"><img src="slider/residencial-greenville-pelotas-raphael-imoveis.jpg" alt="Residencial GreenVille"></a></li>
			<li><a href="http://www.raphaelimoveis.com.br/contato.php"><img src="slider/residencial-monet-pelotas-raphael-imoveis.jpg" alt="Residencial Monet"></a></li>
		</ul>
		<button class="png-bgr-prev"></button>
		<button class="png-bgr-next"></button>
	</div>
		<h3 class="hide-desktop realize">Realize sua busca</h3>

	<div class="boxbusca fl">
		<button class="vendas buscaActive hide-mobile">Vendas</button>
		<button class="alugueis hide-mobile">Aluguéis</button>
		<div class="botoes_mobile">
			<button class="js-vendas active  vendas buscaActive hide-desktop">Vendas</button>
			<button class="js-alugueis alugueis hide-desktop">Aluguéis</button>
		</div>

		<form id="formBuscaVenda" action="pesquisa.php">
		
			<fieldset>
                <input type="hidden" name='busca' value="V"/>
				<label for="codigo_imovel" class="fl clr" width="80px !important;">Código :</label>
				<input type="text" name="codigo_imovel" id="codigo_imovel" class="inputText fr" style="text-transform:uppercase;">

				<label for="cmb_tipo" class="fl clr">Tipo :</label>
				<select name="tipo" id="cmb_tipo_venda" class="fr">
					<option value="" selected="selected">Todos</option>
					<?php
					$dados = mysql_query("SELECT * FROM imobiliar_tipo_imoveis WHERE locVenda = 'V'");
					while($linha = mysql_fetch_array($dados)){
						echo "<option value=\"".($linha["id"])."\">".($linha["descricao"])."</option>";
					}
					?>
				</select>

				<label for="cmb_valor" class="fl clr">Valor :</label>
				<select name="valor" id="cmb_valor_venda" class="fr">	
					<option value="" selected="selected">Indiferente</option> 
                    <option value="0/150000"> Até 150.000,00</option>
                    <option value="150000/250000"> Entre 150.000,00 e 250.000,00</option>   
                    <option value="250000/350000"> Entre 250.000,00 e 350.000,00</option>   
                    <option value="350000/450000"> Entre 350.000,00 e 450.000,00</option>   
                    <option value="450000/550000"> Entre 450.000,00 e 550.000,00</option>   
                    <option value="550000/750000"> Entre 550.000,00 e 750.000,00 </option>
		    <option value="750000/950000"> Entre 750.000,00 e 950.000,00 </option>
		    <option value="950000/"> Acima de 950.000,00 </option>	
				</select>

				<label for="cmb_bairro" class="fl clr">Bairro :</label>
				<select name="bairro" id="cmb_bairro_venda" class="fr">
					<option value="" selected="selected">Indiferente</option>
					<?php
					$dados = mysql_query("SELECT * FROM imobiliar_bairros WHERE locVenda = 'V'");
					while ($linha = mysql_fetch_array($dados)){
						echo "<OPTION value=\"".($linha["nome"])."\">".($linha["nome"])."</OPTION>";
					}
					?>
				</select>
				<input type="submit" class="inputSubmit fr clr" value="buscar">
			</fieldset>
		</form>

		<form id="formBuscaAluguel" class="displayNone" action="pesquisa.php">
			<fieldset>
                <input type="hidden" name='busca' value="L"/>
				<label for="codigo_imovel" class="fl clr" width="80px !important;">Código :</label>
				<input type="text" name="codigo_imovel" id="codigo_imovel" class="inputText fr" style="text-transform:uppercase;">

				<label for="cmb_tipo" class="fl clr">Tipo :</label>
				<select name="tipo" id="cmb_tipo" class="fr">
					<option value="">Todos</option>
					<?php
					$dados = mysql_query("SELECT * FROM imobiliar_tipo_imoveis WHERE locVenda = 'V'");
					while($linha = mysql_fetch_array($dados)){
						echo "<option value=\"".($linha["id"])."\">".($linha["descricao"])."</option>";
					}
					?>
				</select>

				<label for="cmb_valor" class="fl clr">Valor :</label>
				<select name="valor" id="cmb_valor" class="fr">
					<option value="" selected="selected">Indiferente</option>
					<option value="0/1000"> Até R$ 1000,00</option>
                    <option value="1000/1500"> Entre R$ 1000,00 e R$ 1.500,00</option>
                    <option value="1500/2000" style> Entre R$ 1.500,00 e R$ 2.000,00</option>
                    <option value="2000/"> Acima de R$ 2.000,00</option>
				</select>

				<label for="cmb_bairro" class="fl clr">Bairro :</label>
				<select name="bairro" id="cmb_bairro" class="fr">
					<option value="">Indiferente</option>
					<?php
					$dados = mysql_query("SELECT * FROM imobiliar_bairros WHERE locVenda = 'V'");
					while ($linha = mysql_fetch_array($dados)){
						echo "<OPTION value=\"".($linha["nome"])."\">".($linha["nome"])."</OPTION>";
					}
					?>
				</select>

				<input type="submit" class="inputSubmit fr clr" value="buscar">
			</fieldset>
		</form>
	</div>

	<div class="grid fix clr fl">
		<h3 class="chamadaVendas">Vendas</h3>
		<div id="sliderVendas" class="fl">
			<ul id="sliderVendasHome">
				<?php
					$consulta = mysql_query("SELECT * FROM(
					SELECT imobiliar_imoveis.id as id, imobiliar_fotos.arquivo as arquivo, imobiliar_imoveis.tipo as tipo, imobiliar_imoveis.nome_bairro as nome_bairro, imobiliar_imoveis.dormitorios as dormitorios, imobiliar_imoveis.valor as valor
					FROM imobiliar_fotos JOIN imobiliar_imoveis ON imobiliar_fotos.id_imovel = imobiliar_imoveis.id
					WHERE imobiliar_imoveis.locVenda = 'V' AND imobiliar_fotos.arquivo<>'' AND imobiliar_fotos.legenda = 'fa' GROUP BY imobiliar_imoveis.id
					) as nova_tabela ORDER BY id DESC LIMIT 3");
					if(mysql_num_rows($consulta) == 0){
						echo '<p>Nenhum imóvel em destaque no momento</p>';
					}else{
						while($imovel = mysql_fetch_array($consulta)){
                            $carac = "";
							$caracteristicas = mysql_query("SELECT * FROM imobiliar_descricao_imoveis WHERE id_imovel = {$imovel['id']}");
							while ($concat = mysql_fetch_array($caracteristicas)) {
								$carac .= " {$concat['extenso']},";
							}

							$qrtipo =  mysql_query("SELECT * FROM imobiliar_tipo_imoveis WHERE id = {$imovel['tipo']} AND locVenda = 'V'");
		                    
		                    while($tipo = mysql_fetch_array($qrtipo)){
		                        $_tipo = $tipo['descricao'];
		                    }
							
							?>
							<li class="fl fix">
								<div class="boxImg fl" style="width:450px;">
									<a href="javascript:;" data-modal="pesquisa=vendas" class="fl openmodal" rel="<?=$imovel['id']?>">
										<img  src="<?= $imovel['arquivo'] ? "{$path['imagens']}/{$imovel['id']}/{$imovel['arquivo']}" : "http://www.raphaelimoveis.com.br/c65bcf-raphael-sem-imagem.png"?>" class="fl" style="margin-right:10px;">
										<!-- <h4 class="hide-mobile" style="float:left; margin-bottom:5px; width:190px; font-size:20px;"><?=$imovel['id']?></h4> -->
										<div class="home_info" style="float:left; width:190px; line-height:17px;">
											<span><strong>Código:</strong> <?=$imovel['id']?></span>
											<span><strong>Tipo:</strong> <?=$_tipo?> </span>
											<span><strong>Bairro:</strong> <?=($imovel['nome_bairro'])?></span>
											<span><strong>Quartos:</strong> <?=$imovel['dormitorios'] ? $imovel['dormitorios'] : "Sem dormitórios"?></span>
											<span class="valor">R$<?= number_format($imovel['valor'],2,",",".")?></span>
										</div>
									</a>
								</div>
							</li>
						<? }//WHILE ?>
					<? }//ELSE ?>
			</ul>
			<button class="prev-vendas"></button>
			<button class="next-vendas"></button>
		</div>
	</div>

	<div class="grid marginGrid fix fl">
		<h3 class="chamadaAlugueis">Alugueis</h3>
		<div id="sliderAlugueis" class="fl">
			<ul id="sliderAlugueisHome">
			<?php
				$consulta = mysql_query("SELECT * FROM(
				SELECT imobiliar_imoveis.id as id, imobiliar_fotos.arquivo as arquivo, imobiliar_imoveis.tipo as tipo, imobiliar_imoveis.nome_bairro as nome_bairro, imobiliar_imoveis.dormitorios as dormitorios, imobiliar_imoveis.valor as valor
				FROM imobiliar_fotos JOIN imobiliar_imoveis ON imobiliar_fotos.id_imovel = imobiliar_imoveis.id
				WHERE imobiliar_imoveis.locVenda = 'L' AND imobiliar_fotos.arquivo<>'' AND imobiliar_fotos.legenda = 'fa' GROUP BY imobiliar_imoveis.id
				) as nova_tabela ORDER BY id DESC LIMIT 3");
					if(mysql_num_rows($consulta) == 0){
						echo '<p>Nenhum imóvel em destaque no momento</p>';
					}else{
						while($imovel = mysql_fetch_array($consulta)){
                            $carac = "";
							$caracteristicas = mysql_query("SELECT * FROM imobiliar_descricao_imoveis WHERE id_imovel = {$imovel['id']}");
							while ($concat = mysql_fetch_array($caracteristicas)) {
								$carac .= " {$concat['extenso']},";
							}

							$qrtipo =  mysql_query("SELECT * FROM imobiliar_tipo_imoveis WHERE id = {$imovel['tipo']} AND locVenda = 'L'");
		                    
		                    while($tipo = mysql_fetch_array($qrtipo)){
		                        $_tipo = $tipo['descricao'];
		                    }
							
							?>
							<li class="fl fix">
								<div class="boxImg fl" style="width:450px;">
									<a href="javascript:;" data-modal="pesquisa=vendas" class="fl openmodal" rel="<?=$imovel['id']?>">
										<img width="203" height="154" src="<?= $imovel['arquivo'] ? "{$path['imagens']}/{$imovel['id']}/{$imovel['arquivo']}" : "http://www.raphaelimoveis.com.br/c65bcf-raphael-sem-imagem.png"?>" class="fl" style="margin-right:10px;">
										
										<!-- <h4 class="hide-mobile" style="float:left; margin-bottom:5px; width:190px; font-size:20px;"><?=$imovel['id']?></h4> -->
										<div class="home_info" style="float:left; width:190px; line-height:17px;">
											<span><strong>Código:</strong> <?=$imovel['id']?></span>
											<span><strong>Tipo:</strong> <?=$_tipo?> </span>
											<span><strong>Bairro:</strong> <?=($imovel['nome_bairro'])?></span>
											<span><strong>Quartos:</strong> <?=$imovel['dormitorios'] ? $imovel['dormitorios'] : "Sem dormitórios"?></span>
											<span class="valor">R$<?= number_format($imovel['valor'],2,",",".")?></span>
										</div>
									</a>
								</div>
							</li>
						<? }//WHILE ?>
					<? }//ELSE ?>
			</ul>
			<button class="prev-alugueis"></button>
			<button class="next-alugueis"></button>
		</div>
	</div>

	<div class="clr"></div>

	<div class="gridWrapper fix">
		<div class="gridWrapperBoxes fl fix">
			<h3 class="chamadasInternas">Área do cliente</h3>
			<span class="fl">Na área do cliente você pode imprimir a 2ª via do seu boleto e, caso seja condomino, verificar seu demonstrativo de despesas</span>
			<div class="hide-mobile">
				<a  href="http://www.raphaelimoveis.com.br/usuario/" class="fl" style="width: 275px; height: 40px">
	                <style>
	                    .btn.btn-orange{
	                        text-shadow: 0px 1px 0px rgba(0, 0, 0, 0.5);
	                        font-family: "swissLeve";
	                        font-size: 19px;
	                        color: #FFF;
	                        border: medium none;
	                        cursor: pointer;
	                        height: 25px;
	                        margin: 10px 70px;
	                        padding: 8px 0px;
	                        text-align: center;
	                        background: transparent url("../img_index/background/bgr-input.png") no-repeat scroll 0% 0%;
	                    }
					.menuMob{
						margin-bottom: -52px;
					}


	                </style>
				    <div class="btn btn-orange">Acessar</div>
				</a>
				
			</div>
				<a class="hide-desktop btn_laranja" href="http://www.raphaelimoveis.com.br/usuario/">
					acessar
				</a>
		</div>

		<div class="gridWrapperBoxes fl fix">
			<h3 class="chamadasInternas">Newsletter</h3>
			<span class="fl">Cadastre seu email e receba todas as novidades e destaques da Raphael Imóveis.</span>
			<form action="ajax/enviaNews.php" id="formNews" class="clr fl" method="">
				<fieldset>
					<label for="txt_nome" class="fl">Nome :</label>
					<input type="text" name="txt_nome" id="txt_nome" class="inputText fl">

					<label for="txt_email" class="fl clr">Email :</label>
					<input type="text" name="txt_email" id="txt_email" class="inputText fl email">
					
					<input type="submit" value="" class="inputSubmit fr hide-mobile">
					<input type="submit" value="OK" class="inputSubmit fr hide-desktop">

					<span class="aguarde fl clr">Aguarde...</span>
				</fieldset>
			</form>
		</div>

		<div class="gridWrapperBoxes fl fix">
			<h3 class="chamadasInternas">Coligadas</h3>
			
			<img src="img_index/img-coligadas.jpg" title="Coligadas" alt="" class="coligadas">
		</div>
		
	</div>
	<span class="fr hide-mobile">Creci 21507-J</span>
</div>
<?php include "footer.php"; ?>
	

<div class="modal_galeria">
	<div class="modal_galeria2">
		<div class="modal_galeria3">
			<div class="fechar"><a href="javascript:;" class="fechamodal">Fechar</a></div>
			<div class="box_imgzoom">
				<div class="box_imgzoom2"></div>
				<div id="mygallery" class="stepcarousel">
					<div class="belt"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="boxRetorno"></div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53541793-1', 'auto');
  ga('send', 'pageview');

</script>
<script charset="utf-8" src="js/galeria.js" type="text/javascript" ></script>
<? if(isset($_GET['cod'])){ ?>
	<? if($_GET['cod'] != ""){ ?>
	<script>
		var id = <?=intval($_GET['cod'])?>;
	    jQuery('.modal_galeria').before('<div class="mask"> </div>');
	    var maskHeight = jQuery(document).height();
	    var maskWidth = jQuery(window).width();
	    jQuery(".mask").css({height:maskHeight+"px",width:maskWidth+"px",opacity:"0.65"});

	    jQuery.ajax({
	        type: "POST",
	        url: "cria_modal.php",
	        data: {
	            id: id
	        },
	        success:function(conteudo){
	            var var1 = conteudo.split(".quebra.")[0];
	            var var2 = conteudo.split(".quebra.")[1];
	            jQuery('.box_imgzoom2').html(var1);
	            jQuery('.belt').html(var2);

	            jQuery(".modal_galeria").addClass('modal_img_fixed');
	            var pos = jQuery(".modal_galeria").position();
	            jQuery(".modal_galeria").removeClass('modal_img_fixed');
	            jQuery(".modal_galeria").css({position: "absolute",top: pos.top+50});


	            jQuery('.modal_galeria').fadeIn('fast');
	        }
	    });
	</script>
	<? } ?>
<? } ?>
</body>
</html>