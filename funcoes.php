<?php
function sonumeros($pstr) {
	$r = '';
	for ($i = 0; $i < strlen($pstr); $i++) {
		if ((substr($pstr, $i, 1) >= '0') && (substr($pstr, $i, 1) <= '9')) {
			$r .= substr($pstr, $i, 1);
		}
	}
	return $r;
}
function dif_dias($data1, $data2) {
	$d1 = mktime(0, 0, 0, substr($data1, 5, 2), substr($data1, 8, 2), substr($data1, 0, 4));
	$d2 = mktime(0, 0, 0, substr($data2, 5, 2), substr($data2, 8, 2), substr($data2, 0, 4));
	$dias = ($d1 - $d2)/86400;
	$dias = ceil($dias);
	
	return $dias;	
}
?>