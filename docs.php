<?php
require_once "head.php";
?>
</head>
<body id="internas" class="documentos">
<h1 class="seo">Aluguéis</h1>
<h2 class="seo">Documentos Exigidos para Locação</h2>
<? require_once "header.php"; ?>
<div class="content">
	<div class="centro636">
		<h4 class="bordLaranja">Documentos Exigidos para Locação</h4>
		<ul class="logoLista">
			<li>Xerox CI e CIC(inquilino e fiadores)</li>
			<li>Comprovante de renda(inquilino e fiadores)</li>
			<li>Certidão negativa da Justiça(inquilino e fiadores)</li>
			<li>Certidão do Registro de Imóveis(atualizada) de 2(dois) imóveis quitados(fiadores)</li>
			<li>Xerox do comprovante de residência(inquilino e fiadores)</li>
			<li>Xerox da última declaração do Imposto de Renda(inquilino e fiadores)</li>
			<li style="margin:20px 0 0 0;">1º) Não será aprovado cadastro com inclusão no SPC</li>
			<li>2º) Não efetuamos reservas de imóveis</li>
			<li>3º) Não nos responsabilizamos por despesas com terceiros na obtenção de documentos necessários para locação de imóvel</li>
			<li>4º) O imóvel só estará garantido ao pretendente após a assinatura do contrato</li>
			<li>5º) Candidatos a locatário com idade entre 16 e 21 anos, assinatura dos pais com firma reconhecida(no contrato e vistoria)</li>
			<li>6º) Se o fiador for casado, necessita documentação da esposa</li>
			<li>7º) A renda exigida para locação é de três vezes o valor do aluguel</li>
			<li>Obs.: Se o locatário for pessoa jurídica, contrato social com todas as alterações.</li>
			<li>Fiadores somente pessoa física.</li>
		</ul>
	</div>
</div>
</div>
<? require_once "footer.php"; ?>
</body>
</html>
