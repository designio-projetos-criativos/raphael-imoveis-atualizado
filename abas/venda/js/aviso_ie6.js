
// JavaScript Document
$(document).ready(function(){
	$('body').prepend("<div id='mascara'></div><div id='aviso_ie6'><h1>Voc&ecirc; sabia que seu Internet Explorer est&aacute; desatualizado?</h1><p>Para usufruir da melhor experi&ecirc;ncia utilizando nosso website, n&oacute;s recomendamos que voc&ecirc; atualize para a nova vers&atilde;o ou instale um outro navegador. Apresentamos uma lista dos navegadores mais populares logo abaixo.</p><p>Apenas clique em um dos links abaixo para ir para a p&aacute;gina de download:</p><ul><li><a href='http://www.microsoft.com/windows/Internet-explorer/default.aspx'><img src='../../img/ie6/browser_ie.gif' /></a><span>Internet Explorer 7+</span></li><li><a href='http://www.mozilla.com/firefox/'><img src='../../img/ie6/browser_firefox.gif' /></a><span>Firefox 3+</span></li><li><a href='http://www.apple.com/safari/download/'><img src='../../img/ie6/browser_safari.gif' /></a><span>Safari 3+</span></li><li><a href='http://www.opera.com/download/'><img src='../../img/ie6/browser_opera.gif' /></a><span>Opera 9.5+</span></li><li><a href='http://www.google.com/chrome'><img src='../../img/ie6/browser_chrome.gif' /></a><span>Chrome 2.0+</span></li></ul><a href='http://www.comparte.com.br'><img src='../../img/ie6/logo_comparte.jpg' class='comparte' width='205' height='51' /></a><p class='texto'> -&nbsp;&nbsp;Pela evolu&ccedil;&atilde;o da internet, pela sua seguran&ccedil;a.</p><br clear='all' /><p class='continuar'>Se voc&ecirc; deseja navegar em nosso site sem atualizar seu navegador, <a href='javascript:;'>clique aqui.</a></p></div>");

	var cor       = $('body').css("backgroundColor");
	var cor_borda = $('body').css("backgroundColor");
	
	if ( cor == "transparent" ) { 
		cor 	  = "#666"; 
		cor_borda = "#CCC"; 
	}
	
	$('#mascara').width( $(window).width() );
	$('#mascara').height( $(document).height() );
	$('#mascara').css("backgroundColor", cor );
	$('#mascara').css("opacity", "0.7" );
	$('#mascara').css("position", "absolute" );

	$('#aviso_ie6').css("borderColor", cor_borda );
	$('#aviso_ie6').css("left", (($('#mascara').width() - $('#aviso_ie6').width())/2));
	$('#aviso_ie6').css("top", (($(window).height() - $('#aviso_ie6').height())/2)-100);
	$('#aviso_ie6 .continuar a').css("color", cor);

	$('#mascara').fadeIn(1800,function(){
		$('#aviso_ie6').slideDown(1200);
	});

	$('#mascara').click(function(){
		$(this).fadeOut(400);
		$(this).next().fadeOut(550);
	});

	$('#aviso_ie6 .continuar a').click(function(){
		$('#mascara').fadeOut(400);
		$('#aviso_ie6').fadeOut(550);
	});
});
