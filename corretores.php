<?php require "inc/head.php";?>
</head>
<body id="internas" class="corretores">
<? require "inc/menuInt.php"; ?>
<div class="content">
	<div class="centro636">
		<h1 class="bordLaranja">Corretores</h1>
		<h2 class="seo">Compet�ncia, Honestidade e Profissionalismo</h2>
		<table cellpadding="0" cellspacing="0" class="table_corretores">
			<tbody>
				<tr>
					<td><img src="img/corretores/corr_tiberio.jpg" alt="TIB�RIO MEIRELES"/></td>
					<td><p><strong>TIB�RIO MEIRELES</strong></p>
					<p>CRECI: 31799</p>
					<p>Celular: (53) 9115.8855 ou 8129.4334</p>
					<p>E-mail: imobiliaria@raphaelimoveis.com.br </p></td>
				</tr>
				<tr> 
					<td><img src="img/corretores/corr_estela.jpg" alt="ESTELA CIRNE MAGALH�ES"/></td>
					<td><p><strong> ESTELA CIRNE MAGALH�ES</strong></p>
					<p>CRECI: 17176</p>
					<p>Celular: (53) 8114.0294</p>
					<p>E-mail: imobiliaria@raphaelimoveis.com.br</p>
					</td>
				</tr>
				<tr>
					<td><img src="img/corretores/corr_gustavo.jpg" alt="GUSTAVO DORNER"/> </td>
					<td><p><strong> GUSTAVO DORNER </strong></p>
					<p>CRECI: 35403 </p>
					<p>Celular: (53) 8116.4446 </p>
					<p>E-mail: imobiliaria@raphaelimoveis.com.br </p>
					</td>
				</tr>
				<tr> 
					<td><img src="img/corretores/corr_leonardo.jpg" alt="LEONARDO GOMES"/> </td>
					<td><p><strong> LEONARDO GOMES </strong></p>
					<p>CRECI: 36.458 </p>
					<p>Celular: (53) 8402.5103 ou 9166.7785</p>
					<p>E-mail: imobiliaria@raphaelimoveis.com.br </p>
					</td>
				</tr>
				<tr> 
					<td><img src="img/corretores/corr_mariza.jpg" alt="MARISA TAVARES"/> </td>
					<td><p><strong> MARISA TAVARES</strong></p>
					<p>CRECI: 37904 </p>
					<p>Celular: (53) 9988.5425 </p>
					<p>E-mail: imobiliaria@raphaelimoveis.com.br </p>
					</td>
				</tr>
				<tr> 
					<td><img src="img/corretores/corr_henrique.jpg" alt="JOS� HENRIQUE MAGALH�ES"/> </td>
					<td><p><strong> JOS� HENRIQUE MAGALH�ES</strong></p>
					<p>CRECI: 37.109 </p>
					<p>Celular: (53) 9151.1115 </p>
					<p>E-mail: imobiliaria@raphaelimoveis.com.br </p>
					</td>
				</tr>
				<tr> 
					<td><img src="img/corretores/corr_iara.jpg" alt="YARA ROTTA"/></td>
					<td><p><strong>YARA ROTTA</strong></p>
					<p>CRECI: 30554</p>
					<p>Celular: (53) 9128.0607 </p>
					<p>E-mail: imobiliaria@raphaelimoveis.com.br </p>
					</td>
				</tr>
				<tr> 
					<td><img src="img/corretores/corr_paula.jpg" alt="PAULA PARKER" /> </td>
					<td><p><strong>PAULA PARKER</strong></p>
					<p>CRECI (estagi&aacute;ria): 21507-J </p>
					<p>Celular: (53) 8124.1800 </p>
					<p>E-mail: imobiliaria@raphaelimoveis.com.br</p>
					</td>
				</tr>
				<tr> 
					<td><img src="img/corretores/corr_maristela.jpg" alt="MARISTELA VAZ"/></td>
					<td><p><strong> MARISTELA VAZ</strong></p>
					<p>CRECI (estagi&aacute;ria): 21507-J</p>
					<p>Celular: (53) 8137.4647</p>
					<p>E-mail: imobiliaria@raphaelimoveis.com.br</p>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<?php require "inc/rodape.php"; ?>
</body>
</html>
