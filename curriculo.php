<?php
require "head.php";
if ($_SERVER['REQUEST_METHOD'] == "POST"){
	$nome = $_REQUEST['nome'];
	$fone = $_REQUEST['fone'];
	$email = $_REQUEST['email'];
	$mensagem = $_REQUEST['mensagem'];
}
?>
</head>
<body id="internas" class="contato">
<h1 class="seo">Currículo</h1>
<?php require "header.php"; ?>
<div class="content">
	<div class="centro636 centro_curri">
		<h2 class="bordLaranja">Envie seu Currículo</h2>


			<p class="p_curriculo hide-desktop">Preencha o formulário abaixo e envie seu currículo para o nosso Banco de Dados.</p>

			<p class="p_curriculo hide-desktop">Caso haja alguma oportunidade adequada ao seu perfil profissional, entraremos em contato. </p>

		<form class="form_curriculo" name="formcurriculo" id="formcurriculo" method="post" enctype="multipart/form-data" action="curriculo.php">
			

			<fieldset>
			<table cellpadding="0" cellspacing="0">
				
				

				<tr class="hide-mobile">
					<td colspan="2"><p>Preencha o formulário abaixo e envie seu currículo para o nosso Banco de Dados.<br />
					Caso haja alguma oportunidade adequada ao seu perfil profissional, entraremos em contato. </p></td>
				</tr>
				<tr>
					<td><label for="nome">Nome:</label></td>
					<td><input type="text" id="nome" name="nome" value="" style="width:200px;" /></td>
				</tr>
				<tr>
					<td><label for="email">E-mail:</label></td>
					<td><input type="text" id="email" name="email" value="" style="width:200px;" /></td>
				</tr>
				<tr>
					<td><label for="fone">Telefone:</label></td>
					<td><input type="text" id="fone" name="fone" value="" style="width:200px;" /></td>
				</tr>
				<tr>
					<td><label for="curriculo">*Currículo:</label></td>
					<td>
						<div class="file" style="width:200px;">
						<input type="text" name="fakecurriculo" id="fakecurriculo" style="width:200px;" />
						<input type="file" id="curriculo" name="curriculo" class="upload" value="doc" size="30" onChange="this.form.fakecurriculo.value = this.value;" />
						</div>
					</td>
				</tr>
				<tr>
					<td><label for="comentario">Comentário:</label></td>
					<td><textarea cols="1" rows="1" name="mensagem" id="mensagem" style="width:400px;height:100px;" ></textarea></td>
				</tr>
				<tr class="hide-mobile">
					<td colspan="2" align="right">
						<input name="enviar" id="enviar" type="submit" value="Enviar"/>
						<input name="reset" id="reset" type="reset" value="Limpar"/>
					</td>
				</tr>
				<tr class="hide-mobile" ><td colspan="2"><p>* Documento do Word (.doc) preferencialmente com foto</p></td></tr>
				<tr><td colspan="2">
					<p>
						<?php
						if ($_SERVER['REQUEST_METHOD'] == "POST"){
							if(trim($mensagem) == "" or trim($nome) == "" or trim($fone) == "" or trim($email) == ""){
							   echo "Por favor $nome: informe todos os itens do formulário <br> para podermos entrar em contato e ler sua mensagem";
							
							}
							else{
								$destino = "curriculos/".$nome.".doc";
								copy ("$doc",$destino);
								// para um e-mail
								$filename = $destino;
								$content_type = "doc";
								# read a doc file from the disk
								$fd = fopen($filename, "r");
								$data = fread($fd, filesize($filename));
								fclose($fd);
								# create object instance
								$mail = new mime_mail;
								# set all data slots
								$mail->from = $email;
								$mail->to = "imobiliaria@raphaelimoveis.com.br";
								//$mail->to = "edeciofernando@yahoo.com.br";
								$mail->subject = "[Currículo da Página Raphael]";
								$mail->body = "$mensagem\n\nNome: $nome\nFone: $fone";
								# append the attachment
								$mail->add_attachment($data, $filename, $content_type);
								# send e-mail
								$mail->send();
								// para outro e-mail
								$filename = $destino;
								$content_type = "doc";
								# read a doc file from the disk
								$fd = fopen($filename, "r");
								$data = fread($fd, filesize($filename));
								fclose($fd);
								# create object instance
								$mail = new mime_mail;
								# set all data slots
								$mail->from = $email;
								$mail->to = "isan@raphaelimoveis.com.br";
								//$mail->to = "edecio@terra.com.br";
								$mail->subject = "[Currículo da Página Raphael]";
								$mail->body = "$mensagem\n\nNome: $nome\nFone: $fone";
								# append the attachment
								$mail->add_attachment($data, $filename, $content_type);
								# send e-mail
								$mail->send();
								echo "Obrigado $nome! Caso haja alguma oportunidade, entraremos em contato.";
							}
						}	
						?>
						</p>
				</td></tr>
			</table>
				<input class="botao hide-desktop" name="enviar" id="enviar" type="submit" value="Enviar"/>		
				<p class="formato_doc hide-desktop">* Documento do Word (.doc) preferencialmente com foto</p>
			</fieldset>
		</form>

	</div>	
</div>
</div>
<?php require "footer.php"; ?>
</body>
</html>

