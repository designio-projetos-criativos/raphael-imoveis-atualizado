<?php
require_once "head.php";
?>
</head>
<body id="internas" class="garantias">
<h1 class="seo">Aluguéis</h1>
<h2 class="seo">Garantias Exigidas</h2>
<? require_once "header.php"; ?>
<div class="content">
	<div class="centro636 tela_garantias">
		<h4 class="bordLaranja">Garantias Exigidas</h4>
		<ul class="logoLista">
			<li>Aceitamos fiadores de qualquer localidade do Estado.</li>
			<li>Seguro Fiança</li>
			<li>Título de Capitalização</li>
			<li>Analisamos Carta de Fiança</li>
		</ul>
	</div>
</div>
</div>
<? require_once "footer.php"; ?>
</body>
</html>
