<?php require "head.php"; ?>
</head>
<body id="internas" class="contato">
<h1 class="seo">Lojas</h1>
<?php require "header.php"; ?>
<div class="content">
	<div class="centro636">
		<h2 class="bordLaranja">Lojas</h2>
		<table cellpadding="0" cellspacing="0" class="lojas">
			<tbody>
			<tr>
				<td><img src="img/sede-centro.jpg" alt=""/></td>
				<td>
					<p>
						LOJA MATRIZ/CENTRO<br/>
						Rua Santa Cruz, 1992 <br/>
						Fone:(53) 3225.1100 <br/>
						Pelotas - RS
					</p>
					<p>
						Hor�rio:<br />
						- De 2� � 6�: 8:30h �s 11:45h e 13:30h �s 18:30h<br />
						- S�bados: 9:00h �s 12:00h
					</p>
					<p>LIGUE: 3225.1100</p>
				</td>
			</tr>
			<tr>
				<td><img src="img/sede-laranjal.jpg" alt=""/></td>
				<td>
					<p>
						LOJA LARANJAL<br />
						Shopping Mar de Dentro - Loja 14<br />
						Fone: (53) 3226.3006<br />
						Pelotas - RS
					</p>
					<p>
						Hor�rio:
						- Segunda � S�bado: 9:00h �s 12:00h e 13:30h �s 17:50h
					</p>
					<p>
						LIGUE: 3226.3006
					</p>
				</td>
			</tr>
			</tbody>		
		</table>
	</div>	
</div>
</div>
<?php require "footer.php"; ?>
</body>
</html>	