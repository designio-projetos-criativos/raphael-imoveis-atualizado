<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="Em 1991, junto ao escritório de advocacia Morales, advogados associados, foi constituída a empresa Administradora Raphael Ltda., para o fim específico e exclusivo de administração de aluguéis, visto que, alguns clientes do escritório, diante da confiança depositada, solicitaram que fossem administrados os seus imóveis destinados a locação."/>
<meta name="keywords" content="raphael imóveis, imóveis, pelotas, imóveis pelotas, aluguéis, vendas,condomínios" />
<meta name="robots" content="all"/>

<title>.: Raphael Im&oacute;veis :.</title>
<link href="css_index/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="swfobject.js"></script>
<script type="text/javascript" language="javascript" src="jquery-1.3.2.js"></script>
<script type="text/javascript" language="javascript">
$(function($) {
	// Quando o formulário for enviado, essa função é chamada
	$("#formulario").submit(function() {
		// Colocamos os valores de cada campo em uma váriavel para facilitar a manipulação
		var email = $("#email").val();
		var nome = $("#nome").val();
		// Exibe mensagem de carregamento
		$(".topicosmoldenewsletterformverifica").html("<img src='loader.gif' alt='Enviando' />");
		// Fazemos a requisão ajax com o arquivo login_cliente e enviamos os valores de cada campo através do método POST
		$.post('cadastrar_newslatter.php', {email: email,nome: nome }, function(resposta) {
				// Quando terminada a requisição
				// Exibe a div status
				$(".topicosmoldenewsletterformverifica").slideDown();
				// Se a resposta é um erro
				if (resposta != false) {
					// Exibe o erro na div
					$(".topicosmoldenewsletterformverifica").html(resposta);
				} 	// Se resposta for false, ou seja, não ocorreu nenhum erro
				else {
					// Exibe mensagem de sucesso
					$(".topicosmoldenewsletterformverifica").html("E-mail cadastrado com sucesso!");
					// Limpando todos os campos
				
					$("#email").val("Email");
					$("#nome").val("Nome");
				
				}							
		});
	});
	$(".limpa_input").focus(function() {
	if( this.value == this.defaultValue ) {
			this.value = "";
		}
		}).blur(function() {
	if( !this.value.length ) {
			this.value = this.defaultValue;
		}
	});
});
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
</head>

<body>
<?php include "floater.php";?>

<div id="container">
	<div id="center">
		<div id="sidebar">
			<script type="text/javascript">
				// <![CDATA[
				var so = new SWFObject("animacao/menuTopo.swf", "sidebar", "880", "250", "9");
				so.addParam("quality", "best");
				so.addParam("scale", "noscale");
				so.addParam("wmode", "transparent");
				so.addParam("menu","false");
				so.write("sidebar");
				// ]]>
			</script>
		</div>
		<div id="content">
			<div id="contentcenter">
				<div id="contentmolde">
					<div id="contentmoldespace"></div>
					<div id="contentmoldeanimacao">
						<div id="flashanimacao" style="width:auto;margin:0 0 0 0px;">
							<!--<script type="text/javascript">
								// <![CDATA[
								var so = new SWFObject("animacao/animacao.swf", "flashanimacao", "600", "256", "9");
								so.addParam("quality", "best");
								so.addParam("scale", "noscale");
								so.addParam("wmode", "transparent");
								so.addParam("menu","false");
								so.write("flashanimacao");
								// ]]>
							</script>-->
                            
                            <!--<div style="float:left;">
                            	<a href="localiza.php"><img src="img/img-loja-matriz.jpg" title="Loja Matriz/Centro" /></a><br />
                            </div>
                            
                            <div style="float:left;">
                            	<a href="localiza.php"><img src="img/img-loja-laranjal.jpg" title="Loja Laranjal" /></a>
                            </div>
                            
                            <div style="clear:both;"></div>-->
                            
                            <a href="localiza.php"><img src="img/img-lojas-01.jpg" title="Localização" /></a>
                            
						</div>
					</div>
					<div id="contentmoldeatributos">
						<div id="contentmoldeatributoschamada"><img src="img_index/coligadas.png" /></div>
						<div class="contentmoldeatributoslogo1"><img src="img_index/zona_sul.png" /></div>
						<div class="contentmoldeatributoslogo2"><img src="img_index/moralles.png" /></div>
						<div class="contentmoldeatributosclima">
						<!-- Inicio Banner Tempo Agora - http://www.tempoagora.com.br -->
							<iframe src="http://www.tempoagora.com.br/selos_iframe/sky_Pelotas-RS.html" height="152px" width="152px" frameborder="0" allowtransparency="yes" scrolling="no"></iframe>
						<!--Fim Banner Tempo Agora - http://www.tempoagora.com.br -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="topicos">
	<div id="topicoscenter">
		<div id="topicosmolde">
			<div class="topicosmoldechamadasalugueis">
				<div class="topicosmoldechamadasalugueischamada"><img src="img_index/topicos/chamada_alugueis.png" /></div>
				<div class="topicosmoldechamadasalugueisbutton">
					<div class="topicosmoldechamadasalugueisbuttontop"></div>
					<div class="topicosmoldechamadasalugueisbuttonleft"></div>
					<div class="topicosmoldechamadasalugueisbuttonlink"><a href="dest_aluguel.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image9','','img_index/button_e_icon/destaque2.png',0)"><img src="img_index/button_e_icon/destaque1.png" name="Image9" width="105" height="21" border="0" id="Image9" /></a></div>
				</div>
				<div class="topicosmoldechamadasalugueistext">
					<p class="texttopicos">Ache seu im&oacute;vel. Encontre aqui as melhores ofertas de Loca&ccedil;&atilde;o !</p>
				</div>
			</div>
			
			<div class="topicosmoldechamadasvendas">
				<div class="topicosmoldechamadasvendaschamada"><img src="img_index/topicos/chamada_compras.png" /></div>
				<div class="topicosmoldechamadasvendasbutton">
					<div class="topicosmoldechamadasvendasbuttonspacetop"></div>
					<div class="topicosmoldechamadasvendasbuttonspaceleft"></div>
					<div class="topicosmoldechamadasvendasbuttonspacelink"><a href="dest_venda.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image10','','img_index/button_e_icon/destaque2.png',0)"><img src="img_index/button_e_icon/destaque1.png" name="Image10" width="105" height="21" border="0" id="Image10" /></a></div>
				</div>
				<div class="topicosmoldechamadasvendastext">
					<p class="texttopicos">Ache seu im&oacute;vel. Encontre aqui as melhores ofertas de Vendas !</p>
				</div>
			</div>
			
			<div class="topicosmoldenewsletter">
				<div class="topicosmoldenewsletterchamada"><img src="img_index/topicos/chamada_newsletter.png" /></div>
				<div class="topicosmoldenewsletterform">
					<form action="javascript:func()" method="post" id="formulario">
						<input type="text" size="24" name="nome" id="nome" value="Nome" class="limpa_input" /><br />
						<input type="text" size="24" name="email" id="email" value="Email" class="limpa_input" />
						<input name="Submit" type="submit" value="Cadastrar" />
					</form>
					<div class="topicosmoldenewsletterformverifica"></div>
				</div>
				<div class="topicosmoldenewslettertext">
					<p class="texttopicos">Cadastre seu email e receba todas as novidades e
					destaques da Raphael Im&oacute;veis.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div>
	<div id="footer">
		<div id="footercenter">
			<div id="footermolde">
				<div class="creci">Creci 21507-J</div>
				<div id="footermoldesombra"><img src="img_index/background/back_sombra.png" /></div>
				<div id="footermoldecreditos">
					<p class="textcreditos"><img src="img_index/bol1.png" width="10" height="10" /> Copyright 2010 &reg; Raphael Im&oacute;veis | Todos direitos reservados. <img src="img_index/bol2.png" width="10" height="10" /></p>
					<p class="textcreditos"><img src="img_index/bol1.png" width="10" height="10" /> Rua Santa Cruz, 1992 - Fone/Fax: (53) 3225.1100 - Pelotas - RS | Email: imobiliaria@raphaelimoveis.com.br <img src="img_index/bol2.png" width="10" height="10" /></p>
					<p class="textcreditos"><img src="img_index/bol1.png" width="10" height="10" /> Produzido por: <a href="http://www.ankstudioweb.com" target="_blank">ANK STUDIO WEB</a> <img src="img_index/bol2.png" width="10" height="10" /></p>
				</div>
			</div>

			<div style="float:right; margin:-63px 37px 0 0; z-index:9999;position:relative"><a href="https://www.facebook.com/pages/Raphael-Im%C3%B3veis/110853852397561" target="_blank"><img src="img/facebook.png" alt="" title=""></a></div>

		</div>
	</div>
	
</div>
<?php /*?>
<div id="floaterwagnerpar">
	<a href="javascript:fechar();" style="float:right;margin:0 0 0 0;height:32px;width:32px;display:block;text-indent:-99999px;">X</a>
</div>
<?php */?>
<?php
//if($_GET['floater'] == 'argolo'){?>
<?php /*?><div id="mask" class="mask"></div>
<div id="floater" style="margin:40px 0 0 -377px;width:1000px;height:600px;">
	<script type="text/javascript">
		// <![CDATA[
		var so = new SWFObject("animacao/floater2.swf", "floater2", "754", "600", "9");
		so.addParam("quality", "best");
		so.addParam("scale", "noscale");
		so.addParam("wmode", "transparent");
		so.addParam("menu","false");
		so.write("floater");
		// ]]>
	</script>
</div><?php */?>
<?php //}?>
</body>
</html>
