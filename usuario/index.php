<?
ini_set('default_charset','UTF-8');
header("Content-Type: text/html; charset: UTF-8"); 
?>
<!DOCTYPE html>
<html lang="pt_BR" ng-app="app">
    <head>
        <meta charset="UTF-8">
        <title>Raphael Imóveis: Área do usuário</title>
        <base href="http://www.raphaelimoveis.com.br/usuario/">
        <link rel="stylesheet" href="assets/stylesheets/css/style.css">
        <script src="assets/javascripts/lib/angular.min.js"></script>
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <script src="assets/javascripts/lib/ocLazyLoad.min.js"></script>
    </head>
    <body>
        <div class="bg-login"></div>
        <a href="http://www.raphaelimoveis.com.br">
            <img src="assets/images/raphael-imoveis.png" alt="logo" id="logo">
        </a>
    </body>
    <script src="assets/javascripts/app.js"></script>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600italic,800,600">
</html>