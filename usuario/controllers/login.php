<?php
session_start();

header('Access-Control-Allow-Origin: *');
header("Content-Type: application/json; charset=utf-8");

$post = json_decode(file_get_contents("php://input"), true);


if(!isset($post['login']) || !isset($post['password'])){
    exit(json_encode(array('code'=>400,'text'=>'Requisição inválida')));    
}


require("../models/autoload.php");

$loginService = new Integration\Response();
$loginService->getUser($post['login'], $post['password']);