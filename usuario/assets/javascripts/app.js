var app = angular.module("app", ["oc.lazyLoad"]);
app.run(["$rootScope","$ocLazyLoad","$compile", "$injector","$http", function($rootScope, $ocLazyLoad, $compile, $injector, $http){

    $ocLazyLoad.load([
        "assets/javascripts/lib/velocity.min.js",
        "assets/javascripts/lib/angular-cookies.min.js"
    ]).then(function(){

        var $cookies = $injector.get("$cookies"),
            url = $cookies.logged ? "main" : "login";

        $http.get('views/'+url+'.html').success(function(data){
            angular.element(document.body).append($compile(data)($rootScope));

            var main = document.querySelector('main');

            
            if(!!$cookies.logged) {
                $ocLazyLoad.load({
                    serie: true,
                    files: [
                        "assets/javascripts/lib/angular-route.min.js",
                        "assets/javascripts/modules/routes.js"
                    ]
                });
            }
            window.setTimeout(function(){
                (url == "login") && angular.element(document.querySelectorAll("#login input")).on("keyup", function(e){
                    e.keyCode = e.keyCode ? e.keyCode : e.which; 
                    e.keyCode == 13 && $rootScope.loginSubmit();
                });
                Velocity(main, {
                    scale: [1,0]
                },{
                    easing: [0.1,1.3,1.1,1],
                    duration: 500
                }, 1000);
            },0);
        })
    });
}]);

app.controller("Login", ["$ocLazyLoad","$http", "$compile", "$rootScope", "$scope","$injector", function($ocLazyLoad, $http, $compile, $rootScope, $scope, $injector){
    $scope.login = "";
    $scope.password = "";
    $scope.request = {
        "button": "entrar"
    };
    var loginPharse = false;
    $rootScope.loginSubmit = function(){
        console.log(loginPharse);
        if(loginPharse) return;
        loginPharse = true;
        $scope.request.button = "enviando...";
        $http({
            headers: { 
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            url: "controllers/login.php",
            data: {
                "login": $scope.login,
                "password": $scope.password
            },
            method: "post"
        }).success(function(data){
            if(+data.code != 200){
                console.log(data);
                $scope.request = {
                    text: data.text,
                    code: +data.code
                }
                loginPharse = false;
//                $scope.$apply();
                return;
            } 
            var main = document.querySelector('main');
            $rootScope.user = data.user;

            $cookies = $injector.get('$cookies');

            $cookies.logged = data.token;

            $http.get("views/main.html").success(function(data){

                main.setAttribute('id', "main");
                data = angular.element($compile(data)($rootScope));

                angular.element(main).replaceWith(data);

                Velocity(document.querySelector('#main'), {
                    scale: [1,0]
                },{
                    easing: [0.1,1.3,1.1,1],
                    duration: 500
                });

                loginPharse = false;
                $ocLazyLoad.load({
                    serie: true,
                    files: [
                        "assets/javascripts/lib/angular-route.min.js",
                        "assets/javascripts/modules/routes.js"
                    ]
                }).then(function(){
                    var $location = $injector.get("$location");
                    $location.path("/");
                })
            });


        }).error(function(data, status, header, config){
            loginPharse = false;
            $scope.button = "entrar";
            $scope.request = {
                text: "Erro ao enviar o formulário, tente novamente mais tarde.",
                code: 400
            }
//            $scope.$apply();
        });
        
    }
}])
app.config(["$ocLazyLoadProvider", function($ocLazyLoadProvider){

    $ocLazyLoadProvider.config({
        events: true,
        debug: true
    });

}]);