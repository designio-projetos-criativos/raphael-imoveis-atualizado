(function(window,angular){
    var helpers = angular.module('app.helpers', []);

    helpers.factory("dashHelp", ["$injector",function($injector){
        var $cookies = $injector.get("$cookies");
        var UltimoBotao = "", newwindow;

        function TrocaUltimoBotao(valor, valor_extra){
            console.log(arguments);
            if (valor_extra == '' || valor_extra == "{BTN_VAL_EXTRA}")
                UltimoBotao = valor;
            else
                UltimoBotao = valor_extra+'|'+valor;
        }

        function Carga(){
            if (!!document.querySelector('[data-obs]') && document.querySelector('[data-obs]').getAttribute('data-obs') != "T")
                document.getElementById("tem_obscomerc").style.display = 'none';
        }

        function AbreDOC(file){
            var now = new Date();
            var url = "http://www.raphaelimoveis.com.br/imobiliar/prg/exibeBoleto.php?fboleto="+encodeURIComponent(file[0])+"&t="+now.getTime()+"&token="+$cookies.logged;
            newwindow = window.open(url,"DOC","toolbar=no,resizable=yes,scrollbars=yes");
            if (window.focus) {newwindow.focus()}
            return false;
        }
        // {action,id,prod,assessor,assessorEmail,tipo,descServ}
        function AbreRelatorio(f){
            TrocaUltimoBotao(f[8],f[9]);
            var now = new Date();
            var url = "http://www.raphaelimoveis.com.br/imobiliar/prg/"+f[0]+"?id="+f[1]+"&btn="+UltimoBotao;
            if (f[2])
                url += "&PROD="+encodeURIComponent(f[2])+"&ASSESSOR="+encodeURIComponent(f[3])+
                    "&ASSESSOR_EMAIL="+encodeURIComponent(f[4])+"&TIPO="+encodeURIComponent(f[5])+
                    "&CHAVE="+encodeURIComponent(f[6])+"&DESC_SERV="+encodeURIComponent(f[7]);
            url += "&t="+ now.getTime()+"&token="+$cookies.logged;
            newwindow = window.open(url,'extrato','width=750,height=550,toolbar=no,scrollbars=yes,resizable=yes');
            if (window.focus) {newwindow.focus()}
            return false;
        }

        function AbreInfoIR(f){
            TrocaUltimoBotao(f[2]);
            var now = new Date();
            var url = f[0]+"?id="+f[1]+"&comp="+UltimoBotao + "&t="+ now.getTime();
            newwindow = window.open(url,'infoIR','width=750,height=550,toolbar=no,scrollbars=yes,resizable=yes');
            if (window.focus) {newwindow.focus()}
            return false;
        }

        function AbreExtra(f)
        {
            // Implementar funcionalidades extras do condominio
            alert("Nao ha' informacoes disponiveis!");
            return false;
        }

        return {
            version: '1.12.2',
            Carga: Carga,
            TrocaUltimoBotao: TrocaUltimoBotao,
            AbreDOC: AbreDOC,
            AbreRelatorio: AbreRelatorio,
            AbreInfoIR: AbreInfoIR,
            AbreExtra: AbreExtra
        }
    }]);

}(window,angular))