
(function(angular,window){
    angular.module("app").controller("Dashboard", ["$scope","$injector", function($scope, $injector){
        var $http = $injector.get("$http"),
            $cookies = $injector.get("$cookies"),
            $rootScope = $injector.get("$rootScope"),
            $ocLazyLoad = $injector.get("$ocLazyLoad"),
            $compile = $injector.get("$compile");
        
        $rootScope.user = "";        
        
        angular.element(document.querySelector("aside input")).on("keyup", function(){
            var attribute = this.value;
            var matchingElements = [];
            
            var allElements = document.querySelectorAll(".wrapper ul li");
            if(!allElements) return;
            if(!attribute){ 
                Velocity(allElements, "fadeIn", 100);
                return;
            }
            
            for (var i = 0, n = allElements.length; i < n; i++){
                if(!!allElements[i].getAttribute("data-search")){
                    if (allElements[i].getAttribute("data-search").search(new RegExp(attribute)) == -1){
                        Velocity(allElements[i], "fadeOut", 100);
                    }
                }
            }
        });
        
        angular.element(document.querySelector(".menu")).on('click', function(){
            this.classList.toggle("active");
            document.querySelector('#main aside').classList.toggle("active");
        });
        
        $http.get("http://www.raphaelimoveis.com.br/imobiliar/prg/crLogin.php?token=" + $cookies.logged)
        .success(function(data){
            data = $compile(data)($rootScope);
            var base = angular.element(document.querySelector(".wrapper"));
            
            //angular.element(data[2]).css("opacity", "0");
            
            

            $ocLazyLoad.load("assets/javascripts/services/helpers.js")
            .then(function(){
                var dashHelp = $injector.get('dashHelp');

                base.append(data);
                
                Velocity(document.querySelector(".wrapper").firstElementChild, "fadeIn", 1000);
                
                angular.extend($scope, dashHelp);
                $rootScope.user = document.querySelector('[data-user]').getAttribute("data-user");
                var elem = angular.element(document.querySelectorAll('.wrapper .btn'));

                elem.on('click', function(e){
                    e.preventDefault();
                    var opt = this.getAttribute('data-opt').split(','),
                        fn = this.getAttribute('data-action');
                    $scope[fn](opt);
                });
                
                var links = angular.element(document.querySelectorAll("nav li[data-href]"));
                links.on("click", function(e){
                    var $href = this.getAttribute('data-href');
                    
                    (function(){
                        Velocity(document.querySelectorAll(".wrapper ul li.lista-" + ($href == "boletos" ? "servicos" : "boletos")), "fadeOut", 100);
                        Velocity(document.querySelectorAll(".wrapper ul li.lista-" + ($href == "servicos" ? "servicos" : "boletos")), "fadeIn", 100);
                    }());
                });
                !$scope.$$phase && $scope.$apply(); 
            });

        });


    }]);


}(angular,window));