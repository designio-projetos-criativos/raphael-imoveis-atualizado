(function(angular, window){
    var load = function load(config){
        return ["$q", "$rootScope", "$ocLazyLoad", function($q,$rootScope,$ocLazyLoad){
            return $ocLazyLoad.load(config)
        }]
    }
    angular.module("routes", ['ngRoute', 'ng'])
        .config(["$routeProvider", "$locationProvider", function($routeProvider, $locationProvider){
            $routeProvider
                .when("/", {
                    templateUrl: "views/dashboard.html",
                    controller: "Dashboard",
                    resolve: {
                        load: load(["assets/javascripts/controllers/dashboard.js"])
                    }
                })
                .when("/logout", {
                    resolve: {
                        logout: ["$rootScope","$location","$http","$cookies","$compile", function($rootScope, $location, $http, $cookies, $compile){
                            delete $cookies.logged;
                            
                            Velocity(document.querySelectorAll('#main *'), {
                                opacity: 0
                            }, {
                                duration: 300,
                                complete: function(){
                                    var http = $http.get("views/login.html");
                                    Velocity(document.querySelector("#main"),{
                                        minWidth: "0px",
                                        width: "380px",
                                        height: "375px"
                                    }, {
                                        duration: 1000,
                                        complete: function(){
                                            http.success(function(data){
                                                
                                                var main = angular.element(document.querySelector('main')),
                                                    html = $compile(data)($rootScope);

                                                html.css({
                                                    "transform": "scale(1)"
                                                }).children().css({
                                                    "opacity": "0"
                                                });
                                                main.replaceWith(html);
                                                
                                                Velocity(angular.element(document.querySelector("main")).children(), "fadeIn", 300);
                                                $location.url("/");
                                            });
                                        }
                                    });
                                }
                            });
                            
                        }]
                    }
                });
            
            $locationProvider.html5Mode(true).hashPrefix("!");
            
        }])
        .run(["$compile","$rootScope", function($compile,$rootScope){
            $compile(document.querySelector('[ng-view]'))($rootScope);
        }])
}(angular, window));