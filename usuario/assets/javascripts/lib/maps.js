window._GoogleMaps = {
    init: function(){
        for(var i = 0, l = this.get.length; i < l; i++){
            this.get[i]();
        }
        this.loaded = true;
    },
    on: {
        load: function(callback){
            window._GoogleMaps.get.push(callback);
        }
    },
    loaded: false,
    script_loaded: false,
    get: []
}

var GoogleMaps = function(options){
    var opt = options;


    var THIS = this;

    "use strict";
    //    PARAMETROS REQUERIDOS

    if(!options || !options.key || !options.lat || !options.lng || !options.base){

        console.log("parametros inválidos");

        return;
    }

    if(!document.getElementById(opt.base)){
        console.error("Elemento " + opt.base + " não encontrado."); 
        return false;
    }

    //        INICIALIZAÇÃO DAS VARIAVEIS

    var map, marker, busca, direction, route, googleLoad, search, center, overlay;

    // INICIALIZAÇÃO PELO CALLBACK DO GOOGLE

    window._GoogleMaps.on.load(function(){
        THIS.initialize(google);

        opt.automaticRoute && THIS.setAutomaticRoute();
    });

    //        CRIAÇÃO DO MAPA APÓS O CARREGAMENTO DO SCRIPT

    this.initialize = function(google){

        center = new google.maps.LatLng(opt.lat, opt.lng);
        direction = new google.maps.DirectionsRenderer();
        route = new google.maps.DirectionsService();

        // CRIA O MAPA

        map = new google.maps.Map(document.getElementById(opt.base), {
            zoom: 18,
            mapTypeControl: false,
            zoomControl: false,
            panControl: false,
            scaleControl: false,
            streetViewControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            center: center
        });

        direction.setMap(map);

        //        CRIAÇÃO DOS MARCADORES

        var marker = new google.maps.Marker({
            position: center,
            map: map,
            title: opt.title
        });

        overlay = new google.maps.OverlayView();
        search = new google.maps.Geocoder();
    };

    //    CRIAR ELEMENTOS DENTRO DO GOOGLE MAPS - não utilizar ainda

    this.draw = function(element, css, position, callback){

        if(position){position = google.maps.LatLng(position.lat, position.lng);}

        // DEFAULTS

        var dft = THIS.draw.default = {
            element: "div", 
            css: {
                "display":"block",
                "height": "100px",
                "width": "150px",
                "background": "white"
            },
            position: center
        }

        var div = document.createElement(element || dft.element);

        var _css = css || dft.css;

        for(i in _css){
            div.style[i] = _css[i];   
        }

        div.style.position = 'absolute';

        overlay.setMap(map);

        var panes = overlay.getPanes();

        panes.overlayImage.appendChild(div);

        var point = overlay.getProjection().fromLatLngToDivPixel(position || dft.position);

        callback && callback(div);
    }

    // CRIAÇÃO DO OBJETO CREATE PARA POSTERIORMENTE ADCIONAR FUNÇÕES NELE

    this.create = {};

    // PROCURA A LONGETUDE E LATITUDE PELA LOCALIZAÇÃO

    this.search = function(string, callback){

        search.geocode({'region': "BR",'address': string}, function(results, status){

            if(!status == "OK" || !results) return;

            var location = new google.maps.LatLng(
                results[0].geometry.location.lat(),
                results[0].geometry.location.lng()
            );

            callback && callback(location);

            return location;

        });
    }


    this.routeResults = {};

    //    CRIAR ROTA

    this.create.route = function(location){

        route.route({

            origin: location,
            destination: center,
            travelMode: google.maps.TravelMode.DRIVING

        }, function(result, status){

            if(!status == "OK") return;

            direction.setDirections(result);

        })
    }

    //    CRIAÇÃO DA ROTA AUTOMAGICA PELO navigator.geolocation

    this.setAutomaticRoute = function(){

        if(!navigator.geolocation) return;

        navigator.geolocation.getCurrentPosition(function(position) {

            var geolocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

            THIS.create.route(geolocation);

        });



        //    INICIAR A FUNÇÃO GoogleMaps

    }
    var init = (function(){
        if(window._GoogleMaps.script_loaded) return;
        //        CARREGAMENTO DO SCRIPT ASSINCRONO
        window._GoogleMaps.script_loaded = true;
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src = "http://maps.googleapis.com/maps/api/js?key=" + opt.key + "&libraries=places&callback=_GoogleMaps.init";
        document.body.appendChild(script);

    }());

}

$(window).ready(function(){

    var mapsFooter = new GoogleMaps({
        title: "POP Center",
        base: "maps-footer",
        lat: -31.768237,
        lng: -52.348217,
        key: "AIzaSyCJO3sV-AZSScChQFjDcg_YS2Q4E97eeyY"
    });
});
