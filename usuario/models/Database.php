<?php

namespace Integration;

class Database{
    private //$server = "187.86.152.149",
            $server = "localhost",
            $user = "raphaeli_user",
            $password = "raphael334",
            $database = "raphaeli_site1";
    
    public $db;
    protected $hash = "designio_projetos_desruptiveis";
    public function __construct(){
        $this->db = new \mysqli($this->server,$this->user,$this->password,$this->database);
        if(mysqli_connect_errno()){
            exit("Erro na conexão com o banco de dados [".mysqli_connect_error()."]");
        }
    }

    public function destroy(){
        $this->db->close();
    }
    
    protected function query($sql){
        return $this->db->query($sql);
    }
  
}