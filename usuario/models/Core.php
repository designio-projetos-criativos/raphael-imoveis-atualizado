<?php

namespace Integration;

class Core extends Database{

    protected function parseArray($arr){
        $i = 0;
        while($qry = $arr->fetch_assoc()){
            $ret[$i] = $qry;
            $i++;
        }
        return $ret;
    }

    public function is_cpf($string = null){
        if($string == null) return false;

        $cpf = preg_replace('/[^0-9]/', '', $string);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        if (strlen($cpf) != 11) return false;

        else if ($cpf == '00000000000' || 
                 $cpf == '11111111111' || 
                 $cpf == '22222222222' || 
                 $cpf == '33333333333' || 
                 $cpf == '44444444444' || 
                 $cpf == '55555555555' || 
                 $cpf == '66666666666' || 
                 $cpf == '77777777777' || 
                 $cpf == '88888888888' || 
                 $cpf == '99999999999') {

            return false;

        } else {   

            for ($t = 9; $t < 11; $t++) {

                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return false;
                }
            }

            return true;
        }
    }

    public function is_cnpj($string = null){
        $cnpj = preg_replace('/[^0-9]/', '', (string) $string);
        if (strlen($cnpj) != 14) return false;
        
        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++){
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }
        
        $resto = $soma % 11;
        
        if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto)) return false;
        
        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++){
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }
        
        $resto = $soma % 11;
        
        return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
    }

    public function is_email($string = null){
        return filter_var($string, FILTER_VALIDATE_EMAIL);
    }

    public function getPhotos(){
        $res = $this->db->query("SELECT * FROM imobiliar_fotos");
        return $this->parseArray($res);
    }
}