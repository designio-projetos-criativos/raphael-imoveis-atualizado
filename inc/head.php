<?php require_once "connection/conexao.php";?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="description" content="Em 1991, junto ao escritório de advocacia Morales, advogados associados, foi constituída a empresa Administradora Raphael Ltda., para o fim específico e exclusivo de administração de aluguéis, visto que, alguns clientes do escritório, diante da confiança depositada, solicitaram que fossem administrados os seus imóveis destinados a locação."/>
<meta name="keywords" content="raphael imóveis, imóveis, pelotas, imóveis pelotas, aluguéis, vendas,condomínios" />
<meta name="robots" content="all"/>


<link href="css_index/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/modal_galeria.css">
<link href="css/default.css" type="text/css" rel="stylesheet" />


<script type="text/javascript" src="swfobject.js"></script>
<script type="text/javascript" language="javascript" src="jquery-1.7.1.js"></script>
<script type="text/javascript" src="jquery.jcycle.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.2.custom.min.js"></script>

<script type="text/javascript" src="js/jquery.validate.js"></script>
<script type="text/javascript" src="js/jquery.validate.metods.js"></script>

<script type="text/javascript" src="js/jquery.meio.mask.js"></script>

<script type="text/javascript" src="js/selects.js"></script>
<script type="text/javascript" src="js/default.js"></script>
<script type="text/javascript" language="javascript">
$(function($) {
	// Quando o formulário for enviado, essa função é chamada
	$("#formulario").submit(function() {
		// Colocamos os valores de cada campo em uma váriavel para facilitar a manipulação
		var email = $("#email").val();
		var nome = $("#nome").val();
		// Exibe mensagem de carregamento
		$(".topicosmoldenewsletterformverifica").html("<img src='loader.gif' alt='Enviando' />");
		// Fazemos a requisão ajax com o arquivo login_cliente e enviamos os valores de cada campo através do método POST
		$.post('cadastrar_newslatter.php', {email: email,nome: nome }, function(resposta) {
				// Quando terminada a requisição
				// Exibe a div status
				$(".topicosmoldenewsletterformverifica").slideDown();
				// Se a resposta é um erro
				if (resposta != false) {
					// Exibe o erro na div
					$(".topicosmoldenewsletterformverifica").html(resposta);
				} 	// Se resposta for false, ou seja, não ocorreu nenhum erro
				else {
					// Exibe mensagem de sucesso
					$(".topicosmoldenewsletterformverifica").html("E-mail cadastrado com sucesso!");
					// Limpando todos os campos
				
					$("#email").val("Email");
					$("#nome").val("Nome");
				
				}							
		});
	});
	$(".limpa_input").focus(function() {
	if( this.value == this.defaultValue ) {
			this.value = "";
		}
		}).blur(function() {
	if( !this.value.length ) {
			this.value = this.defaultValue;
		}
	});

	$('input').focus(function(){
		if($(this).hasClass('codigo') && !$(this).hasClass('masked')){ $(this).setMask('a-9999').addClass('masked'); }
	});

	
});
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>


<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-31852857-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>