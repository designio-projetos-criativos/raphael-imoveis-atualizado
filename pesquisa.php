<?php
session_start();
require_once "head.php";
$_GET['busca'] = $_GET['busca'] ? $_GET['busca'] : "L";
?>
<title>Raphael Imóveis - Pesquisa de Imóveis</title>

<style type="text/css" media="all">
table tr td, table tr, table tr th,table{border-color:#CCCCCC;_border-color:;}
.tab{color:#114019;font-family:Arial, Helvetica, sans-serif;font-size:11px;font-weight: bold;}

tr.hide { display:none; }
span.btn { float:none; display:block; width:200px; margin:30px auto 0px; padding:10px 0px; text-align:center; cursor:pointer; font-size:14px; color:#FFF; background-color:#ee7c00; cursor:pointer; border-radius:3px }

</style>
</head>
<body id="internas" class="contato">
<? require_once "header.php"; ?>
<div class="content">
	<div class="centro636 cont_pesquisa">
		<h1 class="bordLaranja hide-desktop"><?= $_GET['busca'] == "L" ? "Aluguéis" : "Vendas";?></h1>
		<h1 class="bordLaranja hide-mobile"><?= $_GET['busca'] == "L" ? "Aluguéis" : "Vendas";?> - Pesquisa de Imóveis</h1>
		<h2 class="seo">Pesquisa de Imóveis</h2>
		<table class="tab tabImoveis " cellpadding="5" cellspacing="1" border="1" width="100%">
			<tr class="hide-mobile" bgcolor="#c0c0c0"><td> Fotos</td><td> Código </td><td> Tipo </td><td align="center">Bairro</td><td align="center">Dorm</td><td>R$ Valor</td></tr>	
			<?
			$sql = "SELECT * FROM imobiliar_imoveis WHERE locVenda = '{$_GET["busca"]}' ";
			if($_GET['codigo_imovel']) $sql .= " AND id = {$_GET['codigo_imovel']}";
			if($_GET['tipo']) $sql .= " AND tipo = {$_GET['tipo']}";
			if($_GET['valor']){ 
				$_GET['valor'] = explode("/", $_GET['valor']);
                $_GET['valor'][1] = $_GET['valor'][1] ? $_GET['valor'][1] : 99999999999999999;
				$sql .= " AND valor BETWEEN {$_GET['valor'][0]} and {$_GET['valor'][1]}";
			}
			if($_GET['bairro']) $sql .= " AND nome_bairro = '{$_GET['bairro']}'";
			if($_GET['dorm']) $sql .= " AND dormitórios ". (intval($_GET['dorm']) < 5 ? "= {$_GET['dorm']}" : ">= 5");

			$query = mysql_query($sql);

			$count = 0;

			//echo $sql;
			if(mysql_num_rows($query) == 0){
			 	echo "<h1>Nenhum resultado encontrado</h1>";
			}else{
				while ($imovel = mysql_fetch_array($query)){	
					$qry = mysql_query("SELECT * FROM imobiliar_fotos WHERE id_imovel = {$imovel['id']} AND legenda = 'fa' ORDER BY ordem ASC");
					$img = mysql_fetch_array($qry);

					$qrtipo =  mysql_query("SELECT * FROM imobiliar_tipo_imoveis WHERE id = {$imovel['tipo']} AND locVenda = '{$_GET['busca']}'");
                    
                    while($tipo = mysql_fetch_array($qrtipo)){
                        $_tipo = $tipo['descricao'];
                    }
				?>
				<? if(mysql_num_rows($qry) > 0){ ?>	
				<tr class="hide-mobile <?=($count > 20 ? 'hide' : '')?>">
					<td align="center">
					<? if(mysql_num_rows($qry) > 0){ ?>
						<a href="javascript:;" rel="<?=$imovel['id']?>" class="openmodal">
							<img width="150" height="100" src="imobiliar/Fotos/<?=$imovel['id']."/".$img['arquivo']?>" alt="<?=$imovel['id']?>" />
						</a>
					<? } else {?>
					    <a href="javascript:;" rel="<?=$imovel['id']?>" class="openmodal">
							<img width="150" height="100" src="http://www.raphaelimoveis.com.br/c65bcf-raphael-sem-imagem.png" alt="<?=$imovel['id']?>" />
						</a>
					<? }?></td>
					<td><?=$imovel['id']?></td>
					<td><?=$_tipo?> </td>
					<td><?=($imovel['nome_bairro'])?></td>
					<td><?=$imovel['dormitorios'] ? $imovel['dormitorios'] : "<h6>Sem dormitórios</h6>"?></td>
					<td>R$ <?= number_format($imovel['valor'],2,",",".")?></td>
				</tr>

				<? $count++; ?>
				
				<div class="box_resultado hide-desktop" >
					<div class="foto">
						<? if(mysql_num_rows($qry) > 0){ ?>
						<a href="javascript:;" rel="<?=$imovel['id']?>" class="openmodal">
							<img src="imobiliar/Fotos/<?=$imovel['id']."/".$img['arquivo']?>" alt="<?=$imovel['id']?>" />
						</a>
					<? } else {?>
					    <a href="javascript:;" rel="<?=$imovel['id']?>" class="openmodal">
							<img src="http://www.raphaelimoveis.com.br/c65bcf-raphael-sem-imagem.png" alt="<?=$imovel['id']?>" />
						</a>
					<? } ?>
					</div>
					<div class="info">
						<p>Código: <span><?=$imovel['id']?></span></p>
						<p>Tipo:   <span><?=$_tipo?> </span></p>
						<p>Bairro: <span><?=($imovel['nome_bairro'])?></span></p>
						<p>Dorm.:  <span><?=$imovel['dormitorios'] ? $imovel['dormitorios'] : "Sem dormitórios"?></span></p>
						<p>Valor:  <span>R$ <?= number_format($imovel['valor'],2,",",".")?></span></p>
						<a href="javascript:;" rel="<?=$imovel['id']?>" class="openmodal">ver mais</a>

					</div>
				</div>



			<? }
				} 
			}
			?>							
		</table>
		<span class="btn js-ver-mais">Ver mais</span>
	</div>	
</div>
</div>
</div>
<?php require_once "footer.php"; ?>
<div class="modal_galeria">
	<div class="modal_galeria2">
		<div class="modal_galeria3">
			<div class="fechar hide-mobile"><a href="javascript:;" class="fechamodal">Fechar</a></div>
			<div class="box_imgzoom">
				 <!-- <div class="hide-desktop topo_verde">
			        <p class="hide-desktop"><?=$caracteristicas?></p>
			        <p class="hide-desktop">Valor: R$ <?=number_format($result['valor'],2,",",".") ?></p>
			    </div> -->
				<div class="box_imgzoom2"></div>
				<div id="mygallery" class="stepcarousel">
					<div class="belt"></div>
				</div>
				<div class="fechar fechamodal hide-desktop"><a href="javascript:;" class="fechamodal">Voltar</a></div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="js/galeria.js"></script>

<script>
	$(document).ready(function(){
		$('.js-ver-mais').click(function(){
			$('tr.hide:lt(20)').show();
			$('tr.hide:lt(20)').removeClass('hide');
		});
	});
</script>

</body>
</html>