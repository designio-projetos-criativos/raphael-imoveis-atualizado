<?php
namespace Integration;


class Sync{

    private $path = array(
        "data" => "prg/dados/",
        "photos" => "Fotos/"
    );
    private $db;
    public $_global = array(
        "length" => array(
            "cidades" => 46,
            "bairros" => 71,
            "tipo_imoveis" => 36,
            "caracteristicas" => 40,
            "imoveis" => 260,
            "caracteristicas_imoveis" => 60,
            "situacao_imoveis" => 3,
            "descricao_imoveis" => 8,
            "destaques" => 9,
            "usuarios" => 143,
            "arquivos_locacao" => 189
        ),
        "files" => array(
            "cidades" => array("cidades.txt"),
            "bairros" => array("bairros_loc.txt", "bairros_venda.txt"),
            "tipo_imoveis" => array("tipo_loc.txt", "tipo_venda.txt"),
            "caracteristicas" => array("carac.txt"),
            "imoveis" => array("imov.txt", "imovenda.txt"),
            "caracteristicas_imoveis" => array('caract_imo.txt'),
            "situacao_imoveis" => array('situacaoimo.txt'),
            "descricao_imoveis" => array("imovdescr.txt"),
            "destaques" => array("destaques.txt"),
            "usuarios" => array("usuarios.txt"),
            "arquivos_locacao" => array("AcessoLoc.txt")
        ),
        "schema" => array(
            "cidades" => array(
                array("nome", 0, 40, "capitalize"),
                array("id", 40, 6, "int"), 
                array("uf", 46, 2)
            ),
            "bairros" => array(
                array("id", 0, 5, "int"),
                array("nome", 5, 60, "capitalize"),
                array("id_cidade", 65, 6, "int")
            ),
            "tipo_imoveis" => array(
                array("id", 0, 4, "int"),
                array("descricao", 4, 30),
                array("tipo", 35, 1),
                array("dormitorio", 36, 1),
            ),
            "caracteristicas" => array(
                array("id", 0, 4, "int"),
                array("descricao", 4, 25, "lowercase"),
                array("reservado", 29, 11)
            ),
            "imoveis" => array(
                array("id", 0, 8, "int"),
                array("tipo", 8, 2, "int"),
                array("id_cidade", 10, 6, "int"),
                array("nome_bairro", 16, 60, "capitalize"),
                array("reservado", 76, 3),
                array("endereco", 79, 85, "capitalize"),
                array('imediacao', 164, 30),
                array('dormitorios', 194, 2, "int"),
                array('nome_predio', 196, 30, "capitalize"),
                array('valor', 226, 12, "int"),
                array('valor_condominio', 238, 12, "int"),
                array('valor_iptu', 250, 12, "int"),
                array('id_situacao', 262, 3),
                array('comercial_residencial', 265, 1),
                array('id_caracteristicas', 266, 8, "int"),
                array('area_util', 274, 8, "int"),
                array('tamanho_tipo_endereco', 282, 2),
                array('tamanho_endereco', 284, 2),
                array('tamanho_numero_endereco', 286, 2),
                array('cep', 288, 8),
                array('vagas_estacionamento', 296, 3, "int"),
                array('area_total', 299, 8, "int")
            ),
            "caracteristicas_imoveis" => array(
                array("id_caracteristica", 0, 8, "int"),
                array("quantidade", 8, 6, "int"),
                array("complemento", 14, 10),
                array("id", 24, 4, "int"),
                array("extenso", 28, 25, "lowercase"),
                array("indicador_quantidade", 53, 1),
                array("unidade_quantidade", 54, 10),
                array("tipo_caracteristica", 64, 3)
            ),
            "situacao_imoveis" => array(
                array("id", 0, 3),
                array("descricao", 3, null)
            ),
            "descricao_imoveis" => array(
                array("id_imovel", 0, 8, "int"),
                array("extenso", 8, null)
            ),
            "destaques" => array(
                array("tipo",0,1),
                array("id_imovel",1,8,"int")
            ),
            "usuarios" => array(
                array("id", 0, 10, "int"),
                array("senha", 10, 20, "cript"),
                array("nome", 30, 35, "capitalize"),
                array("cpf", 65, 14, "zero_fill"),
                array("email", 79, 64, "lowercase")
            ),
            "arquivos_locacao" => array(
                array("id_usuario", 0, 10, "int"),
                array("arquivo", 10, 9, "lowercase"),
                array("nome_assessor", 19, 35),
                array("quem_sou", 54, 1),
                array("email_assessor", 55, 50),
                array("nome", 105, 80),
                array("texto_fixo", 185, 1, "uppercase"),
                array("id_filial", 186, 3, "int")
            )
        )
    );
    public $hash = "designio_projetos_desruptiveis";
    private $database = false;
    public $dbSulfix = "imobiliar_";
    public $data = array();

    private function sendError($error){
        header("Content-type: text/html; charset = UTF-8");
        exit("<p style='color: red; background: black'>{$error}</p>");
    }

    public function __construct($path = ""){
        $this->path['data'] = $path.$this->path['data'];
        $this->path['photos'] = $path.$this->path['photos'];

        if(class_exists('Integration\Database')){ 
            $this->database = true;
            $db = new Database();
            $this->db = $db->db;
        }

        foreach($this->path as $key => $value){ 
            if(!file_exists($value)){
                $this->sendError("path '{$value}' not exists in this project");
            }
        }
    }
    private function get($line, $init, $end, $option = NULL){
        $value = "";

        if($end == null){
            $value = trim(substr($line, $init));
        }else{
            $value = trim(substr($line, $init, $end));
        }
        if($option){
            $value = $this->parse($value, $option);
        }

        return $value;
    }
    private function parseSQL($string){
        return "(" . trim($string, ",") . ")";
    }
    public function query($table, $array){
        global $return;

        $qry = "INSERT INTO {$this->dbSulfix}$table";
        $in = "";
        if(!empty($array)){
            if(!(array_keys($array) !== range(0, count($array) - 1))){
                foreach($array[0] as $key => $value){
                    $in .= "$key,";
                }

                $qry .= " {$this->parseSQL($in)} VALUES";

                for($i = 0; $i < sizeof($array); $i++){
                    $valueIn = "";
                    foreach($array[$i] as $key => $value){

                        if(!is_int($value)){
                            $value = "'{$this->db->escape_string($value)}'";
                        }

                        $valueIn .= "$value,";   
                    }
                    $qry .= " {$this->parseSQL($valueIn)},";
                }
                $this->db->query(trim($qry, ",")) or die(mysqli_error($db));
            }else{
                $this->sendError("this array is not associative, table = $table");
            }
        }
    }
    private function parse($value = null, $option = null){
        $option = strtoupper($option);
        switch($option){
            case "INT":
                return intval($value);
            case "CAPITALIZE":
                return ucwords(strtolower($value));
            case "UPPERCASE":
                return strtoupper($value);
            case "LOWERCASE":
                return strtolower($value);
            case "CRIPT":
                return crypt($value, base64_encode($this->hash));
            case "ZERO_FILL":
                if(strlen($value) < 11){
                    return str_pad($value, 11, '0', STR_PAD_LEFT);
                }elseif(strlen($value) == 11 || strlen($value) == 14){
                    return $value;
                }
                elseif(strlen($value) > 11 && strlen($value) < 14){
                    return str_pad($value, 14, '0', STR_PAD_LEFT);
                }
            case "DELETE_EXTENSION":
                $val = explode(".", $value);
                return $val[0];
        }

    }
    public function table($key){
        if($this->database){
            $this->db->query("truncate {$this->dbSulfix}$key") or die(mysqli_error($db));
        }
        for($i = 0; $i < sizeof($this->_global['files'][$key]); $i++){

            $archive = @fopen("{$this->path['data']}{$this->_global['files'][$key][$i]}", "r");

            if($archive !== false){
                $wrapper = array();
                $count = 0;

                while(!feof($archive)){
                    $line = fgets($archive, 1024);

                    if (empty($line) || strlen($line) < $this->_global['length'][$key]){
                        break;
                    }

                    if(count($this->_global['files'][$key]) == 2){
                        if($i == 0){
                            $wrapper[$count]['locVenda'] = "L";
                        }else if($i == 1){
                            $wrapper[$count]['locVenda'] = "V";
                        }
                    }

                    for($j = 0; $j < count($this->_global['schema'][$key]); $j++){
                        $schema = $this->_global['schema'][$key][$j];
                        $wrapper[$count][$schema[0]] = $this->get($line, $schema[1], $schema[2]);
                    }
                    $count++;
                    unset($line);
                }

                fclose($archive);

                if($this->database){
                    $this->query($key, $wrapper);
                }else{
                    //$this->data["{$key}"] = $wrapper;
                }
                $wrapper = NULL;

            }else{
                $this->sendError("The archive {$this->_global['files'][$key]} not exists or is not readable");
                return false;
            }

           unset($archive);
        }
    }

    public function all(){
        foreach($this->_global['files'] as $key => $value){
            $this->table($key);
        }
        $this->photos();
    }
    public function delete($table){
        $this->db->query("truncate {$this->dbSulfix}$table") or die(mysqli_error($db));
    }
    public function deleteAll(){
        foreach($this->_global['files'] as $key => $value){
            $this->delete($key);
        }
        $this->delete('fotos');
        $this->delete('arquivos');
    }
    public function photos(){
        chdir($this->path['photos']);
        $diretorio = getcwd(); 
        $ponteiro  = opendir($diretorio);
        $itensPasta = array();
        $count = 0;
        while ($nome_itens = readdir($ponteiro)) {
            if(!($nome_itens == "." || $nome_itens == ".." || $nome_itens == "Thumbs.db")){
                if(is_dir($nome_itens)){
                    chdir($nome_itens);
                    $diretorio = getcwd(); 
                    $ponteiro2  = opendir($diretorio);
                    while ($nome_itens = readdir($ponteiro2)) {
                        if(!($nome_itens == "." || $nome_itens == ".." || $nome_itens == "Thumbs.db" || $nome_itens == "error_log" )){
                            $itensPasta[$count]['arquivo'] = $this->get($nome_itens, 0, null);
                            $itensPasta[$count]['id_imovel'] = $this->get($nome_itens, 0, 8, "int");
                            $itensPasta[$count]['sigla'] = $this->get($nome_itens, 9, 1);
                            $itensPasta[$count]['legenda'] = $this->get($nome_itens, 11, 2);
                            $itensPasta[$count]['ordem'] = $this->get($nome_itens, 14, 1);

                            $count++;
                        }
                    }
                    chdir("../");
                }
            }
        }

        if($this->database){
            $this->query('fotos', $itensPasta);
        }else{
            $this->data['fotos'] = $itensPasta;
        }
    }
    public function getData($query){
        $count = 0;
        $arr = array();
        while($result = $query->fetch_assoc()){
            foreach($result as $key => $value){
                $arr[$count][$key] = $value;
            }
            $count++;
        }
        return $arr;
    }

    public function getArchives(){
        $query = $this->db->query("SELECT * FROM imobiliar_arquivos") or die(mysqli_error($db));

        return $this->getData($query);
    }
    public function boletos(){
        chdir($this->path['data'] . "bloquetos/");
        $diretorio = getcwd(); 
        $ponteiro  = opendir($diretorio);
        $itensPasta = array();
        $count = 0;
        while ($nome_itens = readdir($ponteiro)) {
            if(!($nome_itens == "." || $nome_itens == ".." || $nome_itens == "Thumbs.db")){
                if(is_dir($nome_itens)){
                    $dirName = $nome_itens;
                    chdir($nome_itens);
                    $diretorio = getcwd(); 
                    $ponteiro2  = opendir($diretorio);
                    while ($nome_itens = readdir($ponteiro2)) {
                        if(!($nome_itens == "." || $dirName == "0" || $dirName == 0 || $nome_itens == ".." || $nome_itens == "Thumbs.db" || $nome_itens == "error_log" || $dirName == "" || $dirName == null)){
                            $itensPasta[$count]['arquivo'] = $nome_itens;
                            $itensPasta[$count]['tipo'] = $this->get($nome_itens, 0, 1);
                            $itensPasta[$count]['id_usuario'] = $dirName;
                            $itensPasta[$count]['id_arquivo'] = $this->get($nome_itens, 1, null, "delete_extension");
                            $itensPasta[$count]['tipo_de_arquivo'] = 1;

                            $count++;
                        }
                    }
                    chdir("../");
                }
            }
        }
        chdir("../../");
        if($this->database){
            $this->query('arquivos', $itensPasta);
        }else{
            $this->data['arquivos'] = $itensPasta;
        }

        return $itensPasta;
    }
    public function services(){
        chdir($this->path['data']);
        $diretorio = getcwd(); 
        $ponteiro  = opendir($diretorio);
        $itensPasta = array();
        $count = 0;
        while ($nome_itens = readdir($ponteiro)) {
            if(!($nome_itens == "." || $nome_itens == ".." || $nome_itens == "Thumbs.db" || is_dir($nome_itens))){
                $letters = str_split($nome_itens);
                if($letters[0] == "C" || $letters[0] == "L" && $letters[1] == "0"){
                    $itensPasta[$count]['arquivo'] = $nome_itens;
                    $itensPasta[$count]['tipo'] = $this->get($nome_itens, 0, 1);
                    $itensPasta[$count]['id_usuario'] = $this->get($nome_itens, 1, null, "delete_extension");
                    $itensPasta[$count]['id_usuario'] = $this->get($itensPasta[$count]['id_usuario'], 0, null, "int");
                    $itensPasta[$count]['id_arquivo'] = $this->get($nome_itens, 1, null, "delete_extension");
                    $itensPasta[$count]['tipo_de_arquivo'] = 2;

                    echo $itensPasta[$count]['arquivo']." - ".$itensPasta[$count]['id_usuario']."<br>";
                    
                    $count++;
                }
            }
        }

        if($this->database){
            $this->query('arquivos', $itensPasta);
        }else{
            $this->data['arquivos'] = $itensPasta;
        }
        chdir("../");
        return $itensPasta;
    }

    public function emailDemonstrativos(){
        chdir($this->path['data']);
        $diretorio = getcwd(); 
        $ponteiro  = opendir($diretorio);
        $itensPasta = array();
        $count = 0;
        while ($nome_itens = readdir($ponteiro)) {
            if(!($nome_itens == "." || $nome_itens == ".." || $nome_itens == "Thumbs.db" || is_dir($nome_itens))){
                $letters = str_split($nome_itens);

                if($letters[0] == "L"){
                    $itensPasta[$count]['arquivo'] = $nome_itens;
                    $itensPasta[$count]['filesize'] = filesize($nome_itens);
                    $itensPasta[$count]['ano'] = $letters[10].$letters[11];
                    $itensPasta[$count]['mes'] = $letters[12].$letters[13];
                    $itensPasta[$count]['tipo'] = $this->get($nome_itens, 0, 1);
                    $itensPasta[$count]['id_usuario'] = $this->get($nome_itens, 1, null, "delete_extension");
                    $itensPasta[$count]['id_usuario'] = $this->get($itensPasta[$count]['id_usuario'], 0, null, "int");
                    $itensPasta[$count]['id_arquivo'] = $this->get($nome_itens, 1, null, "delete_extension");
                    $itensPasta[$count]['tipo_de_arquivo'] = 2;

                    $count++;
                }
            }
        }
        return $itensPasta;
    }
}