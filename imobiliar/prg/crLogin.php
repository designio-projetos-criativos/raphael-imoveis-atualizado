<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors', 'On');

// Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

}



include 'msg.php';
include 'pesqCli.php';
include 'log.php';

header('Content-Type: text/html; charset=UTF-8');

$DirDados = Configuracao('DIR_DADOS');
$DirModelos = Configuracao('DIR_MODELOS_AREACLIENTE');
$DirAnexos = Configuracao('DIR_ANEXOS');
$ServicosExtras = (Configuracao('SERVICOS_EXTRAS_COND') == 'SIM');
$ExibePlanilhaGas = (Configuracao('PLANILHA_GAS_COND') == 'SIM');
$ExibeAnexos = (Configuracao('EXIBE_COND_ANEXOS') == 'SIM');
$ListaCondominos = (Configuracao('EXIBE_LISTA_CONDOMINOS') == 'SIM');
$BoletosEmArvore = (Configuracao('BOLETOS_EM_ARVORE') == 'SIM');
$DirBoletos = Configuracao('DIR_BOLETOS');
$ExtratoComOrdemBloco = (Configuracao('EXTRATO_ORDEMBLOCO') == 'SIM');
$MesesNoSite = Configuracao('MESES_NO_SITE', 3);
$MesesNoSiteBoleto = Configuracao('MESES_NO_SITE_BOLETOS', 2);
$MesesNoSiteExtratoCond = Configuracao('MESES_NO_SITE_EXTRATOCOND', $MesesNoSite);
$ExibeExtratoCondAtual = (Configuracao('EXIBE_EXTRATOCOND_ATUAL', 'SIM') == 'SIM');
$LoginComSenha = (Configuracao('LOGIN_SENHA', 'SIM') == 'SIM');

$CondominiosDoUsuario = array();

if(isset($_GET['token'])){

    $token = $_GET['token'];
    $token = urldecode($token);
    $token = explode(" $ ", $token);
    $_POST['LOGIN'] = base64_decode($token[1]);
    $_POST['SENHA'] = base64_decode($token[2]);
    
}


//--------------------------------------------------------------------------------
//	Obtem o campo do arquivo de boleto
//--------------------------------------------------------------------------------

function GetField($handle, $field)
{
    $Valor = '';
    rewind($handle);
    while (!feof ($handle))
    {
        $buffer = fgets($handle, 1024);
        if (empty($buffer))
            break;

        $out = explode('=', $buffer);
        if ($out[0] == $field)
        {
            $Valor = trim($out[1]);
            break;
        }
    }

    return $Valor;
}

//----------------------------------------------------------------------------------
function GetObsComerc(&$model, $pId)
{
    /*
	Verifica se o usuario tem observacoes de comercializacao.
*/
    GLOBAL $DirDados;

    $bObs = false;
    if (Configuracao('EXIBE_OBS_COMERCIALIZACAO') == 'SIM')
    {
        $file = $DirDados.'obscomerc/P'.$pId.'.txt';
        $bObs = is_file($file);
    }

    $model->assign('OBS_ID', $pId);
    $model->assign('TEM_OBSCOMERC', $bObs? 'T' : '');

    return $bObs;
}

//----------------------------------------------------------------------------------
function GetBoletos(&$model, $pId)
{
    /*
	Busca todos os boletos do usuario.
*/
    GLOBAL $DirBoletos, $BoletosEmArvore, $MesesNoSiteBoleto, $CondominiosDoUsuario;

    $bTemBoleto = false;
    if ($BoletosEmArvore)
        $dir = sprintf("%s%05d000/0%d00/%d", $DirBoletos, $pId/1000, ($pId%1000)/100, $pId);
    else
        $dir = $DirBoletos.$pId;
    if (is_dir($dir))
    {
        $Bloquetos = array();
        $fh = opendir($dir);
        while (false !== ($dirEntry = readdir($fh)))
        {
            if ($dirEntry == '.' || $dirEntry == '..')
                continue;
            if (strstr($dirEntry, '.txt') != '.txt')
                continue;
            $filename = $dir.'/'.$dirEntry;
            $stat = @stat($filename);
            if ($stat === false || $stat['size'] <= 0)
                continue;

            $docextra = strstr($dirEntry, 'E');
            if (empty($docextra))
                $docextra = '';
            else
            {
                $ano = substr($docextra, 1, 4);
                $mes = substr($docextra, 5, 2);
                $dia = substr($docextra, 7, 2);
                if (!checkdate($mes,$dia,$ano))
                    continue;

                $datalimite = mktime(0,0,0,$mes+$MesesNoSiteBoleto,$dia,$ano);
                if (time() > $datalimite)
                {
                    // Bloqueto extra ja' expirou entao nao exibe e tenta remove-lo
                    if (strstr($filename, '.sempre.txt') != '.sempre.txt')
                    {
                        // Nao e' nome especial que indica ser sempre valido
                        @unlink($filename);
                        continue;
                    }
                }

                $datalimite = mktime(0,0,0,$mes-1,$dia,$ano);
                if (time() <= $datalimite) {
                    // Bloqueto extra de mes adiante nao exibe
                    //echo "$datalimite ";
                    continue;
                }

                $docextra = ' EXTRA';
            }

            if (($handle = @fopen ($filename, 'r')) === false)
                continue;
            if (GetField($handle, 'IsRetido') == 'S')
            {
                fclose ($handle);
                continue;
            }

            $Tipo = GetField($handle, 'TIPO');
            if ($Tipo == 'L')
                $TipoDoc = "DOC$docextra: Loca&ccedil;&atilde;o &nbsp;&nbsp;";
            else if ($Tipo == 'C')
            {
                // Verifica se condominio esta ativo
                $CodCondom = GetField($handle, 'IsNumeroDoc');
                $CodCondom = intval(substr($CodCondom, 0, 5));
                /* DESATIVADO
				if (!array_key_exists($CodCondom, $CondominiosDoUsuario))
				{
					fclose ($handle);
					continue;
				}
*/
                $TipoDoc = "DOC$docextra: Condom&iacute;nio &nbsp;&nbsp;";
            }
            else
            {
                fclose ($handle);
                continue;
            }

            $bTemBoleto = true;
            $Vencto = GetField($handle, 'IsVencto');
            if (empty($Vencto))
                $Vencto = GetField($handle, 'IsDataVenc');

            $dia = substr($Vencto, 0, 2);
            $mes = substr($Vencto, 3, 2);
            $ano = substr($Vencto, 6, 4);
            //echo "<!-- $dia/$mes/$ano -->\n";
            if (checkdate($mes,$dia,$ano) !== false)
            {
                $datalimite = mktime(0,0,0,$mes+$MesesNoSiteBoleto,$dia,$ano);
                if (time() > $datalimite)
                {
                    // Bloqueto ja' expirou entao nao exibe e tenta remove-lo
                    if (strstr($filename, '.sempre.txt') != '.sempre.txt')
                    {
                        // Nao e' nome especial que indica ser sempre valido
                        fclose ($handle);
                        @unlink($filename);
                        continue;
                    }
                }
            }

            $CodBarra = GetField($handle, 'IsCodBarra1');
            $Quitado = '';
            if (empty($CodBarra))
            {
                $DebConta = GetField($handle, 'IsDebConta');
                if (stristr($DebConta, 'Quitado') != false)
                    $Quitado = '&nbsp;(QUITADO)';
            }
            $Ender = GetField($handle, 'IsId2');
            $Ender = explode(':', $Ender);

            $Bloquetos[$filename] = array(
                'vencto' => "Vencimento: $Vencto<br>",
                'ender' => trim($Ender[1]).$Quitado,
                'tipo' => $TipoDoc);
            fclose ($handle);
        }
        closedir($fh);

        krsort($Bloquetos);
        foreach ($Bloquetos as $file => $infos)
        {
            $model->assign('VENC_BOLETO', $infos['vencto']);
            $model->assign('END_BOLETO', $infos['ender']);
            $model->assign('TIPO_BOLETO', $infos['tipo']);
            $model->assign('FBOLETO', $file);
            $model->assign('BTN_SEGUNDAVIA', '<input type=submit value=" Exibir" name="btn"><BR><BR>');  // Forma antiga com toda tag do botao
            $model->parse('.LISTA_BOLETOS');
        }
    }

    return $bTemBoleto;
}

//----------------------------------------------------------------------------------
function GetServices(&$model, $CondLoc, $pId)
{
    /*
	registro de acesso{
	id             10;
	produto         1;
	chave           8;
	assessor       35;
	tipo            1;
	assessor_email 50;
	descricao      80;
	condomino       1;      'S'indico; 'N'ormal;
	----> Formato original: 186 bytes ate aqui <----
	tipo_bloco      1;      'N'ormal; 'F'undo Reserva;
	ordembloco      2;
}
*/
    GLOBAL $DirDados, $DirAnexos, $ServicosExtras, $ExtratoComOrdemBloco, $ExibeAnexos, 
    $ListaCondominos, $CondominiosDoUsuario, $ExibeExtratoCondAtual,
    $MesesNoSite, $MesesNoSiteExtratoCond, $ExibePlanilhaGas;

    $iTamReg = 186;
    $PlanilhasGeradas = ' ';

    if ($CondLoc == 'Loc')
    {
        $name = $DirDados.'AcessoLoc.txt';
        $bInadimpSeparado = false;
        $MesesExibir = $MesesNoSite;
    }
    else
    {
        $name = $DirDados.'AcessoCond.txt';
        $bInadimpSeparado = (Configuracao('EXIBE_INADIMPL_SEPARADO') == 'SIM');
        $MesesExibir = $MesesNoSiteExtratoCond;
    }
    $stat = @stat($name);
    if ($stat === false || $stat['size'] <= 0)
        return 0;

    $file = @fopen($name, 'r');
    if ($file === false)
        return 0;
    $bNenhum = 1;
    $slast = '';
    $pId = trim($pId);
    $UltAnexo = '';
    $UltLista = '';
    $UltPlanilhaGas = '';

    $cont = 1;
    for(;;)
    {
        $sReg = fgets($file, 4096);
        if (empty($sReg) || strlen($sReg) < $iTamReg+1)
            break;

        $Id = trim(substr($sReg, 0, 10));
        if ( strcmp($pId, $Id) == 0)
        {
            //echo "<!-- $sReg -->\n";
            $Tipo = substr($sReg, 54, 1);
            if ( strcmp($Tipo, 'P') == 0)
                $Tipo = 'Propriet&aacute;rio';
            elseif ( strcmp($Tipo, 'C') == 0)
                $Tipo = 'Cond&ocirc;mino';
            elseif ( strcmp($Tipo, 'S') == 0)
                $Tipo = 'S&iacute;indico';

            // Verifica o produto.
            $TipoProd = substr($sReg, 10, 1);
            if ($TipoProd == 'C')
                $Produto = 'Extrato de Condom&iacute;nio';
            elseif ($TipoProd == 'A')
                $Produto = 'Extrato de Propriet&aacute;rio';
            elseif ($TipoProd == 'S')
                $Produto = 'Demonstrativo Sint&eacute;tico';
            elseif ($TipoProd == 'L')
                $Produto = 'Demonstrativo de Propriet&aacute;rio';
            elseif ($TipoProd == 'I')
                $Produto = 'Inadimpl&ecirc;encias';
            else
                continue;

            $Assessor = substr($sReg, 19, 35);
            $EAssessor = substr($sReg, 55, 50);
            $Chave = trim(substr($sReg, 11, 8));
            $Descr = trim(substr($sReg, 105, 80));
            $Condomino = substr($sReg, 185, 1);
            SetSessao('tipo_cond', $Condomino);

            if ($CondLoc == 'Loc')
            {
                $TipoBloco = '?';
                $Filial = trim(substr($sReg, 186, 3));
                if (!empty($Filial))
                {
                    $Descr .= " (Filial $Filial)";
                    $Chave = $Chave.'-'.$Filial;
                }
            }
            else
            {
                $CodCondom = intval(substr($Chave, 0, 5));
                $CodBloco = substr($Chave, 5, 3);
                $NomeCondom = trim(substr($sReg, 105, 39));
                $TipoBloco = trim(substr($sReg, 186, 1));
                if (empty($TipoBloco) || $TipoBloco == "\n")
                    // Formato antigo entao assume como normal.
                    $TipoBloco = 'N';

                if ($ExtratoComOrdemBloco)
                {
                    $Ordem = substr($sReg, 187, 2);
                    if (!empty($Ordem))
                        $Chave = sprintf("%s_%02d_%s", substr($Chave, 0, 5),$Ordem,$CodBloco);
                }

                if ($ServicosExtras)
                {
                    if (!array_key_exists($CodCondom, $CondominiosDoUsuario))
                    {
                        $model->assign('CODIGO_CONDOMINIO', $CodCondom);
                        $model->assign('NOME_CONDOMINIO', $NomeCondom);
                        $model->assign('USUARIO_TIPO', $Condomino);
                        $model->parse(count($CondominiosDoUsuario)==0 ? 'SERVICOS_EXTRAS_COND' : '.SERVICOS_EXTRAS_COND');
                    }
                }
                $CondominiosDoUsuario[$CodCondom] = array($NomeCondom);
            }
            //echo "<!-- $Chave -->\n";
            $model->assign('PRODUTO', $Produto);
            $model->assign('DESCR', trim($Descr));
            $model->assign('BTN_PHP', 'exibeDoc.php');
            $model->assign('ID', $Id);
            $model->assign('PROD', $TipoProd);
            $model->assign('ASSESSOR', rtrim($Assessor));
            $model->assign('ASSESSOR_EMAIL', rtrim($EAssessor));
            $model->assign('TIPO', $Tipo);
            $model->assign('TIPO_CONDOMINIO', $TipoBloco);
            $model->assign('TIPO_CONDOMINO', $Condomino);
            $model->assign('CHAVE', $Chave);
            $model->assign('DESC_SERV', trim($Descr));

            // Monta nome do arquivo fisico
            $btn = '';
            $mes = date('m');
            $ano = date('y');
            //echo "<!-- $mes/$ano -->\n";
            $iContItem = 0;
            for($i=0; $i<$MesesExibir; $i++)
            {
                if ($ExibeExtratoCondAtual || $i > 0) {
                    $dt = date('ym', mktime(0,0,0, $mes, 1, $ano));
                    $UserFile = $TipoProd.$Chave.'.'.$dt;
                    if (@file_exists($DirDados.$UserFile))
                    {
                        //echo "<!-- $UserFile -->\n";
                        $data = substr($dt,2,2).'/20'.substr($dt,0,2);
                        $btn .= "<input type=submit value=\" Exibir $data\" name=\"btn\" ><!-- $UserFile ($iContItem)--><BR><BR>\n";

                        // Forma nova com a tag do botao no shtml
                        $model->assign('BTN_VAL', $data);
                        $model->assign('BTN_SEQ', $iContItem+1);
                        $model->assign('BTN_ARQ', $UserFile);
                        $model->parse($iContItem++==0 ? 'LISTA_BOTOES' : '.LISTA_BOTOES');

                    }
                    //else echo "<!-- NAO ABRIU $UserFile -->\n";
                }
                $mes--;
                if ($mes <= 0) {
                    $mes = 12;
                    $ano--;
                }
            }
            $model->assign('BTN_MES', $btn); // Forma antiga com toda tag do botao

            if ($iContItem > 0)
            {
                $model->parse('.LISTA_SERVICOS');
                $bNenhum = 0;

                if ($TipoBloco == 'N')
                {
                    if ($bInadimpSeparado)
                    {
                        // Acrescenta botao para exibir inadimplencia separadamente para o sindico
                        $dt = date('ym', mktime(0,0,0, date('m'), 1, date('y')));
                        $UserFile = $TipoProd.$Chave.'.'.$dt;
                        if (($File=@fopen($DirDados.$UserFile, 'r')) !== false)
                        {
                            // Pula reg. header
                            for (;;) {
                                $sReg = fgets($File, 4098);
                                if (empty($sReg) || substr($sReg, 0, 1) == 'S')
                                    break;
                            }
                            if (substr($sReg, 0, 1) == 'S')
                            {
                                // E' reg. de Sindico
                                $bConselheiro = false;
                                $conselho = explode(',', substr(trim($sReg),1));
                                foreach ($conselho as $aux)
                                {
                                    $aux = intval($aux);
                                    if ($aux == intval($Id))
                                    {
                                        $bConselheiro = true;
                                        break;
                                    }
                                }

                                if ($bConselheiro)
                                {
                                    $model->assign('PRODUTO', 'Inadimpl&ecirc;ncia');
                                    $model->assign('PROD', 'I');
                                    $model->assign('BTN_PHP', 'exibeDoc.php');

                                    // Forma nova com a tag do botao no shtml
                                    $data = substr($dt,2,2).'/20'.substr($dt,0,2);
                                    $model->assign('BTN_VAL', $data);
                                    $model->assign('BTN_SEQ', 1);
                                    $model->assign('BTN_ARQ', $UserFile);
                                    $model->parse('LISTA_BOTOES');

                                    $btn = "<input type=submit value=\" Exibir $data\" name=\"btn\"><!-- $UserFile --><BR><BR>\n";
                                    $model->assign('BTN_MES', $btn); // Forma antiga com toda tag do botao

                                    $model->parse('.LISTA_SERVICOS');
                                }
                            }
                            fclose($File);
                        }
                    }

                    if ($ListaCondominos && $TipoProd == 'C' && $Condomino == 'S' && $CodCondom != $UltLista)
                    {
                        // Acrescenta botao para exibir lista de condominos
                        $UserFile = 'I'.str_pad($CodCondom, 5, '0', STR_PAD_LEFT).'.TXT';
                        $arq = @fopen($DirDados.$UserFile, 'r');
                        if ($arq !== false)
                        {
                            $model->assign('PRODUTO', 'Lista de Condominos');
                            $model->assign('PROD', 'L');
                            $model->assign('BTN_PHP', 'condominos.php');

                            $Descr = fgets($arq, 4096);
                            $TipoReg = $Descr{0};
                            if ($TipoReg == 'C')	// Cabecalho
                            {
                                $Descr = substr($Descr, 7, 60);
                                $iPos = strpos($Descr, '-');
                                $Descr = trim(substr($Descr, $iPos+1));
                                $model->assign('DESCR', $Descr);
                            }
                            fclose($arq);

                            // Forma nova com a tag do botao no shtml
                            $model->assign('BTN_VAL', '');
                            $model->assign('BTN_SEQ', 1);
                            $model->assign('BTN_ARQ', $UserFile);
                            $model->parse('LISTA_BOTOES');

                            $model->assign('BTN_MES', ''); // Forma antiga com toda tag do botao

                            $model->parse('.LISTA_SERVICOS');
                        }
                        $UltLista = $CodCondom;
                    }

                    if ($ExibeAnexos && $TipoProd == 'C' && $CodCondom != $UltAnexo)
                    {
                        $DirAnexosCond = $DirAnexos.'Condom/'.$CodCondom;
                        if (is_dir($DirAnexosCond))
                        {
                            $arq = @fopen($DirDados.$UserFile, 'r');
                            if ($arq !== false)
                            {
                                $Descr = fgets($arq, 4096);
                                if ($Descr{0} == ' ')	// Cabecalho
                                {
                                    $Descr = substr($Descr, 26, 60);
                                    $model->assign('DESCR', trim($Descr));
                                    $model->assign('DESC_SERV', trim($Descr));
                                }
                                fclose($arq);
                            }
                            $model->assign('PRODUTO', 'Documentos do Condom&iacute;nio (atas, convoca&ccedil;&otilde;es, etc.)');
                            $model->assign('PROD', 'C');
                            $model->assign('BTN_PHP', 'anexo.php');
                            $model->assign('CHAVE', $CodCondom);

                            // Forma nova com a tag do botao no shtml
                            $model->assign('BTN_VAL', '');
                            $model->assign('BTN_SEQ', 1);
                            $model->assign('BTN_ARQ', $UserFile);
                            $model->parse('LISTA_BOTOES');

                            $model->assign('BTN_MES', ''); // Forma antiga com toda tag do botao

                            $model->parse('.LISTA_SERVICOS');
                        }
                        $UltAnexo = $CodCondom;
                    }

                    if ($ExibePlanilhaGas && $Condomino == 'S' && $TipoProd == 'C' && $CodCondom != $UltPlanilhaGas)
                    {
                        $UltPlanilhaGas = $CodCondom;
                        $DirGasAgua = $DirDados.'gasagua/'.substr($Chave,0,5);
                        //echo "DirGasAgua=$DirGasAgua\n";
                        if (file_exists($DirGasAgua))
                        {
                            // Acrescenta botao para exibir planilha de gas separadamente para o sindico.
                            // Ex.: .../gasagua/00108/G00108UNC-201404.csv
                            $dtAtual = date('Ym', mktime(0,0,0, date('m'), 1, date('y')));
                            $dtAnt = date('Ym', mktime(0,0,0, date('m')-1, 1, date('y')));
                            $aDir = scandir($DirGasAgua);
                            rsort($aDir);
                            //print_r($aDir);
                            //echo "PlanilhasGeradas=$PlanilhasGeradas\n";
                            for ($i = 0; $i < 2; $i++)
                            {
                                $tipoPlan = ($i == 0 ? 'G' : 'A');
                                $Cont = 0;

                                foreach ($aDir as $UserFile)
                                {
                                    //echo "UserFile=$UserFile\n";
                                    if (!is_file("$DirGasAgua/$UserFile"))
                                        continue;

                                    if (preg_match('/ '.substr($UserFile,0,-11).' /', $PlanilhasGeradas))
                                        continue;

                                    $dt = $dtAtual;
                                    //echo 'preg_match(/'.$tipoPlan.substr($Chave,0,5).'.{1,3}-'.$dt.'\.csv/, "'.$UserFile."\")\n";
                                    $bTem = preg_match('/'.$tipoPlan.substr($Chave,0,5).'.{1,3}-'.$dt.'\.csv/', $UserFile);
                                    if (!$bTem)
                                    {
                                        $dt = $dtAnt;
                                        //echo 'preg_match(/'.$tipoPlan.substr($Chave,0,5).'.{1,3}-'.$dt.'\.csv/, "'.$UserFile."\")\n";
                                        $bTem = preg_match('/'.$tipoPlan.substr($Chave,0,5).'.{1,3}-'.$dt.'\.csv/', $UserFile);
                                    }

                                    if ($bTem)
                                    {
                                        //echo "[[ ACHOU planilha $UserFile ]]\n";
                                        $File = @fopen("$DirGasAgua/$UserFile", 'r+');
                                        if ($File === FALSE)
                                            continue;
                                        $aReg = fgetcsv($File, 1024, ';');
                                        fclose($File);

                                        $PlanilhasGeradas .= substr($UserFile,0,-11).' ';
                                        if ($Cont == 0)
                                        {
                                            $model->assign('PRODUTO', 'Planilha de Consumo de '.($i == 0 ? 'G&aacute;s' : '&Aacute;gua'));
                                            $model->assign('PROD', $tipoPlan);
                                            $model->assign('DESCR', $aReg[3]); // Nome do Condominio
                                            $model->assign('BTN_PHP', 'planilhaGas.php');
                                            $model->assign('CHAVE', $Chave);
                                        }
                                        // Forma nova com a tag do botao no shtml
                                        $bloco = $aReg[4].' - '.substr($dt,4,2).'/'.substr($dt,0,4); // Nome do Bloco e competencia
                                        $model->assign('BTN_VAL', $bloco);
                                        $model->assign('BTN_VAL_EXTRA', $UserFile);
                                        $model->assign('BTN_SEQ', 1);
                                        $model->assign('BTN_ARQ', $UserFile);
                                        $model->parse($Cont == 0 ? 'LISTA_BOTOES' : '.LISTA_BOTOES');
                                        $model->assign('BTN_MES', ''); // Forma antiga com toda tag do botao
                                        $Cont++;
                                        //echo "[[ Botao $bloco ]]\n";
                                    }
                                    //else echo "[[ NAO serve a planilha $UserFile ]]\n";
                                }
                                if ($Cont != 0)
                                    $model->parse('.LISTA_SERVICOS');
                            }
                        }
                    }
                }
            }
        }
    }

    fclose($file);

    if ($bNenhum)
        return 0;
    return 1;
}

//----------------------------------------------------------------------------------
function GetInfoIR(&$model, $pId, $bMultiplos)
{
    /*
	Busca todos as informacoes de IR do usuario.
*/
    GLOBAL $DirDados;

    //echo("<!-- GetInfoIR(model, $pId, $bMultiplos) -->\n");
    if (Configuracao('EXIBE_IR_ANUAL') != 'SIM')
        return false;

    $DirIRanual = Configuracao('DIR_IRANUAL');
    $iContItem = 0;
    $bTemInfo = false;
    $pId = intval($pId);
    $dir = sprintf('%s%d000/%d', $DirIRanual, $pId/1000, $pId);
    if (is_dir($dir))
    {
        $files = scandir($dir);
        foreach ($files as $dirEntry)
        {
            if (intval(substr($dirEntry, 0, 8)) != $pId || strstr($dirEntry, '.csv') != '.csv')
                continue;

            $file = $dir.'/'.$dirEntry;
            $stat = @stat($file);
            if ($stat === false || $stat['size'] <= 0)
                continue;

            $bTemInfo = true;
            $Comp = substr($dirEntry, 9, 4);
            $model->assign('COMPETENCIA', $Comp);
            $model->assign('IR_ID', $bMultiplos ? 'Cod.'.$pId : '');
            $model->parse($iContItem++==0 ? 'LISTA_INFOS_IR' : '.LISTA_INFOS_IR');
        }
    }

    if ($bTemInfo)
    {
        $model->assign('ID', $pId);
        $model->parse('.INFOS_IR_ANUAL');
    }

    //echo("<!-- GetInfoIR: $bTemInfo -->\n");
    return $bTemInfo;
}

//---main-------------------------------------------------------------------------

if (Campo('usuarios') !== false)
    showCli();
/*
session_cache_limiter('public');
$curVer=intval(str_replace('.', '',phpversion()));
if( $curVer >= 420 )
	$cache = session_cache_expire(5);
session_cache_limiter('public');
*/
//Mensagem('Aviso', 'Site em manutencao!'); return 0;

SetSessao('ORIGEM_MSG', 'O'); //cliente on-line

if ($_POST['LOGIN'] !== false)
{
    $pId = $_POST['LOGIN'];
    if (!$pId)
    {
        Mensagem('Erro', 'Usu&aacute;rio inv&aacute;lido!');
        exit;
    }

    // Valida usuario
    if ($LoginComSenha)
    {
        $pPass = $_POST['SENHA'];
        if (!$pPass)
        {
            Mensagem('Erro', 'Senha inv&aacute;lida!');
            exit;
        }
        $usuario_id = pesqCli($pId, $pPass);
    }
    else
        $usuario_id = pesqCli($pId);
    //echo "<!-- "; print_r($usuario_id); print(" -->\n");

    if (is_array($usuario_id))
    {
        $usuario = array_shift($usuario_id);
        $usuario_cpfcnpj = array_shift($usuario_id);
        $usuario_email = array_shift($usuario_id);
        
    }

    if (empty($usuario))
    {
        Mensagem('Erro', 'Usu&aacute;rio n&atilde;o cadastrado!');
        exit;
    }
    if (strcmp($usuario, '####') == 0)
    {
        Mensagem('Erro', 'Senha inv&aacute;lida!');
        exit;
    }
    SetSessao('usuario', $usuario);
    SetSessao('usuario_id', $usuario_id);
    SetSessao('usuario_cod', $pId);
    SetSessao('usuario_cpfcnpj', $usuario_cpfcnpj);
    SetSessao('usuario_email', $usuario_email);
}
else
{
    $logout = GetSessao('logout_url');
    $usuario = GetSessao('usuario');
    $usuario_id = GetSessao('usuario_id');
    $usuario_cpfcnpj = GetSessao('usuario_cpfcnpj');
    $usuario_email = GetSessao('usuario_email');
    if (!empty($logout) || empty($usuario) || empty($usuario_id))
    {
        $page = Campo('page');
        if (empty($page))
            $page = GetSessao('login_url');

        if (empty($page))
            Mensagem('Erro', 'Sess&atilde;o expirada, efetue o LOGIN!');
        else
        {
            SetSessao('login_url', $page);
            header("Location: $page");
        }
        exit;
    }
    $pId = $usuario_id[0];
}

// Vai preencher as linhas da tabela com os servicos disponiveis
$model = new DTemplate($DirModelos);
$model-> define_templates( array ( 'clionline' => 'clionline_new.shtml'));
$model->define_dynamic('LISTA_SERVICOS', 'clionline');		//body is the parent of table
$model->define_dynamic('LISTA_BOTOES', 'LISTA_SERVICOS');	//body is the parent of table
$model->define_dynamic('LISTA_BOLETOS', 'clionline');
$model->define_dynamic('SERVICOS_EXTRAS_COND', 'clionline');
$model->define_dynamic('INFOS_IR_ANUAL', 'clionline');
$model->define_dynamic('LISTA_INFOS_IR', 'INFOS_IR_ANUAL');

//print "\n<!-- "; print_r($usuario_id); print(" -->\n");
$model->assign('ID', $pId);
$model->assign('USUARIO', $usuario);
$model->assign('USUARIO_EMAIL', $usuario_email);

$cont = count($usuario_id) - 1;
$ServicosL = $ServicosC = $ServicosB = $ServicosO = $ServicosIR = false;

for ($i = $cont; $i >= 0; $i--)
    $ServicosL |= GetServices($model, 'Loc', $usuario_id[$i]);
for ($i = $cont; $i >= 0; $i--)
    $ServicosC |= GetServices($model, 'Cond', $usuario_id[$i]);
for ($i = $cont; $i >= 0; $i--)
    $ServicosB |= GetBoletos($model, $usuario_id[$i]);
for ($i = $cont; $i >= 0; $i--)
    $ServicosO |= GetObsComerc($model, $usuario_id[$i]);
for ($i = $cont; $i >= 0; $i--)
    $ServicosIR |= GetInfoIR($model, $usuario_id[$i], $cont>0);

if (!$ServicosL && !$ServicosC && !$ServicosB && !$ServicosO && !$ServicosIR)
{
    Mensagem('Aviso', 'N&atilde;o existem informa&ccedil;&otilde;es para este usu&aacute;rio!');
    return 0;
}
if (!$ServicosL && !$ServicosC)
    $model->clear_dynamic('LISTA_SERVICOS');
if (!$ServicosC)
    // Se nao tem condominios entao nao tem servicos extras de condominio
    $ServicosExtras = false;
if (!$ServicosExtras)
    $model->clear_dynamic('SERVICOS_EXTRAS_COND');
if (!$ServicosB)
    $model->clear_dynamic('LISTA_BOLETOS');
if (!$ServicosIR)
    $model->clear_dynamic('INFOS_IR_ANUAL');

SetSessao('produto', 'cred');
SetSessao('logout_url', '');
phpLog('LI', 'Cliente-online', $pId);


echo "<!--\n";
echo '$_SESSION = ';
print_r($_SESSION);
global $HTTP_SESSION_VARS;
echo '$HTTP_SESSION_VARS = ';
print_r($HTTP_SESSION_VARS);
echo "\n-->\n";

$model->parse('clionline');
$model->DPrint('clionline');
?>
