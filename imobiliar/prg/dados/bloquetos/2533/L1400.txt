TIPO= L
CODPESSOA=2533
IsTipoDoc=DOC DE LOCA&Ccedil;&Atilde;O
IsImob1=RAPHAEL  IM&Oacute;VEIS

IsImob2=R CRUZ, SANTA, 1992 - CEP:96015-710 - BAIRRO CENTRO - PELOTAS/RS - FONE:3225.1100
IsImob3=93.301.976/0001-85

IsId1=IM&Oacute;VEL     : 00001400                           NRO.DOC:00001400.01.10/2017.N
IsId2=ENDERE&Ccedil;O   : R ALVARO CHAVES, 371 - AP 203 - PELOTAS/RS
IsId3=LOCAT&Aacute;RIO  : ISABELLA PEREIRA FERREIRA DE Q-030.720.630/01
IsId4=COMPET&Ecirc;NCIA: 10/2017 (NORMAL)
IsId5=PROPRIET.  : 1846-PURIFICACAO DO CARMO COSTA FARIAS - CPF 022.289.710/49
IsVencto=05/11/2017
IsDetLin1=00001-ALUGUEL                  (20 dias)                              841,34
IsDetLin2=00098-AGUA-REPASSE       10/12 (20 dias)                               82,57
IsDetLin3=00218-CONDOMINIO REPASSE       AP (20 dias)                            80,67
IsDetLin4=00122-DOC                                                               6,00
IsDetLin5=----------------------------------------------------------------------------
IsDetLin6=TOTAL                                                               1.010,58
IsDetLin7=
IsDetLin8=
IsDetLin9=
IsDetLin10=
IsDetLin11=
IsDetLin12=
IsDetLin13=
IsDetLin14=
IsDetLin15=
IsDetLin16=
IsDetLin17=
IsDetLin18=
IsDetLin19=
IsDetLin20=
IsDetLin21=
IsDetLin22=
IsDetLin23=
IsDetLin24=
IsDetLin25=
IsDetLin26=
IsDetLin27=
IsDetLin28=
IsDetLin29=
IsDetLin30=
IsDetLin31=
IsDetLin32=
IsDetLin33=
IsDetLin34=
IsDetLin35=
IsDetLin36=
IsDetLin37=
IsDetLin38=
IsDetLin39=
IsDetLin40=
IsCodBanco=104-0
IsLinDig=10497.85783 88000.100847 10003.558862 9 73340000101058
IsLocalPagto=PREF NAS CASAS LOTERICAS ATE O LIMITE
IsDataVenc=05/11/2017
IsNomeCedente=ADMINISTRADORA RAPHAEL LTDA.  CNPJ 93.301.976/0001-85
IsAgeCodCedente=1594/785788-8
IsDataDoc=26/10/2017
IsDataProc=26/10/2017
IsNumeroDoc=00001400.01.10/2017.N
IsNossoNumero=14/000008100035588-9
IsCarteira=RG
IsUsoBanco=
IsAceite=N
IsEspecieDoc=DM
IsMoeda=R$  
IsValorDoc=  1.010,58
IsOutroAcres=
IsDesconto=
IsOutraDed=
IsMoraMulta=
IsValorCobrado=
IsInstru1=PAG&Aacute;VEL NA REDE BANC&Aacute;RIA AT&Eacute; O VENCIMENTO, AP&Oacute;S SOMENTE NA ADMINISTRADORA
IsInstru2=AP&Oacute;S O DIA 20 DO M&Ecirc;S DO VENCIMENTO, SOMENTE NO JUR&Iacute;DICO, ACRESCIDO DE HONOR&Aacute;RIOS
IsInstru3=O PAGAMENTO DESTE DOC N&Atilde;O QUITA D&Eacute;BITOS ANTERIORES. 
IsInstru4=&Eacute; EXPRESSAMENTE PROIBIDA A DEDU&Ccedil;&Atilde;O DE VALORES.
IsInstru5=**AP&Oacute;S O VENCTO, CORRE&Ccedil;&Atilde;O MONET + JUROS:0,066%AD + MULTA DE 10%.
IsInstru6=
IsInstru7=
IsInstru8=
IsInstru9=
IsSacado1=ISABELLA PEREIRA FERREIRA DE QUADROS         Imov.:1400(030.720.630-01)
IsSacado2=R ALVARO CHAVES, 371 - 203
IsSacado3=96010-760  CENTRO - PELOTAS-RS
IsCodBarra1=10499733400001010587857888000100841000355886
