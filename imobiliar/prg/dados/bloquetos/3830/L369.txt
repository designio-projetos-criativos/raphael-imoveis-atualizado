TIPO= L
CODPESSOA=3830
IsTipoDoc=DOC DE LOCA&Ccedil;&Atilde;O
IsImob1=RAPHAEL  IM&Oacute;VEIS

IsImob2=R CRUZ, SANTA, 1992 - CEP:96015-710 - BAIRRO CENTRO - PELOTAS/RS - FONE:3225.1100
IsImob3=93.301.976/0001-85

IsId1=IM&Oacute;VEL     : 00000369                           NRO.DOC:00000369.02.10/2017.N
IsId2=ENDERE&Ccedil;O   : PRQ ANTONIO ZATTERA, 321 - LJ A - PELOTAS/RS
IsId3=LOCAT&Aacute;RIO  : NATACHA DUARTE MAJER-971.430.060/68          
IsId4=COMPET&Ecirc;NCIA: 10/2017 (NORMAL)
IsId5=PROPRIET.  : 853-ISAN IDIARTE MORALES - CPF 321.845.570/72
IsVencto=05/11/2017
IsDetLin1=00001-ALUGUEL                                                       4.404,12
IsDetLin2=00121-AGUA                                                             45,35
IsDetLin3=00122-DOC                                                               4,00
IsDetLin4=----------------------------------------------------------------------------
IsDetLin5=TOTAL                                                               4.453,47
IsDetLin6=
IsDetLin7=
IsDetLin8=
IsDetLin9=
IsDetLin10=
IsDetLin11=
IsDetLin12=
IsDetLin13=
IsDetLin14=
IsDetLin15=
IsDetLin16=
IsDetLin17=
IsDetLin18=
IsDetLin19=
IsDetLin20=
IsDetLin21=
IsDetLin22=
IsDetLin23=
IsDetLin24=
IsDetLin25=
IsDetLin26=
IsDetLin27=
IsDetLin28=
IsDetLin29=
IsDetLin30=
IsDetLin31=
IsDetLin32=
IsDetLin33=
IsDetLin34=
IsDetLin35=
IsDetLin36=
IsDetLin37=
IsDetLin38=
IsDetLin39=
IsDetLin40=
IsCodBanco=104-0
IsLinDig=10497.85783 88000.100847 00003.567286 7 73340000445347
IsLocalPagto=PREF NAS CASAS LOTERICAS ATE O LIMITE
IsDataVenc=05/11/2017
IsNomeCedente=ADMINISTRADORA RAPHAEL LTDA.  CNPJ 93.301.976/0001-85
IsAgeCodCedente=1594/785788-8
IsDataDoc=26/10/2017
IsDataProc=26/10/2017
IsNumeroDoc=00000369.02.10/2017.N
IsNossoNumero=14/000008000035672-0
IsCarteira=RG
IsUsoBanco=
IsAceite=N
IsEspecieDoc=DM
IsMoeda=R$  
IsValorDoc=  4.453,47
IsOutroAcres=
IsDesconto=
IsOutraDed=
IsMoraMulta=
IsValorCobrado=
IsInstru1=PAG&Aacute;VEL NA REDE BANC&Aacute;RIA AT&Eacute; O VENCIMENTO, AP&Oacute;S SOMENTE NA ADMINISTRADORA
IsInstru2=AP&Oacute;S O DIA 20 DO M&Ecirc;S DO VENCIMENTO, SOMENTE NO JUR&Iacute;DICO, ACRESCIDO DE HONOR&Aacute;RIOS
IsInstru3=O PAGAMENTO DESTE DOC N&Atilde;O QUITA D&Eacute;BITOS ANTERIORES. 
IsInstru4=&Eacute; EXPRESSAMENTE PROIBIDA A DEDU&Ccedil;&Atilde;O DE VALORES.
IsInstru5=**AP&Oacute;S O VENCTO, CORRE&Ccedil;&Atilde;O MONET + JUROS:0,066%AD + MULTA DE 10%.
IsInstru6=
IsInstru7=
IsInstru8=
IsInstru9=
IsSacado1=NATACHA DUARTE MAJER                         Imov.:369(971.430.060-68)
IsSacado2=PRQ ANTONIO ZATTERA, 321 - LOJA A
IsSacado3=96015-180  CENTRO - PELOTAS-RS
IsCodBarra1=10497733400004453477857888000100840000356728
