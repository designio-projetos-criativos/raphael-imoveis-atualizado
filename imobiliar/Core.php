<?php

namespace Integration;

class Core extends Database{

    protected function parseArray($arr){
        $i = 0;
        while($qry = $arr->fetch_assoc()){
            $ret[$i] = $qry;
            $i++;
        }
        return $ret;
    }
    
    public function getPhotos(){
        $res = $this->db->query("SELECT * FROM imobiliar_fotos");
        return $this->parseArray($res);
    }
}