<?php

if(!isset($_GET['debug'])){
	exit();
}

ini_set('display_errors',1);
ini_set('display_startup_errors',1);


require "../Database.php";
require "../Sync.php";


$sync = new Integration\Sync("../");
$database = new Integration\Database;

$files = $sync->emailDemonstrativos();

// $mes = date('m');
// $ano = date('y');

$mes = "12";
$ano = "15";

foreach($files as $file){

    //echo $file['arquivo']." - ".$file['filesize']."<br>";

    if($file['ano'] == $ano && $file['mes'] == $mes){
        $consulta = $database->db->query("SELECT * FROM imobiliar_novos_demonstrativos WHERE arquivo = '{$file['arquivo']}'") or die(mysqli_error($database->db));
        if($consulta->num_rows != 0){
            $arquivo = $consulta->fetch_assoc();
            if($arquivo['tamanho'] != $file['filesize']){
                echo "TAM DIF ({$arquivo['tamanho']}) - ";
                $database->db->query("UPDATE imobiliar_novos_demonstrativos SET status = 1 WHERE id = {$arquivo['id']}") or die(mysqli_error($database->db));
            }else{
                echo "TAM IGUAL - ";
            }
            echo "Ja cadastrado<br>";
        }else{
            $database->db->query("INSERT INTO imobiliar_novos_demonstrativos (arquivo, id_usuario, tamanho, status) VALUES ('{$file['arquivo']}', {$file['id_usuario']}, '{$file['filesize']}', 1)") or die(mysqli_error($database->db));
            echo "Nao cadastrado<br>";
        }
    }
}

?>