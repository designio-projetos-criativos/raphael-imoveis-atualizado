<?
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

require "../Database.php";
require "../Sync.php";

function array_diff_recursive($arr1,$arr2){
    $diferenca = array();
    for($i = 0; $i < sizeof($arr1); $i++){
        if(is_array($arr1[$i])){
            if(!in_array($arr1[$i], $arr2)){
                $diferenca[] = $arr1[$i];
            }
        }
    }
    for($i = 0; $i < sizeof($arr2); $i++){
        if(is_array($arr2[$i])){            
            if(!in_array($arr2[$i], $arr1) && !in_array($arr2[$i], $diferenca)){
                $diferenca[] = $arr2[$i];
            }
        }
    }
    
    return !isset($diferenca) ? 0 : $diferenca;
}

$sync = new Integration\Sync("../");
$sync->services();

$oldChanges = $sync->getArchives();

$sync->delete('arquivos');

$boletos = $sync->boletos();
$services = $sync->services();
$newChanges = array_merge($boletos,$services);

$changes = array_diff_recursive($oldChanges,$newChanges);

$sync->delete('novos_arquivos');
$sync->query("novos_arquivos", $changes);