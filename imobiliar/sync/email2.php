<?php

ini_set('display_errors',1);
ini_set('display_startup_errors',1);

require "../Database.php";
require "../Sync.php";

$sync = new Integration\Sync("../");
$database = new Integration\Database;

$files = $sync->emailDemonstrativos();

// $mes = date('m');
// $ano = date('y');

$mes = "12";
$ano = "15";

foreach($files as $file){
    if($file['ano'] == $ano && $file['mes'] == $mes){
        $consulta = $database->db->query("SELECT * FROM imobiliar_novos_demonstrativos WHERE arquivo = '{$file['arquivo']}'") or die(mysqli_error($database->db));
        if($consulta->num_rows == 0){
            $database->db->query("INSERT INTO imobiliar_novos_demonstrativos (arquivo, id_usuario, tamanho, status) VALUES ('{$file['arquivo']}', {$file['id_usuario']}, '{$file['filesize']}', 1)") or die(mysqli_error($database->db));
        }
        // else{
        //     $arquivo = $consulta->fetch_assoc();
        //     if($arquivo['tamanho'] != $file['filesize']){
        //         $database->db->query("UPDATE imobiliar_novos_demonstrativos SET status = 1 WHERE id = {$arquivo['id']}") or die(mysqli_error($database->db));
        //     }
        // }
        
    }
}

$data = $sync->getData($database->db->query("
    SELECT *, imobiliar_novos_demonstrativos.id as iid
    FROM imobiliar_novos_demonstrativos 
    JOIN imobiliar_usuarios 
    ON imobiliar_novos_demonstrativos.id_usuario = imobiliar_usuarios.id 
    WHERE imobiliar_usuarios.email <>'' AND imobiliar_novos_demonstrativos.status = 1
"));

for($i = 0; $i < sizeof($data); $i++){

    $database->db->query("UPDATE imobiliar_novos_demonstrativos SET status = 2 WHERE id = {$data[$i]['iid']}");

    $hash = urlencode(base64_encode("designio projetos desruptivos") . "+$+" . base64_encode($data[$i]['id']) . "+$+" . base64_encode($data[$i]['email']));
    $linkArquivo = false;
    
    $nome       = $data[$i]['nome'];
    $email      = $data[$i]['email'];
    $assunto    = "[DOCUMENTO] - Raphael Imoveis.";

    $msg = "
        <p>
            <img width='132' src='http://www.raphaelimoveis.com.br/img_index/raphael-imoveis.png'>
        </p>
        <p>
            Olá {$nome}, seu documento já está disponível para download no link abaixo.
        </p>
        <p>
            <a href=".($linkArquivo ? $linkArquivo : 'http://www.raphaelimoveis.com.br/usuario/').">clique aqui</a>
        </p>
    ";

    $headers = "From: contato@raphaelimoveis.com.br" . "\r\n";
    $headers .= 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
    $headers .= 'Reply-To: contato@raphaelimoveis.com.br' . "\r\n";
    $headers .= 'X-Mailer: PHP/' . phpversion();

    // mail($email,$assunto,$msg,$headers);
    $mensagem = addslashes(strip_tags(utf8_decode("LOC - ".$msg)));
    $database->db->query("INSERT INTO imobiliar_emails(id_usuario, email, data) VALUES({$data[$i]["id"]}, '$mensagem', NOW())") or die(mysqli_error($database->db));
}

?>