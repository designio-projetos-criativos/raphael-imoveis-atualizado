<?php

ini_set('display_errors',1);
ini_set('display_startup_errors',1);


require "../Database.php";
require "../Sync.php";


$sync = new Integration\Sync("../");
$database = new Integration\Database;


$data = $sync->getData($database->db->query("
    SELECT * 
    FROM imobiliar_novos_arquivos 
    JOIN imobiliar_usuarios 
    ON imobiliar_novos_arquivos.id_usuario = imobiliar_usuarios.id 
    WHERE imobiliar_usuarios.email <>'' 
"));

for($i = 0; $i < sizeof($data); $i++){
    $hash = urlencode(base64_encode("designio projetos desruptivos") . "+$+" . base64_encode($data[$i]['id']) . "+$+" . base64_encode($data[$i]['email']));
    $linkArquivo = false;
    
    if(intval($data[$i]["tipo_de_arquivo"]) == 2 && $data[$i]["tipo"] == "L"){
        $locacao = $sync->getData("SELECT * FROM imobiliar_arquivos_locacao WHERE id_usuario = {$data[$i]['id']}");
        
        $dataArquivo = explode(".", $data['arquivo']);
        $char = explode("", $dataArquivo[1]);
        
        $data = $char[3] . $char[4] . "/20" . $char[1] . $char[2];
        
        
        $linkArquivo = "http://www.raphaelimoveis.com.br/imobiliar/prg/exibeDoc.php?id={$data[$i]['id']}&btn=$data&PROD=L&ASSESSOR={$locacao[0]['nome_assessor']}&ASSESSOR_EMAIL={$locacao[0]['email_assessor']}&TIPO=Propriet%C3%A1rio&CHAVE={$data[$id]['id_arquivo']}&DESC_SERV=".strtoupper($data[$i]['nome'])."&t=".time()."&token=$hash";

    }



    $nome 		= $data[$i]['nome'];
    $email	    = $data[$i]['email'];
    $assunto    = "[DOCUMENTO] - Raphael Imoveis.";

    $msg = "
        <p>
            <img width='132' src='http://www.raphaelimoveis.com.br/img_index/raphael-imoveis.png'>
        </p>
        <p>
            Olá {$nome}, seu documento já está disponível para download no link abaixo.
        </p>
        <p>
            <a href=".($linkArquivo ? $linkArquivo : 'http://www.raphaelimoveis.com.br/usuario/').">clique aqui</a>
        </p>
    ";

    $headers = "From: contato@raphaelimoveis.com.br" . "\r\n";
    $headers .= 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
    $headers .= 'Reply-To: contato@raphaelimoveis.com.br' . "\r\n";
    $headers .= 'X-Mailer: PHP/' . phpversion();

    // mail($email,$assunto,$msg,$headers);
    $database->db->query("INSERT INTO imobiliar_emails(id_usuario, email, data) VALUES({$data[$i]["id"]}, '".strip_tags(utf8_decode($data[$i]["tipo"]." - ".$msg))."', NOW())") or die(mysqli_error($database->db));
}