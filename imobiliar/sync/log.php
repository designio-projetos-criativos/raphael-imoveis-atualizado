<?php

ini_set('display_errors',1);
ini_set('display_startup_errors',1);

if(!isset($_GET['debug'])){
    exit();
}

require "../Database.php";
require "../Sync.php";


$sync = new Integration\Sync("../");
$database = new Integration\Database;

if(isset($_GET['inicial'])){
    if($_GET['inicial'] != ""){
        $inicial = $_GET['inicial'];
    }else{
       $inicial = date("Y-m-")."01"; 
    }
}else{
    $inicial = date("Y-m-")."01";
}

if(isset($_GET['final'])){
    if($_GET['final'] != ""){
        $final = $_GET['final'];
    }else{
       $final = date("Y-m-t"); 
    }
}else{
    $final = date("Y-m-t");
}

$data = $sync->getData($database->db->query("
    SELECT u.nome, u.id, e.email, DATE_FORMAT(e.data, '%d/%m/%Y - %H:%i') AS date
    FROM imobiliar_emails AS e
    INNER JOIN imobiliar_usuarios AS u
    ON e.id_usuario = u.id
    WHERE (DATE(e.data) BETWEEN '$inicial' AND '$final')
    ORDER BY data ASC
"));

?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css" integrity="sha384-aUGj/X2zp5rLCbBxumKTCw2Z50WgIr1vs/PFN4praOTvYXWlVyh2UtNUU0KAUhAX" crossorigin="anonymous">
<div style="width:1000px; padding:0px 20px;">
    <div class="row">
        <form role="form" mathod="get" action="log.php">
            <input type="hidden" name="debug">
          <div class="col-md-3">
              <div class="form-group">
                <input type="date" class="form-control" name="inicial" value="<?=$inicial?>">
              </div>
          </div>
          <div class="col-md-3">
              <div class="form-group">
                <input type="date" class="form-control" name="final" value="<?=$final?>">
              </div>
          </div>
          <div class="col-md-3">
              <div class="form-group">
                <button type="submit" class="btn btn-default">Submit</button>
              </div>
          </div>
          
        </form>
    </div>
    <div class="row">
        <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Data</th>
                <th>Cliente</th>
                <th>Mensagem</th>
            </tr>
        </thead>
        <tbody>
        <?
        for($i = 0; $i < sizeof($data); $i++){
           ?>
            <tr>
                <td><?=$data[$i]['id']?></td>
                <td><?=$data[$i]['date']?></td>
                <td><?=$data[$i]['nome']?></td>
                <td><?=strip_tags($data[$i]['email'])?></td>
            </tr>
           <? 
        }
        ?>
        </tbody>
        </table>
    </div>
</div>