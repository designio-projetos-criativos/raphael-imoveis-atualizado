<?php

namespace Integration;

class Sync{

    private $path = array(
        "data" => "prg/dados/",
        "photos" => "Fotos/"
    );

    private $_global = array(
        "length" => array(
            "cidades" => 46,
            "bairros" => 71
        ),
        "files" => array(
            "cidades" => array("cidades.txt"),
            "bairros" => array("bairros_loc.txt", "bairros_venda.txt")
        ),
        "schema" => array(
            "cidades" => array(
                array("nome", 0, 40),
                array("id", 40, 6), 
                array("uf", 46, 2)
            ),
            "bairros" => array(
                array("nome", 0, 5),
                array("id", 5, 60),
                array("uf", 5, 6)
            )
        )
    );

    public $data = array(
        "cidades" => array(),
        "bairros" => array()
    );

    private function sendError($error){
        header("Content-type: text/html; charset = UTF-8");
        exit("<p style='color: red; background: black'>{$error}</p>");
    }

    public function __construct($path = ""){
        $this->path['data'] = $path.$this->path['data'];
        $this->path['photos'] = $path.$this->path['photos'];

        foreach($this->path as $key => $value){
            if(!file_exists($value)){
                $this->sendError("path '{$value}' not exists in this project");
            }
        }
    }
    private function get($line, $init, $end){
        return trim(substr($line, $init, $end));
    }
    public function cidades(){

        $archive = @fopen("{$this->path['data']}{$this->_global['key']['cidades']}", "r");

        $cities = array();

        if($archive !== false){

            while(!feof($archive)){

                $line = fgets($archive, 1024);

                if (empty($line) || strlen($line) <= $this->_global['length']['cidades']){
                    break;
                }

                $cities[] = array(
                    "nome" => ucwords(strtolower($this->get($line, 0, 40))),
                    "id" => $this->get($line, 40, 6),
                    "uf" => $this->get($line, 46, 2)
                );
            }

            fclose($archive);

            $this->data['cidades'] = $cities;

            return $this->data['cidades'];
        }else{
            $this->sendError("The archive {$this->_global['files']['cidade']} not exists or is not readable");

            return false;
        }
    }

    public function all(){
        $values = array();

        foreach($this->$_global['files'] as $key => $value){

            for($i = 0; $i < count($this->_global['files'][$key]); $i++){

                $archive = @fopen("{$this->path['data']}{$this->_global['files'][$value][$i]}", "r");


                if($archive !== false){

                    while(!feof($archive)){

                        $line = fgets($archive, 1024);

                        if (empty($line) || strlen($line) <= $this->_global['length'][$key]){
                            break;
                        }
                        
                        for($j = 0; $j < count($_global['schema'][$key]); $j++){
                            $values[$i][$j] = $this->get($line, $j[1], $j[2]);
                        }
                    }

                    fclose($archive);

                    $this->data[$value][] = $values;
                    unset($values);
                    break;
                }else{

                    $this->sendError("The archive {$this->_global[$value]['cidade']} not exists or is not readable");
                    return false;
                }
            }
        }
        return $true;
    }
    public function bairros(){
        $bairros = array();


        for($i = 0; $i < count($this->_global['files']['bairros']); $i++){

            $archive = @fopen("{$this->path['data']}{$this->_global['files']['bairros'][$i]}", "r");


            if($archive !== false){

                while(!feof($archive)){

                    $line = fgets($archive, 1024);

                    if (empty($line) || strlen($line) <= $this->_global['length']['bairros']){
                        break;
                    }

                    $bairros[] = array(
                        "id" => $this->get($line, 0, 5),
                        "nome" => ucwords(strtolower($this->get($line, 5, 60))),
                        "id_cidade" => $this->get($line, 65, 6)
                    );
                }

                fclose($archive);

                $this->data['bairros'][] = $bairros;
                break;
            }else{

                $this->sendError("The archive {$this->_global['files']['cidade']} not exists or is not readable");
                return false;
            }
        }
        return $this->data['bairros'];
    }

}
$sync = new Sync();
$sync->all();

var_dump($sync->data);