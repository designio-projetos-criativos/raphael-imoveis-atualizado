<?php
   include("menu.htm");
   include("titulo.htm");
?>
<br>
<center>
<table width="640" border="8" bordercolor="ffffff" cellspacing="0" cellpadding="0" background="fundo.jpg" align="left">
    <tr valign="top">

      <TD><FONT SIZE="3" FACE="Verdana,Arial,Times N"><B>LEI N&deg; 4.591/64, 
        DE 16 DE DEZEMBRO 1964</B><BR>
        </FONT> 
        <HR SIZE="2" COLOR="" ALIGN="LEFT">
        <FONT FACE="Verdana" SIZE="-1"><B><FONT COLOR="Black">Disp&otilde;e 
        sobre o Condom&iacute;nio em Edifica&ccedil;&otilde;es e as Incorpora&ccedil;&otilde;es 
        Imobili&aacute;rias</FONT></B><FONT COLOR="Black"><BR>
        <BR>
        O Presidente da Rep&uacute;blica:<BR>
        <BR>
        Fa&ccedil;o saber que o Congresso Nacional decreta e eu sanciono a seguinte 
        Lei:</FONT></FONT></TD>
    </TR>
    <TR> 
    <TD>&nbsp;</TD>
    </TR>
    <TR> 
      <TD> 
       <CENTER>
          <DIV ALIGN="CENTER"> 
            <HR>
            <CENTER>
              <FONT FACE="Verdana,Arial,Times N" SIZE="-1"><B><U> </U></B><U><FONT SIZE="4">T&iacute;tulo 
              I<BR>
              Do Condom&iacute;nio</FONT></U></FONT> 
              <HR>
            </CENTER>
            <P ALIGN="LEFT"><FONT COLOR="Black" FACE="Verdana,Arial,Times N" SIZE="-1">Art. 
              1&ordm; - As edifica&ccedil;&otilde;es ou conjuntos de edifica&ccedil;&otilde;es, 
              de um ou mais pavimentos, constru&iacute;dos sob a forma de unidades 
              isoladas entre si, destinadas a fins residenciais ou n&atilde;o-residenciais, 
              poder&atilde;o ser alienados, no todo ou em parte, objetivamente 
              considerados, e constituir&aacute;, cada unidade, propriedade aut&ocirc;noma 
              sujeita &agrave;s limita&ccedil;&otilde;es desta Lei.<BR>
              &sect; 1&ordm; - Cada unidade ser&aacute; assinalada por designa&ccedil;&atilde;o 
              especial, nu&eacute;rica ou alfab&eacute;tica, para efeitos de identifica&ccedil;&atilde;o 

              e discrimina&ccedil;&atilde;o.<BR>

              &sect; 2&ordm; - A cada unidade caber&aacute;, como parte insepar&aacute;vel, 

              uma fra&ccedil;&atilde;o ideal do terreno e coisas comuns, expressa 

              sob forma decimal ou ordin&aacute;ria.<BR>

              <BR>

              Art. 2&ordm; - Cada unidade com sa&iacute;da para a via p&uacute;blica, 

              diretamente ou por processo de passagem comum, ser&aacute; sempre 

              tratada como objeto de propriedade exclusiva, qualquer que seja 

              o n&uacute;mero de suas pe&ccedil;as e sua destina&ccedil;&atilde;o, 

              inclusive (Vetado) edif&iacute;cio-garagem, com ressalva das restri&ccedil;&otilde;es 

              que se lhe imponham.<BR>

              &sect; 1&ordm; - O direito &agrave; guarda de ve&iacute;culos nas 

              garagens ou locais a isso destinados nas edifica&ccedil;&otilde;es 

              ou conjuntos de edifica&ccedil;&otilde;es ser&aacute; tratado como 

              objeto de propriedade exclusiva, com ressalva das restri&ccedil;&otilde;es 

              que ao mesmo sejam impostas por instrumentos contratuais adequados, 

              e ser&aacute; vinculada &agrave; unidade habitacional a que corresponder, 

              no caso de n&atilde;o lhe ser atribu&iacute;da fra&ccedil;&atilde;o 

              ideal espec&iacute;fica de terreno.<BR>

              ________<BR>

              Nota:<BR>

              Acrescentado pela Lei n&ordm;4.864/65<BR>

              ________<BR>

              &sect; 2 - O direito de que trata o &sect; 1&ordm; deste artigo 

              poder&aacute; ser transferido a outro cond&ocirc;mino independentemente 

              da aliena&ccedil;&atilde;o da unidade a que corresponder, vedada 

              sua transfer&ecirc;ncia a pessoas estranhas ao condom&iacute;nio.<BR>

              ________<BR>

              Nota:<BR>

              Acrescentado pela Lei n&ordm;4.864/65<BR>

              ________<BR>

              &sect; 3&ordm; - Nos edif&iacute;cios-garagens, &agrave;s vagas 

              ser&atilde;o atribu&iacute;das fra&ccedil;&otilde;es ideais de terreno 

              espec&iacute;ficas.<BR>

              ________<BR>

              Nota:<BR>

              Acrescentado pela Lei n&ordm;4.864/65<BR>

              <BR>

              Art. 3&ordm; - O terreno em que se levantam a edifica&ccedil;&atilde;o 

              ou conjunto de edifica&ccedil;&otilde;es e suas instala&ccedil;&otilde;es, 

              bem como as funda&ccedil;&otilde;es, paredes externas, o teto, as 

              &aacute;reas internas de ventila&ccedil;&atilde;o, e tudo o mais 

              que sirva a qualquer depend&ecirc;ncia de uso comum dos propriet&aacute;rios 

              ou titulares de direito &agrave; aquisi&ccedil;&atilde;o de unidades 

              ou ocupantes, constituir&atilde;o condom&iacute;nio de todos, e 

              ser&atilde;o insuscet&iacute;veis de divis&atilde;o, ou de aliena&ccedil;&atilde;o 

              destacada da respectiva unidade. Ser&atilde;o, tamb&eacute;m, insuscet&iacute;veis 

              de utiliza&ccedil;&atilde;o exclusiva por qualquer cond&ocirc;mino 

              (Vetado).<BR>

              <BR>

              Art. 4&ordm; - A aliena&ccedil;&atilde;o de cada unidade, a transfer&ecirc;ncia 

              de direitos pertinentes &agrave; sua aquisi&ccedil;&atilde;o e a 

              constitui&ccedil;&atilde;o de direitos reais sobre ela independer&atilde;o 

              do consentimento dos cond&ocirc;minos (Vetado).<BR>

              ________<BR>

              Nota:<BR>

              Acrescentado pela Lei n&ordm;7.182/84|Link para Lei 7.182/84|L7.182/84 

              |0<BR>

              ________<BR>

              Par&aacute;grafo &uacute;nico. A aliena&ccedil;&atilde;o ou transfer&ecirc;ncia 

              de direitos de que trata este artigo depender&aacute; de prova de 

              quita&ccedil;&atilde;o das obriga&ccedil;&otilde;es do alienante 

              para com o respectivo condom&iacute;nio.<BR>

              <BR>

              Art. 5&ordm; - O condom&iacute;nio por mea&ccedil;&atilde;o de parede, 

              soalhos e tetos das unidades isoladas regular-se-&aacute; pelo disposto 

              no C&oacute;digo Civil, no que lhe for aplic&aacute;vel.<BR>

              <BR>

              Art. 6&ordm; - Sem preju&iacute;zo do disposto nesta Lei, regular-se-&aacute; 

              pelas disposi&ccedil;&otilde;es de direito comum o condom&iacute;nio 

              por quota ideal de mais de uma pessoa sobre a mesma unidade aut&ocirc;noma.<BR>

              <BR>

              Art. 7&ordm; - O condom&iacute;nio por unidades aut&ocirc;nomas 

              instituir-se-&aacute; por ato entre vivos ou por testamento, com 

              inscri&ccedil;&atilde;o obrigat&oacute;ria, no Registro de Im&oacute;veis, 

              dele constando: a individualiza&ccedil;&atilde;o de cada unidade, 

              sua identifica&ccedil;&atilde;o e discrimina&ccedil;&atilde;o, bem 

              como a fra&ccedil;&atilde;o ideal sobre o terreno e partes comuns, 

              atribu&iacute;da a cada unidade, dispensando-se a descri&ccedil;&atilde;o 

              interna da unidade.<BR>

              <BR>

              Art. 8&ordm; - Quando, em terreno onde n&atilde;o houver edifica&ccedil;&atilde;o, 

              o propriet&aacute;rio, o promitente comprador, o cession&aacute;rio 

              deste ou promitente cession&aacute;rio sobre ele desejar erigir 

              mais de uma edifica&ccedil;&atilde;o, observar-se-&aacute; tamb&eacute;m 

              o seguinte:<BR>

              a) em rela&ccedil;&atilde;o &agrave;s unidades aut&ocirc;nomas que 

              se constitu&iacute;rem em casas t&eacute;rreas ou assobradadas, 

              ser&aacute; discriminada a parte do terreno ocupada pela edifica&ccedil;&atilde;o 

              e tamb&eacute;m aquela eventualmente reservada como de utiliza&ccedil;&atilde;o 

              exclusiva dessas casas, como jardim e quintal, bem assim a fra&ccedil;&atilde;o 

              ideal do todo do terreno e de partes comuns, que corresponder&aacute; 

              &agrave;s unidades;<BR>

              b) em rela&ccedil;&atilde;o &agrave;s unidades aut&ocirc;nomas que 

              constitu&iacute;rem edif&iacute;cios de dois ou mais pavimentos, 

              ser&aacute; discriminada a parte do terreno ocupada pela edifica&ccedil;&atilde;o, 

              aquela que eventualmente for reservada como de utiliza&ccedil;&atilde;o 

              exclusiva, correspondente &agrave;s unidades do edif&iacute;cio, 

              e ainda a fra&ccedil;&atilde;o ideal do todo do terreno e de partes 

              comuns, que corresponder&aacute; a cada uma das unidades;<BR>

              c) ser&atilde;o discriminadas as partes do total do terreno que 

              poder&atilde;o ser utilizadas em comum pelos titulares de direito 

              sobre os v&aacute;rios tipos de unidades aut&ocirc;nomas;<BR>

              d) ser&atilde;o discriminadas as &aacute;reas que se constitu&iacute;rem 

              em passagem comum para as vias p&uacute;blicas ou para as unidades 

              entre si. <BR>

              </FONT></P>

            <CENTER>

              <HR>

              <FONT SIZE="-1" FACE="Verdana,Arial,Times N"><B><FONT SIZE="3">Cap&iacute;tulo 

              II</FONT></B><FONT SIZE="3"> - Da Conven&ccedil;&atilde;o de Condom&iacute;nio</FONT></FONT> 

              <HR>

              <FONT FACE="Verdana,Arial,Times N" SIZE="-1"><BR>

              </FONT> 

            </CENTER>

            <P ALIGN="LEFT"><FONT COLOR="Black" FACE="Verdana,Arial,Times N" SIZE="-1">Art. 

              9&ordm; - Os propriet&aacute;rios, promitentes compradores, cession&aacute;rios 

              ou promitentes cession&aacute;rios dos direitos pertinentes &agrave; 

              aquisi&ccedil;&atilde;o de unidades aut&ocirc;nomas, em edifica&ccedil;&otilde;es 

              a serem constru&iacute;das, em constru&ccedil;&atilde;o ou j&aacute; 

              constru&iacute;das, elaborar&atilde;o, por escrito, a Conven&ccedil;&atilde;o 

              de Condom&iacute;nio, e dever&atilde;o, tamb&eacute;m, por contrato 

              ou por delibera&ccedil;&atilde;o, em assembl&eacute;ia, aprovar 

              o Regimento Interno da edifica&ccedil;&atilde;o ou conjunto de edifica&ccedil;&otilde;es.<BR>

              &sect; 1&ordm; - Far-se-&aacute; o registro da Conven&ccedil;&atilde;o 

              no Registro de Im&oacute;veis bem como a averba&ccedil;&atilde;o 

              das suas eventuais altera&ccedil;&otilde;es.<BR>

              &sect; 2&ordm; - Considera-se aprovada, e obrigat&oacute;ria para 

              os propriet&aacute;rios de unidades, promitentes compradores, cession&aacute;rios 

              e promitentes cession&aacute;rios, atuais e futuros, como para qualquer 

              ocupante, a Conven&ccedil;&atilde;o que re&uacute;na as assinaturas 

              de titulares de direitos que representem, no m&iacute;nimo, dois 

              ter&ccedil;os das fra&ccedil;&otilde;es ideais que comp&otilde;em 

              o condom&iacute;nio.<BR>

              &sect; 3&ordm; - Al&eacute;m de outras normas aprovadas pelos interessados, 

              a Conven&ccedil;&atilde;o dever&aacute; conter:<BR>

              a) a discrimina&ccedil;&atilde;o das partes de propriedade exclusiva, 

              e as de condom&iacute;nio, com especifica&ccedil;&otilde;es das 

              diferentes &aacute;reas;<BR>

              b) o destino das diferentes partes;<BR>

              c) o modo de usar as coisas e servi&ccedil;os comuns;<BR>

              d) encargos, forma e propor&ccedil;&atilde;o das contribui&ccedil;&otilde;es 

              dos cond&ocirc;minos para as despesas de custeio e para as extraordin&aacute;rias;<BR>

              e) o modo de escolher o s&iacute;ndico e o Conselho Consultivo;<BR>

              f) as atribui&ccedil;&otilde;es do s&iacute;ndico, al&eacute;m das 

              legais;<BR>

              g) a defini&ccedil;&atilde;o da natureza gratuita ou remunerada 

              de suas fun&ccedil;&otilde;es;<BR>

              h) o modo e o prazo de convoca&ccedil;&atilde;o das assembl&eacute;ias 

              gerais dos cond&ocirc;minos;<BR>

              i) o quorum para os diversos tipos de vota&ccedil;&otilde;es;<BR>

              j) a forma de contribui&ccedil;&atilde;o para constitui&ccedil;&atilde;o 

              de fundo de reserva;<BR>

              l) a forma e o quorum para as altera&ccedil;&otilde;es de conven&ccedil;&atilde;o;<BR>

              m) a forma e o quorum para a aprova&ccedil;&atilde;o do Regimento 

              Interno quando n&atilde;o inclu&iacute;dos na pr&oacute;pria Conven&ccedil;&atilde;o.<BR>

              &sect; 4&ordm; - No caso de conjunto de edifica&ccedil;&otilde;es, 

              a que se refere o art. 8, a Conven&ccedil;&atilde;o de Condom&iacute;nio 

              fixar&aacute; os direitos e as rela&ccedil;&otilde;es de propriedade 

              entre os cond&ocirc;minos das v&aacute;rias edifica&ccedil;&otilde;es, 

              podendo estipular formas pelas quais se possam desmembrar e alienar 

              por&ccedil;&otilde;es do terreno, inclusive as edificadas.<BR>

              ________<BR>

              Nota:<BR>

              Acrescentado pela Lei n&ordm;4.864/65<BR>

              ________<BR>

              <BR>

              Art. 10 - &Eacute; defeso a qualquer cond&ocirc;mino:<BR>

              I - alterar a forma externa da fachada;<BR>

              II - decorar as partes e esquadrias externas com tonalidades ou 

              cores diversas das empregadas no conjunto da edifica&ccedil;&atilde;o;<BR>

              III - destinar a unidade a utiliza&ccedil;&atilde;o diversa de finalidade 

              do pr&eacute;dio, ou us&aacute;-la de forma nociva ou perigosa ao 

              sossego, &agrave; salubridade e &agrave; seguran&ccedil;a dos demais 

              cond&ocirc;minos;<BR>

              IV - embara&ccedil;ar o uso das partes comuns.<BR>

              &sect; 1&ordm; - O transgressor ficar&aacute; sujeito ao pagamento 

              de multa prevista na Conven&ccedil;&atilde;o ou no Regulamento do 

              Condom&iacute;nio, al&eacute;m de ser compelido a desfazer a obra 

              ou abster-se da pr&aacute;tica do ato, cabendo ao s&iacute;ndico, 

              com autoriza&ccedil;&atilde;o judicial, mandar desmanch&aacute;-la, 

              &agrave; custa do transgressor, se este n&atilde;o a desfizer no 

              prazo que lhe for estipulado.<BR>

              &sect; 2&ordm; - O propriet&aacute;rio ou titular de direito &agrave; 

              aquisi&ccedil;&atilde;o de unidade poder&aacute; fazer obra que 

              (Vetado) ou modifique sua fachada, se obtiver a aquiesc&ecirc;ncia 

              da unanimidade dos cond&ocirc;minos.<BR>

              <BR>

              Art. 11 - Para efeitos tribut&aacute;rios, cada unidade aut&ocirc;noma 

              ser&aacute; tratada como pr&eacute;dio isolado, contribuindo o respectivo 

              cond&ocirc;mino, diretamente, com as import&acirc;ncias relativas 

              aos impostos e taxas federais, estaduais e municipais, na forma 

              dos respectivos lan&ccedil;amentos. <BR>

              </FONT></P>

            <CENTER>

              <HR>

              <FONT SIZE="-1" FACE="Verdana,Arial,Times N"><B><FONT SIZE="3">Cap&iacute;tulo 

              III</FONT></B><FONT SIZE="3"> - Das Despesas do Condom&iacute;nio</FONT></FONT> 

              <HR>

              <FONT FACE="Verdana,Arial,Times N" SIZE="-1"><BR>

              </FONT> 

            </CENTER>

            <P ALIGN="LEFT"><FONT COLOR="Black" FACE="Verdana,Arial,Times N" SIZE="-1">Art. 

              12 - Cada cond&ocirc;mino concorrer&aacute; nas despesas do condom&iacute;nio, 

              recolhendo, nos prazos previstos na Conven&ccedil;&atilde;o, a quota-parte 

              que lhe couber em rateio.<BR>

              &sect; 1&ordm; - Salvo disposi&ccedil;&atilde;o em contr&aacute;rio 

              na Conven&ccedil;&atilde;o, a fixa&ccedil;&atilde;o da quota do 

              rateio corresponder&aacute; &agrave; fra&ccedil;&atilde;o ideal 

              do terreno de cada unidade.<BR>

              &sect; 2&ordm; - Cabe ao s&iacute;ndico arrecadar as contribui&ccedil;&otilde;es, 

              competindo-lhe promover, por via executiva, a cobran&ccedil;a judicial 

              das quotas atrasadas.<BR>

              &sect; 3&ordm; - O cond&ocirc;mino que n&atilde;o pagar a sua contribui&ccedil;&atilde;o 

              no prazo fixado na Conven&ccedil;&atilde;o fica sujeito ao juro 

              morat&oacute;rio de 1% (um por cento) ao m&ecirc;s, e multa de at&eacute; 

              20% (vinte por cento) sobre o d&eacute;bito, que ser&aacute; atualizado, 

              se o estipular a Conven&ccedil;&atilde;o, com a aplica&ccedil;&atilde;o 

              dos &iacute;ndices de corre&ccedil;&atilde;o monet&aacute;ria levantados 

              pelo Conselho Nacional de Economia, no caso da mora por per&iacute;odo 

              igual ou superior a 6 (seis) meses.<BR>

              &sect; 4&ordm; - As obras que interessarem &agrave; estrutura integral 

              da edifica&ccedil;&atilde;o ou conjunto de edifica&ccedil;&otilde;es, 

              ou ao servi&ccedil;o comum, ser&atilde;o feitas com o concurso pecuni&aacute;rio 

              de todos os propriet&aacute;rios ou titulares de direito &agrave; 

              aquisi&ccedil;&atilde;o de unidades, mediante or&ccedil;amento pr&eacute;vio 

              aprovado em assembl&eacute;ia geral, podendo incumbir-se de sua 

              execu&ccedil;&atilde;o o s&iacute;ndico, ou outra pessoa, com aprova&ccedil;&atilde;o 

              da assembl&eacute;ia.<BR>

              &sect; 5&ordm; - A ren&uacute;ncia de qualquer cond&ocirc;mino aos 

              seus direitos, em caso algum valer&aacute; como escusa para exoner&aacute;-lo 

              de seus encargos. <BR>

              </FONT></P>

            <CENTER>

              <HR>

              <FONT SIZE="-1" FACE="Verdana,Arial,Times N"><B><FONT SIZE="3">Cap&iacute;tulo 

              IV</FONT></B><FONT SIZE="3"> - Do Seguro, do Inc&ecirc;ndio, da 

              Demoli&ccedil;&atilde;o e da Reconstru&ccedil;&atilde;o Obrigat&oacute;ria</FONT></FONT> 

              <HR>

              <FONT FACE="Verdana,Arial,Times N" SIZE="-1"><BR>

              </FONT> 

            </CENTER>

            <P ALIGN="LEFT"><FONT COLOR="Black" FACE="Verdana,Arial,Times N" SIZE="-1">Art. 

              13 - Proceder-se-&aacute; ao seguro da edifica&ccedil;&atilde;o 

              ou do conjunto de edifica&ccedil;&otilde;es, neste caso, discriminadamente, 

              abrangendo todas as unidades aut&ocirc;nomas e partes comuns, contra 

              inc&ecirc;ndio ou outro sinistro que cause destrui&ccedil;&atilde;o 

              no todo ou em parte, computando-se o pr&ecirc;mio nas despesas ordin&aacute;rias 

              do condom&iacute;nio.<BR>

              Par&aacute;grafo &uacute;nico. O seguro de que trata este artigo 

              ser&aacute; obrigatoriamente feito dentro de 120 (cento e vinte) 

              dias, contados da data da concess&atilde;o do &quot;habite-se&quot;, 

              sob Pena de ficar o condom&iacute;nio sujeito &agrave; multa mensal 

              equivalente a um doze avos do imposto predial, cobr&aacute;vel executivamente 

              pela Municipalidade.<BR>

              <BR>

              Art. 14 - Na ocorr&ecirc;ncia de sinistro total, ou que destrua 

              mais de dois ter&ccedil;os de uma edifica&ccedil;&atilde;o, seus 

              cond&ocirc;minos reunir-se-&atilde;o em assembl&eacute;ia especial, 

              e deliberar&atilde;o sobre a sua reconstru&ccedil;&atilde;o ou venda 

              do terreno e materiais, por quorum m&iacute;nimo de votos que representem 

              metade mais uma das fra&ccedil;&otilde;es ideais do respectivo terreno.<BR>

              &sect; 1&ordm; - Rejeitada a proposta de reconstru&ccedil;&atilde;o, 

              a mesma assembl&eacute;ia, ou outra para este fim convocada, decidir&aacute;, 

              pelo mesmo quorum, do destino a ser dado ao terreno, e aprovar&aacute; 

              a partilha do valor do seguro entre os cond&ocirc;minos, sem preju&iacute;zo 

              do que receber cada um pelo seguro facultativo de sua unidade.<BR>

              &sect; 2&ordm; - Aprovada, a reconstru&ccedil;&atilde;o ser&aacute; 

              feita, guardados, obrigatoriamente, o mesmo destino, a mesma forma 

              externa e a mesma disposi&ccedil;&atilde;o interna.<BR>

              &sect; 3&ordm; - Na hip&oacute;tese do par&aacute;grafo anterior, 

              a minoria n&atilde;o poder&aacute; ser obrigada a contribuir para 

              a reedifica&ccedil;&atilde;o, caso em que a maioria poder&aacute; 

              adquirir as partes dos dissidentes, mediante avalia&ccedil;&atilde;o 

              judicial, feita em vistoria.<BR>

              <BR>

              Art. 15 - Na hip&oacute;tese de que trata o &sect; 3&ordm; do artigo 

              antecedente, &agrave; maioria poder&atilde;o ser adjudicadas, por 

              senten&ccedil;a, as fra&ccedil;&otilde;es ideais da minoria.<BR>

              &sect; 1&ordm; - Como condi&ccedil;&atilde;o para o exerc&iacute;cio 

              da a&ccedil;&atilde;o prevista neste artigo, com a inicial, a maioria 

              oferecer&aacute; e depositar&aacute;, &agrave; disposi&ccedil;&atilde;o 

              do ju&iacute;zo, as import&acirc;ncias arbitradas na vistoria para 

              avalia&ccedil;&atilde;o, prevalecendo as de eventual desempatador.<BR>

              &sect; 2&ordm; - Feito o dep&oacute;sito de que trata o par&aacute;grafo 

              anterior, o juiz, liminarmente, poder&aacute; autorizar a adjudica&ccedil;&atilde;o 

              &agrave; maioria, e a minoria poder&aacute; levantar as import&acirc;ncias 

              depositadas; o oficial de registro de im&oacute;veis, nestes casos, 

              far&aacute; constar do registro que a adjudica&ccedil;&atilde;o 

              foi resultante de medida liminar.<BR>

              &sect; 3&ordm; - Feito o dep&oacute;sito, ser&aacute; expedido o 

              mandado de cita&ccedil;&atilde;o, com o prazo de 10 (dez) dias para 

              a contesta&ccedil;&atilde;o (Vetado).<BR>

              &sect; 4&ordm; - Se n&atilde;o contestado, o juiz, imediatamente, 

              julgar&aacute; o pedido.<BR>

              &sect; 5&ordm; - Se contestado o pedido, seguir&aacute; o processo 

              o rito ordin&aacute;rio.<BR>

              &sect; 6&ordm; - Se a senten&ccedil;a fixar valor superior ao da 

              avalia&ccedil;&atilde;o feita na vistoria, o condom&iacute;nio, 

              em execu&ccedil;&atilde;o, restituir&aacute; &agrave; minoria a 

              respectiva diferen&ccedil;a, acrescida de juros de mora &agrave; 

              raz&atilde;o de 1% (um por cento) ao m&ecirc;s, desde a data da 

              concess&atilde;o de eventual liminar, ou pagar&aacute; o total devido, 

              com os juros da mora a contar da cita&ccedil;&atilde;o.<BR>

              &sect; 7&ordm; - Transitada em julgado a senten&ccedil;a, servir&aacute; 

              ela de t&iacute;tulo definitivo para a maioria, que dever&aacute; 

              registr&aacute;-la no Registro de Im&oacute;veis.<BR>

              &sect; 8&ordm; - A maioria poder&aacute; pagar e cobrar da minoria, 

              em execu&ccedil;&atilde;o de senten&ccedil;a, encargos fiscais necess&aacute;rios 

              &agrave; adjudica&ccedil;&atilde;o definitiva a cujo pagamento se 

              recusar a minoria.<BR>

              <BR>

              Art. 16 - Em caso de sinistro que destrua menos de dois ter&ccedil;os 

              da edifica&ccedil;&atilde;o, o s&iacute;ndico <BR>

              promover&aacute; o recebimento do seguro e a reconstru&ccedil;&atilde;o 

              ou os reparos nas partes danificadas.<BR>

              <BR>

              Art. 17 - Os cond&ocirc;minos que representem, pelo menos, dois 

              ter&ccedil;os do total de unidades isoladas e fra&ccedil;&otilde;es 

              ideais correspondentes a 80% (oitenta por cento) do terreno e coisas 

              comuns poder&atilde;o decidir sobre a demoli&ccedil;&atilde;o e 

              reconstru&ccedil;&atilde;o do pr&eacute;dio, ou sua aliena&ccedil;&atilde;o, 

              por motivos urban&iacute;sticos ou arquitet&ocirc;nicos, ou, ainda, 

              no caso de condena&ccedil;&atilde;o do edif&iacute;cio pela autoridade 

              p&uacute;blica, em raz&atilde;o de sua inseguran&ccedil;a ou insalubridade.<BR>

              ________<BR>

              Nota:<BR>

              Reda&ccedil;&atilde;o determinada pela Lei n&ordm;6.709/79<BR>

              ______<BR>

              &sect; 1&ordm; - A maioria n&atilde;o fica obrigada a contribuir 

              para as obras, mas assegura-se &agrave; maioria o direito de adquirir 

              as partes dos dissidentes, mediante avalia&ccedil;&atilde;o judicial, 

              aplicando-se o processo previsto no art. 15.<BR>

              ________<BR>

              Nota:<BR>

              Reda&ccedil;&atilde;o determinada pela Lei n&ordm;6.709/79<BR>

              ______<BR>

              &sect; 2&ordm; - Ocorrendo desgaste, pela a&ccedil;&atilde;o do 

              tempo, das unidades habitacionais de uma edifica&ccedil;&atilde;o, 

              que deprecie seu valor unit&aacute;rio em rela&ccedil;&atilde;o 

              ao valor global do terreno onde se acha constru&iacute;da, os cond&ocirc;minos, 

              pelo quorum m&iacute;nimo de votos que representem dois ter&ccedil;os 

              das unidades isoladas e fra&ccedil;&otilde;es ideais correspondentes 

              a 80% (oitenta por cento) do terreno e coisas comuns, poder&atilde;o 

              decidir por sua aliena&ccedil;&atilde;o total, procedendo-se em 

              rela&ccedil;&atilde;o &agrave; minoria na forma estabelecida no 

              art. 15, e seus par&aacute;grafos, desta Lei.<BR>

              ________<BR>

              Nota:<BR>

              Reda&ccedil;&atilde;o determinada pela Lei n&ordm;6.709/79<BR>

              ______<BR>

              &sect; 3&ordm; - Decidida por maioria a aliena&ccedil;&atilde;o 

              do pr&eacute;dio, o valor atribu&iacute;do &agrave; quota dos cond&ocirc;minos 

              vencidos ser&aacute; correspondente ao pre&ccedil;o efetivo e, no 

              m&iacute;nimo, &agrave; avalia&ccedil;&atilde;o prevista no &sect; 

              2&ordm; ou, a crit&eacute;rio desses, a im&oacute;vel localizado 

              em &aacute;rea pr&oacute;xima ou adjacente com a mesma &aacute;rea 

              &uacute;til de constru&ccedil;&atilde;o.<BR>

              ________<BR>

              Nota:<BR>

              Reda&ccedil;&atilde;o determinada pela Lei n&ordm;6.709/79<BR>

              ______<BR>

              <BR>

              Art. 18 - A aquisi&ccedil;&atilde;o parcial de uma edifica&ccedil;&atilde;o, 

              ou de um conjunto de edifica&ccedil;&otilde;es, ainda que por for&ccedil;a 

              de desapropria&ccedil;&atilde;o, importar&aacute; no ingresso do 

              adquirente no condom&iacute;nio, ficando sujeito &agrave;s disposi&ccedil;&otilde;es 

              desta Lei, bem assim &agrave;s da Conven&ccedil;&atilde;o do Condom&iacute;nio 

              e do Regulamento Interno.<BR>

              _________<BR>

              Nota:<BR>

              Reda&ccedil;&atilde;o determinada pelo Decreto-lei n&ordm;981/69. 

              Anteriormente havia sido alterado pelo art.8&ordm;, da Lei n&ordm;4.864/65</FONT></P>

            <CENTER>

              <HR>

              <FONT SIZE="-1" FACE="Verdana,Arial,Times N"><B><FONT SIZE="3">Cap&iacute;tulo 

              V</FONT></B><FONT SIZE="3"> - Utiliza&ccedil;&atilde;o da Edifica&ccedil;&atilde;o 

              ou do Conjunto de Edifica&ccedil;&otilde;es</FONT></FONT> 

              <HR>

              <FONT FACE="Verdana,Arial,Times N" SIZE="-1"><BR>

              </FONT> 

            </CENTER>

            <P ALIGN="LEFT"><FONT COLOR="Black" FACE="Verdana,Arial,Times N" SIZE="-1">Art. 

              19 - Cada cond&ocirc;mino tem o direito de usar e fruir, com exclusividade, 

              de sua unidade aut&ocirc;noma, segundo suas conveni&ecirc;ncias 

              e interesses, condicionados, umas e outros, &agrave;s normas de 

              boa vizinhan&ccedil;a, e poder&aacute; usar as partes e coisas comuns, 

              de maneira a n&atilde;o causar dano ou inc&ocirc;modo aos demais 

              cond&ocirc;minos ou moradores, nem obst&aacute;culo ou embara&ccedil;o 

              ao bom uso das mesmas partes por todos.<BR>

              Par&aacute;grafo &uacute;nico. (Vetado).<BR>

              <BR>

              Art. 20 - Aplicam-se ao ocupante do im&oacute;vel, a qualquer t&iacute;tulo, 

              todas as obriga&ccedil;&otilde;es referentes ao <BR>

              uso, frui&ccedil;&atilde;o e destino da unidade.<BR>

              <BR>

              Art. 21 - A viola&ccedil;&atilde;o de qualquer dos deveres estipulados 

              na Conven&ccedil;&atilde;o sujeitar&aacute; o infrator &agrave; 

              multa fixada na pr&oacute;pria Conven&ccedil;&atilde;o ou no Regimento 

              Interno, sem preju&iacute;zo da responsabilidade civil ou criminal 

              que, no caso, couber.<BR>

              Par&aacute;grafo &uacute;nico. Compete ao s&iacute;ndico a iniciativa 

              do processo e a cobran&ccedil;a da multa por via executiva, em benef&iacute;cio 

              do condom&iacute;nio, e, em caso de omitir-se ele, a qualquer cond&ocirc;mino.<BR>

              </FONT></P>

            <CENTER>

              <HR>

              <FONT SIZE="-1" FACE="Verdana,Arial,Times N"><B><FONT SIZE="3">Cap&iacute;tulo 

              VI</FONT></B><FONT SIZE="3"> - Da Administra&ccedil;&atilde;o do 

              Condom&iacute;nio</FONT></FONT> 

              <HR>

              <FONT FACE="Verdana,Arial,Times N" SIZE="-1"><BR>

              </FONT> 

            </CENTER>

            <P ALIGN="LEFT"><FONT COLOR="Black" FACE="Verdana,Arial,Times N" SIZE="-1">Art. 

              22 - Ser&aacute; eleito, na forma prevista pela Conven&ccedil;&atilde;o, 

              um s&iacute;ndico do condom&iacute;nio, cujo mandato n&atilde;o 

              poder&aacute; exceder a 2 (dois) anos, permitida a reelei&ccedil;&atilde;o.<BR>

              &sect; 1&ordm; - Compete ao s&iacute;ndico:<BR>

              a) representar, ativa e passivamente, o condom&iacute;nio, em ju&iacute;zo 

              ou fora dele, e praticar os atos de defesa dos interesses comuns, 

              nos limites das atribui&ccedil;&otilde;es conferidas por esta Lei 

              ou pela Conven&ccedil;&atilde;o;<BR>

              b) exercer a administra&ccedil;&atilde;o interna da edifica&ccedil;&atilde;o 

              ou do conjunto de edifica&ccedil;&otilde;es, no que respeita &agrave; 

              sua vigil&acirc;ncia, moralidade e seguran&ccedil;a, bem como aos 

              servi&ccedil;os que interessam a todos os moradores;<BR>

              c) praticar os atos que lhe atribu&iacute;rem as leis, a Conven&ccedil;&atilde;o 

              e o Regimento Interno;<BR>

              d) impor as multas estabelecidas na Lei, na Conven&ccedil;&atilde;o 

              ou no Regimento Interno;<BR>

              e) cumprir e fazer cumprir a Conven&ccedil;&atilde;o e o Regimento 

              Interno, bem como executar e fazer executar as delibera&ccedil;&otilde;es 

              da assembl&eacute;ia;<BR>

              f) prestar contas &agrave; assembl&eacute;ia dos cond&ocirc;minos;<BR>

              g) manter guardada durante o prazo de 5 (cinco) anos, para eventuais 

              necessidades de verifica&ccedil;&atilde;o cont&aacute;bil, toda 

              a documenta&ccedil;&atilde;o relativa ao condom&iacute;nio.<BR>

              ________<BR>

              Nota:<BR>

              Acrescentada pela Lei n&ordm;6.434/77<BR>

              ________<BR>

              &sect; 2&ordm; - As fun&ccedil;&otilde;es administrativas podem 

              ser delegadas a pessoas de confian&ccedil;a do s&iacute;ndico, e 

              sob a sua inteira responsabilidade, mediante aprova&ccedil;&atilde;o 

              da assembl&eacute;ia geral dos cond&ocirc;minos.<BR>

              &sect; 3&ordm; - A Conven&ccedil;&atilde;o poder&aacute; estipular 

              que dos atos do s&iacute;ndico caiba recurso para a assembl&eacute;ia, 

              convocada pelo interessado.<BR>

              &sect; 4&ordm; - Ao s&iacute;ndico, que poder&aacute; ser cond&ocirc;mino 

              ou pessoa f&iacute;sica ou jur&iacute;dica estranha ao condom&iacute;nio, 

              ser&aacute; fixada a remunera&ccedil;&atilde;o pela mesma assembl&eacute;ia 

              que o eleger, salvo se a Conven&ccedil;&atilde;o dispuser diferentemente.<BR>

              &sect; 5&ordm; - O s&iacute;ndico poder&aacute; ser destitu&iacute;do, 

              pela forma e sob as condi&ccedil;&otilde;es previstas na Conven&ccedil;&atilde;o, 

              ou, no sil&ecirc;ncio desta, pelo voto de dois ter&ccedil;os dos 

              cond&ocirc;minos, presentes, em assembl&eacute;ia geral especialmente 

              convocada.<BR>

              &sect; 6&ordm; - A Conven&ccedil;&atilde;o poder&aacute; prever 

              a elei&ccedil;&atilde;o de subs&iacute;ndicos, definindo- lhes atribui&ccedil;&otilde;es 

              e fixando-lhes o mandato, que n&atilde;o poder&aacute; exceder de 

              2 (dois) anos, permitida a reelei&ccedil;&atilde;o.<BR>

              <BR>

              Art. 23 - Ser&aacute; eleito, na forma prevista na Conven&ccedil;&atilde;o, 

              um Conselho Consultivo, constitu&iacute;do de tr&ecirc;s cond&ocirc;minos, 

              com mandatos que n&atilde;o poder&atilde;o exceder de 2 (dois) anos, 

              permitida a reelei&ccedil;&atilde;o.<BR>

              Par&aacute;grafo &uacute;nico. Funcionar&aacute; o Conselho como 

              &oacute;rg&atilde;o consultivo do s&iacute;ndico, para assessor&aacute;-lo 

              na solu&ccedil;&atilde;o dos problemas que digam respeito ao condom&iacute;nio, 

              podendo a Conven&ccedil;&atilde;o definir suas atribui&ccedil;&otilde;es 

              espec&iacute;ficas.<BR>

              </FONT></P>

            <CENTER>

              <HR>

              <FONT SIZE="-1" FACE="Verdana,Arial,Times N"><B><FONT SIZE="3">Cap&iacute;tulo 

              VII</FONT></B><FONT SIZE="3"> - Da Assembl&eacute;ia Geral</FONT></FONT> 

              <HR>

              <FONT FACE="Verdana,Arial,Times N" SIZE="-1"><BR>

              </FONT> 

            </CENTER>

            <P ALIGN="LEFT"><FONT COLOR="Black" FACE="Verdana,Arial,Times N" SIZE="-1">Art. 

              24 - Haver&aacute;, anualmente, uma assembl&eacute;ia geral ordin&aacute;ria 

              dos cond&ocirc;minos, convocada pelo s&iacute;ndico na forma prevista 

              na Conven&ccedil;&atilde;o, &agrave; qual compete, al&eacute;m das 

              demais mat&eacute;rias inscritas na ordem do dia, aprovar, por maioria 

              dos presentes, as verbas para as despesas de condom&iacute;nio, 

              compreendendo as de conserva&ccedil;&atilde;o da edifica&ccedil;&atilde;o 

              ou conjunto de edifica&ccedil;&otilde;es, manuten&ccedil;&atilde;o 

              de seus servi&ccedil;os e correlatas.<BR>

              &sect; 1&ordm; - As decis&otilde;es da assembl&eacute;ia, tomadas, 

              em cada caso, pelo quorum que a Conven&ccedil;&atilde;o fixar, obrigam 

              todos os cond&ocirc;minos.<BR>

              &sect; 2&ordm; - O s&iacute;ndico, nos 8 (oito) dias subseq&uuml;entes 

              &agrave; assembl&eacute;ia, comunicar&aacute; aos cond&ocirc;minos 

              o que tiver sido deliberado, inclusive no tocante &agrave; previs&atilde;o 

              or&ccedil;ament&aacute;ria, o rateio das despesas, e promover&aacute; 

              a arrecada&ccedil;&atilde;o, tudo na forma que a Conven&ccedil;&atilde;o 

              previr.<BR>

              &sect; 3&ordm; - Nas assembl&eacute;ias gerais, os votos ser&atilde;o 

              proporcionais &agrave;s fra&ccedil;&otilde;es ideais do terreno 

              e partes comuns, pertencentes a cada cond&ocirc;mino, salvo disposi&ccedil;&atilde;o 

              diversa da Conven&ccedil;&atilde;o.<BR>

              &sect; 4&ordm; - Nas decis&otilde;es da assembl&eacute;ia que envolvam 

              despesas ordin&aacute;rias do condom&iacute;nio o locat&aacute;rio 

              poder&aacute; votar, caso o cond&ocirc;mino-locador a ela n&atilde;o 

              compare&ccedil;a.<BR>

              ________<BR>

              Nota:<BR>

              Reda&ccedil;&atilde;o determinada pela Lei n&ordm;9.267/96<BR>

              ________<BR>

              <BR>

              Art. 25 - Ressalvado o disposto no &sect; 3&ordm; do Art. 22, poder&aacute; 

              haver assembl&eacute;ias gerais extraordin&aacute;rias, convocadas 

              pelo s&iacute;ndico ou por cond&ocirc;minos que representem um quarto, 

              no m&iacute;nimo, do condom&iacute;nio, sempre que o exigirem os 

              interesses gerais.<BR>

              Par&aacute;grafo &uacute;nico. Salvo estipula&ccedil;&atilde;o diversa 

              da Conven&ccedil;&atilde;o, esta s&oacute; poder&aacute; ser modificada 

              em assembl&eacute;ia geral extraordin&aacute;ria, pelo voto m&iacute;nimo 

              de cond&ocirc;minos que representem dois ter&ccedil;os do total 

              das fra&ccedil;&otilde;es ideais.<BR>

              <BR>

              Art. 26 - (Vetado).<BR>

              <BR>

              Art. 27 - Se a assembl&eacute;ia n&atilde;o se reunir para exercer 

              qualquer dos poderes que lhe competem, 15 (quinze) dias ap&oacute;s 

              o pedido de convoca&ccedil;&atilde;o, o juiz decidir&aacute; a respeito, 

              mediante requerimento dos interessados. <BR>

              </FONT></P>

            <HR>

            <CENTER>

              <FONT FACE="Verdana,Arial,Times N" SIZE="-1"><B><U> </U></B><U><FONT SIZE="4">T&iacute;tulo 

              II<BR>

              Das Incorpora&ccedil;&otilde;es </FONT></U><B><U> </U></B></FONT> 

              <HR>

              <FONT SIZE="-1" FACE="Verdana,Arial,Times N"><B><FONT SIZE="3">Cap&iacute;tulo 

              I</FONT></B><FONT SIZE="3"> - Disposi&ccedil;&otilde;es Gerais</FONT></FONT> 

              <HR>

              <FONT FACE="Verdana,Arial,Times N" SIZE="-1"><BR>

              </FONT> 

            </CENTER>

            <P ALIGN="LEFT"><FONT COLOR="Black" FACE="Verdana,Arial,Times N" SIZE="-1">Art. 

              28 - As incorpora&ccedil;&otilde;es imobili&aacute;rias, em todo 

              o territ&oacute;rio nacional, reger-se-&atilde;o pela presente Lei.<BR>

              Par&aacute;grafo &uacute;nico. Para efeito desta Lei, considera-se 

              incorpora&ccedil;&atilde;o imobili&aacute;ria a atividade exercida 

              com o intuito de promover e realizar a constru&ccedil;&atilde;o, 

              para aliena&ccedil;&atilde;o total ou parcial, de edifica&ccedil;&otilde;es, 

              ou conjunto de edifica&ccedil;&otilde;es compostas de unidades aut&ocirc;nomas 

              (Vetado).<BR>

              <BR>

              Art. 29 - Considera-se incorporador a pessoa f&iacute;sica ou jur&iacute;dica, 

              comerciante ou n&atilde;o, que, embora n&atilde;o efetuando a constru&ccedil;&atilde;o, 

              compromisse ou efetive a venda de fra&ccedil;&otilde;es ideais de 

              terreno objetivando a vincula&ccedil;&atilde;o de tais fra&ccedil;&otilde;es 

              a unidades aut&ocirc;nomas (Vetado), em edifica&ccedil;&otilde;es 

              a serem constru&iacute;das ou em constru&ccedil;&atilde;o sob regime 

              condominial, ou que meramente aceita propostas para efetiva&ccedil;&atilde;o 

              de tais transa&ccedil;&otilde;es, coordenando e levando a termo 

              a incorpora&ccedil;&atilde;o e responsabilizando-se, conforme o 

              caso, pela entrega, a certo prazo, pre&ccedil;o e determinadas condi&ccedil;&otilde;es, 

              das obras conclu&iacute;das.<BR>

              Par&aacute;grafo &uacute;nico. Presume-se a vincula&ccedil;&atilde;o 

              entre a aliena&ccedil;&atilde;o das fra&ccedil;&otilde;es do terreno 

              e o neg&oacute;cio de constru&ccedil;&atilde;o, se, ao ser contratada 

              a venda, ou promessa de venda ou de sess&atilde;o das fra&ccedil;&otilde;es 

              de terreno, j&aacute; houver sido aprovado e estiver em vigor, ou 

              pender de aprova&ccedil;&atilde;o de autoridade administrativa, 

              o respectivo projeto de constru&ccedil;&atilde;o, respondendo o 

              alienante como incorporador.<BR>

              <BR>

              Art. 30 - Estende-se a condi&ccedil;&atilde;o de incorporador aos 

              propriet&aacute;rios e titulares de direitos aquisitivos que contratem 

              a constru&ccedil;&atilde;o de edif&iacute;cios que se destinem a 

              constitui&ccedil;&atilde;o em condom&iacute;nio, sempre que iniciarem 

              as aliena&ccedil;&otilde;es antes da conclus&atilde;o das obras.<BR>

              <BR>

              Art. 31 - A iniciativa e a responsabilidade das incorpora&ccedil;&otilde;es 

              imobili&aacute;rias caber&atilde;o ao incorporador, que somente 

              poder&aacute; ser:<BR>

              a) o propriet&aacute;rio do terreno, o promitente comprador, o cession&aacute;rio 

              deste ou promitente cession&aacute;rio com t&iacute;tulo que satisfa&ccedil;a 

              os requisitos da al&iacute;nea a do Art. 32;<BR>

              b) o construtor (Decretos n&uacute;meros 23.569, de 11 de dezembro 

              de 1933, e 3.995, de 31 de dezembro de 1941, e Decreto-lei n&uacute;mero 

              8.620, de 10 de janeiro de 1946) ou corretor de im&oacute;veis (Lei 

              n&uacute;mero 6.530, de 12 de maio de 1978).<BR>

              &sect; 1&ordm; - No caso da al&iacute;nea b, o incorporador ser&aacute; 

              investido, pelo propriet&aacute;rio do terreno, o promitente comprador 

              e cession&aacute;rio deste ou o promitente cession&aacute;rio, de 

              mandato outorgado por instrumento p&uacute;blico, onde se fa&ccedil;a 

              men&ccedil;&atilde;o expressa desta Lei e se transcreva o disposto 

              no &sect; 4&ordm; do Art. 35, para concluir todos os neg&oacute;cios 

              tendentes &agrave; aliena&ccedil;&atilde;o das fra&ccedil;&otilde;es 

              ideais de terreno, mas se obrigar&aacute; pessoalmente pelos atos 

              que praticar na qualidade de incorporador.<BR>

              &sect; 2&ordm; - Nenhuma incorpora&ccedil;&atilde;o poder&aacute; 

              ser proposta &agrave; venda sem a indica&ccedil;&atilde;o expressa 

              do incorporador, devendo tamb&eacute;m seu nome permanecer indicado 

              ostensivamente no local da constru&ccedil;&atilde;o.<BR>

              &sect; 3&ordm; - Toda e qualquer incorpora&ccedil;&atilde;o, independentemente 

              da forma por que seja constitu&iacute;da, ter&aacute; um ou mais 

              incorporadores solidariamente respons&aacute;veis, ainda que em 

              fase subordinada a per&iacute;odo de car&ecirc;ncia, referido no 

              Art. 34.<BR>

              </FONT></P>

            <CENTER>

              <HR>

              <FONT SIZE="-1" FACE="Verdana,Arial,Times N"><B><FONT SIZE="3">Cap&iacute;tulo 

              II</FONT></B><FONT SIZE="3"> - Das Obriga&ccedil;&otilde;es e Direitos 

              do Incorporador</FONT></FONT> 

              <HR>

              <FONT FACE="Verdana,Arial,Times N" SIZE="-1"><BR>

              </FONT> 

            </CENTER>

            <P ALIGN="LEFT"><FONT COLOR="Black" FACE="Verdana,Arial,Times N" SIZE="-1">Art. 

              32 - O incorporador somente poder&aacute; negociar sobre unidades 

              aut&ocirc;nomas ap&oacute;s ter arquivado, no cart&oacute;rio competente 

              de Registro de Im&oacute;veis, os seguintes documentos:<BR>

              a) t&iacute;tulo de propriedade de terreno, ou de promessa, irrevog&aacute;vel 

              e irretrat&aacute;vel, de compra e venda ou de cess&atilde;o de 

              direitos ou de permuta, do qual conste cl&aacute;usula de imiss&atilde;o 

              na posse do im&oacute;vel, n&atilde;o haja estipula&ccedil;&otilde;es 

              impeditivas de sua aliena&ccedil;&atilde;o em fra&ccedil;&otilde;es 

              ideais e inclua consentimento para demoli&ccedil;&atilde;o e constru&ccedil;&atilde;o, 

              devidamente registrado;<BR>

              b) certid&otilde;es negativas de impostos federais, estaduais e 

              municipais, de protesto de t&iacute;tulos, de a&ccedil;&otilde;es 

              c&iacute;veis e criminais e de &ocirc;nus reais relativamente ao 

              im&oacute;vel, aos alienantes do terreno e ao incorporador;<BR>

              c) hist&oacute;rico dos t&iacute;tulos de propriedade do im&oacute;vel, 

              abrangendo os &uacute;ltimos 20 (vinte) anos, acompanhado de certid&atilde;o 

              dos respectivos registros;<BR>

              d) projeto de constru&ccedil;&atilde;o devidamente aprovado pelas 

              autoridades competentes;<BR>

              e) c&aacute;lculo das &aacute;reas das edifica&ccedil;&otilde;es, 

              discriminando, al&eacute;m da global, a das partes comuns, e indicando, 

              para cada tipo de unidade, a respectiva metragem de &aacute;rea 

              constru&iacute;da;<BR>

              f) certid&atilde;o negativa de d&eacute;bito para com a Previd&ecirc;ncia 

              Social, quando o titular de direitos sobre o terreno for respons&aacute;vel 

              pela arrecada&ccedil;&atilde;o das respectivas contribui&ccedil;&otilde;es;<BR>

              g) memorial descritivo das especifica&ccedil;&otilde;es da obra 

              projetada, segundo modelo a que se refere o inciso IV, do Art. 53, 

              desta Lei;<BR>

              h) avalia&ccedil;&atilde;o do custo global da obra, atualizada &agrave; 

              data do arquivamento, calculada de acordo com a norma do inciso 

              III, do Art. 53, com base nos custos unit&aacute;rios referidos 

              no Art. 54, discriminando-se, tamb&eacute;m, o custo de constru&ccedil;&atilde;o 

              de cada unidade, devidamente autenticada pelo profissional respons&aacute;vel 

              pela obra;<BR>

              i) discrimina&ccedil;&atilde;o das fra&ccedil;&otilde;es ideais 

              de terreno, com as unidades aut&ocirc;nomas que a elas corresponder&atilde;o;<BR>

              j) minuta da futura Conven&ccedil;&atilde;o de Condom&iacute;nio 

              que reger&aacute; a edifica&ccedil;&atilde;o ou o conjunto de edifica&ccedil;&otilde;es;<BR>

              l) declara&ccedil;&atilde;o em que se defina a parcela do pre&ccedil;o 

              de que trata o inciso II, do Art. 39;<BR>

              m) certid&atilde;o do instrumento p&uacute;blico de mandato, referido 

              no &sect; 1&ordm; do Art. 31;<BR>

              n) declara&ccedil;&atilde;o expressa em que se fixe, se houver, 

              o prazo de car&ecirc;ncia (Art. 34);<BR>

              o) atestado de idoneidade financeira, fornecido por estabelecimento 

              de cr&eacute;dito que opere no Pa&iacute;s h&aacute; mais de 5 (cinco) 

              anos;<BR>

              p) declara&ccedil;&atilde;o, acompanhada de plantas elucidativas, 

              sobre o n&uacute;mero de ve&iacute;culos que a garagem comporta 

              e os locais destinados &agrave; guarda dos mesmos.<BR>

              ________<BR>

              Nota:<BR>

              Acrescentada pela Lei n&ordm;4.864/65<BR>

              _________<BR>

              &sect; 1&ordm; - A documenta&ccedil;&atilde;o referida neste artigo, 

              ap&oacute;s o exame do oficial de registro de im&oacute;veis, ser&aacute; 

              arquivada em cart&oacute;rio, fazendo-se o competente registro.<BR>

              &sect; 2&ordm; - Os contratos de compra e venda, promessa de venda, 

              cess&atilde;o ou promessa de cess&atilde;o de unidades aut&ocirc;nomas 

              ser&atilde;o tamb&eacute;m averb&aacute;veis &agrave; margem do 

              registro de que trata este artigo.<BR>

              &sect; 3&ordm; - O n&uacute;mero do registro referido no &sect; 

              1&ordm;, bem como a indica&ccedil;&atilde;o do cart&oacute;rio competente, 

              constar&aacute;, obrigatoriamente, dos an&uacute;ncios, impressos, 

              publica&ccedil;&otilde;es, propostas, contratos, preliminares ou 

              definitivos, referentes &agrave; incorpora&ccedil;&atilde;o, salvo 

              dos an&uacute;ncios &quot;classificados&quot;.<BR>

              &sect; 4&ordm; - O Registro de Im&oacute;veis dar&aacute; certid&atilde;o 

              ou fornecer&aacute;, a quem o solicitar, c&oacute;pia fotost&aacute;tica, 

              heliogr&aacute;fica, termofax, microfilmagem ou outra equivalente, 

              dos documentos especificados neste artigo, ou autenticar&aacute; 

              c&oacute;pia apresentada pela parte interessada.<BR>

              &sect; 5&ordm; - A exist&ecirc;ncia de &ocirc;nus fiscais ou reais, 

              salvo os impeditivos de aliena&ccedil;&atilde;o, n&atilde;o impedem 

              o registro, que ser&aacute; feito com as devidas ressalvas, mencionando-se, 

              em todos os documentos, extra&iacute;dos do registro, a exist&ecirc;ncia 

              e a extens&atilde;o dos &ocirc;nus.<BR>

              &sect; 6&ordm; - Os oficiais de registro de im&oacute;veis ter&atilde;o 

              15 (quinze) dias para apresentar, por escrito, todas as exig&ecirc;ncias 

              que julgarem necess&aacute;rias ao arquivamento, e, satisfeitas 

              as referidas exig&ecirc;ncias, ter&atilde;o o prazo de 15 (quinze) 

              dias para fornecer certid&atilde;o, relacionando a documenta&ccedil;&atilde;o 

              apresentada e devolver, autenticadas, as segundas vias da mencionada 

              documenta&ccedil;&atilde;o, com exce&ccedil;&atilde;o dos documentos 

              p&uacute;blicos. Em casos de diverg&ecirc;ncia, o oficial levantar&aacute; 

              a d&uacute;vida segundo as normas processuais aplic&aacute;veis.<BR>

              &sect; 7&ordm; - O oficial do registro de im&oacute;veis responde, 

              civil e criminalmente, se efetuar o arquivamento de documenta&ccedil;&atilde;o 

              contraveniente &agrave; lei ou der certid&atilde;o ...(Vetado)... 

              sem o arquivamento de todos os documentos exigidos.<BR>

              &sect; 8&ordm; - O oficial do registro de im&oacute;veis que n&atilde;o 

              observar o prazo previsto no &sect; 6&ordm; ficar&aacute; sujeito 

              &agrave; penalidade imposta pela autoridade judici&aacute;ria competente 

              em montante igual ao dos emolumentos devidos pelo registro de que 

              trata este artigo, aplic&aacute;vel por quinzena ou fra&ccedil;&atilde;o 

              de quinzena de supera&ccedil;&atilde;o de cada um daqueles prazos.<BR>

              _________<BR>

              Nota:<BR>

              Acrescentado pela Lei n&ordm;4.864/65<BR>

              ________<BR>

              &sect; 9&ordm; - O oficial do registro de im&oacute;veis n&atilde;o 

              responde pela exatid&atilde;o dos documentos que lhe forem apresentados 

              para arquivamento em obedi&ecirc;ncia ao disposto nas al&iacute;nea 

              e, g, h, p deste artigo, desde que assinados pelo profissional respons&aacute;vel 

              pela obra.<BR>

              _________<BR>

              Nota:<BR>

              Acrescentado pela Lei n&ordm;4.864/65<BR>

              ________<BR>

              &sect; 10 - As plantas do projeto aprovado (al&iacute;nea d deste 

              artigo) poder&atilde;o ser apresentadas em c&oacute;pia autenticada 

              pelo profissional respons&aacute;vel pela obra, acompanhada de c&oacute;pia 

              de licen&ccedil;a de constru&ccedil;&atilde;o.<BR>

              _________<BR>

              Nota:<BR>

              Acrescentado pela Lei n&ordm;4.864/65<BR>

              ________<BR>

              &sect; 11 - At&eacute; 30 de junho de 1966, se, dentro de 15 (quinze) 

              dias da entrega ao cart&oacute;rio do Registro de Im&oacute;veis 

              da documenta&ccedil;&atilde;o completa prevista, neste artigo, feita 

              por carta enviada pelo Oficio de T&iacute;tulos e Documentos, n&atilde;o 

              tiver o Cart&oacute;rio de Im&oacute;veis entregue a certid&atilde;o 

              de arquivamento e registro, nem formulado, por escrito, as exig&ecirc;ncias 

              previstas no &sect; 6&ordm;, considerar-se-&aacute; de pleno direito 

              completado o registro provis&oacute;rio.<BR>

              _________<BR>

              Nota:<BR>

              Acrescentado pela Lei n&ordm;4.864/65<BR>

              ________<BR>

              &sect; 12 - O registro provis&oacute;rio previsto no par&aacute;grafo 

              anterior autoriza o incorporador a negociar as unidades da incorpora&ccedil;&atilde;o, 

              indicando na sua publica&ccedil;&atilde;o o n&uacute;mero do Registro 

              de T&iacute;tulos e Documentos referente &agrave; remessa dos documentos 

              ao Cart&oacute;rio de Im&oacute;veis, sem preju&iacute;zo, todavia, 

              da sua responsabilidade perante o adquirente da unidade e da obriga&ccedil;&atilde;o 

              de satisfazer as exig&ecirc;ncias, posteriormente formuladas pelo 

              Cart&oacute;rio, bem como de completar o registro definitivo.<BR>

              _________<BR>

              Nota:<BR>

              Acrescentado pela Lei n&ordm;4.864/65<BR>

              ________<BR>

              <BR>

              Art. 33 - O registro da incorpora&ccedil;&atilde;o ser&aacute; v&aacute;lido 

              pelo prazo de 180 (cento e oitenta) dias, findo o qual, se ela ainda 

              n&atilde;o se houver concretizado, o incorporador s&oacute; poder&aacute; 

              negociar unidades depois de atualizar a documenta&ccedil;&atilde;o 

              a que se refere o artigo anterior, revalidado o registro por igual 

              prazo.<BR>

              <BR>

              Art. 34 - O incorporador poder&aacute; fixar, para efetiva&ccedil;&atilde;o 

              da incorpora&ccedil;&atilde;o, prazo de car&ecirc;ncia, dentro do 

              qual lhe &eacute; l&iacute;cito desistir do empreendimento.<BR>

              &sect; 1&ordm; - A fixa&ccedil;&atilde;o do prazo de car&ecirc;ncia 

              ser&aacute; feita pela declara&ccedil;&atilde;o a que se refere 

              a al&iacute;nea n, do Art. 32, onde se fixem as condi&ccedil;&otilde;es 

              que autorizar&atilde;o o incorporador a desistir do empreendimento.<BR>

              &sect; 2&ordm; - Em caso algum poder&aacute; o prazo de car&ecirc;ncia 

              ultrapassar o termo final do prazo da validade do registro ou, se 

              for o caso, de sua revalida&ccedil;&atilde;o.<BR>

              &sect; 3&ordm; - Os documentos preliminares de ajuste, se houver, 

              mencionar&atilde;o, obrigatoriamente, o prazo de car&ecirc;ncia, 

              inclusive para efeitos do Art. 45.<BR>

              &sect; 4&ordm; - A desist&ecirc;ncia da incorpora&ccedil;&atilde;o 

              ser&aacute; denunciada, por escrito, ao Registro de Im&oacute;veis 

              ...(Vetado)... e comunicada, por escrito, a cada um dos adquirentes 

              ou candidatos &agrave; aquisi&ccedil;&atilde;o, sob pena de responsabilidade 

              civil e criminal do incorporador.<BR>

              &sect; 5&ordm; - Ser&aacute; averbada no registro da incorpora&ccedil;&atilde;o 

              a desist&ecirc;ncia de que trata o par&aacute;grafo anterior, arquivando-se 

              em cart&oacute;rio o respectivo documento.<BR>

              &sect; 6&ordm; - O prazo de car&ecirc;ncia &eacute; improrrog&aacute;vel.<BR>

              <BR>

              Art. 35 - O incorporador ter&aacute; o prazo m&aacute;ximo de 60 

              (sessenta) dias, a contar do termo final do prazo de car&ecirc;ncia, 

              se houver, para promover a celebra&ccedil;&atilde;o do competente 

              contrato relativo &agrave; fra&ccedil;&atilde;o ideal de terreno, 

              e, bem assim, do contrato de constru&ccedil;&atilde;o e da Conven&ccedil;&atilde;o 

              do Condom&iacute;nio, de acordo com discrimina&ccedil;&atilde;o 

              constante da al&iacute;nea i do Art. 32.<BR>

              &sect; 1&ordm; - No caso de n&atilde;o haver prazo de car&ecirc;ncia, 

              o prazo acima se contar&aacute; na data de qualquer documento de 

              ajuste preliminar.<BR>

              &sect; 2&ordm; - Quando houver prazo de car&ecirc;ncia, a obriga&ccedil;&atilde;o 

              somente deixar&aacute; de existir se o incorporador tiver denunciado, 

              dentro do mesmo prazo e nas condi&ccedil;&otilde;es previamente 

              estabelecidas, por escrito, ao Registro de Im&oacute;veis, a n&atilde;o-concretiza&ccedil;&atilde;o 

              do empreendimento.&sect; 3&ordm; - Se, dentro do prazo de car&ecirc;ncia, 

              o incorporador n&atilde;o denunciar a incorpora&ccedil;&atilde;o, 

              embora n&atilde;o se tenham reunido as condi&ccedil;&otilde;es a 

              que se refere o &sect; 1, o outorgante do mandato, de que trata 

              o &sect; 1, do Art. 31, poder&aacute; faz&ecirc;-lo nos 5 (cinco) 

              dias subseq&uuml;entes ao prazo de car&ecirc;ncia, e nesse caso 

              ficar&aacute; solidariamente respons&aacute;vel com o incorporador 

              pela devolu&ccedil;&atilde;o das quantias que os adquirentes ou 

              candidatos &agrave; aquisi&ccedil;&atilde;o houverem entregue ao 

              incorporador, resguardado o direito de regresso sobre eles, dispensando-se, 

              ent&atilde;o, do cumprimento da obriga&ccedil;&atilde;o fixada no 

              caput deste artigo.<BR>

              &sect; 4&ordm; - Descumprida pelo incorporador e pelo mandante de 

              que trata o &sect; 1 do Art. 31 a obriga&ccedil;&atilde;o da outorga 

              dos contratos referidos no caput deste artigo, nos prazos ora fixados, 

              a carta-proposta ou o documento de ajuste preliminar poder&atilde;o 

              ser averbados no Registro de Im&oacute;veis, averba&ccedil;&atilde;o 

              que conferir&aacute; direito real opon&iacute;vel a terceiros, com 

              o conseq&uuml;ente direito &agrave; obten&ccedil;&atilde;o compuls&oacute;ria 

              do contrato correspondente.<BR>

              &sect; 5&ordm; - Na hip&oacute;tese do par&aacute;grafo anterior, 

              o incorporador incorrer&aacute; tamb&eacute;m na multa de 50% (cinq&uuml;enta 

              por cento) sobre a quantia que efetivamente tiver recebido, cobr&aacute;vel 

              por via executiva, em favor do adquirente ou candidato &agrave; 

              aquisi&ccedil;&atilde;o.<BR>

              &sect; 6&ordm; - Ressalvado o disposto no Art. 43, do contrato de 

              constru&ccedil;&atilde;o dever&aacute; constar expressamente a men&ccedil;&atilde;o 

              dos respons&aacute;veis pelo pagamento da constru&ccedil;&atilde;o 

              de cada uma das unidades. O incorporador responde, em igualdade 

              de condi&ccedil;&otilde;es, com os demais contratantes, pelo pagamento 

              da constru&ccedil;&atilde;o das unidades que n&atilde;o tenham tido 

              a responsabilidade pela sua constru&ccedil;&atilde;o assumida por 

              terceiros e at&eacute; que o tenham.<BR>

              <BR>

              Art. 36 - No caso de den&uacute;ncia de incorpora&ccedil;&atilde;o, 

              nos termos do Art. 34, se o incorporador, at&eacute; 30 (trinta) 

              dias a contar da den&uacute;ncia, n&atilde;o restituir aos adquirentes 

              as import&acirc;ncias pagas, estes poder&atilde;o cobr&aacute;- 

              la por via executiva, reajustado o seu valor a contar da data do 

              recebimento, em fun&ccedil;&atilde;o do &iacute;ndice geral de pre&ccedil;os 

              mensalmente publicado pelo Conselho Nacional de Economia, que reflita 

              as varia&ccedil;&otilde;es no poder aquisitivo da moeda nacional, 

              e acrescido de juros de 6% (seis por cento) ao ano, sobre o total 

              corrigido.<BR>

              <BR>

              Art. 37 - Se o im&oacute;vel estiver gravado de &ocirc;nus real 

              ou fiscal ou se contra os alienantes houver qualquer a&ccedil;&atilde;o 

              que possa compromet&ecirc;-lo, o fato ser&aacute; obrigatoriamente 

              mencionado em todos os documentos de ajuste, com a indica&ccedil;&atilde;o 

              de sua natureza e das condi&ccedil;&otilde;es de libera&ccedil;&atilde;o.<BR>

              <BR>

              Art. 38 - Tamb&eacute;m constar&aacute;, obrigatoriamente, dos documentos 

              de ajuste, se for o caso, o fato de encontrar-se ocupado o im&oacute;vel, 

              esclarecendo se a que t&iacute;tulo se deve esta ocupa&ccedil;&atilde;o 

              e quais as condi&ccedil;&otilde;es de desocupa&ccedil;&atilde;o.<BR>

              <BR>

              Art. 39 - Nas incorpora&ccedil;&otilde;es em que a aquisi&ccedil;&atilde;o 

              do terreno se der com pagamento total ou parcial em unidades a serem 

              constru&iacute;das, dever&atilde;o ser discriminadas em todos os 

              documentos de ajuste:<BR>

              I - a parcela que, se houver, ser&aacute; paga em dinheiro;<BR>

              II - a quota-parte da &aacute;rea das unidades a serem entregues 

              em pagamento do terreno que corresponder&aacute; a cada uma das 

              unidades, a qual dever&aacute; ser expressa em metros quadrados.<BR>

              Par&aacute;grafo &uacute;nico. Dever&aacute; constar, tamb&eacute;m, 

              de todos os documentos de ajuste, se o alienante do terreno ficou 

              ou n&atilde;o sujeito a qualquer presta&ccedil;&atilde;o ou encargo.<BR>

              <BR>

              Art. 40 - No caso de rescis&atilde;o de contrato de aliena&ccedil;&atilde;o 

              do terreno ou de fra&ccedil;&atilde;o ideal, ficar&atilde;o rescindidas 

              as cess&otilde;es ou promessas de cess&atilde;o de direitos correspondentes 

              &agrave; aquisi&ccedil;&atilde;o do terreno.<BR>

              &sect; 1&ordm; - Nesta hip&oacute;tese, consolidar-se-&aacute;, 

              no alienante em cujo favor se opera a resolu&ccedil;&atilde;o, o 

              direito sobre a constru&ccedil;&atilde;o porventura existente.<BR>

              &sect; 2&ordm; - No caso do par&aacute;grafo anterior, cada um dos 

              ex-titulares de direito &agrave; aquisi&ccedil;&atilde;o de unidades 

              aut&ocirc;nomas haver&aacute; do mencionado alienante o valor da 

              parcela de constru&ccedil;&atilde;o que haja adicionado &agrave; 

              unidade, salvo se a rescis&atilde;o houver sido causada pelo ex-titular.<BR>

              &sect; 3&ordm; - Na hip&oacute;tese dos par&aacute;grafos anteriores, 

              sob pena de nulidade, n&atilde;o poder&aacute; o alienante em cujo 

              favor se operou a resolu&ccedil;&atilde;o voltar a negociar seus 

              direitos sobre a unidade aut&ocirc;noma, sem a pr&eacute;via indeniza&ccedil;&atilde;o 

              aos titulares, de que trata o &sect; 2.<BR>

              &sect; 4&ordm; - No caso do par&aacute;grafo anterior, se os ex-titulares 

              tiverem de recorrer &agrave; cobran&ccedil;a judicial do que lhes 

              for devido, somente poder&atilde;o garantir o seu pagamento a unidade 

              e respectiva fra&ccedil;&atilde;o de terreno objeto do presente 

              artigo.<BR>

              <BR>

              Art. 41 - Quando as unidades imobili&aacute;rias forem contratadas 

              pelo incorporador por pre&ccedil;o global compreendendo quota de 

              terreno e constru&ccedil;&atilde;o, inclusive com parte do pagamento 

              ap&oacute;s a entrega da unidade, discriminar-se-&atilde;o, no contrato, 

              o pre&ccedil;o da quota de terreno e o da constru&ccedil;&atilde;o.<BR>

              &sect; 1&ordm; - Poder-se-&aacute; estipular que, na hip&oacute;tese 

              de o adquirente atrasar o pagamento de parcela relativa a constru&ccedil;&atilde;o, 

              os efeitos da mora recair&atilde;o n&atilde;o apenas sobre a aquisi&ccedil;&atilde;o 

              da parte constru&iacute;da, mas, tamb&eacute;m, sobre a fra&ccedil;&atilde;o 

              ideal de terreno, ainda que esta tenha sido totalmente paga.<BR>

              &sect; 2&ordm; - Poder-se-&aacute; tamb&eacute;m estipular que, 

              na hip&oacute;tese de o adquirente atrasar o pagamento da parcela 

              relativa &agrave; fra&ccedil;&atilde;o ideal de terreno, os efeitos 

              da mora recair&atilde;o n&atilde;o apenas sobre a aquisi&ccedil;&atilde;o 

              da fra&ccedil;&atilde;o ideal, mas, tamb&eacute;m, sobre a parte 

              constru&iacute;da, ainda que totalmente paga.<BR>

              <BR>

              Art. 42 - No caso de rescis&atilde;o do contrato relativo &agrave; 

              fra&ccedil;&atilde;o ideal de terreno e partes comuns, a pessoa 

              em cujo favor se tenha operado a resolu&ccedil;&atilde;o sub-rogar-se-&aacute; 

              nos direitos e obriga&ccedil;&otilde;es contratualmente atribu&iacute;dos 

              ao inadimplente, com rela&ccedil;&atilde;o a constru&ccedil;&atilde;o.<BR>

              <BR>

              Art. 43 - Quando o incorporador contratar a entrega da unidade a 

              prazo e pre&ccedil;os certos, determinados ou determin&aacute;veis, 

              mesmo quando pessoa f&iacute;sica, ser-lhe-&atilde;o impostas as 

              seguintes normas:<BR>

              I - informar obrigatoriamente aos adquirentes, por escrito, no m&iacute;nimo 

              de seis (seis) meses, o estado da obra;<BR>

              II - responder civilmente pela execu&ccedil;&atilde;o da incorpora&ccedil;&atilde;o, 

              devendo indenizar os adquirentes ou compromiss&aacute;rios, dos 

              preju&iacute;zos que a estes advierem do fato de n&atilde;o se concluir 

              a edifica&ccedil;&atilde;o ou de se retardar injustificadamente 

              a conclus&atilde;o das obras, cabendo-lhe a&ccedil;&atilde;o regressiva 

              contra o construtor, se for o caso e se a este couber a culpa;<BR>

              III - em caso de fal&ecirc;ncia do incorporador, pessoa f&iacute;sica 

              ou jur&iacute;dica, e n&atilde;o ser poss&iacute;vel &agrave; maioria 

              prosseguir na constru&ccedil;&atilde;o das edifica&ccedil;&otilde;es, 

              os subscritores ou candidatos &agrave; aquisi&ccedil;&atilde;o de 

              unidades ser&atilde;o credores privilegiados pelas quantias que 

              houverem pago ao incorporador, respondendo subsidiariamente os bens 

              pessoais deste;<BR>

              IV - &eacute; vedado ao incorporador alterar o projeto, especialmente 

              no que se refere &agrave; unidade do adquirente e &agrave;s partes 

              comuns, modificar as especifica&ccedil;&otilde;es, ou desviar-se 

              do plano da constru&ccedil;&atilde;o, salvo autoriza&ccedil;&atilde;o 

              un&acirc;nime dos interessados ou exig&ecirc;ncia legal;<BR>

              V - n&atilde;o poder&aacute; modificar as condi&ccedil;&otilde;es 

              de pagamento nem reajustar o pre&ccedil;o das unidades, ainda no 

              caso de eleva&ccedil;&atilde;o dos pre&ccedil;os dos materiais e 

              da m&atilde;o-de-obra, salvo se tiver sido expressamente ajustada 

              a faculdade de reajustamento, procedendo-se, ent&atilde;o, nas condi&ccedil;&otilde;es 

              estipuladas;<BR>

              VI - se o incorporador, sem justa causa devidamente comprovada, 

              paralisar as obras por mais de 30 (trinta) dias, ou retardar-lhes 

              excessivamente o andamento, poder&aacute; o juiz notific&aacute;-lo 

              para que no prazo m&iacute;nimo de 30 (trinta) dias as reinicie 

              ou torne a dar-lhes o andamento normal. Desatendida a notifica&ccedil;&atilde;o, 

              poder&aacute; o incorporador ser destitu&iacute;do pela maioria 

              absoluta dos votos dos adquirentes, sem preju&iacute;zo da responsabilidade 

              civil ou penal que couber, sujeito &agrave; cobran&ccedil;a executiva 

              das import&acirc;ncias comprovadamente devidas, facultando-se aos 

              interessados prosseguir na obra (Vetado).<BR>

              <BR>

              Art. 44 - Ap&oacute;s a concess&atilde;o do &quot;habite-se&quot; 

              pela autoridade administrativa, o incorporador dever&aacute; requerer 

              (Vetado) a averba&ccedil;&atilde;o da constru&ccedil;&atilde;o das 

              edifica&ccedil;&otilde;es, para efeito de individualiza&ccedil;&atilde;o 

              e discrimina&ccedil;&atilde;o das unidades, respondendo perante 

              os adquirentes pelas perdas e danos que resultem da demora no cumprimento 

              dessa obriga&ccedil;&atilde;o.<BR>

              &sect; 1&ordm; - Se o incorporador n&atilde;o requerer a averba&ccedil;&atilde;o 

              (Vetado) o construtor requer&ecirc;-la-&aacute; (Vetado) sob pena 

              de ficar solidariamente respons&aacute;vel com o incorporador perante 

              os adquirentes.<BR>

              &sect; 2&ordm; - Na omiss&atilde;o do incorporador e do construtor, 

              a averba&ccedil;&atilde;o poder&aacute; ser requerida por qualquer 

              dos adquirentes de unidade.<BR>

              <BR>

              Art. 45 - &Eacute; l&iacute;cito ao incorporador recolher o Imposto 

              do Selo devido, mediante apresenta&ccedil;&atilde;o dos contratos 

              preliminares, at&eacute; 10 (dez) dias a contar do vencimento do 

              prazo de car&ecirc;ncia a que se refere o Art. 34, extinta a obriga&ccedil;&atilde;o 

              se, dentro deste prazo, for denunciada a incorpora&ccedil;&atilde;o.<BR>

              <BR>

              Art. 46 - Quando o pagamento do Imposto sobre Lucro Imobili&aacute;rio 

              e respectivos acr&eacute;scimos e adicionais for de responsabilidade 

              do vendedor do terreno, ser&aacute; l&iacute;cito ao adquirente 

              reter o pagamento das &uacute;ltimas presta&ccedil;&otilde;es anteriores 

              &agrave; data-limite em que &eacute; l&iacute;cito pagar, sem reajuste, 

              o referido imposto e os adicionais, caso o vendedor n&atilde;o apresente 

              a quita&ccedil;&atilde;o at&eacute; 10 (dez) dias antes do vencimento 

              das presta&ccedil;&otilde;es cujo pagamento torne inferior ao d&eacute;bito 

              fiscal a parte do pre&ccedil;o a ser ainda paga at&eacute; a referida 

              data-limite.<BR>

              Par&aacute;grafo &uacute;nico. No caso de reten&ccedil;&atilde;o 

              pelo adquirente, esse ficar&aacute; respons&aacute;vel, para todos 

              os efeitos, perante o Fisco, pelo recolhimento do tributo, adicionais 

              e acr&eacute;scimos, inclusive pelos reajustamentos que vier a sofrer 

              o d&eacute;bito fiscal (Vetado).<BR>

              <BR>

              Art. 47 - Quando se fixar no contrato que a obriga&ccedil;&atilde;o 

              do pagamento do Imposto sobre Lucro Imobili&aacute;rio, acr&eacute;scimos 

              e adicionais devidos pelo alienante &eacute; transferida ao adquirente, 

              dever-se-&aacute; explicitar o montante que tal obriga&ccedil;&atilde;o 

              atingiria, se sua satisfa&ccedil;&atilde;o se desse na data da escritura.<BR>

              &sect; 1&ordm; - Neste caso, o adquirente ser&aacute; tido, para 

              todos os efeitos, como respons&aacute;vel perante o Fisco.<BR>

              &sect; 2&ordm; - Havendo parcela restitu&iacute;vel, a restitui&ccedil;&atilde;o 

              ser&aacute; feita ao adquirente, e, se for o caso, em nome deste 

              ser&atilde;o emitidas as Obriga&ccedil;&otilde;es do Tesouro Nacional 

              a que se refere o Art. 4&ordm; da Lei n&uacute;mero 4.357, de 16 

              de julho de 1964.<BR>

              &sect; 3&ordm; - Para efeitos fiscais, n&atilde;o importar&aacute; 

              em aumento do pre&ccedil;o de aquisi&ccedil;&atilde;o a circunst&acirc;ncia 

              de obrigar-se o adquirente ao pagamento do Imposto sobre Lucro Imobili&aacute;rio, 

              seus acr&eacute;scimos e adicionais.<BR>

              </FONT></P>

            <CENTER>

              <HR>

              <FONT SIZE="-1" FACE="Verdana,Arial,Times N"><B><FONT SIZE="3">Cap&iacute;tulo 

              III</FONT></B><FONT SIZE="3"> - Da Constru&ccedil;&atilde;o de Edifica&ccedil;&otilde;es 

              em Condom&iacute;nio</FONT></FONT> 

              <HR>

            </CENTER>

            <CENTER>

              <FONT SIZE="-1" FACE="Verdana,Arial,Times N"><B>Se&ccedil;&atilde;o 

              I<BR>

              Da Constru&ccedil;&atilde;o em Geral</B></FONT> 

            </CENTER>

            <P ALIGN="LEFT"><FONT FACE="Verdana,Arial,Times N" SIZE="-1"><BR>

              <FONT COLOR="Black">Art. 48 - A constru&ccedil;&atilde;o de im&oacute;veis, 

              objeto de incorpora&ccedil;&atilde;o nos moldes previstos nesta 

              Lei, poder&aacute; ser contratada sob o regime da empreitada ou 

              de administra&ccedil;&atilde;o, conforme adiante definidos, e poder&aacute; 

              estar inclu&iacute;da no contrato com o incorporador (Vetado), ou 

              ser contratada diretamente entre os adquirentes e o construtor.<BR>

              &sect; 1&ordm; - O projeto e o memorial descritivo das edifica&ccedil;&otilde;es 

              far&atilde;o parte integrante e complementar do contrato.<BR>

              &sect; 2&ordm; - Do contrato dever&aacute; constar o prazo da entrega 

              das obras e as condi&ccedil;&otilde;es e formas de sua eventual 

              prorroga&ccedil;&atilde;o.<BR>

              <BR>

              Art. 49 - Os contratantes da constru&ccedil;&atilde;o, inclusive 

              no caso do Art. 43, para tratar de seus interesses, com rela&ccedil;&atilde;o 

              a ela, poder&atilde;o reunir-se em assembl&eacute;ia, cujas delibera&ccedil;&otilde;es, 

              desde que aprovadas por maioria simples dos votos presentes, ser&atilde;o 

              v&aacute;lidas e obrigat&oacute;rias para todos eles, salvo no que 

              afetar ao direito de propriedade previsto na legisla&ccedil;&atilde;o.<BR>

              &sect; 1&ordm; - As assembl&eacute;ias ser&atilde;o convocadas, 

              pelo menos, por um ter&ccedil;o dos votos dos contratantes, pelo 

              incorporador ou pelo construtor, com men&ccedil;&atilde;o expressa 

              do assunto a tratar, sendo admitido comparecimento de procurador 

              bastante.<BR>

              &sect; 2&ordm; - A convoca&ccedil;&atilde;o da assembl&eacute;ia 

              ser&aacute; feita por carta registrada ou protocolo, com anteced&ecirc;ncia 

              m&iacute;nima de 5 (cinco) dias para a primeira convoca&ccedil;&atilde;o, 

              e mais 3 (tr&ecirc;s) dias para a segunda, podendo ambas as convoca&ccedil;&otilde;es 

              ser feitas no mesmo aviso.<BR>

              &sect; 3&ordm; - A assembl&eacute;ia instalar-se-&aacute;, no m&iacute;nimo, 

              com metade dos contratantes, em primeira convoca&ccedil;&atilde;o, 

              e com qualquer n&uacute;mero, em segunda, sendo, por&eacute;m, obrigat&oacute;ria 

              a presen&ccedil;a, em qualquer caso, do incorporador ou do construtor, 

              quando convocantes, e, pelo menos, com metade dos contratantes que 

              a tenham convocado, se for o caso.<BR>

              &sect; 4&ordm; - Na assembl&eacute;ia os votos dos contratantes 

              ser&atilde;o proporcionais &agrave;s respectivas fra&ccedil;&otilde;es 

              ideais de terreno.<BR>

              <BR>

              Art. 50 - Ser&aacute; designada no contrato de constru&ccedil;&atilde;o, 

              ou eleita em assembl&eacute;ia especial, devidamente convocada antes 

              do in&iacute;cio da obra, uma Comiss&atilde;o de Representantes 

              composta de tr&ecirc;s membros pelo menos, escolhidos entre os contratantes, 

              para represent&aacute;-los junto ao construtor ou ao incorporador, 

              no caso do Art. 43, em tudo que interessar ao bom andamento da obra.<BR>

              &sect; 1&ordm; - Uma vez eleita a Comiss&atilde;o, cuja constitui&ccedil;&atilde;o 

              se comprovar&aacute; com a ata da assembl&eacute;ia, devidamente 

              inscrita no Registro de T&iacute;tulos e Documentos, esta ficar&aacute; 

              de pleno direito investida dos poderes necess&aacute;rios para exercer 

              todas as atribui&ccedil;&otilde;es e praticar todos os atos que 

              esta Lei e o contrato de constru&ccedil;&atilde;o lhe deferirem, 

              sem necessidade de instrumento especial outorgado pelos contratantes 

              ou, se for o caso, pelos que se sub-rogarem nos direitos e obriga&ccedil;&otilde;es 

              destes.<BR>

              &sect; 2&ordm; - A assembl&eacute;ia poder&aacute; revogar, pela 

              maioria absoluta dos votos dos contratantes, qualquer decis&atilde;o 

              da Comiss&atilde;o, ressalvados os direitos de terceiros quanto 

              aos efeitos j&aacute; produzidos.<BR>

              &sect; 3&ordm; - Respeitados os limites constantes desta Lei, o 

              contrato poder&aacute; discriminar as atribui&ccedil;&otilde;es 

              da Comiss&atilde;o e dever&aacute; dispor sobre os mandatos de seus 

              membros, sua destitui&ccedil;&atilde;o e a forma de preenchimento 

              das vagas eventuais, sendo l&iacute;cita a estipula&ccedil;&atilde;o 

              de que o mandato conferido a qualquer membro, no caso de sub-roga&ccedil;&atilde;o 

              de seu contrato a terceiros, se tenha por transferido, de pleno 

              direito, ao sub rogat&aacute;rio, salvo se este n&atilde;o o aceitar.<BR>

              &sect; 4&ordm; - Nas incorpora&ccedil;&otilde;es em que o n&uacute;mero 

              de contratantes de unidades for igual ou inferior a tr&ecirc;s, 

              a totalidade deles exercer&aacute;, em conjunto, as atribui&ccedil;&otilde;es 

              que esta Lei confere &agrave; Comiss&atilde;o, aplicando se, no 

              que couber, o disposto nos par&aacute;grafos anteriores.<BR>

              <BR>

              Art. 51 - Nos contratos de constru&ccedil;&atilde;o, seja qual for 

              seu regime, dever&aacute; constar expressamente a quem caber&atilde;o 

              as despesas com liga&ccedil;&otilde;es de servi&ccedil;os p&uacute;blicos, 

              devidas ao Poder P&uacute;blico, bem como as despesas indispens&aacute;veis 

              &agrave; instala&ccedil;&atilde;o, funcionamento e regulamenta&ccedil;&atilde;o 

              do condom&iacute;nio.Par&aacute;grafo &uacute;nico. Quando o servi&ccedil;o 

              p&uacute;blico for explorado mediante concess&atilde;o, os contratos 

              de constru&ccedil;&atilde;o dever&atilde;o tamb&eacute;m especificar 

              a quem caber&atilde;o as despesas com as liga&ccedil;&otilde;es 

              que incumbam &agrave;s concession&aacute;rias, no caso de n&atilde;o 

              estarem elas obrigadas a faz&ecirc;-las, ou, em o estando, se a 

              isto se recusarem ou alegarem impossibilidade.<BR>

              <BR>

              Art. 52 - Cada contratante da constru&ccedil;&atilde;o s&oacute; 

              ser&aacute; imitido na posse de sua unidade se estiver em dia com 

              as obriga&ccedil;&otilde;es assumidas, inclusive as relativas &agrave; 

              constru&ccedil;&atilde;o, exercendo o construtor e o condom&iacute;nio, 

              at&eacute; ent&atilde;o, o direito de reten&ccedil;&atilde;o sobre 

              a respectiva unidade; no caso do Art. 43, este direito ser&aacute; 

              exercido pelo incorporador.<BR>

              <BR>

              Art. 53 - O Poder Executivo, atrav&eacute;s do Banco Nacional da 

              Habita&ccedil;&atilde;o, promover&aacute; a celebra&ccedil;&atilde;o 

              de contratos, com a Associa&ccedil;&atilde;o Brasileira de Normas 

              T&eacute;cnicas (ABTN), no sentido de que esta, tendo em vista o 

              disposto na Lei n&uacute;mero 4.150, de 21 de novembro de 1962, 

              prepare, no prazo m&aacute;ximo de 120 (cento e vinte) dias, normas 

              que estabele&ccedil;am, para cada tipo de pr&eacute;dio que padronizar:<BR>

              I - crit&eacute;rios e normas para c&aacute;lculo de custos unit&aacute;rios 

              de constru&ccedil;&atilde;o para uso dos sindicatos, na forma do 

              Art. 54;<BR>

              II - crit&eacute;rios e normas para execu&ccedil;&atilde;o de or&ccedil;amentos 

              de custo de constru&ccedil;&atilde;o, para fins do disposto no Art. 

              59;<BR>

              III - crit&eacute;rios e normas para avalia&ccedil;&atilde;o de 

              custo global de obra, para fins da al&iacute;nea h, do Art. 32;<BR>

              IV - modelo de memorial descritivo dos acabamentos de edifica&ccedil;&atilde;o, 

              para fins do disposto no Art. 32;<BR>

              V - crit&eacute;rio para entrosamento entre o cronograma das obras 

              e o pagamento das presta&ccedil;&otilde;es, que poder&aacute; ser 

              introduzido nos contratos de incorpora&ccedil;&atilde;o inclusive 

              para o efeito de aplica&ccedil;&atilde;o do disposto no &sect; 2 

              do Art. 48.<BR>

              &sect; 1&ordm; - O n&uacute;mero de tipos padronizados dever&aacute; 

              ser reduzido e na fixa&ccedil;&atilde;o se atender&aacute; primordialmente:<BR>

              a) o n&uacute;mero de pavimentos e a exist&ecirc;ncia de pavimentos 

              especiais (subsolo, pilotis etc).;<BR>

              b) o padr&atilde;o da constru&ccedil;&atilde;o (baixo, normal, alto), 

              tendo em conta as condi&ccedil;&otilde;es de acabamento, a qualidade 

              dos materiais empregados, os equipamentos, o n&uacute;mero de elevadores 

              e as inova&ccedil;&otilde;es de conforto;<BR>

              c) as &aacute;reas de constru&ccedil;&atilde;o.<BR>

              &sect; 2&ordm; - Para custear o servi&ccedil;o a ser feito pela 

              ABNT, definido neste artigo, fica autorizado o Poder Executivo a 

              abrir um cr&eacute;dito especial no valor de dez milh&otilde;es 

              de cruzeiros, em favor do Banco Nacional da Habita&ccedil;&atilde;o, 

              vinculado a este fim, podendo o Banco adiantar a import&acirc;ncia 

              &agrave; ABNT, se necess&aacute;rio.<BR>

              &sect; 3&ordm; - No contrato a ser celebrado com a ABNT, estipular-se-&aacute; 

              a atualiza&ccedil;&atilde;o peri&oacute;dica das normas previstas 

              neste artigo, mediante remunera&ccedil;&atilde;o razo&aacute;vel.<BR>

              <BR>

              Art. 54 - Os sindicatos estaduais da ind&uacute;stria da constru&ccedil;&atilde;o 

              civil ficam obrigados a divulgar mensalmente, at&eacute; o dia 5 

              de cada m&ecirc;s, os custos unit&aacute;rios de constru&ccedil;&atilde;o 

              a serem adotados nas respectivas regi&otilde;es jurisdicionais, 

              calculados com observ&acirc;ncia dos crit&eacute;rios e normas a 

              que se refere o inciso I do artigo anterior.<BR>

              &sect; 1&ordm; - O sindicato estadual que deixar de cumprir a obriga&ccedil;&atilde;o 

              prevista neste artigo deixar&aacute; de receber dos cofres p&uacute;blicos, 

              enquanto perdurar a omiss&atilde;o, qualquer subven&ccedil;&atilde;o 

              ou aux&iacute;lio que pleiteie ou a que tenha direito.<BR>

              &sect; 2&ordm; - Na ocorr&ecirc;ncia de omiss&atilde;o de sindicato 

              estadual, o construtor usar&aacute; os &iacute;ndices fixados por 

              outro sindicato estadual, em cuja regi&atilde;o os custos de constru&ccedil;&atilde;o 

              mais lhe pare&ccedil;am aproximados dos da sua.<BR>

              &sect; 3&ordm; - Os or&ccedil;amentos ou estimativas baseados nos 

              custos unit&aacute;rios a que se refere este artigo s&oacute; poder&atilde;o 

              ser considerados atualizados, em certo m&ecirc;s, para os efeitos 

              desta Lei, se baseados em custos unit&aacute;rios relativos ao pr&oacute;prio 

              m&ecirc;s ou a um dos dois meses anteriores.</FONT><BR>

              <BR>

              <BR>

              </FONT></P>

            <CENTER>

              <FONT SIZE="-1" FACE="Verdana,Arial,Times N"><B>Se&ccedil;&atilde;o 

              II<BR>

              Da Constru&ccedil;&atilde;o Por Empreitada</B></FONT> 

            </CENTER>

            <P ALIGN="LEFT"><FONT FACE="Verdana,Arial,Times N" SIZE="-1"><BR>

              <FONT COLOR="Black">Art. 55 - Nas incorpora&ccedil;&otilde;es em 

              que a constru&ccedil;&atilde;o seja feita pelo regime de empreitada, 

              esta poder&aacute; ser a pre&ccedil;o fixo, ou a pre&ccedil;o reajust&aacute;vel 

              por &iacute;ndices previamente determinados.<BR>

              &sect; 1&ordm; - Na empreitada a pre&ccedil;o fixo, o pre&ccedil;o 

              da constru&ccedil;&atilde;o ser&aacute; irreajust&aacute;vel, independentemente 

              das varia&ccedil;&otilde;es que sofrer o custo efetivo das obras 

              e quaisquer que sejam suas causas.<BR>

              &sect; 2&ordm; - Na empreitada a pre&ccedil;o reajust&aacute;vel, 

              o pre&ccedil;o fixado no contrato ser&aacute; reajustado na forma 

              e nas &eacute;pocas nele expressamente previstas, em fun&ccedil;&atilde;o 

              da varia&ccedil;&atilde;o dos &iacute;ndices adotados, tamb&eacute;m 

              previstos obrigatoriamente no contrato.<BR>

              &sect; 3&ordm; - Nos contratos de constru&ccedil;&atilde;o por empreitada, 

              a Comiss&atilde;o de Representantes fiscalizar&aacute; o andamento 

              da obra e a obedi&ecirc;ncia ao projeto e &agrave;s especifica&ccedil;&otilde;es 

              exercendo as demais obriga&ccedil;&otilde;es inerentes &agrave; 

              sua fun&ccedil;&atilde;o representativa dos contratantes e fiscalizadora 

              da constru&ccedil;&atilde;o.<BR>

              &sect; 4&ordm; - Nos contratos de constru&ccedil;&atilde;o fixados 

              sob regime de empreitada, reajust&aacute;vel, a Comiss&atilde;o 

              de Representantes fiscalizar&aacute;, tamb&eacute;m, o c&aacute;lculo 

              do reajustamento.<BR>

              &sect; 5&ordm; - No contrato dever&aacute; ser mencionado o montante 

              do or&ccedil;amento atualizado da obra, calculado de acordo com 

              as normas do inciso III do Art. 53, com base nos custos unit&aacute;rios 

              referidos no Art. 54, quando o pre&ccedil;o estipulado for inferior 

              ao mesmo.<BR>

              &sect; 6&ordm; - Na forma de expressa refer&ecirc;ncia, os contratos 

              de empreitada entendem-se como sendo a pre&ccedil;o fixo.<BR>

              <BR>

              Art. 56 - Em toda a publicidade ou propaganda escrita, destinada 

              a promover a venda de incorpora&ccedil;&atilde;o com constru&ccedil;&atilde;o 

              pelo regime de empreitada reajust&aacute;vel, em que conste pre&ccedil;o, 

              ser&atilde;o discriminados explicitamente o pre&ccedil;o da fra&ccedil;&atilde;o 

              ideal do terreno e o pre&ccedil;o da constru&ccedil;&atilde;o, com 

              indica&ccedil;&atilde;o expressa da reajustabilidade.<BR>

              &sect; 1&ordm; - As mesmas indica&ccedil;&otilde;es dever&atilde;o 

              constar em todos os pap&eacute;is utilizados para a realiza&ccedil;&atilde;o 

              da incorpora&ccedil;&atilde;o, tais como cartas, propostas, escrituras, 

              contratos e documentos semelhantes.<BR>

              &sect; 2&ordm; - Esta exig&ecirc;ncia ser&aacute; dispensada nos 

              an&uacute;ncios &quot;classificados&quot;<BR>

              dos jornais.<BR>

              <BR>

              Art. 57 - Ao construtor que contratar, por empreitada a pre&ccedil;o 

              fixo, uma obra de incorpora&ccedil;&atilde;o, aplicar-se-&aacute;, 

              no que couber, o disposto nos itens II, III, IV (Vetado) e VI do 

              Art. 43.</FONT><BR>

              <BR>

              <BR>

              </FONT></P>

            <CENTER>

              <FONT SIZE="-1" FACE="Verdana,Arial,Times N"><B>Se&ccedil;&atilde;o 

              I<BR>

              Da Constru&ccedil;&atilde;o Por Administra&ccedil;&atilde;o</B></FONT> 

            </CENTER>

            <P ALIGN="LEFT"><FONT FACE="Verdana,Arial,Times N" SIZE="-1"><BR>

              <FONT COLOR="Black">Art. 58 - Nas incorpora&ccedil;&otilde;es em 

              que a constru&ccedil;&atilde;o for contratada pelo regime de administra&ccedil;&atilde;o, 

              tamb&eacute;m chamado &quot;a pre&ccedil;o de custo&quot;, ser&aacute; 

              de responsabilidade dos propriet&aacute;rios ou adquirentes o pagamento 

              do custo integral de obra, observadas as seguintes disposi&ccedil;&otilde;es:<BR>

              I - todas as faturas, duplicatas, recibos e quaisquer documentos 

              referentes &agrave;s transa&ccedil;&otilde;es ou aquisi&ccedil;&otilde;es 

              para constru&ccedil;&atilde;o, ser&atilde;o emitidos em nome do 

              condom&iacute;nio dos contratantes da constru&ccedil;&atilde;o;<BR>

              II - todas as contribui&ccedil;&otilde;es dos cond&ocirc;minos, 

              para qualquer fim relacionado com a constru&ccedil;&atilde;o, ser&atilde;o 

              depositadas em contas abertas em nome do condom&iacute;nio dos contratantes 

              em estabelecimentos banc&aacute;rios, as quais ser&atilde;o movimentadas 

              pela forma que for fixada no contrato.<BR>

              <BR>

              Art. 59 - No regime de constru&ccedil;&atilde;o por administra&ccedil;&atilde;o 

              ser&aacute; obrigat&oacute;rio constar do respectivo contrato o 

              montante do or&ccedil;amento do custo da obra, elaborado com estrita 

              observ&acirc;ncia dos crit&eacute;rios e normas referidos no inciso 

              II do Art. 53, e a data em que se iniciar&aacute; efetivamente a 

              obra.<BR>

              &sect; 1&ordm; - Nos contratos lavrados at&eacute; o t&eacute;rmino 

              das fun&ccedil;&otilde;es, este montante n&atilde;o poder&aacute; 

              ser inferior ao da estimativa atualizada, a que se refere o &sect; 

              3&ordm; do Art. 54.<BR>

              &sect; 2&ordm; - Nos contratos celebrados ap&oacute;s o t&eacute;rmino 

              das fun&ccedil;&otilde;es, este montante n&atilde;o poder&aacute; 

              ser inferior &agrave; &uacute;ltima revis&atilde;o efetivada na 

              forma do artigo seguinte.<BR>

              &sect; 3&ordm; - &Agrave;s transfer&ecirc;ncias e sub-roga&ccedil;&otilde;es 

              do contrato, em qualquer fase da obra, aplicar-se-&aacute; o disposto 

              neste artigo.<BR>

              <BR>

              Art. 60 - As revis&otilde;es da estimativa de custo da obra ser&atilde;o 

              efetuadas, pelo menos semestralmente, em comum entre a Comiss&atilde;o 

              de Representantes e o construtor. O contrato poder&aacute; estipular 

              que, em fun&ccedil;&atilde;o das necessidades da obra, sejam alter&aacute;veis 

              os esquemas de contribui&ccedil;&otilde;es quanto ao total, ao n&uacute;mero, 

              ao valor e &agrave; distribui&ccedil;&atilde;o no tempo das presta&ccedil;&otilde;es.<BR>

              Par&aacute;grafo &uacute;nico. Em caso de majora&ccedil;&atilde;o 

              de presta&ccedil;&otilde;es, o novo esquema dever&aacute; ser comunicado 

              aos contratantes, com anteced&ecirc;ncia m&iacute;nima de 45 (quarenta 

              e cinco) dias da data em que dever&atilde;o ser efetuados os dep&oacute;sitos 

              das primeiras presta&ccedil;&otilde;es alteradas.<BR>

              <BR>

              Art. 61 - A Comiss&atilde;o de Representantes ter&aacute; poderes 

              para, em nome de todos os contratantes e na forma prevista no contrato:<BR>

              a) examinar os balancetes organizados pelos construtores, dos recebimentos 

              e despesas do condom&iacute;nio dos contratantes, aprov&aacute;-los 

              ou impugn&aacute;-los, examinando a documenta&ccedil;&atilde;o respectiva;<BR>

              b) fiscalizar concorr&ecirc;ncias relativas &agrave;s compras dos 

              materiais necess&aacute;rios &agrave; obra ou aos servi&ccedil;os 

              a ela pertinentes;<BR>

              c) contratar, em nome do condom&iacute;nio, com qualquer cond&ocirc;mino, 

              modifica&ccedil;&otilde;es por ele solicitadas em sua respectiva 

              unidade, a serem administradas pelo construtor, desde que n&atilde;o 

              prejudiquem unidade de outro cond&ocirc;mino e n&atilde;o estejam 

              em desacordo com o parecer t&eacute;cnico do construtor;<BR>

              d) fiscalizar a arrecada&ccedil;&atilde;o das contribui&ccedil;&otilde;es 

              destinadas &agrave; constru&ccedil;&atilde;o;<BR>

              e) exercer as demais obriga&ccedil;&otilde;es inerentes a sua fun&ccedil;&atilde;o 

              representativa dos contratantes e fiscalizadora da constru&ccedil;&atilde;o, 

              e praticar todos os atos necess&aacute;rios ao funcionamento regular 

              do condom&iacute;nio.<BR>

              <BR>

              Art. 62 - Em toda publicidade ou propaganda escrita destinada a 

              promover a venda de incorpora&ccedil;&atilde;o com constru&ccedil;&atilde;o 

              pelo regime de administra&ccedil;&atilde;o em que conste pre&ccedil;o, 

              ser&atilde;o discriminados explicitamente o pre&ccedil;o da fra&ccedil;&atilde;o 

              ideal de terreno e o montante do or&ccedil;amento atualizado do 

              custo da constru&ccedil;&atilde;o, na forma dos artigos 59 e 60, 

              com a indica&ccedil;&atilde;o do m&ecirc;s a que se refere o dito 

              or&ccedil;amento e do tipo padronizado a que se vincule o mesmo.<BR>

              &sect; 1&ordm; - As mesmas indica&ccedil;&otilde;es dever&atilde;o 

              constar em todos os pap&eacute;is utilizados para a realiza&ccedil;&atilde;o 

              da incorpora&ccedil;&atilde;o, tais como cartas, propostas, escrituras, 

              contratos e documentos semelhantes.<BR>

              &sect; 2&ordm; - Esta exig&ecirc;ncia ser&aacute; dispensada nos 

              an&uacute;ncios &quot;classificados&quot;<BR>

              dos jornais.</FONT><BR>

              </FONT></P>

            <CENTER>

              <HR>

              <FONT SIZE="-1" FACE="Verdana,Arial,Times N"><B><FONT SIZE="3">Cap&iacute;tulo 

              IV</FONT></B><FONT SIZE="3"> - Das Infra&ccedil;&otilde;es</FONT></FONT> 

              <HR>

              <P ALIGN="LEFT"><FONT FACE="Verdana,Arial,Times N" SIZE="-1"><BR>

                <FONT COLOR="Black">Art. 63 - &Eacute; l&iacute;cito estipular 

                no contrato, sem preju&iacute;zo de outras san&ccedil;&otilde;es, 

                que a falta de pagamento, por parte do adquirente ou contratante, 

                de tr&ecirc;s presta&ccedil;&otilde;es do pre&ccedil;o da constru&ccedil;&atilde;o, 

                quer estabelecidas inicialmente, quer alteradas ou criadas posteriormente, 

                quando for o caso, depois de pr&eacute;via notifica&ccedil;&atilde;o 

                com o prazo de 10 (dez) dias para purga&ccedil;&atilde;o da mora, 

                implique na rescis&atilde;o do contrato, conforme nele se fixar, 

                ou que, na falta de pagamento pelo d&eacute;bito respondem os 

                direitos &agrave; respectiva fra&ccedil;&atilde;o ideal de terreno 

                e &agrave; parte constru&iacute;da adicionada, na forma abaixo 

                estabelecida, se outra forma n&atilde;o fixar o contrato.<BR>

                &sect; 1&ordm; - Se o d&eacute;bito n&atilde;o for liquidado no 

                prazo de 10 (dez) dias, ap&oacute;s solicita&ccedil;&atilde;o 

                da Comiss&atilde;o de Representantes, esta ficar&aacute;, desde 

                logo, de pleno direito, autorizada a efetuar, no prazo que fixar, 

                em p&uacute;blico leil&atilde;o anunciado pela forma que o contrato 

                previr, a venda, promessa de venda ou de cess&atilde;o, ou a cess&atilde;o 

                da quota de terreno e correspondente parte constru&iacute;da e 

                direitos, bem como a sub-roga&ccedil;&atilde;o do contrato de 

                constru&ccedil;&atilde;o.<BR>

                &sect; 2&ordm; - Se o maior lan&ccedil;o obtido for inferior ao 

                desembolso efetuado pelo inadimplente, para a quota do terreno 

                e a constru&ccedil;&atilde;o, despesas acarretadas e as percentagens 

                expressas no par&aacute;grafo seguinte, ser&aacute; realizada 

                nova pra&ccedil;a no prazo estipulado no contrato. Nesta segunda 

                pra&ccedil;a, ser&aacute; aceito o maior lan&ccedil;o apurado, 

                ainda que inferior &agrave;quele total (Vetado).<BR>

                &sect; 3&ordm; - No prazo de 24 (vinte e quatro) horas ap&oacute;s 

                a realiza&ccedil;&atilde;o do leil&atilde;o final, o condom&iacute;nio, 

                por decis&atilde;o un&acirc;nime de assembl&eacute;ia geral em 

                condi&ccedil;&otilde;es de igualdade com terceiros, ter&aacute; 

                prefer&ecirc;ncia na aquisi&ccedil;&atilde;o dos bens, caso em 

                que ser&atilde;o adjudicados ao condom&iacute;nio.<BR>

                &sect; 4&ordm; - Do pre&ccedil;o que for apurado no leil&atilde;o, 

                ser&atilde;o deduzidas as quantias em d&eacute;bito, todas as 

                despesas ocorridas, inclusive honor&aacute;rios de advogado e 

                an&uacute;ncios, e mais 5% (cinco por cento) a t&iacute;tulo de 

                comiss&atilde;o e 10% (dez por cento) de multa compensat&oacute;ria, 

                que reverter&atilde;o em benef&iacute;cio do condom&iacute;nio 

                de todos os contratantes, com exce&ccedil;&atilde;o do faltoso, 

                ao qual ser&aacute; entregue o saldo, se houver.<BR>

                &sect; 5&ordm; - Para os fins das medidas estipuladas neste artigo, 

                a Comiss&atilde;o de Representantes ficar&aacute; investida de 

                mandato irrevog&aacute;vel, isento do Imposto do Selo, na vig&ecirc;ncia 

                do contrato geral de constru&ccedil;&atilde;o da obra, com poderes 

                necess&aacute;rios para, em nome do cond&ocirc;mino inadimplente, 

                efetuar as citadas transa&ccedil;&otilde;es, podendo para este 

                fim fixar pre&ccedil;os, ajustar condi&ccedil;&otilde;es, sub-rogar 

                o arrematante nos direitos e obriga&ccedil;&otilde;es decorrentes 

                do contrato de constru&ccedil;&atilde;o e da quota de terreno 

                e constru&ccedil;&atilde;o; outorgar as competentes escrituras 

                e contratos, receber pre&ccedil;os, dar quita&ccedil;&otilde;es; 

                imitir o arrematante na posse do im&oacute;vel;<BR>

                transmitir dom&iacute;nio, direito e a&ccedil;&atilde;o; responder 

                pela evic&ccedil;&atilde;o; receber cita&ccedil;&atilde;o, propor 

                e variar de a&ccedil;&otilde;es; e tamb&eacute;m dos poderes ad 

                juditia, a serem substabelecidos a advogado legalmente habilitado.<BR>

                &sect; 6&ordm; - A morte, fal&ecirc;ncia ou concordata do cond&ocirc;mino 

                ou sua dissolu&ccedil;&atilde;o, se se tratar de sociedade, n&atilde;o 

                revogar&aacute; o mandato de que trata o par&aacute;grafo anterior, 

                o qual poder&aacute; ser exercido pela Comiss&atilde;o de Representantes 

                at&eacute; a conclus&atilde;o dos pagamentos devidos, ainda que 

                a unidade perten&ccedil;a a menor de idade.<BR>

                &sect; 7&ordm; - Os eventuais d&eacute;bitos, fiscais ou para 

                com a Previd&ecirc;ncia Social, n&atilde;o impedir&atilde;o a 

                aliena&ccedil;&atilde;o por leil&atilde;o p&uacute;blico. Neste 

                caso, ao cond&ocirc;mino somente ser&aacute; entregue o saldo, 

                se houver, desde que prove estar quite com o Fisco e a Previd&ecirc;ncia 

                Social, devendo a Comiss&atilde;o de Representantes, em caso contr&aacute;rio, 

                consignar judicialmente a import&acirc;ncia equivalente aos d&eacute;bitos 

                existentes, dando ci&ecirc;ncia do fato &agrave; entidade credora.<BR>

                &sect; 8&ordm; - Independentemente das disposi&ccedil;&otilde;es 

                deste artigo e seus par&aacute;grafos, e como penalidades preliminares, 

                poder&aacute; o contrato de constru&ccedil;&atilde;o estabelecer 

                a incid&ecirc;ncia de multas e juros de mora em caso de atraso 

                no dep&oacute;sito de contribui&ccedil;&otilde;es sem preju&iacute;zo 

                do disposto no par&aacute;grafo seguinte.&sect; 9&ordm; - O contrato 

                poder&aacute; dispor que o valor das presta&ccedil;&otilde;es, 

                pagas com atraso, seja corrig&iacute;vel em fun&ccedil;&atilde;o 

                da varia&ccedil;&atilde;o do &iacute;ndice geral de pre&ccedil;os 

                mensalmente publicado pelo Conselho Nacional de Economia, que 

                reflita as oscila&ccedil;&otilde;es do poder aquisitivo da moeda 

                nacional.<BR>

                &sect; 10 - O membro da Comiss&atilde;o de Representantes que 

                incorrer na falta prevista neste artigo estar&aacute; sujeito 

                &agrave; perda autom&aacute;tica do mandato e dever&aacute; ser 

                substitu&iacute;do segundo dispuser o contrato.<BR>

                <BR>

                Art. 64 - Os &oacute;rg&atilde;os de informa&ccedil;&atilde;o 

                e publicidade que divulgarem publicidade sem os requisitos exigidos 

                pelo &sect; 3&ordm; do Art. 32 e pelos artigos 56 e 62, desta 

                Lei, sujeitar-se-&atilde;o &agrave; multa em import&acirc;ncia 

                correspondente ao dobro do pre&ccedil;o pago pelo anunciante, 

                a qual reverter&aacute; em favor da respectiva Municipalidade.<BR>

                <BR>

                Art. 65 - &Eacute; crime contra a economia popular promover incorpora&ccedil;&atilde;o, 

                fazendo, em proposta, contratos, prospectos ou comunica&ccedil;&atilde;o 

                ao p&uacute;blico ou aos interessados, afirma&ccedil;&atilde;o 

                falsa sobre a constitui&ccedil;&atilde;o do condom&iacute;nio, 

                aliena&ccedil;&atilde;o das fra&ccedil;&otilde;es ideais do terreno 

                ou sobre a constru&ccedil;&atilde;o das edifica&ccedil;&otilde;es.<BR>

                Pena - reclus&atilde;o de 1 (um) a 4 (quatro) anos e multa de 

                5 (cinco) a 50 (cinq&uuml;enta) vezes o maior sal&aacute;rio m&iacute;nimo 

                legal vigente no Pa&iacute;s.<BR>

                &sect; 1&ordm; - Incorrem na mesma pena:<BR>

                I - o incorporador, o corretor e o construtor, individuais, bem 

                como os diretores ou gerentes de empresa coletiva incorporadora, 

                corretora ou construtora que, em proposta, contrato, publicidade, 

                prospecto, relat&oacute;rio, parecer, balan&ccedil;o ou comunica&ccedil;&atilde;o 

                ao p&uacute;blico ou aos cond&ocirc;minos, candidatos ou subscritores 

                de unidades, fizerem afirma&ccedil;&atilde;o falsa sobre a constitui&ccedil;&atilde;o 

                do condom&iacute;nio, aliena&ccedil;&atilde;o das fra&ccedil;&otilde;es 

                ideais ou sobre a constru&ccedil;&atilde;o das edifica&ccedil;&otilde;es;<BR>

                II - o incorporador, o corretor e o construtor individuais, bem 

                como os diretores ou gerentes de empresa coletiva, incorporadora, 

                corretora ou construtora que usar, ainda que a t&iacute;tulo de 

                empr&eacute;stimo, em proveito pr&oacute;prio ou de terceiro, 

                bens ou haveres destinados &agrave; incorpora&ccedil;&atilde;o 

                contratada por administra&ccedil;&atilde;o, sem pr&eacute;via 

                autoriza&ccedil;&atilde;o dos interessados.<BR>

                &sect; 2&ordm; - O julgamento destes crimes ser&aacute; de compet&ecirc;ncia 

                de ju&iacute;zo singular, aplicando-se os artigos 5&ordm;, 6&ordm; 

                e 7&ordm; da Lei n&ordm; 1.521, de 26 de dezembro de 1951.<BR>

                &sect; 3&ordm; - Em qualquer fase do procedimento criminal objeto 

                deste artigo, a pris&atilde;o do indiciado <BR>

                depender&aacute; sempre de mandado do ju&iacute;zo referido no 

                &sect; 2&ordm;.<BR>

                ________<BR>

                Nota:<BR>

                Acrescentado pelo art.11 da Lei n&ordm;4.864/65<BR>

                _______<BR>

                <BR>

                Art. 66 - S&atilde;o contraven&ccedil;&otilde;es relativas &agrave; 

                economia popular, pun&iacute;veis na forma do Art. 10 da Lei n&uacute;mero 

                1.521, de 26 de dezembro de 1951 :<BR>

                I - negociar o incorporador fra&ccedil;&otilde;es ideais de terreno, 

                sem previamente satisfazer &agrave;s exig&ecirc;ncias constantes 

                desta Lei;<BR>

                II - omitir o incorporador, em qualquer documento de ajuste, as 

                indica&ccedil;&otilde;es a que se referem os artigos 37 e 38, 

                desta Lei;<BR>

                III - deixar o incorporador, sem justa causa, no prazo do Art. 

                35 e ressalvada a hip&oacute;tese de seus par&aacute;grafos 2 

                e 3, de promover a celebra&ccedil;&atilde;o do contrato relativo 

                &agrave; fra&ccedil;&atilde;o ideal de terreno, do contrato de 

                constru&ccedil;&atilde;o ou de conven&ccedil;&atilde;o do condom&iacute;nio;<BR>

                IV - (Vetado);<BR>

                V - omitir o incorporador, no contrato, a indica&ccedil;&atilde;o 

                a que se refere o &sect; 5&ordm; do Art. 55, desta Lei;<BR>

                VI - paralisar o incorporador a obra, por mais de 30 (trinta) 

                dias, ou retardar-lhe excessivamente o andamento sem justa causa.<BR>

                Pena - multa de 5 (cinco) a 20 (vinte) vezes o maior sal&aacute;rio 

                m&iacute;nimo legal vigente no Pa&iacute;s.<BR>

                Par&aacute;grafo &uacute;nico. No caso de contratos relativos 

                a incorpora&ccedil;&otilde;es, de que n&atilde;o participe o incorporador, 

                responder&atilde;o solidariamente pelas faltas capituladas neste 

                artigo o construtor, o corretor, o propriet&aacute;rio ou titular 

                de direitos aquisitivos do terreno, desde que figurem no contrato, 

                com direito regressivo sobre o incorporador, se as faltas cometidas 

                lhe forem imput&aacute;veis.</FONT><BR>

                </FONT></P>

            </CENTER>

            <CENTER>

              <HR>

              <FONT SIZE="-1" FACE="Verdana,Arial,Times N"><B><FONT SIZE="3">Cap&iacute;tulo 

              V</FONT></B><FONT SIZE="3"> - Das Disposi&ccedil;&otilde;es Finais 

              e Transit&oacute;rias</FONT></FONT> 

              <HR>

              <FONT FACE="Verdana,Arial,Times N" SIZE="-1"><BR>

              </FONT> 

            </CENTER>

            <P ALIGN="LEFT"><FONT COLOR="Black" FACE="Verdana,Arial,Times N" SIZE="-1">Art. 

              67 - Os contratos poder&atilde;o consignar exclusivamente as cl&aacute;usulas, 

              termos ou condi&ccedil;&otilde;es vari&aacute;veis ou espec&iacute;ficas.<BR>

              &sect; 1&ordm; - As cl&aacute;usulas comuns a todos os adquirentes 

              n&atilde;o precisar&atilde;o figurar expressamente nos respectivos 

              contratos.<BR>

              &sect; 2&ordm; - Os contratos, no entanto, consignar&atilde;o obrigatoriamente 

              que as partes contratantes adotem e se comprometam a cumprir as 

              cl&aacute;usulas, termos e condi&ccedil;&otilde;es contratuais a 

              que se refere o par&aacute;grafo anterior, sempre transcritas, verbo 

              ad verbum, no respectivo cart&oacute;rio ou of&iacute;cio, mencionando, 

              inclusive, o n&uacute;mero do livro e das folhas do competente registro.<BR>

              &sect; 3&ordm; - Aos adquirentes, ao receberem os respectivos instrumentos, 

              ser&aacute; obrigatoriamente entregue c&oacute;pia, impressa ou 

              mimeografada, autenticada, do contrato-padr&atilde;o contendo as 

              cl&aacute;usulas, termos e condi&ccedil;&otilde;es referidas no 

              &sect; 1&ordm; deste artigo.<BR>

              &sect; 4&ordm; - Os Cart&oacute;rios de Registro de Im&oacute;veis, 

              para os devidos efeitos, receber&atilde;o dos incorporadores, autenticadamente, 

              o instrumento a que se refere o par&aacute;grafo anterior.<BR>

              <BR>

              Art. 68 - Os propriet&aacute;rios ou titulares de direito aquisitivo 

              sobre as terras rurais ou os terrenos onde pretendam construir ou 

              mandar construir habita&ccedil;&otilde;es isoladas para alien&aacute;-las 

              antes de conclu&iacute;das, mediante pagamento do pre&ccedil;o a 

              prazo, dever&atilde;o, previamente, satisfazer &agrave;s exig&ecirc;ncias 

              constantes no Art. 32, ficando sujeitos ao regime institu&iacute;do 

              nesta Lei para os incorporadores, no que lhes for aplic&aacute;vel.<BR>

              <BR>

              Art. 69 - O Poder Executivo baixar&aacute;, no prazo de 90 (noventa) 

              dias, regulamento sobre o registro no Registro de Im&oacute;veis 

              (Vetado).<BR>

              <BR>

              Art. 70 - A presente Lei entrar&aacute; em vigor na data de sua 

              publica&ccedil;&atilde;o, revogados o Decreto n&ordm; 5.481, de 

              25 de junho de 1928 e quaisquer disposi&ccedil;&otilde;es em contr&aacute;rio.<BR>

              Bras&iacute;lia, 16 de dezembro de 1964; 143&ordm; da Independ&ecirc;ncia 

              e 76&ordm; da Rep&uacute;blica. H. Castello Branco<BR>

              DOU 21/12/64</FONT></P>

          </DIV>

        </CENTER>

      </TD>

    </TR>

  </TABLE>



  <p>&nbsp;</p>

<BR>
 </p>
</DIV>
</td></tr>
</table>
</BODY>
</HTML>