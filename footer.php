
<div class="footer fix fl">
    <div class="auto clr ">
        <div class="boxMenuFooter hide-mobile">
            <ul id="mainMenuFooter" class="" style="width:590px;">
                <li class="fl">
                    <a href="historico.php" title="historico">Histórico</a>

                </li>
                <li class="fl separador"></li>
                <li class="fl">
                    <a href="busca.php?busca=L">Aluguéis</a>
                    <ul class="subMenu subMenuAlugueis">
                        <li><a href="busca.php?busca=L">Pesquisa imóveis</a></li>
                        <li><a href="aval_imo.php">Avaliação do imóvel</a></li>
                        <li><a href="devo_imo.php">Devolução do imóveis</a></li>
                        <li><a href="docs.php">Doc. para locação</a></li>
                        <li><a href="garantias.php">Garantias exigidas</a></li>
                        <!-- <li><a href="taxas.php">Taxas/Proprietários</a></li> -->
                        <li><a href="/usuario/">2º Via de boleto</a></li>
                    </ul>
                </li>
                <li class="fl separador"></li>
                <li class="fl">
                    <a href="busca.php?busca=V">Vendas </a>
                    <ul class="subMenu subMenuVendas">
                        <li><a href="busca.php?busca=V">Pesquisa imóveis</a></li>
                        <li><a href="aval_imov.php">Avaliação do imóvel</a></li>
                        <li><a href="responsa.php">Responsabilidade</a></li>				
                    </ul>
                </li>
                <li class="fl separador"></li>
                <li class="fl">
                    <a href="juridico.php">Jurídico </a>
                    <ul class="subMenu subMenuVendas">
                        <li><a href="juridico.php">Advogados coligados</a></li>
                        <li><a href="lei_inq.php">Lei do inquilinato</a></li>
                        <li><a href="lei_cond.php">Lei dos condomínios</a></li>				
                    </ul>
                </li>
                <li class="fl separador"></li>
                <li class="fl">
                    <a href="contato.php">Contato </a>
                    <ul class="subMenu subMenuVendas">
                        <li><a href="contato.php">Fale conosco</a></li>
                        <li><a href="curriculo.php">Envie seu currículo</a></li>							
                    </ul>
                </li>
                <li class="fl separador"></li>
                <li class="fl">
                    <a href="servicos.php">Serviços</a>
                    <ul class="subMenu subMenuVendas">
                        <li><a href="servicos.php">Obras</a></li>
                    </ul>
                </li>
                <li class="fl separador"></li>
                <li class="fl">
                    <a href="adm_cond.php">Condomínios</a>
                    <ul class="subMenu subMenuCond">
                        <li><a href="adm_cond.php">Proposta administração</a></li>
                        <li><a href="condominios.php">Nosos serviços</a></li>							
                        <li><a href="/usuario/">2ª via de boleto</a></li>							
                    </ul>
                </li>
            </ul>
        </div>
        <img src="img_index/raphael-imoveis-footer.png" alt="" class="fl hide-mobile" style="margin-left:30px;">
        <p class="fl lojasFooter hide-mobile">
            Rua Santa Cruz, 1992 | Pelotas-RS<br />
            Horários: De segunda à sexta das 08:30h até as 18:30h.<br />
            Sábados das 09:00h até o meio-dia.<br />
            Fone: (53) 3225.1100
        </p>
        <a href="http://designio.com.br" title="Designio Projetos Criativos" class="fr comparte hide-mobile"><img src="img_responsa/foot_bichano.png" alt="Designio"></a>
    </div>

    <div class="footerMob hide-desktop">
        <h3>Fale Conosco</h3>
        <p><img src="img_responsa/foot_end.png" alt=""> Rua Santa Cruz, 1992 | Pelotas - RS</p>
        <p><img src="img_responsa/foot_end.png" alt=""> Horários: De segunda à sexta das 08:30h até as 18:30h. Sábados das 09:00h até o meio-dia.</p>
        <p><img src="img_responsa/foot_tel.png" alt=""> Fone: (53) 3225.1100</p>
        <h3>Curta nossa página</h3>
        <div class="box_fb">
            
            <div class="fb-page" data-href="https://www.facebook.com/RaphaelImoveisPelotas" data-width="590" data-height="130" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/RaphaelImoveisPelotas"><a href="https://www.facebook.com/RaphaelImoveisPelotas">Raphael Imóveis</a></blockquote></div></div>
        </div>
        <p><img src="img_responsa/foot_creci.png" alt="">Creci 21507-J</p>

        <div class="faixa_footerMob">
            <p> Projetado por <a href="http://projetodesignio.com.br" target="_blank">Designio Projetos Criativos</a> <img src="img_responsa/foot_bichano.png" alt=""></p>
        </div>
    </div>


</div>




<div id="facebutton">
   <svg class="messenger-logo" xmlns="http://www.w3.org/2000/svg" width="24.8" height="25" viewBox="96 93 322 324"><path d="M257 93c-88.918 0-161 67.157-161 150 0 47.205 23.412 89.31 60 116.807V417l54.82-30.273C225.45 390.8 240.947 393 257 393c88.918 0 161-67.157 161-150S345.918 93 257 93zm16 202l-41-44-80 44 88-94 42 44 79-44-88 94z" fill="#0084ff"/></svg>

   <div id="facebox">
        <div class="fb-page" 
            data-href="https://www.facebook.com/110853852397561/" 
            data-tabs="messages" 
            data-width="400" 
            data-height="300" 
            data-small-header="true">
                     
            <div class="fb-xfbml-parse-ignore">
                <blockquote></blockquote>
            </div>
        </div>
    </div>
</div>

<script>
    var botao = document.getElementById("facebutton"),
        box = document.getElementById("facebox");

    botao.addEventListener("click", function(){
        box.classList.toggle("active");    
    });
</script>