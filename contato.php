<?php require "head.php"; ?>
</head>
<body id="internas" class="contato">
<h1 class="seo">Contato</h1>
<?php require "header.php"; ?>
<div class="content">
	<div class="centro636">
		<h2 class="bordLaranja">Fale Conosco</h2>
		<form name="contato" id="contato" action="envia.php" method="post">
			<fieldset>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td><label for="nome">Nome:</label></td>
					<td><input type="text" id="nome" name="nome" value="" style="width:200px;" /></td>
				</tr>
				<tr>
					<td><label for="email">E-mail:</label></td>
					<td><input type="text" id="email" name="email" value="" style="width:200px;" /></td>
				</tr>
				<tr>
					<td><label for="fone">Telefone:</label></td>
					<td><input type="text" id="fone" name="fone" value="" style="width:200px;" /></td>
				</tr>
				<tr>
					<td><label for="mensagem">Mensagem:</label></td>
					<td><textarea cols="1" rows="1" name="mensagem" id="mensagem" style="width:400px;height:100px;" ></textarea></td>
				</tr>
				<tr class="hide-mobile" >
					<td colspan="2" align="right">
						<input name="enviar" id="enviar" type="submit" value="Enviar"/>
						<input name="reset" id="reset" type="reset" value="Limpar"/>
					</td>
				</tr>
				
			</table>	
				<input class="botao hide-desktop" name="enviar" id="enviar" type="submit" value="Enviar"/>
				
			</fieldset>
		</form>
	</div>	
</div>
</div>
<?php require "footer.php"; ?>
</body>
</html>
