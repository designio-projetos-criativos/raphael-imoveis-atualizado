<? 
session_start();
require_once "../../connection/conexao.php";
require_once "../inc/http_comply.php";
require_once "../inc/corta_imagem.php";
include_once "../inc/fckeditor_noimage/fckeditor.php"; //editor
include ("../layout/inc/cabecalho.php");
include ("../layout/inc/topo.php");

$strMethod    = getMethod();

$msg  = $_GET['msg'];
$erro = $_GET['erro'];


if ($erro == "true" ) {

	$galeria_nome  = $_SESSION['galeria_nome'];
	$galeria_data  = $_SESSION['galeria_data'];
}
else {

	$galeria_nome  = "";
	$galeria_data  = date("d/m/Y");
}


if ($strMethod == "POST") {

	$galeria_codigo = trim(getPost("galeria_codigo"));
	$galeria_tipo   = getPost("galeria_tipo");
	$galeria_descricao = getPost("galeria_descricao");
	$galeria_descricao = strip_tags($galeria_descricao, '<a></a><p><br></p></br><br/><strong></strong><em></em><u></u>');	
	$galeria_bairro   = getPost("galeria_bairro");
	$galeria_valor    = getPost("galeria_valor");
	$galeria_destaque = getPost("galeria_destaque");
	
	if($galeria_valor != ""){
		$galeria_valor = str_replace(".", "", $galeria_valor);
		$galeria_valor = str_replace(",", ".", $galeria_valor);
	}

	$galeria_data  = date("Y-m-d");

	$strGaleriagaleria_fotos = " INSERT INTO galerias ( 
													galeria_codigo,
													galeria_tipo,
													galeria_descricao,
													galeria_bairro,
													galeria_valor,
													galeria_destaque,
													galeria_data
													)
						 VALUES ( 
								'$galeria_codigo',
								'$galeria_tipo',
								'$galeria_descricao',
								'$galeria_bairro',
								'$galeria_valor',
								'$galeria_destaque',
								'$galeria_data'
								)";
			
	$rstGaleriagaleria_fotos = mysql_query($strGaleriagaleria_fotos) or die(mysql_error());
	
	echo "<script>document.location = 'editar_listar.php';</script>";
}	
	
?>
<script type="text/javascript" language="JavaScript" src="../js/script.js"></script>
<table height="78%" border="0" cellspacing="0" cellpadding="0" >
<tr>
<td height="147" valign="top" width="227" >
	<table border="0" cellspacing="0" cellpadding="0">
	<tr>
    <? include ("../inc/itens_menu.php");?>
	</tr>
	</table>
</td>
<td valign="top" width="549">
	<table border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td valign="top">
		<table width="530" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="../../images/pixel.gif" width="1" height="33"></td>
		</tr>
		<tr>
		<td>
			<table width="540"  border="0" cellpadding="0" cellspacing="2" class="bordaVerde">
			<tr>
			<td align="left">
				<table height="5" border="0" cellpadding="0" cellspacing="0" >
				<tr>
				<td width="60" align="center" class="bordaVerde barraTit">Inserir</td>
				<td width="60" align="center" background="../layout/img/bgr_menu.jpg" class='bordaVerde ver10cinzaesc'><a href="editar_listar.php" class="ver10cinzaesc">Gerenciar</a></td>
				</tr>
				</table>
				<table width="533"  border="0" cellpadding="0" cellspacing="0">
				<tr class="barraTit">
				<td>Inserir Im&oacute;veis - Vendas</td>
				</tr>
				<tr class="bordaSubtit">
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td height="80" align="left" valign="top" class="corpo_dentro_tabs">
					<form name="formGaleriagaleria_fotos" id="formGaleriagaleria_fotos" action="inserir.php" method="post">
					<table  border="0" cellspacing="0" cellpadding="0" class="ver10cinzaesc">
					<tr>
					<td height="24" align="left" class="ver10cinzaesc">C&oacute;digo</td>
					<td><input name="galeria_codigo" type="text" class="ver10cinzaesc" style="width:372px;" value="<?php echo $galeria_nome ?>" size="150" maxlength="255" /></td>
					</tr>
					<tr>
					<td height="24" align="left" class="ver10cinzaesc">Tipo</td>
					<td><select size="1" name="galeria_tipo" class="ver10cinzaesc" style="width:372px">
      					<option value="0">Escolha uma op&ccedil;&atilde;o</option>
        				<?
        		        $sql = "select tipo_id, tipo_nome from galerias_tipos order by tipo_nome";
        				$resultado = mysql_query($sql) or die (mysql_error());
        				while ($linha = mysql_fetch_array($resultado)){
              				$tipo_id   = $linha['tipo_id'];
              				$tipo_nome = $linha['tipo_nome'];
              
              				print "<option value=".$tipo_id.">".$tipo_nome."</option>";
              			}
        				?>
						</select></td>
					</tr>					
					<tr>
					<td height="24" align="left" class="ver10cinzaesc" valign="middle">Descri&ccedil;&atilde;o</td>
					<td><?php
  						$oFCKeditor = new FCKeditor('galeria_descricao');
						$oFCKeditor->BasePath = "../inc/fckeditor_noimage/";
					  	$oFCKeditor->Value    = "";
						$oFCKeditor->Width    = 375;
						$oFCKeditor->Height   = 200;
						echo $oFCKeditor->CreateHtml(); ?></td>
					</tr>
					<tr>
					<td height="24" align="left" class="ver10cinzaesc">Bairro</td>
					<td><select size="1" name="galeria_bairro" class="ver10cinzaesc" style="width:372px">
      					<option value="0">Escolha uma op&ccedil;&atilde;o</option>
        				<?
        		        $sql = "select bairro_id, bairro_nome from galerias_bairros order by bairro_nome";
        				$resultado = mysql_query($sql) or die (mysql_error());
        				while ($linha = mysql_fetch_array($resultado)){
              				$bairro_id   = $linha['bairro_id'];
              				$bairro_nome = $linha['bairro_nome'];
              
              				print "<option value=".$bairro_id.">".$bairro_nome."</option>";
              			}
        				?>
						</select></td>
					</tr>			
					<tr>
					<td height="24" align="left" class="ver10cinzaesc">Valor (R$)</td>
					<td><input name="galeria_valor" type="text" class="ver10cinzaesc" style="width:372px;" value="<?php echo $produto_valor ?>" maxlength="255" onblur="valor_monetario(this,15);" onkeyup="valor_monetario(this,15);"  /></td>
					</tr>		
<tr>
					<td height="24" align="left" class="ver10cinzaesc">Destaque</td>
					<td>
						<input type="checkbox" value="1" name="galeria_destaque">&nbsp;&nbsp;
					</td>
					</tr>															
					<tr>
					<td width="75" height="24" align="left" class="ver10cinzaesc">&nbsp;</td>
					<td width="380" align="right" class="ver10cinzaesc" ><div align="left"><?php echo $msg ?>
						<div align="right"><input name="Submit2" type="submit" class="ver10cinzaesc" value="Ok" /></div>
					</div></td>
					</tr>
					</table>
					</form>
				</td>
				</tr>
				</table>
			</td>
			</tr>
			</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
</td>
</tr>
<tr>
<td colspan="3" height="100%"></td>
</tr>	 
</table>
<? include "../layout/inc/rodape.php";?>
</body>
</html>
