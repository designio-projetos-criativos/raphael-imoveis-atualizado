<?php 
session_start();
require_once "../../connection/conexao.php";
require_once "../inc/http_comply.php";
require_once "../inc/corta_imagem.php";
include('../inc/redimensiona.php');

include ("../layout/inc/cabecalho.php");
include ("../layout/inc/topo.php");


$strMethod = getMethod();
$diretorio = "../../uploads/galeria_fotos/";  //pasta destino - no esquecer de dar CHMOD 777

if ( $strMethod == "POST" ) {

	$galeria_id    = getPost("galeria_id");
	$foto_id       = getPost("foto_id");
	$foto_legenda  = getPost("foto_legenda");
	$foto_arquivo  = $_FILES['foto_arquivo']['name'];

	if ($foto_arquivo != "") {

		$array_arq = explode(".", $foto_arquivo); 
		$extensao_arquivo = $array_arq[1]; 
	
		if (($extensao_arquivo == "jpg") or ($extensao_arquivo == "JPG")) {
			$arquivo = "ok";
		}
		else {
		   	$msg = "ATEN��O: Envie somente arquivos .jpg";
			echo "<script>document.location = 'editarFoto.php?foto_id=$foto_id&erro=true&msg=$msg';</script>";
			exit();
		}
	}

	if ($arquivo == "ok") {	

		$sql = "SELECT foto_arquivo FROM galerias_fotos WHERE foto_id = '$foto_id'";
		$resultado = mysql_query($sql) or die (mysql_error());
		$linha = mysql_fetch_array($resultado);
		$imagematual  = $linha['foto_arquivo'];			
      		
		if ($imagematual != "") {

			$del_imagem = "../../uploads/galeria_fotos/$imagematual";
			unlink($del_imagem);
			
			$del_thumb      = split("\.", $imagematual);
			
			$imagematual2   = $del_thumb[0]."_index.".$del_thumb[1];				
			$del_imagem_thumb = "../../uploads/galeria_fotos/$imagematual2";
			unlink($del_imagem_thumb);
			
			$imagematual3   = $del_thumb[0]."_detalhe.".$del_thumb[1];				
			$del_imagem_thumb = "../../uploads/galeria_fotos/$imagematual3";
			unlink($del_imagem_thumb);
			
			$imagematual4   = $del_thumb[0]."_mini.".$del_thumb[1];				
			$del_imagem_thumb = "../../uploads/galeria_fotos/$imagematual4";
			unlink($del_imagem_thumb);
		}
			
		$nomearq = $foto_id.".".strtolower($extensao_arquivo);
			
		// Upload da imagem //
		$arquivo_e  = $_FILES['foto_arquivo']['tmp_name'];
		$arquivo_d  = $diretorio.$nomearq;

		if (move_uploaded_file($arquivo_e, $arquivo_d)) {
		corta_imagem($diretorio, $nomearq, 72, 55, "_index");   // imagem ampliada
		corta_imagem($diretorio, $nomearq, 420, 315, "_detalhe"); // capa da galeria
		corta_imagem($diretorio, $nomearq, 56, 41, "_mini"); // foto da galeria
		}			

		$strFotos = " UPDATE galerias_fotos SET foto_arquivo = '$nomearq' WHERE foto_id = '$foto_id' ";
		$rstFotos = mysql_query($strFotos) or die(mysql_error());
	}
	
	$strFotos = " UPDATE galerias_fotos SET foto_legenda = '$foto_legenda' WHERE foto_id = '$foto_id' ";
	$rstFotos = mysql_query($strFotos) or die(mysql_error());

	echo "<script>document.location = 'visualizar.php?galeria_id=$galeria_id';</script>";
}


if ( isset($_GET['foto_id']) ) {

	$foto_id = getQuery("foto_id");

	$strFotos = " SELECT *
				  FROM galerias_fotos
				  LEFT JOIN galerias ON galerias_fotos.galeria_id = galerias.galeria_id
				  WHERE foto_id = '$foto_id'";

	$rstFotos = mysql_query($strFotos) or die(mysql_error());


	while ( $intLinha = mysql_fetch_array($rstFotos) ) {

		$galeria_id   = $intLinha["galeria_id"];
		$galeria_nome = $intLinha["galeria_nome"];
		$foto_legenda = $intLinha["foto_legenda"];
		
		$imagem       = split("\.", $intLinha['foto_arquivo']);
		$foto_arquivo = "../../uploads/galeria_fotos/".$imagem[0]."_index.".$imagem[1];
	}
}
else {
	echo "<script>document.location = 'editar_listar.php';</script>";
}
?>

<script type="text/javascript" language="JavaScript" src="../js/script.js"></script>
<table height="78%" border="0" cellspacing="0" cellpadding="0" >
<tr>
<td height="147" valign="top" width="227" >
	<table border="0" cellspacing="0" cellpadding="0">
	<tr>
    <? include ("../inc/itens_menu.php");?>
	</tr>
	</table>
</td>
<td valign="top" width="549">
	<table border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td valign="top">
		<table width="530" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="../../images/pixel.gif" width="1" height="33"></td>
		</tr>
		<tr>
		<td>
			<table width="540"  border="0" cellpadding="0" cellspacing="2" class="bordaVerde">
			<tr>
			<td align="left">
				<table height="5" border="0" cellpadding="0" cellspacing="0" >
				<tr>
				<td width="60" align="center" background="../layout/img/bgr_menu.jpg" class='bordaVerde ver10cinzaesc'><a href="inserir.php" class="ver10cinzaesc">Inserir</a></td>
				<td width="60" align="center" class="bordaVerde barraTit">Editar</td>
				<td width="60" align="center" background="../layout/img/bgr_menu.jpg" class='bordaVerde ver10cinzaesc'><a href="editar_listar.php" class="ver10cinzaesc">Gerenciar</a></td>
				</tr>
				</table>
				<table width="533"  border="0" cellpadding="0" cellspacing="0">
				<tr class="barraTit">
				<td>Editar Foto</td>
				</tr>
				<tr class="bordaSubtit">
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td height="80" align="left" valign="top" class="corpo_dentro_tabs">
					<form id="formFoto" name="formFoto" enctype="multipart/form-data" method="post" action="editarFoto.php">
                    <input name="galeria_id" type="hidden" value="<? echo $galeria_id ?>" />
                    <input name="foto_id" type="hidden" value="<? echo $foto_id ?>" />
					<table  border="0" cellspacing="0" cellpadding="0" class="ver10cinzaesc">
					<tr>
					<td height="24" align="left" class="ver10cinzaesc" valign="center">Imagem</td>
					<td>
						<?php
						if (!empty($foto_arquivo)) {
						?>
						<img src="<?php echo $foto_arquivo ?>"><br>
						<?php
						}
						?>
						<input name="foto_arquivo" type="file" id="foto_arquivo" class="ver10cinzaesc" style="width:300px;" />
					</td>
					</tr>
					<tr>
					<td eight="24" align="left" class="ver10cinzaesc">Local</td>
					<td>
						<input name="foto_legenda" type="text" id="foto_legenda" value="<?php echo $foto_legenda ?>" class="ver10cinzaesc" style="width:300px;" />
					</td>
					</tr>
					<tr>
					<td width="75" height="24" align="left" class="ver10cinzaesc">&nbsp;</td>
					<td width="380" align="right" class="ver10cinzaesc" ><div align="left"><?php echo $msg ?>
						<div align="right"><input name="Submit2" type="submit" class="ver10cinzaesc" value="Ok" /></div>
					</div></td>
					</tr>
					</table>
					</form>
				</td>
				</tr>
				</table>
			</td>
			</tr>
			</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
</td>
</tr>
<tr>
<td colspan="3" height="100%"></td>
</tr>	 
</table>
<? include "../layout/inc/rodape.php";?>
</body>
</html>
