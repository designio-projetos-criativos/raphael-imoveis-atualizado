<?php 
session_start();
require_once "../../connection/conexao.php";
require_once "../inc/http_comply.php";
require_once "../inc/corta_imagem.php";
include_once "../inc/fckeditor_noimage/fckeditor.php"; //editor
include ("../layout/inc/cabecalho.php");
include ("../layout/inc/topo.php");
include('../inc/redimensiona.php');

$strMethod = getMethod();

if ( $strMethod == "POST" ) {

	$galeria_id    = getPost("galeria_id");
	$galeria_codigo = trim(getPost("galeria_codigo"));
	$galeria_tipo   = getPost("galeria_tipo");
    $galeria_tipo_aluguel   = getPost("galeria_tipo_aluguel");
	$galeria_descricao = getPost("galeria_descricao");
	$galeria_descricao = strip_tags($galeria_descricao, '<a></a><p><br></p></br><br/><strong></strong><em></em><u></u>');	
	$galeria_bairro   = getPost("galeria_bairro");
	$galeria_valor    = getPost("galeria_valor");
	$galeria_endereco = getPost("galeria_endereco");
	$galeria_dormitorios = getPost("galeria_dormitorios");	
	$galeria_destaque = getPost("galeria_destaque");	
	
	if($galeria_valor != ""){
		$galeria_valor = str_replace(".", "", $galeria_valor);
		$galeria_valor = str_replace(",", ".", $galeria_valor);
	}


	$strGaleriagaleria_fotos = "UPDATE galerias2
								SET 
								galeria_codigo = '$galeria_codigo',
								galeria_tipo = '$galeria_tipo',
                                galeria_tipo_aluguel = '$galeria_tipo_aluguel',
								galeria_descricao = '$galeria_descricao', 
								galeria_bairro = '$galeria_bairro',
								galeria_endereco = '$galeria_endereco',
								galeria_dormitorios = '$galeria_dormitorios',
								galeria_destaque = '$galeria_destaque',
								galeria_valor = '$galeria_valor'
								WHERE galeria_id = $galeria_id ";
							
	$rstGaleriagaleria_fotos = mysql_query($strGaleriagaleria_fotos) or die(mysql_error());
						
}


if ( isset($_GET['galeria_id']) ) {

	$galeria_id = getQuery("galeria_id");

	$strGaleriagaleria_fotos = " SELECT * FROM galerias2
								 WHERE galeria_id = $galeria_id";
	$rstGaleriagaleria_fotos = mysql_query($strGaleriagaleria_fotos) or die(mysql_error());

	while ( $intLinha = mysql_fetch_array($rstGaleriagaleria_fotos) ) {

		$galeria_codigo  = $intLinha["galeria_codigo"];
		$galeria_valor   = $intLinha["galeria_valor"];
		$galeria_bairro  = $intLinha["galeria_bairro"];
		$galeria_tipo    = $intLinha["galeria_tipo"];
		$galeria_descricao = $intLinha["galeria_descricao"];
		$galeria_endereco  = $intLinha["galeria_endereco"];
		$galeria_dormitorios = $intLinha["galeria_dormitorios"];
		$galeria_destaque  = $intLinha["galeria_destaque"];
        $galeria_tipo_aluguel = $intLinha['galeria_tipo_aluguel'];

		$data          = @split("-", $intLinha["galeria_data"]);
		$galeria_data  = $data[2]."/".$data[1]."/".$data[0]; 
		
		$galeria_valor = number_format($galeria_valor, 2, ",", "."); 		
	}
}
else {
	echo "<script>document.location = 'editar_listar.php';</script>";
}
?>

<script type="text/javascript" language="JavaScript" src="../js/script.js"></script>
<table height="78%" border="0" cellspacing="0" cellpadding="0" >
<tr>
<td height="147" valign="top" width="227" >
	<table border="0" cellspacing="0" cellpadding="0">
	<tr>
    <? include ("../inc/itens_menu.php");?>
	</tr>
	</table>
</td>
<td valign="top" width="549">
	<table border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td valign="top">
		<table width="530" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="../../images/pixel.gif" width="1" height="33"></td>
		</tr>
		<tr>
		<td>
			<table width="540"  border="0" cellpadding="0" cellspacing="2" class="bordaVerde">
			<tr>
			<td align="left">
				<table height="5" border="0" cellpadding="0" cellspacing="0" >
				<tr>
				<td width="60" align="center" background="../layout/img/bgr_menu.jpg" class='bordaVerde ver10cinzaesc'><a href="inserir.php" class="ver10cinzaesc">Inserir</a></td>
				<td width="60" align="center" class="bordaVerde barraTit">Editar</td>
				<td width="60" align="center" background="../layout/img/bgr_menu.jpg" class='bordaVerde ver10cinzaesc'><a href="editar_listar.php" class="ver10cinzaesc">Gerenciar</a></td>
				</tr>
				</table>
				<table width="533"  border="0" cellpadding="0" cellspacing="0">
				<tr class="barraTit">
				<td>Editar Im&oacute;veis - Alugu&eacute;is</td>
				</tr>
				<tr class="bordaSubtit">
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td height="80" align="left" valign="top" class="corpo_dentro_tabs">
					<form id="formGaleriagaleria_fotos" name="formGaleriagaleria_fotos" enctype="multipart/form-data" method="post" action="editar.php">
                    <input name="galeria_id" type="hidden" value="<? echo $galeria_id ?>" />
					<table  border="0" cellspacing="0" cellpadding="0" class="ver10cinzaesc">
					<tr>
					<td height="24" align="left" class="ver10cinzaesc">C&oacute;digo</td>
					<td><input name="galeria_codigo" type="text" class="ver10cinzaesc" style="width:372px;" value="<?php echo $galeria_codigo ?>" size="150" maxlength="255" /></td>
					</tr>
					<tr>
					<td height="24" align="left" class="ver10cinzaesc">Tipo</td>
					<td><select size="1" name="galeria_tipo" class="ver10cinzaesc" style="width:372px">
      					<option value="0">Escolha uma op&ccedil;&atilde;o</option>
        				<?
        		        $sql = "select tipo_id, tipo_nome from galerias_tipos2 order by tipo_nome";
        				$resultado = mysql_query($sql) or die (mysql_error());
        				while ($linha = mysql_fetch_array($resultado)){
              				$tipo_id   = $linha['tipo_id'];
              				$tipo_nome = $linha['tipo_nome'];
							$sel = "";
              				if($tipo_id == $galeria_tipo) $sel = "selected";
              				print "<option value=".$tipo_id." $sel>".$tipo_nome."</option>";
              			}
        				?>
						</select></td>
					</tr>
                    <?
                        if ($galeria_tipo_aluguel == "Temporada")
                        {
                            $temporada_cmb_selected = "selected";
                        }elseif ($galeria_tipo_aluguel == "Anual")
                        {
                            $anual_cmb_selected = "selected";
                        }else
                        {
                            $option_null_selected = "selected";
                        }
                    ?>
                    <tr>
					<td height="24" align="left" class="ver10cinzaesc" style="padding-right: 20px;">Tipo de Aluguel</td>
					<td><select size="1" name="galeria_tipo_aluguel"  id="galeria_tipo_aluguel" class="ver10cinzaesc" style="width:372px">
                        <option value="0" <?=$option_null_selected?>>Escolha uma op��o</option>
      					<option value="temporada" <?=$temporada_cmb_selected?>>Temporada</option>
                        <option value="anual" <?=$anual_cmb_selected?>>Anual</option>
						</select></td>
					</tr>	
					<tr>
					<td height="24" align="left" class="ver10cinzaesc">Endere&ccedil;o</td>
					<td><input name="galeria_endereco" type="text" class="ver10cinzaesc" style="width:372px;" value="<?php echo $galeria_endereco ?>" size="150" maxlength="255" /></td>
					</tr>	
					<tr>
					<td height="24" align="left" class="ver10cinzaesc">Bairro</td>
					<td><select size="1" name="galeria_bairro" class="ver10cinzaesc" style="width:372px">
      					<option value="0">Escolha uma op&ccedil;&atilde;o</option>
        				<?
        		        $sql = "select bairro_id, bairro_nome from galerias_bairros2 order by bairro_nome";
        				$resultado = mysql_query($sql) or die (mysql_error());
        				while ($linha = mysql_fetch_array($resultado)){
              				$bairro_id   = $linha['bairro_id'];
              				$bairro_nome = $linha['bairro_nome'];
							$sel = "";
              				if($bairro_id == $galeria_bairro) $sel = "selected";              
              				print "<option value=".$bairro_id." $sel>".$bairro_nome."</option>";
              			}
        				?>
						</select></td>
					</tr>
					<tr>
					<td height="24" align="left" class="ver10cinzaesc">N&deg; dormit&oacute;rios</td>
					<td><input name="galeria_dormitorios" type="text" class="ver10cinzaesc" style="width:372px;" value="<?php echo $galeria_dormitorios ?>" size="150" maxlength="255" /></td>
					</tr>																			
					<tr>
					<td height="24" align="left" class="ver10cinzaesc" valign="middle">Descri&ccedil;&atilde;o</td>
					<td><?php
  						$oFCKeditor = new FCKeditor('galeria_descricao');
						$oFCKeditor->BasePath = "../inc/fckeditor_noimage/";
					  	$oFCKeditor->Value    = "$galeria_descricao";
						$oFCKeditor->Width    = 375;
						$oFCKeditor->Height   = 200;
						echo $oFCKeditor->CreateHtml(); ?></td>
					</tr>			
					<tr>
					<td height="24" align="left" class="ver10cinzaesc">Valor (R$)</td>
					<td><input name="galeria_valor" type="text" class="ver10cinzaesc" style="width:372px;" value="<?php echo $galeria_valor ?>" maxlength="255" onblur="valor_monetario(this,15);" onkeyup="valor_monetario(this,15);"  /></td>
					</tr>	
<tr>
					<td height="24" align="left" class="ver10cinzaesc">Destaque</td>
					<td>
						<input type="checkbox" value="1" <? if($galeria_destaque==1){ ?>checked="checked"<? }?> name="galeria_destaque">&nbsp;&nbsp;
					</td>
					</tr>																
					<tr>
					<td width="75" height="24" align="left" class="ver10cinzaesc">&nbsp;</td>
					<td width="380" align="right" class="ver10cinzaesc" ><div align="left"><?php echo $msg ?>
						<div align="right"><input name="Submit2" type="submit" class="ver10cinzaesc" value="Ok" /></div>
					</div></td>
					</tr>
					</table>
					</form>
				</td>
				</tr>
				</table>
			</td>
			</tr>
			</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
</td>
</tr>
<tr>
<td colspan="3" height="100%"></td>
</tr>	 
</table>
<? include "../layout/inc/rodape.php";?>
</body>
</html>
