<?php 
session_start();
require_once "../../connection/conexao.php";
require_once "../inc/http_comply.php";
require_once "../inc/corta_imagem.php";

include ("../layout/inc/cabecalho.php");
include ("../layout/inc/topo.php");

if ($strMethod == "POST") {
	echo "<script>document.location = 'editar_listar.php';</script>";
}

$galeria_id = $_GET['galeria_id'];

?>
<script type="text/javascript" language="JavaScript" src="../js/script.js"></script>
<link href="css/default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="swfupload/swfupload.js"></script>
<script type="text/javascript" src="js/swfupload.queue.js"></script>
<script type="text/javascript" src="js/fileprogress.js"></script>
<script type="text/javascript" src="js/handlers.js"></script>
<script type="text/javascript">
		var swfu;

		window.onload = function() {
			var settings = {
				flash_url : "swfupload/swfupload.swf",
				upload_url: "upload.php",
				post_params: {"PHPSESSID" : "<?php echo session_id(); ?>",
                              "galeria_id" : "<?php echo $galeria_id; ?>"
                },
				file_size_limit : "100 MB",
                file_types : "*.jpg",
				file_types_description : "Imagens JPG",
				file_upload_limit : 200,
				file_queue_limit : 0,
				custom_settings : {
					progressTarget : "fsUploadProgress",
					cancelButtonId : "btnCancel"
				},
				debug: false,

				// Button settings
				button_image_url: "images/TestImageNoText_65x29.png",
				button_width: "65",
				button_height: "29",
				button_placeholder_id: "spanButtonPlaceHolder",
				button_text: '<span class="theFont">Enviar</span>',
				button_text_style: ".theFont { font-size: 16;color: #FFFFFF; }",
				button_text_left_padding: 12,
				button_text_top_padding: 3,

				// The event handler functions are defined in handlers.js
				file_queued_handler : fileQueued,
				file_queue_error_handler : fileQueueError,
				file_dialog_complete_handler : fileDialogComplete,
				upload_start_handler : uploadStart,
				upload_progress_handler : uploadProgress,
				upload_error_handler : uploadError,
				upload_success_handler : uploadSuccess,
				upload_complete_handler : uploadComplete,
				queue_complete_handler : queueComplete	// Queue plugin event
			};

			swfu = new SWFUpload(settings);
	     };
	</script>
<table height="78%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="147" valign="top" width="227" >
	<table border="0" cellspacing="0" cellpadding="0">
	<tr>
    <? include ("../inc/itens_menu.php");?>
	</tr>
	</table>
</td>
<td valign="top" width="549">
	<table border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td valign="top">
		<table width="530" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="../../images/pixel.gif" width="1" height="33"></td>
		</tr>
		<tr>
		<td>
			<table width="540"  border="0" cellpadding="0" cellspacing="2" class="bordaVerde">
			<tr>
			<td align="left">
				<table height="5" border="0" cellpadding="0" cellspacing="0" >
				<tr>
				<td width="60" align="center" class="bordaVerde barraTit">Inserir</td>
				<td width="60" align="center" background="../layout/img/bgr_menu.jpg" class='bordaVerde ver10cinzaesc'><a href="editar_listar.php" class="ver10cinzaesc">Gerenciar</a></td>
				</tr>
				</table>
				<table width="533"  border="0" cellpadding="0" cellspacing="0">
				<tr class="barraTit">
				<td>Inserir fotos</td>
				</tr>
				<tr class="bordaSubtit">
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td height="80" align="left" valign="top" class="corpo_dentro_tabs">
                <!--div  swfupload -->
                <div id="content">
                <h2></h2>
                <form id="form1" action="addFoto.php" method="post" enctype="multipart/form-data">
		        <p></p>

                      <div class="fieldset flash" id="fsUploadProgress">
			          <span class="legend">Imagens</span>
			          </div>
		        <div id="divStatus">0 Arquivos Enviados</div>
			         <div>
              		 <span id="spanButtonPlaceHolder"></span>
                   	 <input id="btnCancel" type="button" value="Cancelar Uploads" onclick="swfu.cancelQueue();" disabled="disabled" style="margin-left: 2px; font-size: 8pt; height: 29px;" />
			         </div>
             	</form>
                </div>
                <!-- fim div swfupload -->
                </td>
				</tr>
				</table>
			</td>
			</tr>
			</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
</td>
</tr>
<tr>
<td colspan="3" height="100%"></td>
</tr>	 
</table>
<? include "../layout/inc/rodape.php";?>
</body>
</html>
