<?
	session_start();
    require_once "../../suporte/classes/objetos.php";
    require_once "../../suporte/classes/cabecalho.php";
    
	$strPagAtual = "menus/inserir.php";
	$blnScript = false;
    
    if ( $objHttp->getMethod() == "POST" ) {
        
        $menu_nome   = $objHttp->getPost("menu_nome");
	    $perfil      = $objHttp->getPost("perfil");

        $objConsulta->Campo("menu_nome",  $menu_nome  );
        
        $intID = $objConsulta->Insert("manager_menus");
		
		for ( $i = 0; $i < sizeof($perfil); $i++ ) {
			
			$objConsulta->Campo("menu_id",  $intID  );
			$objConsulta->Campo("perfil_id",  $perfil  );
			
			$objConsulta->Insert("manager_menus_perfis");
		}

		
        $objHttp->Redirect("editar_listar.php");
    }


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>RSWEB Manager :::::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../_interface/style/ie.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript" src="../_interface/js/default.js"></script>
<?
if ($blnScript) {
    header("Location: editar_listar.php");
}
?>
</head>

<body background="../_interface/img/bgr_2px_azulclaro.gif" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="25" background="../_interface/img/bgr_topo_claro.jpg">&nbsp;</td>
<td width="770">
<? //Usado com iframe, inc/cabecalho.php ?>
<iframe name="cabecalho" frameborder=0 marginheight=0 marginwidth=0 src="../_interface/inc/php/cabecalho.php" scrolling="no" width="770" height="25"></iframe>
</td>
<td height="25" background="../_interface/img/bgr_topo_claro.jpg">&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td background="../_interface/img/bgr_2px_azulescuro.gif" class="corpo">
				<table width="100%"  border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
				<tr>
				<td height="90"><? require_once('../inc/logo.php'); ?></td>
				</tr>
				<tr>
				<td><? require_once('../inc/menu.php'); ?></td>
				</tr>
				<tr>
				<td class="espacamento">
								<table width="100%"  border="0" cellspacing="0" cellpadding="0">
								<tr>
								<td align="center" valign="top">
								<? 
								if ( isset($_SESSION['usuario_id']) ) { 
    								require_once('../inc/login_on.php'); 
								}
								else {
				    				require_once('../inc/login.php'); 
								} 
								?>
								</td>
								<td width="550" align="center" valign="top" style="padding-top:10px;">
												<table width="540"  border="0" cellpadding="0" cellspacing="2" class="bordaVerde">
												<tr>
												<td align="left">
																<table height="5" border="0" cellpadding="0" cellspacing="0" >
																<tr>
																<td width="60" align="center" class="bordaVerde barraTit"><!--<a href="../menus/inserir.php" class="ver10cinzaesc">Inserir</a>-->Inserir</td>
																<td width="60" align="center" background="../_interface/img/bgr_menu.jpg" class='bordaVerde ver10cinzaesc'><a href="../menus/editar_listar.php" class="ver10cinzaesc">Editar</a></td>
																<td width="60" align="center" background="../_interface/img/bgr_menu.jpg" class='bordaVerde ver10cinzaesc'><a href="../menus/excluir_listar.php" class="ver10cinzaesc">Excluir</a></td>
																</tr>
																</table>												
												    <table width="533"  border="0" cellpadding="0" cellspacing="0">
																<tr class="barraTit">
																<td>Inserir Menu</td>
																</tr>
																<tr class="bordaSubtit">
																<td>&nbsp;</td>
																</tr>
																<tr>
																<td height="80" align="left" valign="top" class="corpo_dentro_tabs">
																<form name="form" action="inserir.php" method="post">
																				<table  border="0" cellspacing="0" cellpadding="0">
																				<tr>
																				<td width="75" height="24" align="left" class="ver10cinzaesc">Nome</td>
																				<td><input name="menu_nome" type="text" class="ver10cinzaesc" style="width:200px;"></td>
																				</tr>
																				<tr>
																				<td width="75" height="24" align="left" class="ver10cinzaesc">Perfis</td>
																				<td align="left" class="ver10cinzaesc">Pressione "Ctrl" para selecionar mais de um perfil.<br>
																				<?
																				
																				$objConsulta->Campo("perfil_id");
																				$objConsulta->Campo("perfil_nome");
																				
																				$objConsulta->Select("manager_perfis");
																				
																				?>
																				<select name="perfil[]" class="ver10cinzaesc" style="width:145px" size="4" multiple>
																				<?
																				for ( $i=0; $i < $objConsulta->NumLinhas(); $i++ ) {
																	
																					$perfil_id   = $objConsulta->Valor($i, "perfil_id");
																					$perfil_nome = $objConsulta->Valor($i, "perfil_nome");
																																						
																					$bgcolor = ($i % 2) ? "#FFFFFF" : "#E5E5E5";					
																							
																				?>
																				<option value="<? echo $perfil_id; ?>"><? echo $perfil_nome?></option>
																				<?
																				}
																				?>
																				</select> 
																				</td>
																				</tr>
																				<tr>
																				<td height="24" align="left" class="ver10cinzaesc">&nbsp;</td>
																				<td align="right"><input name="Submit2" type="submit" class="ver10cinzaesc" value="Ok"></td>
																				</tr>
																				</table>
																</form>
																</td>
																</tr>
																</table>
												</td>
												</tr>
												</table>
								</td>
								</tr>
								</table>
				</td>
				</tr>
				<tr>
				<td height="99" align="right" valign="top">
				<? //Usado com iframe, inc/rodape.php ?>
				<iframe name="rodape" frameborder=0 marginheight=0 marginwidth=0 src="../_interface/inc/rodape.htm" scrolling="no" width="760" height="99">
				</iframe>
				</td>
				</tr>
				</table>
</td>
<td>&nbsp;</td>
</tr>
</table>
</body>
</html>
<?
/*
else {
    header("Location: ../login/index_erro.php");
}
*/
?>
