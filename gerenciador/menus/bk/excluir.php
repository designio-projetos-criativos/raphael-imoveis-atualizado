<?
	session_start();
	require_once "../../suporte/classes/objetos.php";
	require_once "../../suporte/classes/cabecalho.php";
	
	$strPagAtual = "menus/excluir_listar.php";	

    if ($objHttp->getMethod() == "POST") {
        
        $menu_id = $objHttp->getPost("menu_id");
        $escolha    = $objHttp->getPost("escolha");
        
        if ( strtoupper($escolha) == "SIM" ) {
	        $objConsulta->RegistroID('menu_id', $menu_id);
            $intID = $objConsulta->Delete("manager_menus");
        }
		
        $objHttp->Redirect("excluir_listar.php");
    
    }
    else {
        $menu_id = $objHttp->getQuery("menu_id");
        
        $objConsulta->RegistroID ("menu_id", $menu_id);
        
		$objConsulta->Campo("menu_nome");

        $objConsulta->Select("manager_menus");
        
        $menu_nome  = $objConsulta->Valor(0, "menu_nome");

    }

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>RSWEB Manager :::::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../_interface/style/ie.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript" src="../_interface/js/default.js"></script>
</head>

<body background="../_interface/img/bgr_2px_azulclaro.gif" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="25" background="../_interface/img/bgr_topo_claro.jpg">&nbsp;</td>
<td width="770">
<? //Usado com iframe, inc/cabecalho.php ?>
<iframe name="cabecalho" frameborder=0 marginheight=0 marginwidth=0 src="../_interface/inc/php/cabecalho.php" scrolling="no" width="770" height="25"></iframe>
</td>
<td height="25" background="../_interface/img/bgr_topo_claro.jpg">&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td background="../_interface/img/bgr_2px_azulescuro.gif" class="corpo">
				<table width="100%"  border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
				<tr>
				<td height="90"><? require_once('../inc/logo.php'); ?></td>
				</tr>
				<tr>
				<td>
				<? require_once('../inc/menu.php'); ?>
				</td>
				</tr>
				<tr>
				<td class="espacamento">
								<table width="100%"  border="0" cellspacing="0" cellpadding="0">
								<tr>
								<td align="center" valign="top">
								<? 
								if ( isset($_SESSION['usuario_id']) ) { 
    								require_once('../inc/login_on.php'); 
								}
								else {
				    				require_once('../inc/login.php'); 
								} 
								?>
								</td>
								<td width="550" align="center" valign="top" style="padding-top:10px;">
								<form name="form" action="excluir.php" method="post">
								<input name="menu_id" type="hidden" value="<?=$menu_id?>">
												<table width="540"  border="0" cellpadding="0" cellspacing="2" class="bordaVerde">
												<tr>
												<td align="left">
																<table height="5" border="0" cellpadding="0" cellspacing="0" >
																<tr>
																<td width="60" align="center" background="../_interface/img/bgr_menu.jpg" class='bordaVerde ver10cinzaesc'><a href="../menus/inserir.php" class="ver10cinzaesc">Inserir</a></td>
																<td width="60" align="center" background="../_interface/img/bgr_menu.jpg" class='bordaVerde ver10cinzaesc'><a href="../menus/editar_listar.php" class="ver10cinzaesc">Editar</a></td>
																<td width="60" align="center" class="bordaVerde barraTit"><!--<a href="../menus/excluir_listar.php" class="ver10cinzaesc">Excluir</a>-->Excluir</td>
																</tr>
																</table>												
												    <table width="533"  border="0" cellpadding="0" cellspacing="0">
																<tr class="barraTit">
																<td>Exclus&atilde;o de Registros</td>
																</tr>
																<tr class="bordaSubtit">
																<td>Confirma a exclus&atilde;o do registro?</td>
																</tr>
																<tr>
																<td height="80" align="left" valign="top" class="corpo_dentro_tabs ver10cinzaesc"><? echo $menu_nome; ?><br>
																<br>
																<input name="escolha" type="submit" class="ver10cinzaesc" value="Sim">
																&nbsp;&nbsp;&nbsp;
																<input name="escolha" type="submit" class="ver10cinzaesc" value="N&atilde;o" onClick="history.go(-1);">
																</td>
																</tr>
																</table>
												</td>
												</tr>
												</table>
								</form>
								</td>
								</tr>
								</table>
				</td>
				</tr>
				<tr>
				<td height="99" align="right" valign="top">
				<? //Usado com iframe, inc/rodape.php ?><iframe name="rodape" frameborder=0 marginheight=0 marginwidth=0 src="../_interface/inc/rodape.htm" scrolling="no" width="760" height="99"></iframe>
				</td>
				</tr>
				</table></td>
<td>&nbsp;</td>
</tr>
</table>
</body>
</html>

