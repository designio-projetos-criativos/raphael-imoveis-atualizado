<?php 
/* Verifica a vers�o atual do PHP
 * retornando a matriz SESSION adequada.
 */
function getSession($str_session_var) {
   global $HTTP_SESSION_VARS;
   if (version_compare(phpversion(), "4.1.0", ">=") == 1) {
      if (isset($_SESSION[$str_session_var]))
         return $_SESSION[$str_session_var];
	  else 
	     return ""; 
   }
   else {
      if (isset($HTTP_SESSION_VARS[$str_session_var]))
         return $HTTP_SESSION_VARS[$str_session_var]; 
	  else 
	     return "";	 
   }
}

/* Verifica a vers�o atual do PHP
 * retornando a matriz GET adequada
 */
function getQuery ($str_query_var) {
   global $HTTP_GET_VARS;
   if (version_compare(phpversion(), "4.1.0", ">=") == 1) {
      if (isset($_GET[$str_query_var]))
         return $_GET[$str_query_var];
	  else
	     return "";	 
   }
   else {
      if (isset($HTTP_GET_VARS[$str_query_var]))
         return $HTTP_GET_VARS[$str_query_var];
	  else 
	     return "";	 
   }
}

/* Verifica a vers�o atual do PHP
 * retornando a matriz POST adequada
 */
function getPost ($str_post_var) {
   global $HTTP_POST_VARS;
   if (version_compare(phpversion(), "4.1.0", ">=") == 1) {
       if (isset($_POST[$str_post_var])) {
           return $_POST[$str_post_var];
	   }
	   else {
	       return "";	 
	   }
   }
   else {
       if (isset($HTTP_POST_VARS[$str_post_var]))
           return $HTTP_POST_VARS[$str_post_var];
	   else 
	       return "";
   }
}

function getMethod () {
	return strtoupper($_SERVER['REQUEST_METHOD']);
}


function converte_data($data){
	if (strstr($data, "/")){
		$A = explode ("/", $data);
		$V_data = $A[2] . "-". $A[1] . "-" . $A[0];
	}
	else{
		$A = explode ("-", $data);
		$V_data = $A[2] . "/". $A[1] . "/" . $A[0];	
	}
	return $V_data;
}
?>