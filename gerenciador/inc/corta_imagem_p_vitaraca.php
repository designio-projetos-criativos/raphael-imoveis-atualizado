<?php

function corta_imagem($diretorio, $imagem, $nova_largura, $nova_altura, $complemento)  {

	$img_origem     = imagecreatefromjpeg($diretorio.$imagem);
 	/*$tamanho_imagem = getimagesize($diretorio.$imagem);
	$largura        = $tamanho_imagem[0];
	$altura         = $tamanho_imagem[1];
*/
	$thumbnail = explode(".", $imagem); 
	$thumbnail = $thumbnail[0].$complemento.".jpg";
	
// DEFINIR AS DIMENS�ES PARA O THUMBNAIL
   $x = $nova_largura; // Largura
   $y = $nova_altura; // Altura	

// PEGA AS DIMENS�ES DA IMAGEM DE ORIGEM
  //  $origem_x = imagesx($diretorio.$imagem); // Largura
   // $origem_y = imagesy($diretorio.$imagem); // Altura

list($origem_x, $origem_y) = getimagesize($diretorio.$imagem);	

// ESCOLHE A LARGURA MAIOR E, BASEADO NELA, GERA A LARGURA MENOR
    if($origem_x > $origem_y) { // Se a largura for maior que a altura
       $final_x = $x; // A largura ser� a do thumbnail
       $final_y = floor($x * $origem_y / $origem_x); // A altura � calculada
       $f_x = 0; // Colar no x = 0
       $f_y = round(($y / 2) - ($final_y / 2)); // Centralizar a imagem no meio y do thumbnail
    } else { // Se a altura for maior ou igual � largura
       $final_x = floor($y * $origem_x / $origem_y); // Calcula a largura
       $final_y = $y; // A altura ser� a do thumbnail
       $f_x = round(($x / 2) - ($final_x / 2)); // Centraliza a imagem no meio x do thumbnail
       $f_y = 0; // Colar no y = 0
    }
	
#gerando a a miniatura da imagem
$img_final = imagecreatetruecolor($x,$y);

imagecopyresampled($img_final, $img_origem, $f_x, $f_y, 0, 0, $final_x, $final_y, $origem_x, $origem_y);

#o 3� argumento � a qualidade da miniatura de 0 a 100
imagejpeg($img_final, $diretorio.$thumbnail, 100);
// LIBERA A MEM�RIA
    imagedestroy($img_origem);
    imagedestroy($img_final);
}


/*
//Exemplo:

$diretorio    = "../teste/";
$imagem       = "teste.jpg";
//$nova_largura = "150";
//$nova_altura  = "150";
$nova_largura = "111";
$nova_altura  = "74";
$complemento  = "_mini";

corta_imagem($diretorio, $imagem, $nova_largura, $nova_altura, $complemento);
<img src="<?php echo $diretorio.$imagem ?>" /><br>
<img src="<?php echo $diretorio."teste_mini.jpg" ?>" /><br>
*/
?>
