<?
function corta_imagem($diretorio, $imagem, $nova_largura, $nova_altura, $complemento)  {

	$thumbnail = explode(".", $imagem); 
	$thumbnail = $thumbnail[0].$complemento.".jpg";

	$imageWidth  = $nova_largura;
	$imageHeight = $nova_altura;

    $originalImage = $diretorio.$imagem;
    $newImagePath= $diretorio.'resized.jpg';
    
    // Dimens�es da imagem original
    $info = getimagesize($originalImage);
    $sizes['height'] = $info[1];
    $sizes['width'] = $info[0];
	
	// se imagem tipo retrato ALT>LARG
	if($sizes['height'] > $sizes['width']){
	
	$img_origem     = imagecreatefromjpeg($diretorio.$imagem);	
	
	// Se a altura for maior ou igual � largura
    $final_x = floor($imageHeight * $sizes['width'] / $sizes['height']); // Calcula a largura
    $final_y = $imageHeight; // A altura ser� a do thumbnail
    $f_x = round(($final_x / 2) - ($final_x / 2)); // Centraliza a imagem no meio x do thumbnail
    $f_y = 0; // Colar no y = 0
    
	//gerando a a miniatura da imagem
	$img_final = imagecreatetruecolor($final_x,$imageHeight);	
	
	imagecopyresampled($img_final, $img_origem, $f_x, $f_y, 0, 0, $final_x, $final_y, $sizes['width'], $sizes['height']);

	imagejpeg($img_final, $diretorio.$thumbnail, 90);
	
    imagedestroy($img_origem);
    imagedestroy($img_final);	
		
	
	}else{
	
    $originalImageHeight = (float) $sizes['height'];
    $originalImageWidth = (float) $sizes['width'];

    // Dimens�es do tipo de imagem
    $imageHeight = (float) $imageHeight;
    $imageWidth = (float) $imageWidth;

    // Descobre a escala
    $scaleWidth = $originalImageWidth / $imageWidth;
    $scaleHeight = $originalImageHeight / $imageHeight;

    if ($scaleWidth < $scaleHeight)
      $scale = $scaleWidth;
    else
      $scale = $scaleHeight;
	  
    // Dimens�es da imagem original
    $originalImageHeight = (float) $sizes['height'];
    $originalImageWidth = (float) $sizes['width'];

    // Cria uma nova imagem de acordo com a escala
    $newImageWidth = ceil($originalImageWidth / $scale);
    $newImageHeight = ceil($originalImageHeight / $scale);	
	
   // Cria a imagem resized
    $newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
    $source = imagecreatefromjpeg($originalImage);
	imagecopyresampled($newImage, $source, 0, 0, 0, 0, $newImageWidth, $newImageHeight, $originalImageWidth, $originalImageHeight);
   	 
    imagejpeg($newImage, $newImagePath, 90);
    
//----

    // Caminho da imagem a ser realizado crop
    $croppedImagePath = $diretorio.$thumbnail;

       // Dimens�es da imagem que foi realizada resize
    $info = getimagesize($diretorio.'resized.jpg');
    $sizes['height'] = $info[1];
    $sizes['width'] = $info[0];
    
    // Pontos x e y para crop
    $startHeight = ($sizes['height'] - $imageHeight) / 2;
    $startWidth = ($sizes['width'] - $imageWidth ) / 2;
    
    // Cria a nova imagem
  	$newImage = imagecreatetruecolor($imageWidth, $imageHeight);
    $source = imagecreatefromjpeg($diretorio.'resized.jpg');
    imagecopy($newImage, $source, 0, 0, $startWidth, $startHeight, $sizes['width'], $sizes['height']);

    imagejpeg($newImage, $croppedImagePath, 90);

    imagedestroy($source);
    imagedestroy($newImage);

    // Apaga a imagem de resize
    unlink($diretorio.'resized.jpg');
	
	} // fim else
}
?>