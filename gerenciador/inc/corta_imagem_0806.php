<?
function corta_imagem($diretorio, $imagem, $nova_largura, $nova_altura, $complemento )
{
$nw = $nova_largura;
$nh = $nova_altura;

$size = getimagesize($diretorio.$imagem);
$w = $size[0];
$h = $size[1];

$simg = imagecreatefromjpeg($diretorio.$imagem);

$thumbnail = explode(".", $imagem); 
$thumbnail = $thumbnail[0].$complemento.".jpg";

$dimg = imagecreatetruecolor($nw, $nh);

$white = imagecolorallocate($dimg, 255, 255, 255);
imagefill($dimg, 0, 0, $white);

$wm = $w/$nw;
$hm = $h/$nh;

$h_height = $nh/2;
$w_height = $nw/2;

if($w> $h)
{
$adjusted_width = $w / $hm;
$half_width = $adjusted_width / 2;
$int_width = $half_width - $w_height;

imagecopyresampled($dimg,$simg,-$int_width,0,0,0,$adjusted_width,$nh,$w,$h);
}
elseif(($w <$h) || ($w == $h))
{
$adjusted_height = $h / $wm;
$half_height = $adjusted_height / 2;
$int_height = $half_height - $h_height;

imagecopyresampled($dimg,$simg,0,-$int_height,0,0,$nw,$adjusted_height,$w,$h);
}
else
{
imagecopyresampled($dimg,$simg,0,0,0,0,$nw,$nh,$w,$h);
}

imagejpeg($dimg, $diretorio.$thumbnail, 90); //diminuir qualidade se img muito pesada

imagedestroy($dimg);

}

?>