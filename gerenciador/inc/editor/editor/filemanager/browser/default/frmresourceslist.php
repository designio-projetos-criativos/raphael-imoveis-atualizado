<?php 
    session_start();
    
    header ("Pragma: no-chache");
    header ("Cache-Control: no-chache");
    
    require_once ("include/php/arquivos.php");
    require_once ("include/php/stringtools.php");
    require_once ("include/php/http_comply.php");
    
    $blnUploadCorreto = false;
    
    if ( $_SERVER ['REQUEST_METHOD'] == "POST" ) {
        
        $strDirDestino  = stripslashes(getPost("DiretorioDestino"));
        $arrNovoArquivo = getFile("NovoArquivo");
        
        if ( !empty($strDirDestino) && is_array($arrNovoArquivo) ) {
            if ( move_uploaded_file($arrNovoArquivo['tmp_name'], $strDirDestino.$arrNovoArquivo['name']) ) {
                chmod($strDirDestino.$arrNovoArquivo['name'], 0777);
                $blnUploadCorreto = true;
            }
        }
        
    }  

  
  
  
//***************** SITE EXPRESSO EMBAIXADOR REMOTO **********************
/*    $objPasta = new Pasta("/home/httpd/html/php.rsweb.com.br/html/embaixador/uploads/");
    $objPasta->AjustaURL("http://php.rsweb.com.br/embaixador/uploads/");
*/
  
    $parComando = getQuery("parComando");
    
    if ( !empty ($parComando) ) {
        switch ( $parComando) {
            case "MudaDiretorio":
                $parDiretorio = getQuery("parDiretorio");
                if ( !empty($parDiretorio) ) {
                    if ( $parDiretorio != ".." && $parDiretorio != "." ) {
                        $objPasta->AcessaDiretorio($parDiretorio);
                    }
                    elseif ($parDiretorio != ".") {
                        $objPasta->AbandonaDiretorio();
                    }
                }
                break;    
            
            case "CriaDiretorio":
                $parDiretorio = getQuery("parNovoDiretorio");
                if ( !empty($parDiretorio) ) {
                    
                }
                break;    
        }
    }
    
    //
    function DevolvePastaAtual () {
        GLOBAL $objPasta;
        return "'".addslashes($objPasta->DiretorioAtual())."'";
    }
    
    
        
?>
<!--
 * FileBrowser Component 
 * Programador: Charlis Aristimunha
 *
 * GUI baseada no projeto:   
 * FCKeditor - The text editor for internet
 * www.fckeditor.net
 * Copyright (C) 2003-2004 Frederico Caldeira Knabben
 * File Authors:
 *     Frederico Caldeira Knabben (fredck@fckeditor.net)
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
    <head>
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
        <link href="Browser.css" type="text/css" rel="stylesheet">
        <script type="text/javascript" src="js/common.js"></script>
        <script language="javascript">
            function OpenFile( fileUrl ) {
                window.top.opener.SetUrl( fileUrl ) ;
                window.top.close() ;
                window.top.opener.focus() ;
            }
            
            function AcessaDiretorio (strRecurso) {
                document.location = "frmresourceslist.php?parComando=MudaDiretorio&parDiretorio=" + strRecurso;
            }        
            
            function ObtemRecurso() {
                document.location = "frmresourceslist.php?parComando=parAcessaRecurso&parRecurso=" + strRecurso;
            }      
            
            // Seta o diretório de destino
            function SetaPastaAtual () {
                objFrame = window.parent.frames['frmUpload'].document.getElementById('DiretorioDestino');
                objFrame.value = <?php echo DevolvePastaAtual();?> ;
                //alert(objFrame.value);
            }
        </script>
    </head>
    <body class        = "FileArea" 
          bottomMargin = "10" 
          leftMargin   = "10" 
          topMargin    = "10" 
          rightMargin  = "10"
          onLoad       = "SetaPastaAtual()">
    <?php 
        $objPasta->ListaRecursos();
        $objPasta->DepuraSessao();
    ?>
    <table id="tableFiles" cellSpacing="1" cellPadding="0" width="100%" border="0">
    </table>
    </body>
</html>
