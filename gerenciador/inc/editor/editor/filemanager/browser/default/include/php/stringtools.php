<?php 
/* 
 Capitaliza uma dada string em uma das seguintes formas:
 0 - Retorna a string como informada(opcional)
 1 - Retorna a string como o primeiro caractere da [frase] Mai�sculo
 2 - Retorna a string como o primeiro caractere de [cada palavra] em Mai�sculo
*/
function capitalizaString ($string, $modo=0) {
   if (($modo < 0) || ($modo > 2) || empty($string)) {
      return "Argumento(s) incorreto(s) informado(s) em "; 
   }
   elseif ($modo == 0) {
      return $string;   
   }
   elseif($modo == 1) {
      return ucfirst(strtolower($string));
   }
   else {
      return ucwords(strtolower($string));
   }
}

function FormataData ($strData) {
    if ( strlen($strData) > 0 ) { 
        $arrCampos = explode("/", $strData);
        return $arrCampos[2]."/".$arrCampos[1]."/".$arrCampos[0]; 
    }
    else return "";
} 


?>
