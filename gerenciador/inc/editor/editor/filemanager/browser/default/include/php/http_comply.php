<?php 
/*
  Padr�o estabelecido: 
  todas as fun��es devem retornar o seu valor se vari�vel setada ou ""(empty)
  apenas getSession obedece ao padr�o.
  motivo:

  [BEGIN:
  $var = getSession("nomevar")
  if (!empty($var)) {
    // agora temos certeza q a vari�vel possui um valor
  }
  else  {
    // a vari�vel n�o estava setada ou n�o possui um valor
  }
  :END]
  
  a depura��o da vari�vel pode ser feita com if(isset($_SESSION["nomevar"]))
  status verifica��o: fun��es de sess�o ok.
*/
//$var = getQuery("teste"); 

//if (!empty($var=getQuery("teste"))) {
//  echo "est�: ".getQuery("teste");
//}
//else {
//  echo "putz";
//}

//if (isset($var)) {
//  echo "vari�vel criada!";
//}

//session_start();
//$var = getSession("teste");
//if (empty($var)) {
//   setSession("teste", "valor");
//}
//else {
//   echo $var;
//}

/* Fun��es de compatibiliza��o HTTP/PHP */

/* Verifica a vers�o atual do PHP
 * retornando a matriz FILE adequada.
 */
function getFile($str_file_var) {
   global $HTTP_POST_FILES;
   if (version_compare(phpversion(), "4.1.0", ">=") == 1) {
       if (isset($_FILES[$str_file_var]))
           return $_FILES[$str_file_var];
       else 
           return "";	 
   }
   else {
       if (isset($HTTP_POST_FILES[$str_file_var]))  
           return $HTTP_POST_FILES[$str_file_var];
       else
           return "";	 
   }
}

/* Verifica a vers�o atual do PHP
 * retornando a matriz COOKIE adequada.
 */
function getCookie($str_cookie_var) {
   global $HTTP_COOKIE_VARS;
   if (version_compare(phpversion(), "4.1.0", ">=") == 1) {
      if (isset($_COOKIE[$str_cookie_var]))
         return $_COOKIE[$str_cookie_var];
      else 
         return "";	 
   }
   else {
      if (isset($HTTP_COOKIE_VARS[$str_cookie_var]))  
         return $HTTP_COOKIE_VARS[$str_cookie_var];
      else
         return "";	 
   }
}

/* Verifica a vers�o atual do PHP
 * retornando a matriz SESSION adequada.
 */
function getSession($str_session_var) {
   global $HTTP_SESSION_VARS;
   if (version_compare(phpversion(), "4.1.0", ">=") == 1) {
      if (isset($_SESSION[$str_session_var]))
         return $_SESSION[$str_session_var];
      else 
         return ""; 
   }
   else {
      if (isset($HTTP_SESSION_VARS[$str_session_var]))
         return $HTTP_SESSION_VARS[$str_session_var]; 
      else 
         return "";	 
   }
}

function setSession($str_session_var, $mix_session_val) {
   global $HTTP_SESSION_VARS;
   if (version_compare(phpversion(), "4.1.0", ">=") == 1) {
      $_SESSION[$str_session_var] = $mix_session_val;
   }
   else {
      $HTTP_SESSION_VARS[$str_session_var] = $mix_session_val;
   }
}

/* Verifica a vers�o atual do PHP
 * retornando a matriz GET adequada
 */
function getQuery ($str_query_var) {
   global $HTTP_GET_VARS;
   if (version_compare(phpversion(), "4.1.0", ">=") == 1) {
      if (isset($_GET[$str_query_var]))
         return $_GET[$str_query_var];
      else
         return "";	 
   }
   else {
      if (isset($HTTP_GET_VARS[$str_query_var]))
         return $HTTP_GET_VARS[$str_query_var];
      else 
         return "";	 
   }
}

/* Verifica a vers�o atual do PHP
 * retornando a matriz POST adequada
 */
function getPost ($str_post_var) {
   global $HTTP_POST_VARS;
   if (version_compare(phpversion(), "4.1.0", ">=") == 1) {
       if (isset($_POST[$str_post_var])) {
           return $_POST[$str_post_var];
       }
       else {
           return "";	 
       }
   }
   else {
       if (isset($HTTP_POST_VARS[$str_post_var]))
           return $HTTP_POST_VARS[$str_post_var];
       else 
           return "";
   }
}


?>