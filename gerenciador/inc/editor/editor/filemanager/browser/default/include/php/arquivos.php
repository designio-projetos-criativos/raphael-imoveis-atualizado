<?php 

// fun��es de aux�lio � classe Pasta {}
// devolve a exten��o do arquivo.
function RetornaExt ($strNome) {
    $arrNomeExt = explode(".", $strNome);
    if (is_array($arrNomeExt))
        return $arrNomeExt[sizeof($arrNomeExt)-1];
    return "_";
}
// devolve o �cone correspondente ao �cone
function RetornaIcone ($strExt) {
    if ( is_file("./images/icons/".$strExt.".gif") ) {
        return $strExt.".gif";
    }
    return "default.icon.gif";
}

// recebe uma url e retira o seu ultimo nivel
function URLSobeNivel ($strURL) {
    $arrURL = explode ("/", $strURL);
    $strURL = "";
    $strSeparador = "";
    for ( $i=0; $i<(sizeof($arrURL)-2); $i++ ) {
        $strSeparador="/";
        if ($i==0) $strSeparador = "//";
        $strURL = $strURL.$arrURL[$i].$strSeparador;
        //echo "$i ->".$arrURL[$i]."\n";
    }
    return $strURL;
}




/*
    Teste da classe Pasta().
*/

/*
Classe Pasta: 
Manipula��o de Diret�rios integrada com SESSAO HTTP
Programador: charlis saraiva aristimunha.
*/

//$objPasta = new Pasta("d:\\clientes\\TESTE\\FileBrowser\\");
//echo "<pre>";
//$objPasta->AcessaDiretorio("include");
//$objPasta->AcessaDiretorio("php");
//$objPasta->AbandonaDiretorio();
//$objPasta->AbandonaDiretorio();
//$objPasta->AbandonaDiretorio();
//$objPasta->ListaRecursos();
//echo "</pre>";


class Pasta {
    
    var $strDiretorioRaiz;
    var $strSeparador;
    var $ptrDiretorio;
    
    function Pasta($strDiretorioRaiz) {
        // inicializa a classe
        // separador de pastas
        $this->strSeparador = "/";
        if ( strstr($strDiretorioRaiz, "\\") != "") {
            $this->strSeparador = "\\";
        }
        
        $this->$strDiretorioRaiz   = $strDiretorioRaiz;
        $_SESSION['DiretorioRaiz'] = $strDiretorioRaiz;
        if ( !isset($_SESSION['DiretorioAtual']) ) {
            $_SESSION['DiretorioAtual'] = $strDiretorioRaiz;
        }
        if ( strrpos($_SESSION['DiretorioAtual'], $this->strSeparador) < (strlen($_SESSION['DiretorioAtual'])-1) ) {
            $_SESSION['DiretorioAtual'] = $_SESSION['DiretorioAtual'].$this->strSeparador;    
        }
        $this->ptrDiretorio = opendir($_SESSION['DiretorioAtual']);
    }
    
    // sincroniza a URL
    function AjustaURL($strURL) {
        
        if ( strrpos($strURL, "/") < (strlen($strURL)-1) ) {
            $strURL = $strURL."/";
        }
        if ( !isset($_SESSION['URLAtual']) )
            $_SESSION['URLAtual'] = $strURL;
    }
    
    function AcessaDiretorio($strNomeDiretorio) {
        $strDirAtual = $_SESSION['DiretorioAtual'];
        $strURLAtual = $_SESSION['URLAtual'];
        $strDirAtual = $strDirAtual.$strNomeDiretorio.$this->strSeparador;
        $strURLAtual = $strURLAtual.$strNomeDiretorio."/";
        
        if ( is_dir($strDirAtual) ) {
            $this->ptrDiretorio = opendir($strDirAtual);
            // salva o diret�rio atual na sess�o do aplicativo.
            $_SESSION['DiretorioAtual'] = $strDirAtual;
            $_SESSION['URLAtual']       = $strURLAtual;
        }
        else {
            echo "'$strDirAtual' n�o � um diret�rio.";
        }
    }
    
    function AbandonaDiretorio() {
        
        $arrPastas       = explode ($this->strSeparador, $_SESSION['DiretorioAtual']);
        $strTmpPasta     = "";
        $strTmpSeparador = "";
        
        for ( $i=0; $i < sizeof($arrPastas) - 2; $i++ ) {
            if ( strlen($arrPastas[$i]) > 0 ) {
                $strTmpPasta     = $strTmpPasta.$strTmpSeparador.$arrPastas[$i];
                $strTmpSeparador = $this->strSeparador;
            }
        }
        $strTmpPasta  = $strTmpPasta.$strTmpSeparador;
        if ( $strTmpSeparador == "/" ) {
            $strTmpPasta = $strTmpSeparador.$strTmpPasta;
        }
        
        // impede que uma pasta superior � raiz seja acessada,
        // comparando o elemento da pasta raiz correspondente ao elemento da
        // pasta atual.
        // se eles forem divergentes, a pasta � invalida e o m�todo retorna 0.
        $arrPastaRaiz       = explode($this->strSeparador, $_SESSION['DiretorioRaiz']);
        $arrPastas          = explode($this->strSeparador, $strTmpPasta);
        $blnDiretorioValido = true;
        
        for ( $i=0; $i < sizeof($arrPastaRaiz); $i++ ) {
            if ( strlen(trim($arrPastaRaiz[$i])) > 0 && strlen(trim($arrPastas[$i])) > 0 ) {
                if ( $arrPastaRaiz[$i] !== $arrPastas[$i] ) {
                    $blnDiretorioValido = false;
                }
            }
        }
                
        if ( sizeof($arrPastaRaiz) > sizeof($arrPastas) ) {
            $blnDiretorioValido = false;
        }
        
        if ( $blnDiretorioValido ) {
            $_SESSION['DiretorioAtual'] = $strTmpPasta;
            $_SESSION['URLAtual']       = str_replace("///","//",URLSobeNivel($_SESSION['URLAtual']));
            $this->ptrDiretorio = opendir($_SESSION['DiretorioAtual']);
        }
    }
    
    function ListaRecursos () {
        $url = $_SESSION['URLAtual'];
        $arrPastas   = Array();
        $arrArquivos = Array();
        
        while (false !== ($strNome = readdir($this->ptrDiretorio))) {
            if ( is_file($_SESSION['DiretorioAtual'].$strNome) ) {
                $arrArquivos[] = $strNome;
            }
            else  {
                $arrPastas[] = $strNome;
            }
        }
        
        echo "<table border=0 cellspacing=0 cellpadding=0>";
        if ( is_array($arrPastas) ) {
            for ( $i=0; $i < sizeof($arrPastas); $i++ ) {
                echo "<tr><td><img src='images/Folder.gif'></td><td><a href='javascript:AcessaDiretorio(\"$arrPastas[$i]\")'>".$arrPastas[$i]."</a></td></tr>";
            }
        }
        if ( is_array($arrArquivos) ) {
            for ( $i=0; $i < sizeof($arrArquivos); $i++ ) {
                $strExt          = RetornaExt($arrArquivos[$i]);
                $strArquivoIcone = RetornaIcone($strExt);
                echo "<tr><td width='30'><img src='images/icons/".$strArquivoIcone."'></td><td><a href='javascript:OpenFile(\"$url$arrArquivos[$i]\")'>".$arrArquivos[$i]."</a></td></tr>";
            }
        }
        echo "</table>";
    }
    
    function DiretorioAtual() {
        return $_SESSION['DiretorioAtual'];
    }
    function DepuraSessao () {
        echo "<pre>";
        print_r($_SESSION);
        echo "</pre>";
    }
            
           
    function NumeroRecursos(){}
    function NumeroArquivos(){}
    function NumeroPastas  (){}
    function RecursoExiste (){}
} 

?>