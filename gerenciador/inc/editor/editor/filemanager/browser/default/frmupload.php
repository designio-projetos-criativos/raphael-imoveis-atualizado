<?php 
header("Pragma: no-cache");
header("Cache-control: no-cache");
?>
<!--
 * FileBrowser Component 
 * Programador: Charlis Aristimunha
 *
 * GUI baseada no seguinte projeto:   
 * FCKeditor - The text editor for internet
 * www.fckeditor.net
 * Copyright (C) 2003-2004 Frederico Caldeira Knabben
 * File Authors:
 *     Frederico Caldeira Knabben (fredck@fckeditor.net)
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
    <head>
        <link href="Browser.css" type="text/css" rel="stylesheet" />
            <script type="text/javascript" src="js/common.js"></script>
            <script language="javascript">
            </script>
        </head>
        
        <body bottomMargin="0" topMargin="0">
        <form id="frmUpload" action="frmresourceslist.php" target="frmResourcesList" method="post" enctype="multipart/form-data" >
            <table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
            <tr>
            <td nowrap>
            <span id="eUploadMessage">Envia um novo arquivo.</span><br>
                <table cellSpacing="0" cellPadding="0" width="100%" border="0">
                <tr>
                <td width="100%">
                <input id="DiretorioDestino" name="DiretorioDestino" type="hidden" value = "">
                <input id="NovoArquivo"      name="NovoArquivo" style="WIDTH: 100%" type="file">
                </td>
                <td nowrap><input id="btnUpload" type="submit" value="Upload"></td>
                </tr>
                </table>
            </td>
            </tr>
            </table>
        </form>
        </body>
</html>
