/*
 * FCKeditor - The text editor for internet
 * Copyright (C) 2003-2004 Frederico Caldeira Knabben
 * 
 * Licensed under the terms of the GNU Lesser General Public License:
 * 		http://www.opensource.org/licenses/lgpl-license.php
 * 
 * For further information visit:
 * 		http://www.fckeditor.net/
 * 
 * File Name: en.js
 * 	English language file.
 * 
 * Version:  2.0 RC1
 * Modified: 2004-11-26 01:58:51
 * 
 * File Authors:
 * 		Frederico Caldeira Knabben (fredck@fckeditor.net)
 */

var FCKLang =
{
// Language direction : "ltr" (left to right) or "rtl" (right to left).
Dir					: "ltr",

ToolbarCollapse		: "Collapse Toolbar",
ToolbarExpand		: "Expandir",

// Toolbar Items and Context Menu
Save				: "Salvar",
NewPage				: "Novo",
Preview				: "Prever",
Cut					: "Recortar",
Copy				: "Copiar",
Paste				: "Colar",
PasteText			: "Colar como planilha",
PasteWord			: "Colar no Word",
Print				: "Imprimir",
SelectAll			: "Selecionar tudo",
RemoveFormat		: "Remover formata��o",
InsertLinkLbl		: "Inserir link",
InsertLink			: "Inserir/editar Link",
RemoveLink			: "Removee link",
InsertImageLbl		: "Inserir imagem",
InsertImage			: "Inserir/editar Imagem",
InsertTableLbl		: "Inserir tabela",
InsertTable			: "Inserir/Editar tabela",
InsertLineLbl		: "Linha",
InsertLine			: "Inserir linha horizontal",
InsertSpecialCharLbl: "Caracteres especiais",
InsertSpecialChar	: "Inserir caractere especial",
InsertSmileyLbl		: "Smiley",
InsertSmiley		: "Inserir Smiley",
About				: "Sobre",
Bold				: "Negrito",
Italic				: "Italico",
Underline			: "Sublinhado",
StrikeThrough		: "Tachado",
Subscript			: "Subscrito",
Superscript			: "Sobrescrito",
LeftJustify			: "Alinhar a esquerda",
CenterJustify		: "Justificar",
RightJustify		: "Alinhar a direita",
BlockJustify		: "Block Justify",
DecreaseIndent		: "Decrease Indent",
IncreaseIndent		: "Increase Indent",
Undo				: "Desfazer",
Redo				: "Refazer",
NumberedListLbl		: "Lista numerada",
NumberedList		: "Inserir/remover lista numerada",
BulletedListLbl		: "Marcador",
BulletedList		: "Inserir/remover marcador",
ShowTableBorders	: "Mostrar bordas",
ShowDetails			: "Detalhes",
Style				: "Estilo",
FontFormat			: "Formato",
Font				: "Fonte",
FontSize			: "Tamanho",
TextColor			: "Cor do texto",
BGColor				: "Cor de fundo",
Source				: "C�digo",
Find				: "Procurar",
Replace				: "Substituir",

// Context Menu
EditLink			: "Editar link",
InsertRow			: "Inserir linha",
DeleteRows			: "Deletar linhas",
InsertColumn		: "Inserir coluna",
DeleteColumns		: "Deletar colunas",
InsertCell			: "Inserir c�lula",
DeleteCells			: "Deletar c�lulas",
MergeCells			: "Agrupar c�lulas",
SplitCell			: "Separar c�lulas",
CellProperties		: "Propriedade da c�lula",
TableProperties		: "Propriedade da tabela",
ImageProperties		: "Propriedade da imagem",

FontFormats			: "Normal;Formatado;Endere�o;Cabe�alho 1;Cabe�alho 2;Cabe�alho 3;Cabe�alho 4;Cabe�alho 5;Cabe�alho 6",

// Alerts and Messages
ProcessingXHTML		: "Processando XHTML. Aguarde por favor...",
Done				: "Feito",
PasteWordConfirm	: "The text you want to paste seems to be copied from Word. Do you want to clean it before pasting?",
NotCompatiblePaste	: "This command is available for Internet Explorer version 5.5 or more. Do you want to paste without cleaning?",
UnknownToolbarItem	: "Unknown toolbar item \"%1\"",
UnknownCommand		: "Unknown command name \"%1\"",
NotImplemented		: "Command not implemented",
UnknownToolbarSet	: "Toolbar set \"%1\" doesn't exist",

// Dialogs
DlgBtnOK			: "OK",
DlgBtnCancel		: "Cancelar",
DlgBtnClose			: "Fechar",
DlgAdvancedTag		: "Avan�ado",

// General Dialogs Labels
DlgGenNotSet		: "&lt;not set&gt;",
DlgGenId			: "Id",
DlgGenLangDir		: "Language Direction",
DlgGenLangDirLtr	: "Esquerda para a direita (LTR)",
DlgGenLangDirRtl	: "Direita para a esquerda (RTL)",
DlgGenLangCode		: "C�digo do idioma",
DlgGenAccessKey		: "Chave de acesso",
DlgGenName			: "Nome",
DlgGenTabIndex		: "�ndice de tabula��o",
DlgGenLongDescr		: "Descri��o da URL",
DlgGenClass			: "CSS",
DlgGenTitle			: "T�tulo de consulta",
DlgGenContType		: "Tipo de consulta",
DlgGenLinkCharset	: "Linked Resource Charset",
DlgGenStyle			: "Estilo",

// Image Dialog
DlgImgTitle			: "Image Properties",
DlgImgInfoTab		: "Image Info",
DlgImgBtnUpload		: "Send it to the Server",
DlgImgURL			: "URL",
DlgImgUpload		: "Upload",
DlgImgBtnBrowse		: "Browse Server",
DlgImgAlt			: "Alternative Text",
DlgImgWidth			: "Width",
DlgImgHeight		: "Height",
DlgImgLockRatio		: "Lock Ratio",
DlgBtnResetSize		: "Reset Size",
DlgImgBorder		: "Border",
DlgImgHSpace		: "HSpace",
DlgImgVSpace		: "VSpace",
DlgImgAlign			: "Align",
DlgImgAlignLeft		: "Left",
DlgImgAlignAbsBottom: "Abs Bottom",
DlgImgAlignAbsMiddle: "Abs Middle",
DlgImgAlignBaseline	: "Baseline",
DlgImgAlignBottom	: "Bottom",
DlgImgAlignMiddle	: "Middle",
DlgImgAlignRight	: "Right",
DlgImgAlignTextTop	: "Text Top",
DlgImgAlignTop		: "Top",
DlgImgPreview		: "Preview",
DlgImgMsgWrongExt	: "Sorry, only the following file types uploads are allowed:\n\n" + FCKConfig.ImageUploadAllowedExtensions + "\n\nOperation canceled.",
DlgImgAlertSelect	: "Please select an image to upload.",
DlgImgAlertUrl		: "Please type the image URL",

// Link Dialog
DlgLnkWindowTitle	: "Link",
DlgLnkInfoTab		: "Link Info",
DlgLnkTargetTab		: "Target",

DlgLnkType			: "Link Type",
DlgLnkTypeURL		: "URL",
DlgLnkTypeAnchor	: "Anchor in this page",
DlgLnkTypeEMail		: "E-Mail",
DlgLnkProto			: "Protocol",
DlgLnkProtoOther	: "&lt;other&gt;",
DlgLnkURL			: "URL",
DlgLnkBtnBrowse		: "Browse Server",
DlgLnkAnchorSel		: "Select an Anchor",
DlgLnkAnchorByName	: "By Anchor Name",
DlgLnkAnchorById	: "By Element Id",
DlgLnkNoAnchors		: "&lt;No anchors available in the document&gt;",
DlgLnkEMail			: "E-Mail Address",
DlgLnkEMailSubject	: "Message Subject",
DlgLnkEMailBody		: "Message Body",
DlgLnkUpload		: "Upload",
DlgLnkBtnUpload		: "Send it to the Server",

DlgLnkTarget		: "Target",
DlgLnkTargetFrame	: "&lt;frame&gt;",
DlgLnkTargetPopup	: "&lt;popup window&gt;",
DlgLnkTargetBlank	: "New Window (_blank)",
DlgLnkTargetParent	: "Parent Window (_parent)",
DlgLnkTargetSelf	: "Same Window (_self)",
DlgLnkTargetTop		: "Topmost Window (_top)",
DlgLnkTargetFrame	: "Target Frame Name",
DlgLnkPopWinName	: "Popup Window Name",
DlgLnkPopWinFeat	: "Popup Window Features",
DlgLnkPopResize		: "Resizable",
DlgLnkPopLocation	: "Location Bar",
DlgLnkPopMenu		: "Menu Bar",
DlgLnkPopScroll		: "Scroll Bars",
DlgLnkPopStatus		: "Status Bar",
DlgLnkPopToolbar	: "Toolbar",
DlgLnkPopFullScrn	: "Full Screen (IE)",
DlgLnkPopDependent	: "Dependent (Netscape)",
DlgLnkPopWidth		: "Width",
DlgLnkPopHeight		: "Height",
DlgLnkPopLeft		: "Left Position",
DlgLnkPopTop		: "Top Position",

DlgLnkMsgWrongExtA	: "Sorry, only the following file types uploads are allowed:\n\n" + FCKConfig.LinkUploadAllowedExtensions + "\n\nOperation canceled.",
DlgLnkMsgWrongExtD	: "Sorry, the following file types uploads are not allowed:\n\n" + FCKConfig.LinkUploadDeniedExtensions + "\n\nOperation canceled.",

DlnLnkMsgNoUrl		: "Please type the link URL",
DlnLnkMsgNoEMail	: "Please type the e-mail address",
DlnLnkMsgNoAnchor	: "Please select an anchor",

// Color Dialog
DlgColorTitle		: "Select Color",
DlgColorBtnClear	: "Clear",
DlgColorHighlight	: "Highlight",
DlgColorSelected	: "Selected",

// Smiley Dialog
DlgSmileyTitle		: "Insert a Smiley",

// Special Character Dialog
DlgSpecialCharTitle	: "Select Special Character",

// Table Dialog
DlgTableTitle		: "Table Properties",
DlgTableRows		: "Rows",
DlgTableColumns		: "Columns",
DlgTableBorder		: "Border size",
DlgTableAlign		: "Alignment",
DlgTableAlignNotSet	: "<Not set>",
DlgTableAlignLeft	: "Left",
DlgTableAlignCenter	: "Center",
DlgTableAlignRight	: "Right",
DlgTableWidth		: "Width",
DlgTableWidthPx		: "pixels",
DlgTableWidthPc		: "percent",
DlgTableHeight		: "Height",
DlgTableCellSpace	: "Cell spacing",
DlgTableCellPad		: "Cell padding",
DlgTableCaption		: "Caption",

// Table Cell Dialog
DlgCellTitle		: "Cell Properties",
DlgCellWidth		: "Width",
DlgCellWidthPx		: "pixels",
DlgCellWidthPc		: "percent",
DlgCellHeight		: "Height",
DlgCellWordWrap		: "Word Wrap",
DlgCellWordWrapNotSet	: "<Not set>",
DlgCellWordWrapYes	: "Yes",
DlgCellWordWrapNo	: "No",
DlgCellHorAlign		: "Horizontal Alignment",
DlgCellHorAlignNotSet	: "<Not set>",
DlgCellHorAlignLeft	: "Left",
DlgCellHorAlignCenter	: "Center",
DlgCellHorAlignRight: "Right",
DlgCellVerAlign		: "Vertical Alignment",
DlgCellVerAlignNotSet	: "<Not set>",
DlgCellVerAlignTop	: "Top",
DlgCellVerAlignMiddle	: "Middle",
DlgCellVerAlignBottom	: "Bottom",
DlgCellVerAlignBaseline	: "Baseline",
DlgCellRowSpan		: "Rows Span",
DlgCellCollSpan		: "Columns Span",
DlgCellBackColor	: "Background Color",
DlgCellBorderColor	: "Border Color",
DlgCellBtnSelect	: "Select...",

// Find Dialog
DlgFindTitle		: "Find",
DlgFindFindBtn		: "Find",
DlgFindNotFoundMsg	: "The specified text was not found.",

// Replace Dialog
DlgReplaceTitle			: "Replace",
DlgReplaceFindLbl		: "Find what:",
DlgReplaceReplaceLbl	: "Replace with:",
DlgReplaceCaseChk		: "Match case",
DlgReplaceReplaceBtn	: "Replace",
DlgReplaceReplAllBtn	: "Replace All",
DlgReplaceWordChk		: "Match whole word",

// Paste Operations / Dialog
PasteErrorPaste	: "Your browser security settings don't permit the editor to automaticaly execute pasting operations. Please use the keyboard for that (Ctrl+V).",
PasteErrorCut	: "Your browser security settings don't permit the editor to automaticaly execute cutting operations. Please use the keyboard for that (Ctrl+X).",
PasteErrorCopy	: "Your browser security settings don't permit the editor to automaticaly execute copying operations. Please use the keyboard for that (Ctrl+C).",

PasteAsText		: "Paste as Plain Text",
PasteFromWord	: "Paste from Word",

DlgPasteMsg		: "The editor was not able to automaticaly execute pasting because of the <STRONG>security settings</STRONG> of your browser.<BR>Please paste inside the following box using the keyboard (<STRONG>Ctrl+V</STRONG>) and hit <STRONG>OK</STRONG>.",

// Color Picker
ColorAutomatic	: "Autom�tico",
ColorMoreColors	: "Mair cores...",

// About Dialog
DlgAboutVersion	: "vers�o",
DlgAboutLicense	: "Licensed under the terms of the GNU Lesser General Public License",
DlgAboutInfo	: "Para informa��es:"
}