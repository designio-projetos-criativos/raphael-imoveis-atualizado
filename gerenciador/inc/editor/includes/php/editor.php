<?php 
if ( isset($_POST['conteudo']) ) {
	$conteudo = $_POST['conteudo'];
	echo $conteudo;
} 
else {
	include("../inc/editor/fckeditor.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>RSWEB Manager</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="robots" content="noindex, nofollow">
<link href="../editor.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?
// Automatically calculates the editor base path based on the _samples directory.
// This is usefull only for these samples. A real application should use something like this:
// $oFCKeditor->BasePath = '/FCKeditor/' ;	// '/FCKeditor/' is the default value.
//$sBasePath = $_SERVER['PATH_INFO'] ;
	$sBasePath = './../inc/editor/';
	
	if ( !isset($conteudo) ) {
		$conteudo = "";
	}
	$oFCKeditor = new FCKeditor('conteudo');
	$oFCKeditor->BasePath = $sBasePath;
	$oFCKeditor->Value    = $conteudo;
	$oFCKeditor->Create();
	echo "<br>";

}
?>
</body>
</html>