<script language="JavaScript">
function txtBoxFormat(objForm, strField, sMask, evtKeyPress) {
     var i, nCount, sValue, fldLen, mskLen,bolMask, sCod, nTecla;
     nTecla = (evtKeyPress.which) ? evtKeyPress.which : evtKeyPress.keyCode;
     sValue = objForm[strField].value;
     // Limpa todos os caracteres de formata��o que
     // j� estiverem no campo.
     expressao = /[\.\/\-\(\)\,\;\: ]/gi;
     sValue = sValue.toString().replace(expressao, '');
     fldLen = sValue.length;
     mskLen = sMask.length;

     i = 0;
     nCount = 0;
     sCod = "";
     mskLen = fldLen;

     while (i <= mskLen) {
       bolMask = ((sMask.charAt(i) == "-") || (sMask.charAt(i) == ".") || (sMask.charAt(i) == "/") || (sMask.charAt(i) == ",") || (sMask.charAt(i) == ";") || (sMask.charAt(i) == ":"))
       bolMask = bolMask || ((sMask.charAt(i) == "(") || (sMask.charAt(i) == ")") || (sMask.charAt(i) == " "))

       if (bolMask) {
         sCod += sMask.charAt(i);
         mskLen++; }
       else {
         sCod += sValue.charAt(nCount);
         nCount++;
       }

       i++;
     }

     objForm[strField].value = sCod;

     if (nTecla != 8 && nTecla != 13)
      { // backspace enter
       if (sMask.charAt(i-1) == "9")
       { // apenas n�meros...
         return ((nTecla > 47) && (nTecla < 58));
       } // n�meros de 0 a 9
       else
       {
            if (sMask.charAt(i-1) == "x")
           { // apenas letras... Sem espaco
             return ((nTecla > 64) && (nTecla < 123));
           } // maiusculas e minusculas de A a z sem acentos
           else
           { // qualquer caracter...
            return true;
          }
       }
      }
     else
     {
       return true;
     }
   }
//Fim da Fun��o M�scaras Gerais
</script>
<script language=javascript>
<!--
function ScrollMarquee() {
window.setTimeout('Marquee()', ScrollSpeed);
var msg = document.scrollform.box.value;
document.scrollform.box.value = msg.substring(ScrollChars) + msg.substring(0, ScrollChars);
}
//-->
</script>