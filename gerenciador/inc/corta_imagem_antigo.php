<?php

function corta_imagem($diretorio, $imagem, $nova_largura, $nova_altura, $complemento)  {

	$img_origem     = imagecreatefromjpeg($diretorio.$imagem);
 	$tamanho_imagem = getimagesize($diretorio.$imagem);
	$largura        = $tamanho_imagem[0];
	$altura         = $tamanho_imagem[1];

	$thumbnail = explode(".", $imagem); 
	$thumbnail = $thumbnail[0].$complemento.".jpg";


	if ($largura < $altura) {

		$largura_final = $nova_largura;
		$altura_final  = floor($nova_largura * ($altura/$largura));
		$x = 0;

		if ($altura_final > $nova_altura) {
			$y = (($altura_final - $nova_altura)/2);
		}
		else {
			$y = (($nova_altura - $altura_final)/2);
		}
	}
	else { 

		$largura_final = floor($nova_altura * ($largura/$altura)); 
		$altura_final  = $nova_altura;
		
		if ($largura_final > $nova_largura) {
			$x = (($largura_final - $nova_largura)/2);
		}
		else {
			$x = (($nova_largura - $largura_final)/2);
		}

		$y = 0;
	}


/*
	if ($largura < $altura) {

		$largura_final = $nova_largura;
		$altura_final  = floor($nova_largura * ($altura/$largura));
		$x = 0;
		$y = (($altura - $altura_final)/2);
	}
	else { 

		$largura_final = floor($nova_altura * ($largura/$altura)); 
		$altura_final  = $nova_altura; 
		$x = (($largura - $largura_final)/2);
		$y = 0;
	}
*/
	$f_x = 0;
	$f_y = 0;

	$img_final = imagecreatetruecolor($nova_largura, $nova_altura);
	$white     = imagecolorallocate($img_final, 255, 255, 255);

	imagefill($img_final, 0, 0, $white);
	imagecopyresampled($img_final, $img_origem, $f_x, $f_y, $x, $y, $largura_final, $altura_final, $largura, $altura);
	imagejpeg($img_final, $diretorio.$thumbnail);

	imagedestroy($img_origem);
	imagedestroy($img_final);
}


/*
//Exemplo:

$diretorio    = "../teste/";
$imagem       = "teste.jpg";
//$nova_largura = "150";
//$nova_altura  = "150";
$nova_altura  = "111";
$nova_largura = "74";
$complemento  = "_mini";

corta_imagem($diretorio, $imagem, $nova_largura, $nova_altura, $complemento);
<img src="<?php echo $diretorio.$imagem ?>" /><br>
<img src="<?php echo $diretorio."teste_mini.jpg" ?>" /><br>
*/
?>
