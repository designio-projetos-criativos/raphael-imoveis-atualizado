<?php

function corta_imagem_capa($diretorio, $imagem, $nova_largura, $nova_altura, $complemento)  {

    $largura = $nova_largura;
    $altura = $nova_altura;

	$img = imagecreatefromjpeg($diretorio.$imagem);

	$thumbnail = explode(".", $imagem); 
	$thumbnail = $thumbnail[0].$complemento.".jpg";

         if ($img)
            {
            $largura_atual = imagesx($img);
            $altura_atual = imagesy($img);

			//--ve se imagem vertical
			/*if($altura_atual > $largura_atual){
				$largura = $nova_altura;
				$altura = $nova_largura;
			}*/
			//--			   

            $escala = min($largura/$largura_atual, $altura/$altura_atual);

            if ($escala < 1)
               {
               $novalargura = floor($escala*$largura_atual);
               $novaaltura = floor($escala*$altura_atual);

               $img_temp = imagecreatetruecolor($novalargura, $novaaltura);
			   
			   imagecopyresampled($img_temp, $img, 0, 0, 0, 0, $novalargura, $novaaltura, $largura_atual, $altura_atual);

               imagedestroy($img);
               $img = $img_temp;
               }

     		imagejpeg($img,$diretorio.$thumbnail,100);
			}
	
}


/*
//Exemplo:

$diretorio    = "../teste/";
$imagem       = "teste.jpg";
//$nova_largura = "150";
//$nova_altura  = "150";
$nova_altura  = "111";
$nova_largura = "74";
$complemento  = "_mini";

corta_imagem($diretorio, $imagem, $nova_largura, $nova_altura, $complemento);
<img src="<?php echo $diretorio.$imagem ?>" /><br>
<img src="<?php echo $diretorio."teste_mini.jpg" ?>" /><br>
*/
?>
