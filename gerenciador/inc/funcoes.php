<?php
function listaUF($uf = "") {

	$arrUF = array("AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO", "MA", "MG", "MG", "MS", "MT", 
					"PA", "PB", "PE", "PI", "PR", "RJ", "RN", "RO", "RR", "RS", "SC", "SE", "SP", "TO");

	$lista_estados = "";
	for ($i = 0; $i < sizeof($arrUF); $i++) {
		$selecionado = ($uf == $arrUF[$i] ? "selected" : "");
		$lista_estados .= "<option value='".$arrUF[$i]."' ".$selecionado.">".$arrUF[$i]."</option>\n";
	}

	return $lista_estados;
}

function inject($str){
				if(!is_numeric($str)){ 
					$str = get_magic_quotes_gpc() ? 
						    stripslashes($str) : $str; 
					$str = function_exists('mysql_real_scape_string') ? 
						    mysql_real_escape_string($str) : mysql_escape_string($str); 
				}
				return $str;
	}
?>