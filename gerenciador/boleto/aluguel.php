<?php
session_start();
require_once "../../connection/conexao.php";
require_once "../inc/http_comply.php";
include ("../layout/inc/cabecalho.php");
include ("../layout/inc/topo.php");

$strMethod    = getMethod();

$msg  = $_GET['msg'];

if ($strMethod == "POST") {

	if(!empty($_FILES['upload_boleto'])) {

		$caminho = '../../uploads/boleto/boleto.txt';

		if(file_exists($caminho)){
			rename($caminho, '../../uploads/boleto/boleto_'.date('YmdHis').'.txt');
		}


		move_uploaded_file($_FILES['upload_boleto']['tmp_name'], $caminho);

		$ponteiro = fopen ($caminho, "r");

		mysql_query("delete from boletos");

		while (!feof ($ponteiro)) {
			$linha = fgets($ponteiro, 800);
			$codimv    = substr($linha, 0, 4);
			$mes       = substr($linha, 4, 6);
			$cpfcnpj   = substr($linha, 10, 14);
			$vencto    = substr($linha, 24, 8);
			$valor     = substr($linha, 32, 10);
			$imovel    = substr($linha, 42, 55);
			$locador   = substr($linha, 97, 55);
			$locatario = substr($linha, 152, 55);
			$compet    = substr($linha, 207, 23);
			$inst1     = substr($linha, 230, 70);
			$inst2     = substr($linha, 300, 70);
			$inst3     = substr($linha, 370, 70);
			$inst4     = substr($linha, 440, 70);
			$inst5     = substr($linha, 510, 70);
			$sacado    = substr($linha, 580, 40);
			$nossonum  = substr($linha, 620, 10);
			$endereco1 = substr($linha, 630, 70);
			$endereco2 = substr($linha, 700, 70);
			$dtlimite  = substr($linha, 770, 8);

			$vencto = substr($vencto, 0, 4) . '-' . substr($vencto, 4, 2) . '-' . substr($vencto, 6, 2);
			$dtlimite = substr($dtlimite, 0, 4) . '-' . substr($dtlimite, 4, 2) . '-' . substr($dtlimite, 6, 2);
			$valor = $valor / 100;

			$sql = mysql_query("insert into boletos (codimovel, mes, cpf_cnpj, vencimento, valor, imovel, locador, " . 
					"locatario, competencia, inst1, inst2, inst3, inst4, inst5, sacado, nossonumero, endereco1, endereco2, dtlimite) " .
					"values ('$codimv', '$mes', '$cpfcnpj', '$vencto', $valor, '$imovel', '$locador', '$locatario', '$compet', " .
					"'$inst1', '$inst2', '$inst3', '$inst4', '$inst5', '$sacado', '$nossonum', '$endereco1', '$endereco2', '$dtlimite')");
		}

		fclose ($ponteiro);

		$msg = 'Upload efetuado com sucesso.';
	}

	else{
		$msg = 'Nenhum arquivo enviado.';
	}

echo "<script>document.location = 'aluguel.php?msg=$msg';</script>";
}
?>
<script type="text/javascript" language="JavaScript" src="../js/script.js"></script>
<table height="78%" border="0" cellspacing="0" cellpadding="0" >
	<tr>
		<td height="147" valign="top" width="227" >
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<? include ("../inc/itens_menu.php");?>
				</tr>
			</table>
		</td>
		<td valign="top" width="549">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="top">
						<table width="530" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td><img src="../../images/pixel.gif" width="1" height="33"></td>
							</tr>
							<tr>
								<td>
									<table width="540"  border="0" cellpadding="0" cellspacing="2" class="bordaVerde">
										<tr>
											<td align="left">
												<table width="533"  border="0" cellpadding="0" cellspacing="0">
													<tr class="barraTit">
														<td>Boleto - Aluguel</td>
													</tr>
													<tr class="bordaSubtit">
														<td>&nbsp;</td>
													</tr>
													<tr>
														<td height="80" align="left" valign="top" class="corpo_dentro_tabs">
															<form name="form_boleto_aluguel" id="form_boleto_aluguel" action="aluguel.php" method="post" enctype="multipart/form-data">
																<table  border="0" cellspacing="0" cellpadding="0" class="ver10cinzaesc">
																	<tr>
																		<td height="24" align="left" class="ver10cinzaesc">Arquivo</td>
																		<td><input name="upload_boleto" type="file" class="ver10cinzaesc" /></td>
																	</tr>
																	<tr>
																		<td width="75" height="24" align="left" class="ver10cinzaesc">&nbsp;</td>
																		<td width="380" align="right" class="ver10cinzaesc" ><div align="left"><?php echo $msg ?>
																			<div align="right"><input name="Submit2" type="submit" class="ver10cinzaesc" value="Ok" /></div>
																		</div></td>
																	</tr>
																</table>
															</form>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3" height="100%"></td>
	</tr>	 
</table>
<? include "../layout/inc/rodape.php";?>
</body>
</html>
