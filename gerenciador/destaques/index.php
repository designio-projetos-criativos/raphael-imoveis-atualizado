<?php 
session_start();
require_once "../../connection/conexao.php";
require_once "../inc/http_comply.php";
require_once "../inc/paginador.php";

include ("../layout/inc/cabecalho.php");
include ("../layout/inc/topo.php");

?>

<table height="78%" border="0" cellspacing="0" cellpadding="0" >
<tr>
<td height="147" valign="top" width="227" >
	<table border="0" cellspacing="0" cellpadding="0">
	<tr>
    <? include ("../inc/itens_menu.php");?>
	</tr>
	</table>
</td>
<td valign="top" width="549">
	<table border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td valign="top">
		<table width="530" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="../../images/pixel.gif" width="1" height="33"></td>
		</tr>
		<tr>
		<td>
			<table width="540" border="0" cellpadding="0" cellspacing="2" class="bordaVerde">
			<tr>
			<td width="535" align="left">
				<table height="5" border="0" cellpadding="0" cellspacing="0" >
				<tr>
				<td width="60" align="center" background="../layout/img/bgr_menu.jpg" class='bordaVerde ver10cinzaesc'><a href="inserir.php?<?=$_SERVER['QUERY_STRING']?>" class="ver10cinzaesc">Inserir</a></td>
				<td width="60" align="center" class="bordaVerde barraTit">Gerenciar</td>
				</tr>
				</table>
				<table width="100%"  border="0" cellpadding="0" cellspacing="0">
				<tr class="barraTit">
				<td colspan="3" class="bordaDirVer">Slider</td>
				</tr>
				<tr class="bordaSubtitLista">
				<td width="400" align="center" class="bordaDirVer"><a href="index.php?ordena=titulo">Titulo</a></td>
                <td width="80" align="center" class="bordaDirVer"><a href="index.php?ordena=status">Status</a></td>
				<td width="45" align="center">A&ccedil;&atilde;o</td>
				</tr>
				<?php
				$tabela      = "destaques";           // TABELA DO DATABASE
				$urlhomepage = "index.php";  // LINK PARA SE $home=1
				$nrows       = 10;                   // RESULTADOS POR P&Aacute;GINA
				$nlinks      = 2;

				$ordena = $_GET['ordena'];
				if ($ordena == "titulo") {
					$ordena = "ORDER BY destaque_titulo";
				}
				if ($ordena == "link") {
					$ordena = "ORDER BY destaque_link";
				}
				if ($ordena == "status") {
					$ordena = "ORDER BY destaque_status";
				}
				else {
					$ordena = "ORDER BY destaque_id";
				}

				$intLinhas   = 0;
				$strNoticias = " SELECT * FROM destaques";
				$strNoticias = $strNoticias . " " . $ordena;
				$sqlfrase    = $strNoticias;

				$arquivo = $PHP_SELF."?ordena=$ordena&r=1";
				$id = $_GET['id'];

				if (!isset($id)) {
					$param = 0;
					$id    = 0;
					$temp  = 0;
				}
				else {
					$temp   = $id;
					$passo1 = $temp - 1;
					$passo2 = $passo1*$nrows;
					$param  = $passo2;
				}

				$sqllimit   = $sqlfrase . " LIMIT $param,$nrows";
				$rs1        = mysql_query($sqlfrase) or die("Query falhou NA COLSULTA");
				$totreg     = mysql_num_rows($rs1);
				$rs2        = mysql_query($sqllimit) or die("Query falhou NO LIMITE");
				$limitreg   = mysql_num_rows($rs2);
				$reg_final  = $param + $limitreg;
				$result_div = $totreg/$nrows;
				$n_inteiro  = (int)$result_div;

				if ($n_inteiro < $result_div) {
					$n_paginas = $n_inteiro + 1;
				}
				else {
					$n_paginas = $result_div;
				}

				$pg_atual      = $param/$nrows+1;
				$pg_anterior   = $pg_atual - 1;
				$pg_proxima    = $pg_atual + 1;
				$lnk_impressos = 0;

				$i      = 0;
				$cont   = 0;
				$record = array();
				while($intLinha = mysql_fetch_array($rs2)) {
				
					$destaque_id      = $intLinha['destaque_id'];
					$destaque_link  = $intLinha['destaque_link'];
                    $destaque_titulo = $intLinha['destaque_titulo'];
                    $destaque_status = $intLinha['destaque_status'];

					$intLinhas++;

					if ( $intLinhas%2 == 1 ) {
						$bgcolor = "#FFFFFF";
					}
					else {
						$bgcolor = "#E5E5E5";
					}
					$cont = $cont +1;
				?>
				<tr bgcolor="<?php echo $bgcolor ?>">
                
				<td height="17" align='left' class='bordaDirVer ver10cinzaesc' style='padding-left:15px'><?=$destaque_titulo?>&nbsp;</td>
                <td height="17" align='left' class='bordaDirVer ver10cinzaesc' style='padding-left:15px'><?=($destaque_status == 1) ? 'Ativo' : 'Inativo';?>&nbsp;</td>
                
				<td align="center" class="ver10cinzaesc">				
					<a href="editar.php?destaque_id=<?php echo $destaque_id ?>&<?=$_SERVER['QUERY_STRING']?>" class="ver10cinzaesc"><img src="../img/editar.gif" alt="Editar" width="16" height="16" border="0" /></a>
					<a href="excluir.php?destaque_id=<?php echo $destaque_id ?>&<?=$_SERVER['QUERY_STRING']?>" class="ver10cinzaesc"><img src="../img/excluir.gif" alt="Excluir" width="16" height="16" border="0" /></a></td>
				</tr>
				<?php
				}
				?>
				</table>
			</td>
			</tr>
			</table>
			<div align="left">
				<span class="ver10cinzaesc">&nbsp;&nbsp;Mostrando registro de
				<? if ($totreg == 0) echo $param; else echo $param+1; ?>
				at&eacute; <? echo $param + $limitreg; ?>. Total de registros: <? echo $totreg; ?>
				</span><br />
				<table width="200" border="0">
				<tr>
				<td>&nbsp;
				<? if ($totreg > 10)
				navipages($home=1,$center=1);?>
				</td>
				</tr>
				</table>
			</div>
		<!---->	
		</td>
		</tr>
		</table>	
	</td>
	</tr>
	</table>
</td>
</tr>
<tr>
<td colspan="3" height="100%"></td>
</tr>	 
</table>
<? include "../layout/inc/rodape.php";?>
</body>
</html>
