<?php 
session_start();
require_once "../../connection/conexao.php";
require_once "../inc/http_comply.php";
require_once "../inc/corta_imagem.php";

include ("../layout/inc/cabecalho.php");
include ("../layout/inc/topo.php");
include('../inc/redimensiona.php');

$url = 	$_SERVER['QUERY_STRING'];
if($_SERVER['REQUEST_METHOD'] == "GET"){
	$url = explode("&", $url);
	$url = $url[1]."&".$url[2]."&".$url[3];
}

$strMethod  = getMethod();
$diretorio = "../../uploads/destaques/";

if ( $strMethod == "POST" ) {

	$destaque_id     = getPost("destaque_id");	
	$site     = getPost("site");	
    $status = getPost("status");
    $titulo = getPost("titulo");
	$noticia_imagem  = $_FILES['noticia_imagem']['name'];	
	
	if($noticia_imagem != ""){
		$array_arq = explode(".", $noticia_imagem); 
		$extensao_arquivo = $array_arq[1]; 
	
	if (($extensao_arquivo == "jpg") or ($extensao_arquivo == "JPG")){
		$arquivo = "ok";
		}
		else{
		   	$msg = "Envie somente arquivos .jpg";
			echo "<script>document.location = 'editar.php?destaque_id=$destaque_id&erro=true&msg=$msg';</script>";
			exit();
			}
	}
			
	$site = str_replace("http://", "", $site);
	$site = "http://".$site;

			$strNoticias = " UPDATE destaques
							 SET 								 							 
							 destaque_link = '$site', destaque_titulo = '$titulo', destaque_status = '$status' WHERE destaque_id = '$destaque_id' ";

			$rstNoticias = mysql_query($strNoticias) or die(mysql_error());

	if ($arquivo == "ok") {	
		    $sql = "SELECT destaque_imagem from destaques where destaque_id='$destaque_id'";
	  		$resultado = mysql_query($sql) or die (mysql_error());
      		$linha = mysql_fetch_array($resultado);
      		$imagematual  = $linha['destaque_imagem'];			
      		
			if($imagematual != ""){
		        $del_imagem = $diretorio.$imagematual;
        		unlink($del_imagem);
				
				$del_thumb      = split("\.", $imagematual);
				$imagematual2   = $del_thumb[0]."_thumb.".$del_thumb[1];				
		        $del_imagem_thumb = $diretorio.$imagematual2;
        		unlink($del_imagem_thumb);
				
				/*$imagematual2   = $del_thumb[0]."_thumb2.".$del_thumb[1];				
		        $del_imagem_thumb = $diretorio.$imagematual2;
        		unlink($del_imagem_thumb);	*/			
				}
			
      		$nomearq = $destaque_id.".".strtolower($extensao_arquivo);
			
			$arquivo_e  = $_FILES['noticia_imagem']['tmp_name'];
			$arquivo_d  = $diretorio.$nomearq;
			if (move_uploaded_file($arquivo_e, $arquivo_d)) {
				corta_imagem($diretorio, $nomearq, 695, 316, "_thumb");
				//corta_imagem($diretorio, $nomearq, 117, 92, "_thumb2");
			}
            
            unlink($diretorio.$nomearq);
     
            $nomearq = explode(".", $nomearq);
            $nomearq = $nomearq[0]. '_thumb.' . $nomearq[1];
            
	 		$sql = "UPDATE destaques SET destaque_imagem='$nomearq' where destaque_id='$destaque_id'";
	        $resultado = mysql_query($sql) or die (mysql_error());
	}

	echo "<script>document.location = 'index.php?$url';</script>";
}


if (isset($_GET['destaque_id'])) {

	$destaque_id = getQuery("destaque_id");

	$strNoticias = " SELECT *
					 FROM destaques
					 WHERE destaque_id = '$destaque_id'";

	$rstNoticias = mysql_query($strNoticias) or die(mysql_error());

	while ($intLinha = mysql_fetch_array($rstNoticias)) {

        $titulo = $intLinha['destaque_titulo'];
        $status = $intLinha['destaque_status'];
		$site    = $intLinha['destaque_link'];
		$imagem  = $intLinha['destaque_imagem'];								
	}
}
else {
	echo "<script>document.location = 'index.php';</script>";
}

?>
<table height="78%" border="0" cellspacing="0" cellpadding="0" >
<tr>
<td height="147" valign="top" width="227" >
	<table border="0" cellspacing="0" cellpadding="0">
	<tr>
    <? include ("../inc/itens_menu.php");?>
	</tr>
	</table>
</td>
<td valign="top" width="549">
	<table border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td valign="top">
		<table width="530" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="../../images/pixel.gif" width="1" height="33"></td>
		</tr>
		<tr>
		<td>
		<!---->
			<table width="540"  border="0" cellpadding="0" cellspacing="2" class="bordaVerde">
			<tr>
			<td align="left">
				<table height="5" border="0" cellpadding="0" cellspacing="0" >
				<tr>
				<td width="60" align="center" background="../layout/img/bgr_menu.jpg" class='bordaVerde ver10cinzaesc'><a href="inserir.php" class="ver10cinzaesc">Inserir</a></td>
				<td width="60" align="center" class="bordaVerde barraTit">Editar</td>
				<td width="60" align="center" background="../layout/img/bgr_menu.jpg" class='bordaVerde ver10cinzaesc'><a href="index.php" class="ver10cinzaesc">Gerenciar</a></td>
				</tr>
				</table>
				<table width="533"  border="0" cellpadding="0" cellspacing="0">
				<tr class="barraTit">
				<td>Editar destaque de Venda</td>
				</tr>
				<tr class="bordaSubtit">
				<td>&nbsp;</td>
				</tr>
				<tr>
								<tr>
				<td height="80" align="left" valign="top" class="corpo_dentro_tabs">
					<form name="formNoticia" id="formNoticia" action="editar.php?<?=$url?>" method="post" enctype="multipart/form-data">
					<table  border="0" cellspacing="0" cellpadding="0">
                    <input type="hidden" name="destaque_id" value="<?=$destaque_id?>"/>
					<tr>
					<td height="24" align="left" class="ver10cinzaesc">Titulo</td>
					<td><input name="titulo" type="text" class="ver10cinzaesc" style="width:372px;" value="<?php echo $titulo; ?>" size="150" maxlength="255" /></td>
					</tr>
                    <tr>
					<td height="24" align="left" class="ver10cinzaesc">Link</td>
					<td><input name="site" type="text" class="ver10cinzaesc" style="width:372px;" value="<?php echo $site ?>" size="150" maxlength="255" /></td>
					</tr>
                    
											
					<tr>
					<td height="24" align="left" class="ver10cinzaesc">Imagem</td>
					<td><input name="noticia_imagem" type="file" id="noticia_imagem" class="ver10cinzaesc" /></td>
                    <td height="24" align="left" class="ver10cinzaesc">Resolu��o: 695x316</td>
					</tr>																	
					<tr>
                    <td width="75" height="24" align="left" class="ver10cinzaesc">&nbsp;</td>
					<td width="380" align="right" class="ver10cinzaesc" ><div align="left"><?php echo $msg ?>
                    
                    <tr>
					<td height="24" align="left" class="ver10cinzaesc">Status</td>
					<td>
                        <select name="status">
                            <option value="1" <?=$option1?>>Ativo</option>
                            <option value="0" <?=$option2?>>Inativo</option>
                        </select>
                    </td>
					</tr>
                    		

					<div align="right">
					<input name="Submit2" type="submit" class="ver10cinzaesc" value="Ok" />
					</div>
					</div></td>
					</tr>
					</table>
					</form>
				</td>
				</tr>
				</table>
			</td>
			</tr>
			</table>
		<!---->	
		</td>
		</tr>
		</table>	
	</td>
	</tr>
	</table>
</td>
</tr>
<tr>
<td colspan="3" height="100%"></td>
</tr>	 
</table>
<? include "../layout/inc/rodape.php";?>
</body>
</html>
