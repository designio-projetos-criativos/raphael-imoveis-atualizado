
function carregaLinhas(codigo_tipo)
{
	document.getElementById('linha_id').options.length = 1;
	id_opcao  = document.getElementById('linha_id').options[0];
	
	ajax();
	xmlhttp.open('POST', '../xml/linhas.php', true);
	xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 1)
		{
			id_opcao.innerHTML = 'carregando...'; 
		}

		if (xmlhttp.readyState == 4 )
		{
			if (xmlhttp.responseXML)
			{
				document.getElementById('linha_id').options.length = 0;
				carregaLista(xmlhttp.responseXML, 'linha', 'linha_id');
			}
			else 
			{
				id_opcao.innerHTML = '--primeiro selecione o tipo--';
			}
		}
	}
	var params = 'tipo='+codigo_tipo;
	xmlhttp.send(params);
}


function carregaCodigos(codigo_modelo)
{
	document.getElementById('codigo').options.length = 1;

	id_opcao = document.getElementById('codigo').options[0];

	ajax();
	xmlhttp.open('POST', '../../xml/modelos_codigos.php', true);
	xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

	xmlhttp.onreadystatechange = function() {

		if (xmlhttp.readyState == 1)
		{
			id_opcao.innerHTML = 'carregando...';   
		}

		if (xmlhttp.readyState == 4 )
		{
			if (xmlhttp.responseXML)
			{
				document.getElementById('codigo').options.length = 0;
				carregaLista(xmlhttp.responseXML, 'modelo_codigo', 'codigo');
			}
			else 
			{
				id_opcao.innerHTML = '--primeiro selecione o modelo--';
			}
		}
	}
	var params = 'modelo='+codigo_modelo;
	xmlhttp.send(params);
}
   

function carregaLista(dados_xml, tag_dados, id_select)
{
	var dataArray = dados_xml.getElementsByTagName(tag_dados);

	if (dataArray.length > 0)
	{
		for (var i = 0 ; i < dataArray.length ; i++) 
		{
			var item = dataArray[i];

			var codigo    = item.getElementsByTagName('codigo')[0].firstChild.nodeValue;
			var descricao = item.getElementsByTagName('descricao')[0].firstChild.nodeValue;

			var novo   = document.createElement('option');
			novo.value = codigo;
			novo.text  = descricao;
			document.getElementById(id_select).options.add(novo);
		}
	}
	else 
	{
		document.getElementById(id_select).options[0].innerHTML = 'dados n�o dispon�veis';
	}	  
}
