function go()
{
	box = document.forms[1].navi; // o numero 1 significa a posicao do formulario no html no caso 2� form
	destination = box.options[box.selectedIndex].value;
	if (destination) location.href = destination;
}


function go_url()
{
	box = document.forms[1].select; // o numero 1 significa a posicao do formulario no html no caso 2� form e select eh o nome do select do form
	destination = box.options[box.selectedIndex].value;
	if (destination) location.href = destination;
}


function FormataData(campo,teclapres) {
	var tecla = teclapres.keyCode;
	vr = document.form[campo].value;
	vr = vr.replace( ".", "" );
	vr = vr.replace( "/", "" );
	vr = vr.replace( "/", "" );
	tam = vr.length + 1;

	if ( tecla != 9 && tecla != 8 ){
		if ( tam > 2 && tam < 5 )
			document.form[campo].value = vr.substr( 0, tam - 2  ) + '/' + vr.substr( tam - 2, tam );
		if ( tam >= 5 && tam <= 10 )
			document.form[campo].value = vr.substr( 0, 2 ) + '/' + vr.substr( 2, 2 ) + '/' + vr.substr( 4, 4 ); 
	}
}

<!--Para os campos do topo - Select 1-->

var req; 

function loadXMLDoc_Select1(url,valor) 
{ 
    req = null; 
    // Procura por um objeto nativo (Mozilla/Safari) 
    if (window.XMLHttpRequest) { 
        req = new XMLHttpRequest(); 
        req.onreadystatechange = processReqChange_Select1; 
        req.open("GET", url+'?filial='+valor, true); 
        req.send(null); 
    // Procura por uma versao ActiveX (IE) 
    } else if (window.ActiveXObject) { 
        req = new ActiveXObject("Microsoft.XMLHTTP"); 
        if (req) { 
            req.onreadystatechange = processReqChange_Select1; 
            req.open("GET", url+'?filial='+valor, true); 
            req.send(); 
        } 
    } 
} 

function processReqChange_Select1() 
{ 
    // apenas quando o estado for "completado" 
    if (req.readyState == 4) { 
        // apenas se o servidor retornar "OK" 
        if (req.status == 200) { 
            // procura pela div id="atualiza" e insere o conteudo 
            // retornado nela, como texto HTML 
            document.getElementById('div_sessao').innerHTML = req.responseText; 
        } else { 
            alert("Houve um problema ao obter os dados:\n" + req.statusText); 
        } 
    } 
} 

function Atualiza_Select1(valor) 
{ 
    loadXMLDoc_Select1("atualiza_sessao.php",valor);
	window.top.location.href=document.location;
} 

<!--Para os campos do topo - Select 1-->

var req; 

function loadXMLDoc_Select2(url) 
{ 
    req = null; 
    // Procura por um objeto nativo (Mozilla/Safari) 
    if (window.XMLHttpRequest) { 
        req = new XMLHttpRequest(); 
        req.onreadystatechange = processReqChange_Select2; 
        req.open("GET", url, true); 
        req.send(null); 
    // Procura por uma versao ActiveX (IE) 
    } else if (window.ActiveXObject) { 
        req = new ActiveXObject("Microsoft.XMLHTTP"); 
        if (req) { 
            req.onreadystatechange = processReqChange_Select2; 
            req.open("GET", url, true); 
            req.send(); 
        } 
    } 
} 

function processReqChange_Select2() 
{ 
    // apenas quando o estado for "completado" 
    if (req.readyState == 4) { 
        // apenas se o servidor retornar "OK" 
        if (req.status == 200) { 
            // procura pela div id="atualiza" e insere o conteudo 
            // retornado nela, como texto HTML 
            document.getElementById('veiculo').innerHTML = req.responseText; 
        } else { 
            alert("Houve um problema ao obter os dados:\n" + req.statusText); 
        } 
    } 
} 

function Adiciona_veiculo() 
{ 
     loadXMLDoc_Select2("mm.html");
	
	 
} 


var isNav4 = false, isNav5 = false, isIE4 = false
var strSeperator = "/"; 
var vDateType = 3; // Global value for type of date format
//                1 = mm/dd/yyyy
//                2 = yyyy/dd/mm  (Unable to do date check at this time)
//                3 = dd/mm/yyyy
var vYearType = 4; //Set to 2 or 4 for number of digits in the year for Netscape
var vYearLength = 2; // Set to 4 if you want to force the user to enter 4 digits for the year before validating.
var err = 0; // Set the error code to a default of zero
if(navigator.appName == "Netscape") {
if (navigator.appVersion < "5") {
isNav4 = true;
isNav5 = false;
}
else
if (navigator.appVersion > "4") {
isNav4 = false;
isNav5 = true;
   }
}
else {
isIE4 = true;
}
function DateFormat(vDateName, vDateValue, e, dateCheck, dateType) {
vDateType = dateType;
// vDateName = object name
// vDateValue = value in the field being checked
// e = event
// dateCheck 
// True  = Verify that the vDateValue is a valid date
// False = Format values being entered into vDateValue only
// vDateType
// 1 = mm/dd/yyyy
// 2 = yyyy/mm/dd
// 3 = dd/mm/yyyy
//Enter a tilde sign for the first number and you can check the variable information.
if (vDateValue == "~") {
alert("AppVersion = "+navigator.appVersion+" \nNav. 4 Version = "+isNav4+" \nNav. 5 Version = "+isNav5+" \nIE Version = "+isIE4+" \nYear Type = "+vYearType+" \nDate Type = "+vDateType+" \nSeparator = "+strSeperator);
vDateName.value = "";
vDateName.focus();
return true;
}
var whichCode = (window.Event) ? e.which : e.keyCode;
// Check to see if a seperator is already present.
// bypass the date if a seperator is present and the length greater than 8
if (vDateValue.length > 8 && isNav4) {
if ((vDateValue.indexOf("-") >= 1) || (vDateValue.indexOf("/") >= 1))
return true;
}

/* modifica��o feita para n�o 'cortar' o �ltimo digito do ano no Firefox 3 */
if (vDateValue.length > 8 && isNav5) {
if ((vDateValue.indexOf("-") >= 1) || (vDateValue.indexOf("/") >= 1))
return true;
}


//Eliminate all the ASCII codes that are not valid
var alphaCheck = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-";
if (alphaCheck.indexOf(vDateValue) >= 1) {
if (isNav4) {
vDateName.value = "";
vDateName.focus();
vDateName.select();
return false;
}
else {
vDateName.value = vDateName.value.substr(0, (vDateValue.length-1));
return false;
   }
}
if (whichCode == 8) //Ignore the Netscape value for backspace. IE has no value
return false;
else {
//Create numeric string values for 0123456789/
//The codes provided include both keyboard and keypad values
var strCheck = '47,48,49,50,51,52,53,54,55,56,57,58,59,95,96,97,98,99,100,101,102,103,104,105';
if (strCheck.indexOf(whichCode) != -1) {
if (isNav4) {
if (((vDateValue.length < 6 && dateCheck) || (vDateValue.length == 7 && dateCheck)) && (vDateValue.length >=1)) {
alert("Data inv�lida\nPor favor, digite novamente.");
vDateName.value = "";
vDateName.focus();
vDateName.select();
return false;
}
if (vDateValue.length == 6 && dateCheck) {
var mDay = vDateName.value.substr(2,2);
var mMonth = vDateName.value.substr(0,2);
var mYear = vDateName.value.substr(4,4)
//Turn a two digit year into a 4 digit year
if (mYear.length == 2 && vYearType == 4) {
var mToday = new Date();
//If the year is greater than 30 years from now use 19, otherwise use 20
var checkYear = mToday.getFullYear() + 30; 
var mCheckYear = '20' + mYear;
if (mCheckYear >= checkYear)
mYear = '19' + mYear;
else
mYear = '20' + mYear;
}
var vDateValueCheck = mMonth+strSeperator+mDay+strSeperator+mYear;
if (!dateValid(vDateValueCheck)) {
alert("Data inv�lida\nPor favor, digite novamente.");
vDateName.value = "";
vDateName.focus();
vDateName.select();
return false;
}
return true;
}
else {
// Reformat the date for validation and set date type to a 1
if (vDateValue.length >= 8  && dateCheck) {
if (vDateType == 1) // mmddyyyy
{
var mDay = vDateName.value.substr(2,2);
var mMonth = vDateName.value.substr(0,2);
var mYear = vDateName.value.substr(4,4)
vDateName.value = mMonth+strSeperator+mDay+strSeperator+mYear;
}
if (vDateType == 2) // yyyymmdd
{
var mYear = vDateName.value.substr(0,4)
var mMonth = vDateName.value.substr(4,2);
var mDay = vDateName.value.substr(6,2);
vDateName.value = mYear+strSeperator+mMonth+strSeperator+mDay;
}
if (vDateType == 3) // ddmmyyyy
{
var mMonth = vDateName.value.substr(2,2);
var mDay = vDateName.value.substr(0,2);
var mYear = vDateName.value.substr(4,4)
vDateName.value = mDay+strSeperator+mMonth+strSeperator+mYear;
}
//Create a temporary variable for storing the DateType and change
//the DateType to a 1 for validation.
var vDateTypeTemp = vDateType;
vDateType = 1;
var vDateValueCheck = mMonth+strSeperator+mDay+strSeperator+mYear;
if (!dateValid(vDateValueCheck)) {
alert("Data inv�lida\nPor favor, digite novamente.");
vDateType = vDateTypeTemp;
vDateName.value = "";
vDateName.focus();
vDateName.select();
return false;
}
vDateType = vDateTypeTemp;
return true;
}
else {
if (((vDateValue.length < 8 && dateCheck) || (vDateValue.length == 9 && dateCheck)) && (vDateValue.length >=1)) {
alert("Data inv�lida\nPor favor, digite novamente.");
vDateName.value = "";
vDateName.focus();
vDateName.select();
return false;
         }
      }
   }
}
else {
// Non isNav Check
if (((vDateValue.length < 8 && dateCheck) || (vDateValue.length == 9 && dateCheck)) && (vDateValue.length >=1)) {
alert("Data inv�lida\nPor favor, digite novamente.");
vDateName.value = "";
vDateName.focus();
return true;
}
// Reformat date to format that can be validated. mm/dd/yyyy
if (vDateValue.length >= 8 && dateCheck) {
// Additional date formats can be entered here and parsed out to
// a valid date format that the validation routine will recognize.
if (vDateType == 1) // mm/dd/yyyy
{
var mMonth = vDateName.value.substr(0,2);
var mDay = vDateName.value.substr(3,2);
var mYear = vDateName.value.substr(6,4)
}
if (vDateType == 2) // yyyy/mm/dd
{
var mYear = vDateName.value.substr(0,4)
var mMonth = vDateName.value.substr(5,2);
var mDay = vDateName.value.substr(8,2);
}
if (vDateType == 3) // dd/mm/yyyy
{
var mDay = vDateName.value.substr(0,2);
var mMonth = vDateName.value.substr(3,2);
var mYear = vDateName.value.substr(6,4)
}
if (vYearLength == 4) {
if (mYear.length < 4) {
alert("Data inv�lida\nPor favor, digite novamente.");
vDateName.value = "";
vDateName.focus();
return true;
   }
}
// Create temp. variable for storing the current vDateType
var vDateTypeTemp = vDateType;
// Change vDateType to a 1 for standard date format for validation
// Type will be changed back when validation is completed.
vDateType = 1;
// Store reformatted date to new variable for validation.
var vDateValueCheck = mMonth+strSeperator+mDay+strSeperator+mYear;
if (mYear.length == 2 && vYearType == 4 && dateCheck) {
//Turn a two digit year into a 4 digit year
var mToday = new Date();
//If the year is greater than 30 years from now use 19, otherwise use 20
var checkYear = mToday.getFullYear() + 30; 
var mCheckYear = '20' + mYear;
if (mCheckYear >= checkYear)
mYear = '19' + mYear;
else
mYear = '20' + mYear;
vDateValueCheck = mMonth+strSeperator+mDay+strSeperator+mYear;
// Store the new value back to the field.  This function will
// not work with date type of 2 since the year is entered first.
if (vDateTypeTemp == 1) // mm/dd/yyyy
vDateName.value = mMonth+strSeperator+mDay+strSeperator+mYear;
if (vDateTypeTemp == 3) // dd/mm/yyyy
vDateName.value = mDay+strSeperator+mMonth+strSeperator+mYear;
} 
if (!dateValid(vDateValueCheck)) {
alert("Data inv�lida\nPor favor, digite novamente.");
vDateType = vDateTypeTemp;
vDateName.value = "";
vDateName.focus();
return true;
}
vDateType = vDateTypeTemp;
return true;
}
else {
if (vDateType == 1) {
if (vDateValue.length == 2) {
vDateName.value = vDateValue+strSeperator;
}
if (vDateValue.length == 5) {
vDateName.value = vDateValue+strSeperator;
   }
}
if (vDateType == 2) {
if (vDateValue.length == 4) {
vDateName.value = vDateValue+strSeperator;
}
if (vDateValue.length == 7) {
vDateName.value = vDateValue+strSeperator;
   }
} 
if (vDateType == 3) {
if (vDateValue.length == 2) {
vDateName.value = vDateValue+strSeperator;
}
if (vDateValue.length == 5) {
vDateName.value = vDateValue+strSeperator;
   }
}
return true;
   }
}
if (vDateValue.length == 10&& dateCheck) {
if (!dateValid(vDateName)) {
// Un-comment the next line of code for debugging the dateValid() function error messages
//alert(err);  
alert("Data inv�lida\nPor favor, digite novamente.");
vDateName.focus();
vDateName.select();
   }
}
return false;
}
else {
// If the value is not in the string return the string minus the last
// key entered.
if (isNav4) {
vDateName.value = "";
vDateName.focus();
vDateName.select();
return false;
}
else
{
vDateName.value = vDateName.value.substr(0, (vDateValue.length-1));
return false;
         }
      }
   }
}
function dateValid(objName) {
var strDate;
var strDateArray;
var strDay;
var strMonth;
var strYear;
var intday;
var intMonth;
var intYear;
var booFound = false;
var datefield = objName;
var strSeparatorArray = new Array("-"," ","/",".");
var intElementNr;
// var err = 0;
var strMonthArray = new Array(12);
strMonthArray[0] = "Jan";
strMonthArray[1] = "Feb";
strMonthArray[2] = "Mar";
strMonthArray[3] = "Apr";
strMonthArray[4] = "May";
strMonthArray[5] = "Jun";
strMonthArray[6] = "Jul";
strMonthArray[7] = "Aug";
strMonthArray[8] = "Sep";
strMonthArray[9] = "Oct";
strMonthArray[10] = "Nov";
strMonthArray[11] = "Dec";
//strDate = datefield.value;
strDate = objName;
if (strDate.length < 1) {
return true;
}
for (intElementNr = 0; intElementNr < strSeparatorArray.length; intElementNr++) {
if (strDate.indexOf(strSeparatorArray[intElementNr]) != -1) {
strDateArray = strDate.split(strSeparatorArray[intElementNr]);
if (strDateArray.length != 3) {
err = 1;
return false;
}
else {
strDay = strDateArray[0];
strMonth = strDateArray[1];
strYear = strDateArray[2];
}
booFound = true;
   }
}
if (booFound == false) {
if (strDate.length>5) {
strDay = strDate.substr(0, 2);
strMonth = strDate.substr(2, 2);
strYear = strDate.substr(4);
   }
}
//Adjustment for short years entered
if (strYear.length == 2) {
strYear = '20' + strYear;
}
strTemp = strDay;
strDay = strMonth;
strMonth = strTemp;
intday = parseInt(strDay, 10);
if (isNaN(intday)) {
err = 2;
return false;
}
intMonth = parseInt(strMonth, 10);
if (isNaN(intMonth)) {
for (i = 0;i<12;i++) {
if (strMonth.toUpperCase() == strMonthArray[i].toUpperCase()) {
intMonth = i+1;
strMonth = strMonthArray[i];
i = 12;
   }
}
if (isNaN(intMonth)) {
err = 3;
return false;
   }
}
intYear = parseInt(strYear, 10);
if (isNaN(intYear)) {
err = 4;
return false;
}
if (intMonth>12 || intMonth<1) {
err = 5;
return false;
}
if ((intMonth == 1 || intMonth == 3 || intMonth == 5 || intMonth == 7 || intMonth == 8 || intMonth == 10 || intMonth == 12) && (intday > 31 || intday < 1)) {
err = 6;
return false;
}
if ((intMonth == 4 || intMonth == 6 || intMonth == 9 || intMonth == 11) && (intday > 30 || intday < 1)) {
err = 7;
return false;
}
if (intMonth == 2) {
if (intday < 1) {
err = 8;
return false;
}
if (LeapYear(intYear) == true) {
if (intday > 29) {
err = 9;
return false;
   }
}
else {
if (intday > 28) {
err = 10;
return false;
      }
   }
}
return true;
}
function LeapYear(intYear) {
if (intYear % 100 == 0) {
if (intYear % 400 == 0) { return true; }
}
else {
if ((intYear % 4) == 0) { return true; }
}
return false;
}
//  End -->




	// Functions in JavaScript to fields edition,
	// like currency, date, integer and number, in the european standard.

	// License: Shareware.
	// Version: Brazil - 2005.
	// TradeMark: amf2005.
	// Author: Arnaldo Machado Filho, Phycisist (USP-1978), wewill@ig.com.br

function Mascara (formato, keypress, objeto){
campo = eval (objeto);

// cep
if (formato=='cep'){
separador = '-';
conjunto1 = 5;
if (campo.value.length == conjunto1){
campo.value = campo.value + separador;}
}

// cpf
if (formato=='cpf'){
separador1 = '.'; 
separador2 = '-'; 
conjunto1 = 3;
conjunto2 = 7;
conjunto3 = 11;
if (campo.value.length == conjunto1)
  {
  campo.value = campo.value + separador1;
  }
if (campo.value.length == conjunto2)
  {
  campo.value = campo.value + separador1;
  }
if (campo.value.length == conjunto3)
  {
  campo.value = campo.value + separador2;
  }
}

// nascimento
if (formato=='nascimento'){
separador = '/'; 
conjunto1 = 2;
conjunto2 = 5;
if (campo.value.length == conjunto1)
  {
  campo.value = campo.value + separador;
  }
if (campo.value.length == conjunto2)
  {
  campo.value = campo.value + separador;
  }
}

// telefone
if (formato=='telefone'){
separador1 = '(';
separador2 = ')';
separador3 = '-';
conjunto1 = 0;
conjunto2 = 3;
conjunto3 = 8;
if (campo.value.length == conjunto1){
campo.value = campo.value + separador1;
}
if (campo.value.length == conjunto2){
campo.value = campo.value + separador2;
}
if (campo.value.length == conjunto3){
campo.value = campo.value + separador3;
}
}


}



function valor_monetario(cur,len)
{
   n='__0123456789';
   d=cur.value;
   l=d.length;
   r='';
   if (l > 0)
   {
	z=d.substr(0,l-1);
	s='';
	a=2;
	for (i=0; i < l; i++)
	{
		c=d.charAt(i);
		if (n.indexOf(c) > a)
		{
			a=1;
			s+=c;
		};
	};
	l=s.length;
	t=len-1;
	if (l > t)
	{
		l=t;
		s=s.substr(0,t);
	};
	if (l > 2)
	{
		r=s.substr(0,l-2)+','+s.substr(l-2,2);
	}
	else
	{
		if (l == 2)
		{
			r='0,'+s;
		}
		else
		{
			if (l == 1)
			{
				r='0,0'+s;
			};
		};
	};
	if (r == '')
	{
		r='0,00';
	}
	else
	{
		l=r.length;
		if (l > 6)
		{
			j=l%3;
			w=r.substr(0,j);
			wa=r.substr(j,l-j-6);
			wb=r.substr(l-6,6);
			if (j > 0)
			{
				w+='.';
			};
			k=(l-j)/3-2;
			for (i=0; i < k; i++)
			{
				w+=wa.substr(i*3,3)+'.';
			};
			r=w+wb;
		};
	};
   };
   if (r.length <= len)
   {
	cur.value=r;
   }
   else
   {
	cur.value=z;
   };
   return 'ok';
};


function txtBoxFormat(objForm, strField, sMask, evtKeyPress) {
     var i, nCount, sValue, fldLen, mskLen,bolMask, sCod, nTecla;
     nTecla = (evtKeyPress.which) ? evtKeyPress.which : evtKeyPress.keyCode;
     sValue = objForm[strField].value;
     // Limpa todos os caracteres de formata��o que
     // j� estiverem no campo.
     expressao = /[\.\/\-\(\)\,\;\: ]/gi;
     sValue = sValue.toString().replace(expressao, '');
     fldLen = sValue.length;
     mskLen = sMask.length;

     i = 0;
     nCount = 0;
     sCod = "";
     mskLen = fldLen;

     while (i <= mskLen) {
       bolMask = ((sMask.charAt(i) == "-") || (sMask.charAt(i) == ".") || (sMask.charAt(i) == "/") || (sMask.charAt(i) == ",") || (sMask.charAt(i) == ";") || (sMask.charAt(i) == ":"))
       bolMask = bolMask || ((sMask.charAt(i) == "(") || (sMask.charAt(i) == ")") || (sMask.charAt(i) == " "))

       if (bolMask) {
         sCod += sMask.charAt(i);
         mskLen++; }
       else {
         sCod += sValue.charAt(nCount);
         nCount++;
       }

       i++;
     }

     objForm[strField].value = sCod;

     if (nTecla != 8 && nTecla != 13)
      { // backspace enter
       if (sMask.charAt(i-1) == "9")
       { // apenas n�meros...
         return ((nTecla > 47) && (nTecla < 58));
       } // n�meros de 0 a 9
       else
       {
            if (sMask.charAt(i-1) == "x")
           { // apenas letras... Sem espaco
             return ((nTecla > 64) && (nTecla < 123));
           } // maiusculas e minusculas de A a z sem acentos
           else
           { // qualquer caracter...
            return true;
          }
       }
      }
     else
     {
       return true;
     }
   }
//Fim da Fun��o M�scaras Gerais

function txtBoxFormat_com_letras(objForm, strField, sMask, evtKeyPress) {
     var i, nCount, sValue, fldLen, mskLen,bolMask, sCod, nTecla;
     nTecla = (evtKeyPress.which) ? evtKeyPress.which : evtKeyPress.keyCode;
     sValue = objForm[strField].value;
     // Limpa todos os caracteres de formata��o que
     // j� estiverem no campo.
     expressao = /[\.\/\-\(\)\,\;\: ]/gi;
     sValue = sValue.toString().replace(expressao, '');
     fldLen = sValue.length;
     mskLen = sMask.length;

     i = 0;
     nCount = 0;
     sCod = "";
     mskLen = fldLen;

     while (i <= mskLen) {
       bolMask = ((sMask.charAt(i) == "-") || (sMask.charAt(i) == ".") || (sMask.charAt(i) == "/") || (sMask.charAt(i) == ",") || (sMask.charAt(i) == ";") || (sMask.charAt(i) == ":"))
       bolMask = bolMask || ((sMask.charAt(i) == "(") || (sMask.charAt(i) == ")") || (sMask.charAt(i) == " "))

       if (bolMask) {
         sCod += sMask.charAt(i);
         mskLen++; }
       else {
         sCod += sValue.charAt(nCount);
         nCount++;
       }

       i++;
     }

     objForm[strField].value = sCod;

     if (nTecla != 8 && nTecla != 13)
      { // backspace enter
       if (sMask.charAt(i-1) != "9")
       { // apenas n�meros...
         return ((nTecla > 47) && (nTecla < 58));
       } // n�meros de 0 a 9
       else
       {
            if (sMask.charAt(i-1) == "x")
           { // apenas letras... Sem espaco
             return ((nTecla > 64) && (nTecla < 123));
           } // maiusculas e minusculas de A a z sem acentos
           else
           { // qualquer caracter...
            return true;
          }
       }
      }
     else
     {
       return true;
     }
   }
//Fim da Fun��o M�scaras Gerais

function go()
{
	box = document.forms[1].navi; // o numero 1 significa a posicao do formulario no html no caso 2� form
	destination = box.options[box.selectedIndex].value;
	if (destination) location.href = destination;
}



/*
Fun��es para formata��o de hora.
Fonte: http://codigofonte.uol.com.br/codigo/js-dhtml/data-hora/mascara-e-validacao-de-data-e-hora
*/
	function mascara_hora(campo){ 

		// verifica se foram digitados valores incorretos
		var alphaCheck = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-";
		if (alphaCheck.indexOf(campo.value) >= 1) {
			alert("Hora inv�lida!"); 
			campo.value = "";
		}


		var myhora = ''; 
		var hora = campo.value;

		myhora = myhora + hora; 
		if (myhora.length == 2){ 
			myhora = myhora + ':'; 
			campo.value = myhora; 
		} 
		if (myhora.length == 5){ 
			verifica_hora(campo); 
		} 
	} 


	function verifica_hora(campo){ 

		hrs = (campo.value.substring(0,2)); 
		min = (campo.value.substring(3,5)); 
               
//              alert('hrs '+ hrs); 
//              alert('min '+ min); 
               
		situacao = ""; 
		// verifica data e hora 
		if ((hrs < 00 ) || (hrs > 23) || ( min < 00) ||( min > 59)){ 
			situacao = "falsa"; 
		} 
		
		if (campo.value == "") { 
			situacao = "falsa"; 
		} 
		
		if (situacao == "falsa") { 
			alert("Hora inv�lida!"); 
			campo.focus(); 
		} 
	} 


