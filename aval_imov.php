<?php
require "head.php";
if($_SERVER['REQUEST_METHOD'] == "POST"){
	$endereco_imo   = $_REQUEST['endereco_imo'];
	$bairro_imo  = $_REQUEST['bairro_imo'];
	$cidade_imo = $_REQUEST['cidade_imo'];	
	$nome   = $_REQUEST['nome'];
	$endereco   = $_REQUEST['endereco'];
	$foneres   = $_REQUEST['foneres'];
	$fonecom   = $_REQUEST['fonecom'];
	$fonecel   = $_REQUEST['fonecel'];
	$email   = $_REQUEST['email'];
}
?>
</head>
<body id="internas" class="avaliacao">
<? require_once "header.php"; ?>
<div class="content">
	<div class="centro636 tela_aval">
		<h1 class="bordLaranja">Avaliação de Imóvel para Locação</h1>
		<h2 class="seo">Avaliação de Imóvel para Locação</h2>
		<form name="aval_imo" id="aval_imo" action="aval_imo.php" method="post">
			<fieldset>
			<?php if ($_SERVER['REQUEST_METHOD'] == "POST"){ ?>
			<table cellpadding="0" cellspacing="0">
				<tbody>
					<tr><td>
					<?php
					   if (trim($endereco_imo) == "" or trim($nome) == "")
						 { echo "Por favor $nome: informe seus dados e do imóvel <p> para podermos entrar em contato para avaliação do imóvel";}
					   else
						 {
						   mail("imobiliaria@raphaelimoveis.com.br","[Avaliação Imóvel(Aluguel): Página Raphael]","Dados do Imóvel:\nEndereço: $endereco_imo\nBairro: $bairro_imo\nCidade: $cidade_imo\n\nDados do Proprietário:\nNome: $nome\nEndereço: $endereco\nFone Residencial: $foneres\nFone Comercial: $fonecom\nFone Celular: $fonecel\n","From: $email");
						   mail("isan@raphaelimoveis.com.br","[Avaliação Imóvel(Aluguel): Página Raphael]","Dados do Imóvel:\nEndereço: $endereco_imo\nBairro: $bairro_imo\nCidade: $cidade_imo\n\nDados do Proprietário:\nNome: $nome\nEndereço: $endereco\nFone Residencial: $foneres\nFone Comercial: $fonecom\nFone Celular: $fonecel\n","From: $email");
						   echo "Obrigado $nome! Em breve entraremos<p> em contato para avaliação do seu imóvel.";
						 }
					?>
					</td></tr>
				</tbody>
			</table>
			<? }?>
			<table class="tabela_aval" cellpadding="0" cellspacing="0">
				<tr><td colspan="2"><label class="under">Dados do Imóvel:</label></td></tr>
				<tr>
					<td><label for="endereco_imo">Endereço:</label></td>
					<td><input type="text" id="endereco_imo" name="endereco_imo" value="" style="width:200px;" /></td>
				</tr>
				<tr>
					<td><label for="bairro_imo">Bairro:</label></td>
					<td><input type="text" id="bairro_imo" name="bairro_imo" value="" style="width:200px;" /></td>
				</tr>
				<tr>
					<td ><label>Cidade:</label></td>
					<td><input type="radio" name="cidade_imo" value="pelotas" checked="checked" />Pelotas<br />
					<input type="radio" name="cidade_imo" value="porto alegre" />Porto Alegre</td>
				</tr>
				<tr><td class="dados_prop" colspan="2"><label class="under"><br/>Dados do Proprietário:</label></td></tr>
				<tr>
					<td><label for="nome">Nome:</label></td> 
					<td><input type="text" id="nome" name="nome" maxlength="50" style="width:200px;"/></td>
				</tr>
				<tr>
					<td><label for="endereco">Endereço:</label></td>
					<td><input type="text" id="endereco" name="endereco" maxlength="50" style="width:200px;"/></td>
				</tr>
				<tr>
					<td><label for="foneres">Telefone Residencial:</label></td>
					<td><input type="text" id="foneres" name="foneres" maxlength="50" style="width:200px;"/></td>
				</tr>
				<tr>
					<td><label for="fonecom">Telefone Comercial:</label></td>
					<td><input type="text" id="fonecom" name="fonecom" maxlength="50" style="width:200px;"/></td>
				</tr>
				<tr>
					<td><label for="fonecel">Telefone Celular:</label></td>
					<td><input type="text" id="fonecel" name="fonecel" maxlength="50" style="width:200px;"/></td>
				</tr>
				<tr>
					<td><label for="email">E-mail:</label></td>
					<td><input type="text" id="email" name="email" maxlength="50" style="width:200px;"/></td>
				</tr>
				<tr class="hide-mobile">
					<td colspan="2" align="right">
						<input name="enviar" id="enviar" type="submit" value="Enviar"/>
						<input name="reset" id="reset" type="reset" value="Limpar"/>
					</td>
				</tr>
			</table>
			<input class="botao hide-desktop" name="enviar" id="enviar" type="submit" value="Enviar"/>
			</fieldset>
		</form>
	</div>	
</div>
</div>
<? require_once "footer.php"; ?>
</body>
</html>

