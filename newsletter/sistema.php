<?php 
session_start();
include "functions.php";
conexao();
ini_set('default_charset','UTF-8');

if($_SESSION['verif'] != "ok"){
	?><script>location.href = "/newsletter?acesso"</script><?
	exit();
}
?>
<!doctype html>
<html lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="UTF-8">
<title>Envio de Newsletter</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="style.css">
</head>
<body>

<header id="header">
	<div class="container">	
		<div class="col-md-12 col-lg-12 col-xs-12">
			<div class="logo">			
				<img src="logo_raphael.png" width="100" alt="">
			</div><!-- logo -->
		</div>
	</div><!-- container -->
</header><!-- header -->

<section id="conteudo">
	<div class="container">
		<div class="col-md-12 col-lg-12 col-xs-12">
			<form action="mailContato.php" method="post" enctype="multipart/form-data">
				<span class="info">Assunto:</span>	
				<input type="text" name="assunto" id="assunto">

				<span class="info">Imagem: <a href="http://dev.quadroweb.com.br/upload_imagens/" target="popup" onclick="window.open('http://dev.quadroweb.com.br/upload_imagens/','Popup','width=800,height=700')">Enviar imagem</a></span>	
				<input type="text" name="imagem" id="imagem">

				<span class="info">Tipo de envio:</span>
				<select name="template" id="template">
					<option value="Vendas">Vendas</option>
					<option value="Locação">Locação</option>
					<option value="Condomínio">Condomínio</option>
					<option value="Institucional">Institucional</option>
				</select>

				<span class="info">Lista para envio:</span>
				<select name="lista" id="lista">
					<option value="teste">Teste</option>
					<option value="cadastrado">Cadastrados</option>
				</select>

				<span class="info">Descri&ccedil;&atilde;o ou texto institucional:</span>
				<textarea name="texto" id="texto"></textarea>
			
				<input type="submit" name="enviar" id="enviar" value="enviar">

				<span class="view" href="visualizar.php" target="_blank">Visualizar</span>

				<a href="logout.php">Sair</a> |
				<a href="listarUsuarios.php">Lista de usu&aacute;rios</a> |
				<a href="newsEspecial.php">Envio especial</a>
			</form>
		</div>
	</div><!-- container -->
</section><!-- conteudo -->

<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" scr="js/scripts.js"></script>
<script>
	
	$('.view').click(function(){

		var assunto = $('#assunto').val();
		var imagem = $('#imagem').val();
		var template = $('#template').val();
		var texto = $('#texto').val();

		$('#visualizar').append('<!doctype html><html lang="pt-br"><head><meta charset="utf-8"></head><body><span class="fechar">Fechar</span><table style="width:600px;background:#bea181;"><tr style="width:600px;height:210px;float:left;"><td><img src="http://dev.quadroweb.com.br/templates_news/img/header_'+template+'.jpg" height="210" width="600" alt=""></td></tr><tr style="float:left;text-align:center;margin-top:10px;margin-left:70px;"><td><img src="'+imagem+'" width="450" height="250" alt=""></td></tr><tr style="width:600px;height:83px;float:left;"><td><img src="http://dev.quadroweb.com.br/templates_news/img/faixa_'+template+'.jpg" height="83" width="600" alt=""></td></tr><tr style="width:600px;height:200px;float:left;text-align:center;margin-top:10px;"><td style="font-size:20px;">'+texto+'</td></tr><tr style="width:600px;height:142px;float:left;text-align:center;"><td><img src="http://dev.quadroweb.com.br/templates_news/img/footer.jpg" height="142" width="600" alt=""></td></tr><tr style="width:600px;height:50px;float:left;background:#ed7d00;margin-left:2px;"><td style="text-align:center;width:600px;"><a href="https://www.facebook.com/pages/Raphael-Im%C3%B3veis/110853852397561?fref=ts">http://facebook.com/raphaelimoveis</a></td></tr></table></body></html>');

		$('#visualizar').fadeIn(200);

		$('.fechar').click(function(){
			$('#visualizar').fadeOut(200);
		});
	});

</script>
</body>
</html>