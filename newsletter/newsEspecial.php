<?php 
session_start();
include "functions.php";
conexao();

if($_SESSION['verif'] != "ok"){
	?><script>location.href = "/newsletter?acesso"</script><?
	exit();
}
?>
<!doctype html>
<html lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="UTF-8">
<title>Envio de Newsletter</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="style.css">
</head>
<body>

<header id="header">
	<div class="container">	
		<div class="col-md-12 col-lg-12 col-xs-12">
			<div class="logo">			
				<img src="logo_raphael.png" width="100" alt="">
			</div><!-- logo -->
		</div>
	</div><!-- container -->
</header><!-- header -->

<section id="conteudo">
	<div class="container">
		<div class="col-md-12 col-lg-12 col-xs-12">
			<form action="mailEspecial.php" method="post" enctype="multipart/form-data">
				<span class="info">Assunto:</span>	
				<input type="text" name="assunto" id="assunto">

				<span class="info">Imagem:</span>	
				<input type="file" name="imagem" id="imagem">

				<span class="info">Lista para envio:</span>
				<select name="lista" id="lista">
					<option value="teste">Teste</option>
					<option value="cadastrado">Cadastrados</option>
				</select>
			
				<input type="submit" name="enviar" id="enviar" value="enviar">

				<a href="logout.php">Sair</a> |
				<a href="listarUsuarios.php">Lista de usu&aacute;rios</a> |
				<a href="sistema.php">Envio normal</a>
			</form>
		</div>
	</div><!-- container -->
</section><!-- conteudo -->

</div>

<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" scr="js/scripts.js"></script>
</body>
</html>