<?php 
ini_set('default_charset','UTF-8');
include "functions.php";
conexao();

$assunto 	= $_POST['assunto'];
$texto 		= utf8_decode($_POST['texto']);
$template 	= utf8_decode($_POST['template']);
$tipo 		= $_POST['lista'];
$imagem 	= $_POST['imagem'];

$html = '
	<!DOCTYPE html>
	<html style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 100%; line-height: 1.6em; text-align: center; margin: 0; padding: 0;">
	<head>
	<meta name="viewport" content="width=device-width">
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Raphael Im&oacute;veis</title>

	<script>
	@import url(https://fonts.googleapis.com/css?family=Raleway:400,700);
	</script>

	</head>

	<body bgcolor="#E9952A" style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 100%; line-height: 1.6em; text-align: center; -webkit-font-smoothing: antialiased; height: 100%; -webkit-text-size-adjust: none; width: 100% !important; margin: 0; padding: 0;">

	<!-- body -->
	<table class="body-wrap" bgcolor="#E9952A" style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 100%; line-height: 1.6em; text-align: center; width: 100%; margin: 0; padding: 20px;">
	  <tr style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 100%; line-height: 1.6em; text-align: center; margin: 0; padding: 0;" align="center">
	    <td style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 100%; line-height: 1.6em; text-align: center; margin: 0; padding: 0;" align="center"></td>
	    <td class="container" bgcolor="#E9952A" style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 100%; line-height: 1.6em; text-align: center; clear: both !important; display: block !important; max-width: 600px !important; margin: 0 auto; padding: 0;" align="center">

	      <!-- content -->
	      <div class="content" style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 100%; line-height: 1.6em; text-align: center; display: block; max-width: 600px; margin: 0 auto; padding: 0;" align="center">
	      <table style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 100%; line-height: 1.6em; text-align: center; width: 100%; margin: 0; padding: 0;">
	        <tr style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 100%; line-height: 1.6em; text-align: center; margin: 0; padding: 0;" align="center">
	          <td style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 100%; line-height: 1.6em; text-align: center; margin: 0; padding: 0;" align="center">
	            <img src="http://raphaelimoveis.com.br/newsletter/template/1/header.jpg" alt="" style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 100%; line-height: 1.6em; text-align: center; float: left; max-width: 600px; width: auto; margin: 0px; padding: 0px;" align="left">
	            <table class="header" style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 100%; line-height: 1.6em; text-align: center; width: 100%; float: left; background-color: #053A22; margin: 0; padding: 0;" bgcolor="#053A22">
	              <tr style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 100%; line-height: 1.6em; text-align: center; margin: 0; padding: 0;" align="center">
	                <td style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 100%; line-height: 1.6em; text-align: center; margin: 0; padding: 0;" align="center">
	                  <h2 style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 28px; line-height: 1.2em; text-align: center; color: #FFF; font-weight: 200; text-transform: uppercase; margin: 0; padding: 0;" align="center">Im&oacute;vel em destaque: <strong style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 100%; line-height: 1.6em; text-align: center; color: #DD772A; margin: 0; padding: 0;">'.$template.'</strong>
	</h2>
	                </td>
	              </tr>
	            </table>
	            <img src="'.$imagem.'" class="imovel-image" style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 100%; line-height: 1.6em; text-align: center; float: left; max-width: 600px; width: auto; border-bottom-width: 6px; border-bottom-color: #053A22; border-bottom-style: solid; margin: 0px 0px 40px; padding: 0px;" align="left">
	            <p style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 20px; line-height: 24px; text-align: center; font-weight: normal; float: left; text-transform: uppercase; color: #FFF; margin: 0 0 10px; padding: 0;" align="center">'.$texto.'</p>
	            <h4 style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 36px; line-height: 34px; text-align: center; float: left; width: 100%; text-transform: uppercase; color: #FFF; margin: 0; padding: 0;" align="center">
				<!-- <strong style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 100%; line-height: 1.6em; text-align: center; color: #125637; margin: 0; padding: 0;">R$ 230.000,00</strong> - COD 3611</h4> -->
	            <img src="http://raphaelimoveis.com.br/newsletter/template/1/footer-1.png" alt="" style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 100%; line-height: 1.6em; text-align: center; float: left; max-width: 600px; width: auto; margin: 0px; padding: 0px;" align="left">
	            <a href="http://facebook.com/RaphaelImoveisPelotas/" style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 100%; line-height: 1.6em; text-align: center; color: #348eda; margin: 0; padding: 0;">
	              <img src="http://raphaelimoveis.com.br/newsletter/template/1/facebook.png" class="facebook" style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 100%; line-height: 1.6em; text-align: center; float: left; max-width: 600px; width: auto; background-color: #053A22; margin: 0px; padding: 20px 50px 40px;" align="left">
	            </a>
	            <p class="info" style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 12px; line-height: 16px; text-align: center; font-weight: normal; float: left; text-transform: initial; color: #FFF; width: 100%; background-color: #053A22; margin: 0 0 0px; padding: 0;" align="center">Voc&#234; est&aacute; recebendo este email pois est&aacute; cadastrado<br style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 100%; line-height: 1.6em; text-align: center; margin: 0; padding: 0;"> na newsletter da Raphael Im&oacute;veis</p>
	            <p class="info last" style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 12px; line-height: 16px; text-align: center; font-weight: normal; float: left; text-transform: initial; color: #FFF; width: 100%; background-color: #053A22; margin: 0 0 0px; padding: 0 0 60px;" align="center">Para parar de receber, basta <a href="http://www.raphaelimoveis.com.br/newsletter/desinscrever.php" style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 100%; line-height: 1.6em; text-align: center; color: #348eda; margin: 0; padding: 0;">desinscrever da lista</a></p>
	          </td>
	        </tr>
	      </table>
	      </div>
	      <!-- /content -->
	      
	    </td>
	    <td style="font-family: Bebas Neue, Raleway, Helvetica Neue, Arial, sans-serif; font-size: 100%; line-height: 1.6em; text-align: center; margin: 0; padding: 0;" align="center"></td>
	  </tr>
	</table>
	<!-- /body -->

	</body>
	</html>

';

$email 		= "imobiliaria@raphael.com.br";
$mensagem	=	'<!doctype html><html lang="pt-br"><head><meta charset="utf-8"></head><body><table style="width:600px;background:#bea181;"><tr style="width:600px;height:210px;float:left;"><td><img src="http://dev.quadroweb.com.br/templates_news/img/header_'.$template.'.jpg" height="210" width="600" alt=""></td></tr><tr style="float:left;text-align:center;margin-top:10px;margin-left:70px;"><td><img src="'.$imagem.'" width="450" height="250" alt=""></td></tr><tr style="width:600px;height:83px;float:left;"><td><img src="http://dev.quadroweb.com.br/templates_news/img/faixa_'.$template.'.jpg" height="83" width="600" alt=""></td></tr><tr style="width:600px;height:200px;float:left;text-align:center;margin-top:10px;"><td style="font-size:20px;">'.$texto.'</td></tr><tr style="width:600px;height:142px;float:left;text-align:center;"><td><img src="http://dev.quadroweb.com.br/templates_news/img/footer.jpg" height="142" width="600" alt=""></td></tr><tr style="width:600px;height:50px;float:left;background:#ed7d00;margin-left:2px;"><td style="text-align:center;width:600px;"><a href="https://www.facebook.com/pages/Raphael-Im%C3%B3veis/110853852397561?fref=ts">http://facebook.com/raphaelimoveis</a></td></tr></table><br>Deseja parar de receber nossas newsletter? <a href="http://www.raphaelimoveis.com.br/newsletter/desinscrever.php">Clique aqui</a></body></html>';
$headers	=	'From: Raphael Imoveis <imobiliaria@raphaelimoveis.com.br>' . "\r\n" .
				'Reply-To: Raphael Imoveis <imobiliaria@raphaelimoveis.com.br>' . "\r\n" .
				'X-Mailer: PHP/' . phpversion() .
				'MIME-Version: 1.0' . "\r\n" .
				'Content-Type: text/html; charset=ISO-8859-2'."\r\n";

if($tipo == 'cadastrado'){
	$consulta = mysql_query("SELECT * FROM phplist_user_user WHERE disabled = 0");
	while($res = mysql_fetch_array($consulta)){
		mail($res['email'],$assunto,$html,$headers);
	}
} else{
	$consulta = mysql_query("SELECT * FROM teste WHERE status = 1");
	while($res = mysql_fetch_array($consulta)){
		mail($res['email'],$assunto,$html,$headers);
	}
}


header("Location: http://www.raphaelimoveis.com.br/newsletter/sistema.php?enviado");
?>