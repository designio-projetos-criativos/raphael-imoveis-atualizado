<?php require "head.php"; ?>
<style type="text/css" media="all">
table tr td, table tr, table tr th,table{border-color:#CCCCCC;_border-color:;}
.tab{color:#114019;font-family:Arial, Helvetica, sans-serif;font-size:11px;font-weight: bold}
</style>
</head>
<body id="internas" class="contato">

<h1 class="seo"><?= $_GET['busca'] == "L" ? "Aluguéis " : "Vendas " ?></h1>
<h2 class="seo">Pesquisa de Imóveis</h2>
<?php 
	include "header.php";
	$_GET['busca'] = $_GET['busca'] ? $_GET['busca'] : "L";
?>
<div class="content">
	<div class="centro636 cont_busca">
		<h4 class="bordLaranja"><?= $_GET['busca'] == "L" ? "Aluguéis " : "Vendas " ?>- Pesquisa de Imóveis</h4>
		<form name="alugueis" id="alugueis" action="pesquisa.php" method="GET">
			<fieldset>
			<table cellpadding="0" cellspacing="0" width="100%">
				<colgroup>
					<col/>
					<col width="85%"/>
				</colgroup>
				<tbody>
					<tr>
						<td><label for="tipo">Código:</label></td>
						<td>
							<input name="codigo_imovel" id="codigo_imovel_" width="1px" class="codigo_" style="text-transform:uppercase;">
						</td>
					</tr>
					<tr>
						<td><label for="tipo">Tipo de Imóvel:</label></td>
						<td>
							<select name="tipo" id="tipo">
								<option value="" selected="selected"> </option> 
								<?php	
								$dados = mysql_query("SELECT * FROM imobiliar_tipo_imoveis WHERE locVenda = '{$_GET['busca']}'");
								while($linha = mysql_fetch_array($dados)){ echo "<option value=\"".$linha["id"]."\">".$linha["descricao"]."</option>"; }
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td><label for="valor">Valor R$:</label></td>
						<td>
                            <?if($_GET['busca'] == "L"){?>
							<select name="valor" id="valor">
								<option value="" selected="selected"> </option> 
								<option value="0/500"> Até R$ 500,00</option>
								<option value="500/1000"> Entre R$ 500,00 e R$ 1.000,00</option>
								<option value="1000/1500" style> Entre R$ 1.000,00 e R$ 1.500,00</option>
								<option value="1500/"> Acima de R$ 1.500,00</option>
							</select>
                            <?}else{?>
                            <select name="valor" id="valor">
								<option value="" selected="selected"> </option> 
								<option value="0/100000"> Até 100.000,00</option>
								<option value="100000/200000"> Entre 100.000,00 e 200.000,00</option>   
								<option value="200000/300000"> Entre 200.000,00 e 300.000,00</option>   
								<option value="300000/400000"> Entre 300.000,00 e 400.000,00</option>   
								<option value="400000/500000"> Entre 400.000,00 e 500.000,00</option>   
								<option value="500000/"> Acima de 500.000,00</option>
							</select>
                            <?}?>
						</td>
					</tr>
					<tr>
						<td><label for="bairro">Bairro:</label></td>
						<td>
							<select name="bairro" id="bairro">
								<option value="" selected="selected"> </option> 
								<?php
								$dados = mysql_query("SELECT * FROM imobiliar_bairros WHERE locVenda = '{$_GET['busca']}'");
								while ($linha = mysql_fetch_array($dados)){
									echo "<option value=\"".$linha["nome"]."\">".($linha["nome"])."</option>";
								}
								?>
							</select>
						</td>
					</tr>
                    <?if($_GET['busca'] == "L"){?>
					<tr>
						<td><label for="dorm">Nº dorm:</label></td>
						<td>
						<select name="dorm" id="dorm">
						   <option value="" selected="selected"> </option> 
						   <option value="1"> 1</option>  
						   <option value="2"> 2</option>  
						   <option value="3"> 3</option>  
						   <option value="4"> 4</option>  
						   <option value="5"> 5 ou +</option>  
						</select>
						</td>
					</tr>
                    <?}?>
					<tr class="hide-mobile">
						<td colspan="2" align="center">
						    <input type="hidden" name='busca' value="<?=$_GET['busca']?>"/>
							<input name="enviar" id="enviar" type="submit" value="Pesquisar"/>
						</td>
					</tr>
					<tr class="hide-mobile">
						<td colspan="2" align="right">
							<a href="pesquisa.php?busca=<?=$_GET['busca']?>" class="tab">listar todos</a>
						</td>
					</tr>
					<tr class="hide-mobile">
						<td colspan="2"><h5>os imóveis aqui apresentados são algumas de nossas opções. <br/> se você procura outro imóvel consulte-nos pelo fone 3225.1100 </h5></td>
					</tr>
				</tbody>
			</table>
			<a class="hide-desktop botao botao1" href="pesquisa.php?busca=<?=$_GET['busca']?>" class="tab">listar todos</a>
			<input class="hide-desktop botao"  name="enviar" id="enviar" type="submit" value="Pesquisar"/>


			</fieldset>
		</form>
		<h5 class="hide-desktop">Os imóveis aqui apresentados são algumas de nossas opções. Se você procura outro imóvel consulte-nos pelo fone 3225.1100 </h5>
	</div>	
</div>
</div>
<? require_once "footer.php"; ?>
</body>
</html>
