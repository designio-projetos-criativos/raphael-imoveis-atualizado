<?php
session_start();
require_once "head.php";
require_once "paginador.php";

function inject($str){
				if(!is_numeric($str)){ 
					$str = get_magic_quotes_gpc() ? 
						    stripslashes($str) : $str; 
					$str = function_exists('mysql_real_scape_string') ? 
						    mysql_real_escape_string($str) : mysql_escape_string($str); 
				}
				return $str;
	}

?>
<link href="css/modal_galeria.css" type="text/css" rel="stylesheet" />
<style type="text/css" media="all">
table tr td, table tr, table tr th,table{border-color:#CCCCCC;_border-color:;}
.tab{color:#114019;font-family:Arial, Helvetica, sans-serif;font-size:11px;font-weight: bold;}
</style>
</head>
<body id="internas" class="contato">
<? require_once "header.php"; ?>
<div class="content">
	<div class="centro636">
		<h1 class="bordLaranja">Aluguéis - Pesquisa de Imóveis</h1>
		<h2 class="seo">Pesquisa de Imóveis</h2>
		<?php
		
        $codigo_imovel = inject($_REQUEST['codigo_imovel']);
        $tipo   = inject($_REQUEST['tipo']);
		$valor  = inject($_REQUEST['valor']);
		$bairro = inject($_REQUEST['bairro']);
        
        if ($bairro == 4)
        {
            $tipo_aluguel = inject($_REQUEST['galeria_tipo_aluguel']);
        }else
        {
            $tipo_aluguel = "";
        }
        	
		$dorm   = $_REQUEST['dorm'];																				
		
		$sql = "select * from galerias2 where galeria_codigo <> ''";
								
		if ($valor==300)
								$sql .= " and galeria_valor <= '300.00'";						
						    elseif ($valor==301)
								$sql .= " and galeria_valor BETWEEN '300.00' and '600.00'";
						    elseif ($valor==600)
								$sql .= " and galeria_valor BETWEEN '600.00' and '900.00'";
						    elseif ($valor==900)
								$sql .= " and galeria_valor BETWEEN '900.00' and '1200.00'";
						    elseif ($valor==1200)
								$sql .= " and galeria_valor >= '1200.00'";
			
		if ($dorm != ""){ 
			if($dorm == 5){
				$sql .= " and galeria_dormitorios >= '5'";
			}else{
				$sql .= " and galeria_dormitorios = '$dorm'";
			}
		}
			
		if ($bairro != "") $sql .= " and galeria_bairro = '$bairro'";
        if ($bairro == 4)  $sql .= " and galeria_tipo_aluguel = '$tipo_aluguel'";        
		if ($tipo != "") $sql .= " and galeria_tipo = '$tipo'";
		
		$sql .= " order by galeria_codigo asc";												   

        //ignora as outras consultas, caso tenha codigo
        if ($codigo_imovel != "") $sql = "select * from galerias2 where galeria_codigo = '$codigo_imovel'";

		$mostrar = 10;
		$_SESSION['arquivo'] = "";
		$_SESSION['mostrar'] = $mostrar;
		$_SESSION['minha_consulta'] = $sql;
		require_once 'replicador.php';											
		
		if(mysql_num_rows($rs2) == 0){?>
		<p>Não há Imóveis com as características selecionadas</p>
		<form name="alugueis" id="alugueis" action="pesqalu.php" method="post">
			<fieldset>
			<table cellpadding="0" cellspacing="0" width="100%">
				<colgroup>
					<col/>
					<col width="85%"/>
				</colgroup>
				<tbody>
					<tr>
						<td><label for="tipo">Código:</label></td>
						<td>
							<input name="codigo_imovel" id="codigo_imovel" width="1px"/>
						</td>
					</tr>
					<tr>
						<td><label for="tipo">Tipo de Imóvel:</label></td>
						<td>
							<select name="tipo" id="tipo">
								<option value="" selected="selected"> </option> 
								<?php	
								$dados = mysql_query("select * from galerias_tipos2");
								while($linha = mysql_fetch_array($dados)){ echo "<option value=\"".$linha["tipo_id"]."\">".($linha["tipo_nome"])."</option>"; }
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td><label for="valor">Valor R$:</label></td>
						<td>
							<select name="valor" id="valor">
							   <OPTION VALUE="300"> Até 300,00</OPTION>  
   <OPTION VALUE="301"> Entre 300,00 e 600,00</OPTION>  
   <OPTION VALUE="600"> Entre 600,00 e 900,00</OPTION>     
   <OPTION VALUE="900"> Entre 900,00 e 1.200,00</OPTION>   
   <OPTION VALUE="1200"> Acima de 1.200,00</OPTION>     
							</select>
						</td>
					</tr>
					<tr>
						<td><label for="bairro">Bairro:</label></td>
						<td>
							<select name="bairro" id="bairro">
								<option value="" selected="selected"> </option> 
								<?php
								$dados = mysql_query("select * from galerias_bairros2");
								while ($linha = mysql_fetch_array($dados)){
									echo "<option value=\"".$linha["bairro_id"]."\">".($linha["bairro_nome"])."</option>";
								}
								?>
							</select>
						</td>
					</tr>
					<tr id="hideGTA" style="display: none;">
						<td><label for="galeria_tipo_aluguel">Tipo de Aluguel:</label></td>
						<td>
							<select name="galeria_tipo_aluguel" id="galeria_tipo_aluguel">
							   <OPTION VALUE="Temporada">Temporada</OPTION>  
                               <OPTION VALUE="Anual">Anual</OPTION>  
							</select>
						</td>
					</tr>
					<tr>
						<td><label for="dorm">Nº dorm:</label></td>
						<td>
						<select name="dorm" id="dorm">
						   <option value="" selected="selected"> </option> 
						   <option value="1"> 1</option>  
						   <option value="2"> 2</option>  
						   <option value="3"> 3</option>  
						   <option value="4"> 4</option>  
						   <option value="5"> 5 ou +</option>  
						</select>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<input name="enviar" id="enviar" type="submit" value="Pesquisar"/>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="right">
							<a href="pesqalu.php?numx=-2" class="tab">listar todos</a>
						</td>
					</tr>
					<tr>
						<td colspan="2"><h5>os imóveis aqui apresentados são algumas de nossas opções. <br/> se você procura outro imóvel consulte-nos pelo fone 3225.1100 </h5></td>
					</tr>
				</tbody>
			</table>
			</fieldset>
		</form>
		<? }else{?>
		<table class="tab tabImoveis" cellpadding="5" cellspacing="1" border="1" width="100%">
			<tr bgcolor="#c0c0c0"><td> Fotos</td><td> Código </td><td> Tipo </td><td align="center">Endereço</td><td align="center">Bairro</td><td align="center">Dorm</td><td>R$ Valor</td></tr>	
			<?
			while ($inf = mysql_fetch_array($rs2)){							
				$galeria_id     = $inf['galeria_id'];
				$galeria_codigo = $inf['galeria_codigo'];
				$galeria_tipo   = $inf['galeria_tipo'];
				$galeria_descricao = $inf['galeria_descricao'];
				$galeria_bairro = $inf['galeria_bairro'];
				$galeria_valor  = $inf['galeria_valor'];
				$galeria_endereco = $inf['galeria_endereco'];
				$galeria_dormitorios = $inf['galeria_dormitorios'];																								
				
				$rstImg = mysql_query("select foto_arquivo from galerias_fotos2 where galeria_id = '$galeria_id' order by foto_id asc limit 1");								
				if(mysql_num_rows($rstImg) > 0){
					$imagem      = @split("\.", mysql_result($rstImg,0,0));
					$galeria_img = $imagem[0]."_mini.".$imagem[1];
				}else{
					$galeria_img = "";
				}
				
				$rstImg = mysql_query("select bairro_nome from galerias_bairros2 where bairro_id = '$galeria_bairro'");								
				$galeria_bairro = @mysql_result($rstImg,0,0);
				
				$rstImg = mysql_query("select tipo_nome from galerias_tipos2 where tipo_id = '$galeria_tipo'");								
				$galeria_tipo = @mysql_result($rstImg,0,0);	
				
				$galeria_valor = number_format($galeria_valor, 2, ",", ".");														
			?>	
			<tr>
				<td align="center">
				<? if($galeria_img != "_mini." && $galeria_img != ""){ ?>
					<a href="javascript:;" rel="id=<?=$galeria_id?>&pesquisa=aluguel" class="openmodal">
					<img src="../uploads/galeria_fotos2/<?=$galeria_img?>" alt="<?=$galeria_codigo?>" />
					</a>
				<? } else {?> <? }?></td>
				<td><?=$galeria_codigo?></td>
				<td><?=($galeria_tipo)?> </td>
				<td><?=$galeria_endereco?></td>
				<td><?=($galeria_bairro)?></td>
				<td><?=$galeria_dormitorios?></td>
				<td><?=$galeria_valor?></td>
			</tr>
			<? } } ?>							
		</table>
		<table cellpadding="5" cellspacing="0" align="center" width="98%">
			<tr><?php if ($totreg > $mostrar) navipages($home = 1, $center = 1, $complemento = "&tipo=$tipo&valor=$valor&bairro=$bairro&dorm=$dorm") ?></tr>
		</table>
	</div>	
</div>
</div>
</div>
<?php require_once "footer.php"; ?>
<div class="modal_galeria">
	<div class="modal_galeria2">
		<div class="modal_galeria3">
			<div class="fechar"><a href="javascript:;" class="fechamodal">Fechar</a></div>
			<div class="box_imgzoom">
				<div class="box_imgzoom2"></div>
				<div id="mygallery" class="stepcarousel">
					<div class="belt"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="js/galeria.js"></script>

</body>
</html>