<?php 
require_once "connection/conexao.php";
ini_set("default_charset", "UTF-8");
$path = array(
	"dados" => "http://www.raphaelimoveis.com.br/imobiliar/prg/dados/", 
	"imagens" => "http://www.raphaelimoveis.com.br/imobiliar/Fotos"
);
$caminho = 'http://www.raphaelimoveis.com.br/';
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 
<base href="http://<?=$_SERVER['SERVER_NAME']?>"> -->

<meta name="description" content="Em 1991, junto ao escritório de advocacia Morales, advogados associados, foi constituída a empresa Administradora Raphael Ltda., para o fim específico e exclusivo de administração de aluguéis, visto que, alguns clientes do escritório, diante da confiança depositada, solicitaram que fossem administrados os seus imóveis destinados a locação."/>
<meta name="keywords" content="raphael imóveis, imóveis, pelotas, imóveis pelotas, aluguéis, vendas,condomínios" />
<meta name="robots" content="all"/>

<link href="css_index/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/modal_galeria.css">
<link href="css/default.css" type="text/css" rel="stylesheet" />
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
	
<link href="css/responsa.css" rel="stylesheet" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script type="text/javascript" src="swfobject.js"></script>
<script type="text/javascript" language="javascript" src="jquery-1.4.2.js"></script>
<script type="text/javascript" src="jquery.jcycle.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.2.custom.min.js"></script>

<script type="text/javascript" src="js/jquery.validate.js"></script>
<script type="text/javascript" src="js/jquery.validate.metods.js"></script>

<script type="text/javascript" src="js/jquery.meio.mask.js"></script>
<script>
	window.fbAsyncInit = function() {
	   FB.init({
	     appId      : '95100348886',
	     xfbml      : true,
	     version    : 'v2.6'
	   });
	 };

	 (function(d, s, id){
	   var js, fjs = d.getElementsByTagName(s)[0];
	   if (d.getElementById(id)) {return;}
	   js = d.createElement(s); js.id = id;
	   js.src = "//connect.facebook.net/en_US/sdk.js";
	   fjs.parentNode.insertBefore(js, fjs);
	 }(document, 'script', 'facebook-jssdk'));
</script>

<script type="text/javascript" src="js/selects.js"></script>
<script type="text/javascript" src="js/default.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<script type="text/javascript" language="javascript">
$(function($) {
	// Quando o formulário for enviado, essa função é chamada
	$("#formulario").submit(function() {
		// Colocamos os valores de cada campo em uma váriavel para facilitar a manipulação
		var email = $("#email").val();
		var nome = $("#nome").val();
		// Exibe mensagem de carregamento
		$(".topicosmoldenewsletterformverifica").html("<img src='loader.gif' alt='Enviando' />");
		// Fazemos a requisão ajax com o arquivo login_cliente e enviamos os valores de cada campo através do método POST
		$.post('cadastrar_newslatter.php', {email: email,nome: nome }, function(resposta) {
				// Quando terminada a requisição
				// Exibe a div status
				$(".topicosmoldenewsletterformverifica").slideDown();
				// Se a resposta é um erro
				if (resposta != false) {
					// Exibe o erro na div
					$(".topicosmoldenewsletterformverifica").html(resposta);
				} 	// Se resposta for false, ou seja, não ocorreu nenhum erro
				else {
					// Exibe mensagem de sucesso
					$(".topicosmoldenewsletterformverifica").html("E-mail cadastrado com sucesso!");
					// Limpando todos os campos
				
					$("#email").val("Email");
					$("#nome").val("Nome");
				
				}							
		});
	});
	$(".limpa_input").focus(function() {
	if( this.value == this.defaultValue ) {
			this.value = "";
		}
		}).blur(function() {
	if( !this.value.length ) {
			this.value = this.defaultValue;
		}
	});

	$('input').focus(function(){
		if($(this).hasClass('codigo') && !$(this).hasClass('masked')){ $(this).setMask('a-9999').addClass('masked'); }
	});


	
});
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>