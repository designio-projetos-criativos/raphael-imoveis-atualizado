<?php
require_once "head.php";
?>
</head>
<body id="internas" class="taxas">
<h1 class="seo">Alugu�is</h1>
<h2 class="seo">Taxas Cobradas dos Propriet�rios</h2>
<? require_once "header.php"; ?>
<div class="content">
	<div class="centro636">
		<h4 class="bordLaranja">Taxas Cobradas dos Propriet�rios</h4>
		<ul class="logoLista f13">
			<li>Nossa <strong>taxa de administra��o</strong> mensal � de <strong>10%</strong> sobre o valor do aluguel recebido.
				Ressalta-se que a taxa n�o incide sobre os demais itens recebidos, tais como, �gua, condom�nio, IPTU  e outros.</li>
			<li style="margin:40px 0 0 0;">Quando o im�vel estiver desocupado e for efetivada a loca��o, ser� cobrado a <strong>taxa de
				intermedia��o</strong> de 40% sobre o valor do aluguel - para im�veis da cidade de Pelotas, e 
				de 50% sobre o valor do aluguel - para im�veis da cidade de Porto Alegre, que inclui as seguintes despesas:
				an�lise cadastral de inquilinos e fiadores; divulga��o na internet(em alguns casos, com fotos
				digitalizadas); an�ncios e publicidade em geral para oferecer o im�vel; vistoria do estado
				do im�vel em laudo discriminado; avalia��o do im�vel para efeito de ser efetuado o seguro
				contra inc�ncio; negocia��o e elabora��o do Contrato de Loca��o.</li>
		</ul>
	</div>
</div>
</div>
<? require_once "footer.php"; ?>
</body>
</html>
