<?php 
session_start();
include "functions.php";
conexao();

if($_SESSION['verif'] != "ok"){
	?><script>location.href = "/mtowernews?acesso"</script><?
	exit();
}
?>
<!doctype html>
<html lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="UTF-8">
<title>Envio de Newsletter</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="style.css">
</head>
<body>

<header id="header">
	<div class="container">	
		<div class="col-md-12 col-lg-12 col-xs-12">
			<div class="logo">			
				<img src="logo_mtower.png" alt="">
			</div><!-- logo -->
		</div>
	</div><!-- container -->
</header><!-- header -->

<section id="conteudo">
	<div class="container">
		<div class="col-md-12 col-lg-12 col-xs-12">

			<form action="mailEspecial.php" method="post" enctype="multipart/form-data">
			<div class="check-box">
			<ul class="chk-container">
			<li><input type="checkbox" id="selecctall"/> Selecionar Todos</li>			
			</ul>		
				<div class="col-md-4 col-lg-4">
					<?php 

					$consulta1 = mysql_query("SELECT * FROM phplist_user_user WHERE disabled = 0");
					while($res1 = mysql_fetch_array($consulta1)){ ?>
						
						<div class="col-md-12 col-lg-12">
							<input type="checkbox" class="marcar" name="checkbox[]" id="checkbox" value="<?php echo $res1['email'];?>"><?php echo $res1['email'];?>
						</div>

					<?php } ?>
				</div>
				<div class="col-md-4 col-lg-4">
					<?php 

					$consulta2 = mysql_query("SELECT * FROM phplist_user_user2 WHERE disabled = 0");
					while($res2 = mysql_fetch_array($consulta2)){ ?>
						
						<div class="col-md-12 col-lg-12">
							<input type="checkbox" class="marcar" name="checkbox[]" id="checkbox" value="<?php echo $res2['email'];?>"><?php echo $res2['email'];?>
						</div>

					<?php } ?>
				</div>
				<div class="col-md-4 col-lg-4">
					<?php 

					$consulta3 = mysql_query("SELECT * FROM teste WHERE disabled = 0");
					while($res3 = mysql_fetch_array($consulta3)){ ?>
						
						<div class="col-md-12 col-lg-12">
							<input type="checkbox" class="marcar"name="checkbox[]" id="checkbox" value="<?php echo $res3['email'];?>"><?php echo $res3['email'];?>


						</div>

					<?php } ?>										
				</div>



			</div>
				<span class="info">Assunto:</span>	
				<input type="text" name="assunto" id="assunto">

				<span class="info">Imagem:</span>	
				<input type="file" name="imagem" id="imagem">

				<!-- <span class="info">Tipo de envio:</span>
				<select name="template" id="template">
					<option value="venda">Vendas</option>
					<option value="aluguel">Aluguel</option>
					<option value="institucional">Institucional</option>
				</select> -->

				<span class="info">Lista para envio:</span>
				<select name="lista" id="lista">
					<option value="teste">Teste</option>
					<option value="cadastrado">Cadastrados</option>
					<option value="raphael">Lista Raphael Im&oacute;veis</option>
					<option value="selecionados">Selecionados</option>
				</select>

				<span class="info">E-mail de remetente</span>
				<select name="remetente" id="lista" required>
					<option value="">Selecione</option>
					<option value="1">MTower (contato@mtoweraparthotel.com.br)</option>
					<option value="2">Raphael Im&oacute;veis (imobiliaria@raphaelimoveis.com.br)</option>
				</select>
			
				<input type="submit" name="enviar" id="enviar" value="enviar">

				<a href="logout.php">Sair</a> |
				<a href="listarUsuarios.php">Lista de usu&aacute;rios</a> |
				<a href="sistema.php">Envio normal</a>
			</form>
		</div>
	</div><!-- container -->
</section><!-- conteudo -->

</div>

<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="js/jquery.jscrollpane.min.js"></script>
<script src="js/jquery.mousewheel.js"></script>
<script type="text/javascript" scr="js/scripts.js"></script>
<script>
$(document).ready(function() {
    $('#selecctall').click(function(event) {  
        if(this.checked) {
            $('.marcar').each(function() { 
                this.checked = true;            
            });
        }else{
            $('.marcar').each(function() { 
                this.checked = false;                 
            });         
        }
    });
    
});
</script>
</body>
</html>