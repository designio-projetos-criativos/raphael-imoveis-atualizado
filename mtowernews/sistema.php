<?php 
session_start();
include "functions.php";
conexao();

if($_SESSION['verif'] != "ok"){
	?><script>location.href = "/mtowernews?acesso"</script><?
	exit();
}
?>
<!doctype html>
<html lang="pt-BR">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="UTF-8">
<title>Envio de Newsletter</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="style.css">
</head>
<body>

<header id="header">
	<div class="container">	
		<div class="col-md-12 col-lg-12 col-xs-12">
			<div class="logo">			
				<img src="logo_mtower.png" alt="">
			</div><!-- logo -->
		</div>
	</div><!-- container -->
</header><!-- header -->

<section id="conteudo">
	<div class="container">
		<div class="col-md-12 col-lg-12 col-xs-12">

			<form action="mailContato.php" method="post" enctype="multipart/form-data">
			<div class="check-box">
			<ul class="chk-container">
			<li><input type="checkbox" id="selecctall"/> Selecionar Todos</li>			
			</ul>		
				<div class="col-md-4 col-lg-4">
					<? $total = mysql_fetch_array(mysql_query("SELECT count(id) as total FROM phplist_user_user WHERE disabled = 0")); ?>
					<div class="col-md-12" style="font-size:14px; margin-bottom:5px"><?=$total['total']?> emails cadastrados</div>
					<?php 

					$consulta1 = mysql_query("SELECT * FROM phplist_user_user WHERE disabled = 0");
					while($res1 = mysql_fetch_array($consulta1)){ ?>
						
						<div class="col-md-12 col-lg-12">
							<input type="checkbox" class="marcar" name="checkbox[]" id="checkbox" value="<?php echo $res1['email'];?>"><?php echo $res1['email'];?>
						</div>

					<?php } ?>
				</div>
				<div class="col-md-4 col-lg-4">
					<? $total2 = mysql_fetch_array(mysql_query("SELECT count(id) as total FROM phplist_user_user2 WHERE disabled = 0")); ?>
					<div class="col-md-12" style="font-size:14px; margin-bottom:5px"><?=$total2['total']?> emails cadastrados</div>
					<?php 

					$consulta2 = mysql_query("SELECT * FROM phplist_user_user2 WHERE disabled = 0");
					while($res2 = mysql_fetch_array($consulta2)){ ?>
						
						<div class="col-md-12 col-lg-12">
							<input type="checkbox" class="marcar" name="checkbox[]" id="checkbox" value="<?php echo $res2['email'];?>"><?php echo $res2['email'];?>
						</div>

					<?php } ?>
				</div>
				<div class="col-md-4 col-lg-4">
					<? $total3 = mysql_fetch_array(mysql_query("SELECT count(id) as total FROM teste WHERE disabled = 0")); ?>
					<div class="col-md-12" style="font-size:14px; margin-bottom:5px"><?=$total3['total']?> emails cadastrados</div>
					<?php 

					$consulta3 = mysql_query("SELECT * FROM teste WHERE disabled = 0");
					while($res3 = mysql_fetch_array($consulta3)){ ?>
						
						<div class="col-md-12 col-lg-12">
							<input type="checkbox" class="marcar"name="checkbox[]" id="checkbox" value="<?php echo $res3['email'];?>"><?php echo $res3['email'];?>


						</div>

					<?php } ?>										
				</div>



			</div>
				<span class="info">Assunto:</span>	
				<input type="text" name="assunto" id="assunto">

				<span class="info">Escolha se voc&ecirc; quer usar um template ou n&atilde;o:</span>
				<select name="escolha_template" id="escolha_template">
					<option value="usar">Usar</option>
					<option value="nao_usar">N&atilde;o usar</option>
				</select>

				<span class="info">Formato da imagem:</span>
				<select name="formato_imagem" id="formato_imagem">
					<option value="paisagem">Paisagem</option>
					<option value="retrato">Retrato</option>
				</select>

				<span class="info">Imagem: <a href="http://dev.quadroweb.com.br/upload_imagens/" target="popup" onclick="window.open('http://dev.quadroweb.com.br/upload_imagens/','Popup','width=800,height=700')">Enviar imagem</a> (SOMENTE SE FOR USAR TEMPLATE)</span>	
				<input type="text" name="imagem" id="imagem">

				<span class="info">Apresenta&ccedil;&atilde;o: <a href="http://dev.quadroweb.com.br/upload_apresentacao/" target="popup" onclick="window.open('http://dev.quadroweb.com.br/upload_apresentacao/','Popup','width=800,height=700')">Enviar apresenta&ccedil;ao</a> (SOMENTE SE N&Atilde;O FOR USAR TEMPLATE)</span>	
				<input type="text" name="anexo" id="anexo">

				<!-- <span class="info">Tipo de envio:</span>
				<select name="template" id="template">
					<option value="venda">Vendas</option>
					<option value="aluguel">Aluguel</option>
					<option value="institucional">Institucional</option>
				</select> -->

				<span class="info">Lista para envio:</span>
				<select name="lista" id="lista">
					<option value="teste">Teste</option>
					<option value="cadastrado">Cadastrados</option>
					<option value="raphael">Lista Raphael Im&oacute;veis</option>
					<option value="selecionados">Selecionados</option>
				</select>

				<span class="info">Descri&ccedil;&atilde;o ou texto institucional:</span>
				<textarea name="texto" id="texto"></textarea>
			
				<input type="submit" name="enviar" id="enviar" value="enviar">

				<span class="view light" href="visualizar.php" target="_blank">Visualizar</span>

				<span class="view" href="mailPatrimonio.php" target="_blank">Dia do patrim&ocirc;nio</span>

				<a href="logout.php">Sair</a> |
				<a href="listarUsuarios.php">Lista de usu&aacute;rios</a> |
				<a href="newsEspecial.php">Envio especial</a>
			</form>
		</div>
	</div><!-- container -->
</section><!-- conteudo -->

<div id="visualizar" style="display:none;position: absolute;width: 600px;left: 50%;margin-left: -325px;height:100px;margin-top:50px;">

</div>

<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="js/jquery.jscrollpane.min.js"></script>
<script src="js/jquery.mousewheel.js"></script>
<script type="text/javascript" scr="js/scripts.js"></script>
<script>
	
	$('.light').click(function(){

		var assunto = $('#assunto').val();
		var imagem = $('#imagem').val();
		// var template = $('#template').val();
		var texto = $('#texto').val();
		var formato_imagem = $('#formato_imagem').val();

		if(formato_imagem == 'paisagem'){
			imagem_formato = '<img src="'+imagem+'" width="450" height="250" alt="">';
			imagem_tr = '<tr style="float:left;text-align:center;margin-top:10px;margin-left:70px;"><td>'+imagem_formato+'</td></tr>';
		} else{
			imagem_formato = '<img src="'+imagem+'" width="250" height="450" alt="">';
			imagem_tr = '<tr style="float:left;text-align:center;margin-top:10px;margin-left:170px;"><td>'+imagem_formato+'</td></tr>';
		}

		$('#visualizar').append('<!doctype html><html lang="pt-br"><head><meta charset="utf-8"></head><body><span class="fechar">Fechar</span><table style="width:600px;background:#a8a8a8;"><tr style="width:600px;height:210px;float:left;"><td><img src="http://dev.quadroweb.com.br/templates_news/img/mtower/header.jpg" height="210" width="600" alt=""></td></tr>'+imagem_tr+'<tr style="width:600px;height:83px;float:left;"><td><img src="http://dev.quadroweb.com.br/templates_news/img/mtower/faixa_baixo.jpg" height="83" width="600" alt=""></td></tr><tr style="width:600px;height:200px;float:left;text-align:center;margin-top:10px;"><td style="font-size:20px;">'+texto+'</td></tr><tr style="width:600px;height:142px;float:left;text-align:center;"><td><img src="http://dev.quadroweb.com.br/templates_news/img/mtower/footer.jpg" height="142" width="600" alt=""></td></tr><tr style="width:600px;height:45px;float:left;background:#000;"><td><a href="https://www.facebook.com/pages/MTower-Apart-Hotel/158982764310979?ref=ts&fref=ts"><img src="http://dev.quadroweb.com.br/templates_news/img/mtower/facebook.jpg" height="45" width="600" alt=""></a></td></tr></table></body></html>');

		$('#visualizar').fadeIn(200);

		$('.fechar').click(function(){
			$('#visualizar').fadeOut(200);
			$('#visualizar').empty();
		});
	});

</script>
<script>
$(document).ready(function() {
    $('#selecctall').click(function(event) {  
        if(this.checked) {
            $('.marcar').each(function() { 
                this.checked = true;            
            });
        }else{
            $('.marcar').each(function() { 
                this.checked = false;                 
            });         
        }
    });
    
});
</script>
</body>
</html>