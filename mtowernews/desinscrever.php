<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<title>Deseja desinscrever-se?</title>
	<style>
	*{
		margin: 0;
		padding: 0;
	}
	body{
		background: #222;
		text-align: center;
	}
	.input-h1{
		margin: 250px auto;
	}
	input{
		width: 550px;
		height: 30px;
		padding: 10px;
		margin: 10px auto;
	}
	h1{
		font-family: Trebuchet MS;
		font-size: 26px;
		color: #fff;
	}
	</style>
</head>
<body>
	
	<div class="input-h1">
		<h1>Deseja se desinscrever da newsletter do MTower?</h1>
		<form action="desinscreverForm.php" method="post">
			<input type="text" name="email_unsub" id="email_unsub" placeholder="Digite seu e-mail para desinscrever-se e pressione enter...">
		</form>
	</div>

</body>
</html>