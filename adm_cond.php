<?php
require "head.php";
$nome = $_REQUEST['nome'];
$fone = $_REQUEST['fone'];
$email = $_REQUEST['email'];
$mensagem = $_REQUEST['mensagem'];

?>
<script type="text/javascript">
<!--
var dataOK=false
function verifcampos(){
	if (document.Proposta.Nome_condominio.value == 0 ||
		document.Proposta.endereco_condominio.value == 0 ||
		document.Proposta.apto_sindico.value == 0 ||
		document.Proposta.Nome_sindico.value == 0 ||
		document.Proposta.Tipo == 0 ||
		document.Proposta.fone.value == 0){
		alert ("Atenção: Os seguintes campos são de preenchimento obrigatório:\n\- Nome do Condomínio\n\- Endereço de entrega da Proposta\n\- Nome do Síndico\n\- Apto. do Síndico\n\- Fone Residencial\n\- Tipo do Condomínio\n\Obrigado pela sua atenção!")
		return false;
	}
	
	else {
		return true;
	}
}

//-->
</script>
</head>
<body id="internas" class="contato">
<h1 class="seo">Contato</h1>
<h2 class="seo">Fale Conosco</h2>
<?php require "header.php"; ?>
<div class="content">
	<div class="centro636 proposta_adm">
		<h4 class="bordLaranja">Proposta para Administração de Condomínio</h4>
		<form method="post" name="Proposta" action="envia_cond.php" onSubmit="return verifcampos()">
			<fieldset>
			<table cellpadding="0" cellspacing="0" class="tableproposta">
				<tr>
					<td><label for="Nome_condominio">Nome do Condomínio:</label></td>
					<td><input type="text" id="Nome_condominio" name="Nome_condominio" size="46" tabindex="1"/></td>
				</tr>
				<tr>
					<td><label for="endereco_condominio">Endereço:</label></td>
					<td><input type="text" id="endereco_condominio" name="endereco_condominio" size="46" tabindex="2"/></td>
				</tr>
				<tr>
					<td><label for="numero_economias">Nº de Econ.:</label></td>
					<td><input type="text" id="numero_economias" name="numero_economias" size="5" tabindex="3"/></td>
				</tr>
				<tr>
					<td><label>Tipo Cond.</label></td>
					<td>
						<table cellpadding="0" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td><input type="radio" id="tipo1" name="Tipo" value="Residencial" tabindex="4"/>
									<label for="tipo1">Res.</label></td>
									<td><input type="radio" id="tipo2" name="Tipo" value="Comercial" tabindex="5"/>
									<label for="tipo2">Com.</label></td>
									<td><input type="radio" id="tipo3" name="Tipo" value="Misto" tabindex="6"/>
									<label for="tipo3">Misto</label></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td><label for="Nome_sindico">Nome do Síndico:</label></td>
					<td><input type="text" id="Nome_sindico" name="Nome_sindico" size="46" tabindex="7"/></td>
				</tr>
				<tr>
					<td><label for="numero_economias">Ap./Sl. do Síndico:</label></td>
					<td><input type="text" id="apto_sindico" name="apto_sindico" size="5" tabindex="8"/></td>
				</tr>
				<tr>
					<td><label>Síndico</label></td>
					<td>
						<table cellpadding="0" cellspacing="0" width="50%">
							<tbody>
								<tr>
									<td><input type="radio" id="sindico1" name="sindico" value="sindico" tabindex="9"/>
									<label class="bt_sindico" for="sindico1">Sim</label></td>
									<td><input type="radio" id="sindico2" name="sindico" value="não sindico" tabindex="10"/>
									<label class="bt_sindico" for="sindico2">Não</label></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr class="linha_gestao">
					<td><label>Admin. Atual:</label></td>
					<td>
						<table cellpadding="0" cellspacing="0" width="100%">
							<tbody>
								<tr class="linha_adm1">
									<td>
										<input type="radio" id="adm1" name="Adm" value="Administradora" tabindex="11"/>
										<label  for="adm1">Administ.</label>
										<br/>
										<input type="radio" id="adm2" name="Adm" value="Auto-Gestao" tabindex="13"/>
										<label  for="adm2">Auto-Gestão</label>
									</td>
									<td valign="top" style="vertical-align:top;">
										<label for="qual">Qual?</label>
										<input type="text" id="qual" name="Qual" size="20" tabindex="12"/>
									</td>
								</tr>								
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td><label for="endereco_envio_proposta">Endereço para envio:</label></td>
					<td><input type="text" id="endereco_envio_proposta" name="endereco_envio_proposta" size="46" tabindex="14"/></td>
				</tr>
				<tr>
					<td><label for="fone">Fone Residencial:</label></td>
					<td><input type="text" id="fone" name="fone" size="15" tabindex="15"/></td>
				</tr>
				<tr>
					<td><label for="fonecel">Fone Celular:</label></td>
					<td><input type="text" id="fonecel" name="fonecel" size="15" tabindex="16"/></td>
				</tr>
				<tr>
					<td><label for="fonecom">Fone Comercial:</label></td>
					<td><input type="text" id="fonecom" name="fonecom" size="15" tabindex="17"/></td>
				</tr>
				<tr>
					<td><label for="email">E-mail:</label></td>
					<td><input type="text" id="email" name="email" size="46" tabindex="18"/></td>
				</tr>
				<tr class="hide-mobile">
					<td colspan="2" align="right">
						<input name="enviar" id="enviar" type="submit" value="Enviar"/>
						<input name="reset" id="reset" type="reset" value="Limpar"/>
					</td>
				</tr>
				<!-- <tr>
					<td colspan="2">
					<p><?php
					   if (trim($mensagem) == "" or trim($nome) == "" or trim($fone) == "" or trim($email) == "")
						 {
							echo "Por favor $nome: informe todos os itens do formulário <br/> para podermos entrar em contato e ler sua mensagem";
						 }
					   else
						 {
						   mail("imobiliaria@raphaelimoveis.com.br","[Mensagem da Página Raphael]","$mensagem\n\nNome: $nome\nFone: $fone","From: $email");
					
					//       mail("edecio@terra.com.br","[Mensagem da Página Raphael]","$mensagem\n\nNome: $nome\nFone: $fone","From: $email");
					
					
						   mail("isan@raphaelimoveis.com.br","[Mensagem da Página Raphael]","$mensagem\n\nNome: $nome\nFone: $fone","From: $email");
					
					//      mail("edeciofernando@yahoo.com.br","[Mensagem da Página Raphael]","$mensagem\n\nNome: $nome\nFone: $fone","From: $email");
					
						   echo "Obrigado $nome! Em breve entraremos em contato.";
						 }
					?></p>
					</td>
				</tr> -->
				
			</table>		
			<input class="botao hide-desktop" name="enviar" id="enviar" type="submit" value="Enviar"/>
				<?php
					   if (trim($mensagem) == "" or trim($nome) == "" or trim($fone) == "" or trim($email) == "")
						 {
							echo "Por favor $nome: informe todos os itens do formulário <br/> para podermos entrar em contato e ler sua mensagem";
						 }
					   else
						 {
						   mail("imobiliaria@raphaelimoveis.com.br","[Mensagem da Página Raphael]","$mensagem\n\nNome: $nome\nFone: $fone","From: $email");
					
					//       mail("edecio@terra.com.br","[Mensagem da Página Raphael]","$mensagem\n\nNome: $nome\nFone: $fone","From: $email");
					
					
						   mail("isan@raphaelimoveis.com.br","[Mensagem da Página Raphael]","$mensagem\n\nNome: $nome\nFone: $fone","From: $email");
					
					//      mail("edeciofernando@yahoo.com.br","[Mensagem da Página Raphael]","$mensagem\n\nNome: $nome\nFone: $fone","From: $email");
					
						   echo "Obrigado $nome! Em breve entraremos em contato.";
						 }
					?>

			</fieldset>
		</form>
	</div>	
</div>
</div>
<?php require "footer.php"; ?>

</body>
</html> 
