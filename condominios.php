<?php
include "head.php";
?>
</head>
<body id="internas" class="documentos">
<?php include "header.php"; ?>
<div class="content">
	<div class="centro636 tela_condominios">
		<h2 class="bordLaranja" style="border:none;margin:0 0 5px 0;">Sr. Síndico:</h2>
		<h1 class="hide-mobile bordLaranja">Não é opção - é a solução<br/>
		para administrar o seu condomínio!</h1>
		<h4 class="hide-desktop">Não é opção - é a solução para administrar o seu condomínio!</h4>
		<ul class="logoLista">
			<li>Recrutamento, seleção e administração de pessoal.</li>
			<li>Na contratação de funcionários, o síndico, se assim preferir, ocupa-se somente da escolha final do candidato.</li>
			<li>Registros contábeis do condomínio, mês a mês.</li>
			<li>A documentação é mantida rigorosamente em dia, sempre a disposição do síndico.</li>
			<li>Administração dos recursos financeiros arrecadados no condomínio, compatibilizando receita com despesa.</li>
			<li>Recebimento e pagamento de taxas, contas e fornecedores.</li>
			<li>Orientação Jurídica em questões de toda ordem envolvendo o condomínio.</li>
			<li>Fornecimento mensal do demonstrativo de receitas e despesas do condomínio, para todos os condôminos.</li>
			<li>Organização e arquivamento da documentação do condomínio.</li>
			<li>Assessoria em assembléias opcional.</li>
			<li>Assessoria na contratação de fornecedores fixos e prestadores de serviços eventuais, como pintores, jardineiros, pedreiros, etc.</li>
		</ul>
		<h3 class="hide-mobile bordLaranja" style="border:none;">Traga seu condomínio para cá!</h3>
		<h4 class="hide-desktop">Traga seu condomínio para cá!</h4>
	</div>
</div>
</div>
<?php include "footer.php"; ?>
</body>
</html>
