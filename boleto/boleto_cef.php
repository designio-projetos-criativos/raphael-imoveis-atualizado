<?php
// +----------------------------------------------------------------------+
// | BoletoPhp - Vers�o Beta                                              |
// +----------------------------------------------------------------------+
// | Este arquivo est� dispon�vel sob a Licen�a GPL dispon�vel pela Web   |
// | em http://pt.wikipedia.org/wiki/GNU_General_Public_License           |
// | Voc� deve ter recebido uma c�pia da GNU Public License junto com     |
// | esse pacote; se n�o, escreva para:                                   |
// |                                                                      |
// | Free Software Foundation, Inc.                                       |
// | 59 Temple Place - Suite 330                                          |
// | Boston, MA 02111-1307, USA.                                          |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Originado do Projeto BBBoletoFree que tiveram colabora��es de Daniel |
// | William Schultz e Leandro Maniezo que por sua vez foi derivado do	  |
// | PHPBoleto de Jo�o Prado Maia e Pablo Martins F. Costa				  |
// | 																	  |
// | Se vc quer colaborar, nos ajude a desenvolver p/ os demais bancos :-)|
// | Acesse o site do Projeto BoletoPhp: www.boletophp.com.br             |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Equipe Coordena��o Projeto BoletoPhp: <boletophp@boletophp.com.br>   |
// | Desenvolvimento Boleto CEF: Elizeu Alcantara         		            |
// | Customiza��es necess�rias para implementa��o no cliente feitas       |
// | Anderson Pacheco Alves - 11-06-2009                                  |
// +----------------------------------------------------------------------+

 
// ------------------------- DADOS DIN�MICOS DO SEU CLIENTE PARA A GERA��O DO BOLETO (FIXO OU VIA GET) -------------------- //
// Os valores abaixo podem ser colocados manualmente ou ajustados p/ formul�rio c/ POST, GET ou de BD (MySql,Postgre,etc)	//


// DADOS DO BOLETO PARA O SEU CLIENTE
$taxa_boleto = 0;
$data_venc = implode(preg_match("~\/~", $row['vencimento']) == 0 ? "/" : "-", array_reverse(explode(preg_match("~\/~", $row['vencimento']) == 0 ? "-" : "/", $row['vencimento'])));
$valor_cobrado = $row['valor']; // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
$valor_cobrado = str_replace(",", ".",$valor_cobrado);
$valor_boleto=number_format($valor_cobrado+$taxa_boleto, 2, ',', '');

$dadosboleto["inicio_nosso_numero"] = substr($row['nossonumero'], 0, 2);  // Carteira SR: 80, 81 ou 82  -  Carteira CR: 90 (Confirmar com gerente qual usar)
$dadosboleto["nosso_numero"] = substr($row['nossonumero'], 2, 8);  // Nosso numero sem o DV - REGRA: M�ximo de 8 caracteres!
$dadosboleto["numero_documento"] = $row['competencia'];	// Num do pedido ou do documento
$dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
$dadosboleto["data_documento"] = date("d/m/Y"); // Data de emiss�o do Boleto
$dadosboleto["data_processamento"] = date("d/m/Y"); // Data de processamento do boleto (opcional)
$dadosboleto["valor_boleto"] = $valor_boleto; 	// Valor do Boleto - REGRA: Com v�rgula e sempre com duas casas depois da virgula

// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = str_replace(' ', '&nbsp;', $row['sacado']);
$dadosboleto["endereco1"] = str_replace(' ', '&nbsp;', $row['endereco1']);
$dadosboleto["endereco2"] = str_replace(' ', '&nbsp;', $row['endereco2']);

// INFORMACOES PARA O CLIENTE
$dadosboleto["demonstrativo1"] = str_replace(' ', '&nbsp;', $row['imovel']);
$dadosboleto["demonstrativo2"] = str_replace(' ', '&nbsp;', $row['locador']);
$dadosboleto["demonstrativo3"] = str_replace(' ', '&nbsp;', $row['locatario']);
$dadosboleto["demonstrativo4"] = '';
$dadosboleto["demonstrativo5"] = str_replace(' ', '&nbsp;', $row['inst1']);
$dadosboleto["demonstrativo6"] = str_replace(' ', '&nbsp;', $row['inst2']);
$dadosboleto["demonstrativo7"] = str_replace(' ', '&nbsp;', $row['inst3']);
$dadosboleto["demonstrativo8"] = str_replace(' ', '&nbsp;', $row['inst4']);
$dadosboleto["demonstrativo9"] = str_replace(' ', '&nbsp;', $row['inst5']);

// INSTRU��ES PARA O CAIXA
$dadosboleto["instrucoes1"] = $dadosboleto["demonstrativo1"];
$dadosboleto["instrucoes2"] = $dadosboleto["demonstrativo2"];
$dadosboleto["instrucoes3"] = $dadosboleto["demonstrativo3"];
$dadosboleto["instrucoes4"] = $dadosboleto["demonstrativo4"];
$dadosboleto["instrucoes5"] = $dadosboleto["demonstrativo5"];
$dadosboleto["instrucoes6"] = $dadosboleto["demonstrativo6"];
$dadosboleto["instrucoes7"] = $dadosboleto["demonstrativo7"];
$dadosboleto["instrucoes8"] = $dadosboleto["demonstrativo8"];
$dadosboleto["instrucoes9"] = $dadosboleto["demonstrativo9"];

// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = "";
$dadosboleto["valor_unitario"] = "";
$dadosboleto["aceite"] = "";		
$dadosboleto["especie"] = "R$";
$dadosboleto["especie_doc"] = "";


// ---------------------- DADOS FIXOS DE CONFIGURA��O DO SEU BOLETO --------------- //


// DADOS DA SUA CONTA - CEF
$dadosboleto["agencia"] = "1594"; // Num da agencia, sem digito
$dadosboleto["conta"] = ""; 	// Num da conta, sem digito
$dadosboleto["conta_dv"] = ""; 	// Digito do Num da conta

// DADOS PERSONALIZADOS - CEF
$dadosboleto["conta_cedente"] = "87000000013"; // ContaCedente do Cliente, sem digito (Somente N�meros)
$dadosboleto["conta_cedente_dv"] = "8"; // Digito da ContaCedente do Cliente
$dadosboleto["carteira"] = "SR";  // C�digo da Carteira: pode ser SR (Sem Registro) ou CR (Com Registro) - (Confirmar com gerente qual usar)

// SEUS DADOS
$dadosboleto["identificacao"] = "Segunda via - Boleto de aluguel";
$dadosboleto["cpf_cnpj"] = "";
$dadosboleto["endereco"] = "Rua Santa Cruz, 1992 - Fone/Fax: (53) 3225.1100";
$dadosboleto["cidade_uf"] = "PELOTAS - RS";
$dadosboleto["cedente"] = "Administradora Raphael Ltda.";

// N�O ALTERAR!
include("boleto/funcoes_cef.php"); 
include("boleto/layout_cef.php");
?>
