<?php
require_once "head.php";
?>
</head>
<body id="internas" class="devolucao">
<h1 class="seo">Aluguéis</h1>
<h2 class="seo">Procedimento para Devolução do Imóvel</h2>
<? require_once "header.php"; ?>
<div class="content">
	<div class="centro636">
		<h4 class="bordLaranja">Procedimento para Devolução do Imóvel</h4>
		<div class="just">
			<p><strong>LEI N° 8.245/91, DE 18 DE OUTUBRO DE 1991 (Dispõe sobre as Locações dos Imóveis Urbanos e os Procedimentos a Elas Pertinentes.)</strong></p>
			<p><strong>Art. 4º </strong>- Durante o prazo estipulado para a duração do contrato, não poderá o locador reaver o imóvel alugado. O locatário, todavia, poderá devolvê-lo, pagando a multa pactuada, segundo a proporção prevista no Art. 924 do Código Civil e, na sua falta, a que for judicialmente estipulada. </p>
			<p><strong> Parágrafo único. </strong>O locatário ficará dispensado da multa se a devolução do imóvel decorrer de transferência, pelo seu empregador, privado ou público, para prestar serviços em localidades diversas daquela do início do contrato, e se notificar, por escrito, o locador com prazo de, no mínimo, trinta dias de antecedência.” </p>
			<p><strong>Art. 6º </strong>- O locatário poderá denunciar a locação por prazo indeterminado mediante aviso por escrito ao locador, com antecedência mínima de trinta dias. </p>
			<p><strong>Parágrafo único. </strong>Na ausência do aviso, o locador poderá exigir quantia correspondente a um mês de aluguel e encargos, vigentes quando da rescisão. </p>
			<p><strong>Art. 23 </strong>- O locatário é obrigado a: <br/>
			  III - restituir o imóvel, finda a locação, no estado em que o recebeu, salvo as deteriorações decorrentes do seu uso normal; </p>
			<p><strong>Aviso Desocupação</strong><br/>
			  Conforme determinado pela Lei do Inquilinato e pelo Contrato de Locação de Imóvel, os locatários com contrato vencido e vigorando por prazo indeterminado, devem comunicar a sua desocupação por escrito e com 30 (trinta) dias de antecedência à entrega das chaves através de uma carta, e-mail, fax, telegrama.
			  Caso a desocupação ocorra antes da data do vencimento do contrato,  o locatário sujeita-se ao pagamento da multa contratual por rescisão antecipada do contrato. </p>
			<p><strong>Entrega das Chaves</strong><br/>
			  No momento da entrega das chaves do imóvel, solicitamos: a entrega do controle eletrônico do portão da garagem (nos casos em que houver); as chaves do imóvel, incluindo a chave da correspondência; </p>
			<p>Nos casos em que o inquilino não devolve a chave da correspondência e/ou o controle eletrônico do portão da garagem (se houver), eles são cobrados com os demais débitos.
			  Após a vistoria de desocupação o locatário deverá solicitar o encerramento do fornecimento de luz junto à CEEE e apresentar a Conta Final na administradora, sob pena de não receber a quitação total de seus débitos. </p>
			<p><strong>Vistoria de Desocupação</strong><br/>
			  Em até 02 (dois) dias úteis após a entrega das chaves, é realizada a vistoria de desocupação, que consiste na comparação da Vistoria de Locação inicial com a situação do imóvel no momento da entrega.
			  O inquilino deve, no prazo de 02 (dois) dias úteis, contatar a imobiliária a fim de obter o resultado da vistoria de desocupação e caso não haja divergências, o inquilino é comunicado para comparecer na imobiliária, a fim de efetuar o pagamento de eventuais débitos de aluguéis e encargos e receber a Carta de Quitação de Débitos. </p>
			<p>Em caso de divergências, o inquilino é comunicado a comparecer na imobiliária para retirar as chaves do imóvel para a realização dos reparos necessários ou optar por pagar o valor estabelecido para realizar todos os reparos e receber a Carta de Quitação de Débitos.</p>

		</div>
	</div>
</div>
</div>
<? require_once "footer.php"; ?>
</body>
</html>
    