<?php
$nomeImagem = 'floater.png';
$areaMap = '688,0,737,17';

$info = getimagesize($nomeImagem);
$largura = '-'.ceil($info[0]/2).'px';
?>

<script type="text/javascript">
	$(document).ready(function(){
		$(".fechar , .fundo,#floater a").click(function(){
			$(".fundo , .modal,#floater").fadeOut();
		});
		if($(".fundo").length > 0 && $(".fundo").is(":visible")){
			var maskHeight = $(document).height();
			var maskWidth = $(window).width();
			$(".fundo").css({'height':maskHeight});
			$(".fundo").fadeIn();
			$(".fundo").fadeTo("slow",0.8);
		}
	});
</script>
<style type="text/css">
.fundo {background:#FFFFFF; position:fixed;_position:absolute; top:0; left:0; z-index:9999999; width:100%; min-width:880px;  height:100%; }
#floater { position:absolute; left:50%; top: 10px; display:block; margin:30px 0 0 <?php echo $largura;?>; z-index:9999999; }
</style>
<div class="fundo"></div>
<div id="floater">
    <div style="position:relative;z-index:1;">
    <img src="<?php echo $nomeImagem;?>" alt="" name="floater_fechar" border="0" usemap="#floater_fecharMap" id="floater_fechar"  />
    <map name="floater_fecharMap" id="floater_fecharMap">
      <area shape="rect" coords="<?php echo $areaMap;?>" href="javascript:;" class="fechar" alt="Fechar" title="Fechar" />
    </map>
  </div>
</div>