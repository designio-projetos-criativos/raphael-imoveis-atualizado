<?php require "head.php";?>
</head>
<body id="internas" class="historico">
<h1 class="seo">História</h1>
<?php require "header.php"; ?>
<div class="content">
	<div class="centro636">
		<h2 class="bordLaranja hide-mobile">Competência, Honestidade e Profissionalismo</h2>
		<h2 class="bordLaranja hide-desktop"><p>Competência,</p> <p>Honestidade e</p> <p>Profissionalismo</p></h2>
		<div class="just">
			<p>Em 21 de janeiro de 1991, junto ao escritório de advocacia Morales, advogados associados, foi constituída a empresa Administradora Raphael Ltda., para o fim específico e exclusivo de administração de aluguéis, visto que, alguns clientes do escritório, diante da confiança depositada, solicitaram que fossem administrados os seus imóveis destinados a locação.</p>
			<p>O escritório era localizado à Rua Voluntários da Pátria, n 742, em uma área de 30m2, inicialmente administrando uma carteira de aproximadamente 50 imóveis, com dois funcionários.</p>
			<p>Com o passar dos anos, a empresa foi crescendo e se somaram os departamentos de vendas, condomínios, vistoria, processamento de dados, pessoal e de serviços de obras.</p>
			<p>Aliados ao escritório de advocacia Morales, advogados associados e a construtora Zona Sul Ltda., empresas coligadas, hoje entre funcionários e colaboradores a equipe é de aproximadamente 55 pessoas.</p>
			<p>A matriz está sediada no centro da cidade à rua Santa Cruz, 1992, ocupando uma área de 1.800 m2.</p>
			<p>A carteira de alugueis, atualmente possui aproximadamente 1500 imóveis, sendo que, alguns dos diferenciais é que a taxa de administração incide somente sobre o valor do aluguel recebido pelo proprietário e o baixíssimo índice de inadimplência de aluguéis, diante do rigor empreendido no momento de efetuar a locação.</p>
			<p>O departamento de vendas é composto de corretores altamente qualificados, tendo em seu cadastro as melhores ofertas de imóveis avulsos e de empreendimentos imobiliários.</p>
			<p>E o departamento de condomínio, dada a seriedade da empresa administra alguns condomínios a quase duas décadas.</p>
			<p>Para quem não acompanhou esta evolução, este é um minúsculo relato do histórico da empresa.</p>
		</div>
	</div>
</div>
</div>
<?php require "footer.php"; ?>
</body>
</html>

