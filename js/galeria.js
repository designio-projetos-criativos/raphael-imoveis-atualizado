jQuery('.fechamodal').click(function(){
    jQuery('.mask,.modal_galeria').fadeOut();
});

function rolaRight(){
    var rel = jQuery('.recebe_img img').attr('src');
    jQuery('.modal_galeria .btn_seta_esq').removeClass('opac');
    var prox_img = jQuery('.belt a.atual').next().html();
    var ultima = jQuery('.belt a.atual').attr('id');
    var ultima2 = jQuery('.belt a.atual').next().attr('id');
    if(ultima2 == 'ultima'){ jQuery('.modal_galeria .btn_seta_dir').addClass('opac'); }
    if(prox_img == null && ultima != 'ultima'){
        jQuery('.rightbutton,.leftbutton').removeClass('opac');
        var proxdiv = jQuery('.belt div.atual').next('div').attr('id');
        var proxdiv = jQuery('div a[rel='+rel+']').parent('div').next('div').attr('id');
        if(proxdiv == 'ultima'){ jQuery('.rightbutton').addClass('opac'); }
        jQuery('div.atual').removeClass('atual');
        jQuery('div a[rel='+rel+']').parent('div').next().addClass('atual');
        var imgZoom = jQuery('div.atual').find('a:first').attr('rel');
        loadImage(imgZoom,'');
    }
    else if(ultima != 'ultima'){
        jQuery('.belt a.atual').next().addClass('atual');
        jQuery('.belt a.atual:first').removeClass('atual');
        var imgZoom = jQuery('.belt a.atual').attr('rel');
        loadImage2(imgZoom);
    }
    else{ jQuery('.modal_galeria .btn_seta_dir').addClass('opac'); }
};
function rolaLeft(){
    var rel = jQuery('.recebe_img img').attr('src');
    jQuery('.modal_galeria .btn_seta_dir').removeClass('opac');
    var prox_img = jQuery('.belt a.atual').prev().html();
    var ultima = jQuery('.belt a.atual').attr('id');
    var ultima2 = jQuery('.belt a.atual').prev().attr('id');
    if(ultima2 == 'primeira'){ jQuery('.modal_galeria .btn_seta_esq').addClass('opac'); }
    if(prox_img == null && ultima != 'primeira'){
        jQuery('.rightbutton,.leftbutton').removeClass('opac');
        var proxdiv = jQuery('div a[rel='+rel+']').parent('div').prev('div').attr('id');
        if(proxdiv == 'primeira'){ jQuery('.leftbutton').addClass('opac'); }
        jQuery('div.atual').removeClass('atual');
        jQuery('div a[rel='+rel+']').parent('div').prev().addClass('atual');
        var imgZoom = jQuery('div.atual').find('a:last').attr('rel');
        loadImage(imgZoom,'');
    }
    else if(ultima != 'primeira'){
        jQuery('.belt a.atual').prev().addClass('atual');
        jQuery('.belt a.atual:last').removeClass('atual');
        var imgZoom = jQuery('.belt a.atual').attr('rel');
        loadImage2(imgZoom);
    }
    else{ jQuery('.modal_galeria .btn_seta_esq').addClass('opac'); }
};
function loadImage(imgZoom,id){

    if(imgZoom != jQuery('.recebe_img img').attr('src')){
        console.log("imagem "+imgZoom);
        jQuery('a.atual').removeClass('atual');
        jQuery('a[rel='+imgZoom+']').addClass('atual');
        if(id == 'primeira'){ jQuery('.modal_galeria .btn_seta_esq').addClass('opac');jQuery('.modal_galeria .btn_seta_dir').removeClass('opac'); }
        else if(id == 'ultima'){ jQuery('.modal_galeria .btn_seta_dir').addClass('opac');jQuery('.modal_galeria .btn_seta_esq').removeClass('opac'); }
        if(id == ""){ jQuery('.modal_galeria .btn_seta_dir,.modal_galeria .btn_seta_esq').removeClass('opac'); }
        if(jQuery('div.atual').find('a:first').attr('id') == 'ultima'){ jQuery('.btn_seta_dir').addClass('opac');}
        loadImage2(imgZoom);
    }
}
function loadImage2(imgZoom){

    jQuery(".recebe_img img").remove();
    var img = new Image();
    jQuery(img).error(function(){
        alert("Imagem não disponível no momento.");
        jQuery(".recebe_img img").remove();
    });

    jQuery(img).load(function (){

        jQuery('.recebe_img td[align="center"]').html(img);
        var pic = jQuery(".recebe_img img");
        pic.removeAttr("width");
        // pic.css({'max-width': "420px", height: "250px"});
        $(".recebe_img img").addClass('pos_img');


    }).attr('src', imgZoom);
}
function esteiraLeft(){
    jQuery('.rightbutton').removeClass('opac');
    if(jQuery('.belt div.atual').attr('id') != 'primeira'){
        if(jQuery('.belt div.atual').prev().attr('id') == 'primeira'){jQuery('.leftbutton').addClass('opac');}
        jQuery('.belt div.atual').prev().addClass('atual');
        jQuery('.belt div.atual:last').removeClass('atual');
    }
    else {jQuery('.leftbutton').addClass('opac');}
}
function esteiraRight(){
    jQuery('.leftbutton').removeClass('opac');
    if(jQuery('.belt div.atual').attr('id') != 'ultima'){
        if(jQuery('.belt div.atual').next().attr('id') == 'ultima'){jQuery('.rightbutton').addClass('opac');}
        jQuery('.belt div.atual').next().addClass('atual');
        jQuery('.belt div.atual:first').removeClass('atual');
    }
    else {jQuery('.rightbutton').addClass('opac');}
}
jQuery('.openmodal').click(function(){
    var id = jQuery(this).attr('rel');
    jQuery('.modal_galeria').before('<div class="mask"> </div>');
    var maskHeight = jQuery(document).height();
    var maskWidth = jQuery(window).width();
    jQuery(".mask").css({height:maskHeight+"px",width:maskWidth+"px",opacity:"0.65"});

    jQuery.ajax({
        type: "POST",
        url: "cria_modal.php",
        data: {
            id: id
        },
        success:function(conteudo){
            var var1 = conteudo.split(".quebra.")[0];
            var var2 = conteudo.split(".quebra.")[1];
            jQuery('.box_imgzoom2').html(var1);
            jQuery('.belt').html(var2);

            jQuery(".modal_galeria").addClass('modal_img_fixed');
            var pos = jQuery(".modal_galeria").position();
            jQuery(".modal_galeria").removeClass('modal_img_fixed');
            jQuery(".modal_galeria").css({position: "absolute",top: pos.top+50});


            jQuery('.modal_galeria').fadeIn('fast');
        }
    });
    return false;
});
jQuery('.mask').live('click', function() {
    jQuery('.mask,.modal_galeria').fadeOut();
});
