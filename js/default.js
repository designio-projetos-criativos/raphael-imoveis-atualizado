j = jQuery;
j(document).ready(function(){
    
    j('#bairro').change(function(){
       if($(this).val() == 4){
            $("#hideGTA").show();
       }else
       {
            $("#hideGTA").hide();
       }
    });
    
	j(".boxbusca button").click(function(){
		if(j(this).hasClass("buscaActive")){
			return false;
		}
		j(this).closest("div").find("button").removeClass("buscaActive");
		j(this).addClass("buscaActive");
		if(j(this).hasClass("vendas")){
			j("#formBuscaAluguel").addClass("displayNone");
			j("#formBuscaVenda").removeClass("displayNone");

			
		}
		if(j(this).hasClass("alugueis")){
			j("#formBuscaVenda").addClass("displayNone");
			j("#formBuscaAluguel").removeClass("displayNone");
			
		}
	});

	j("#cmb_tipo, #cmb_valor, #cmb_bairro, #cmb_tipo_venda, #cmb_valor_venda, #cmb_bairro_venda,").selectmenu();

	j("#formBuscaVenda .inputSubmit, #formBuscaAluguel .inputSubmit").hover(
		function(){
			j(this).addClass("inputSubmitHover");
		},
		function(){
			j(this).removeClass("inputSubmitHover");
		}
	)

	j('#sliderHome').cycle({
		fx: 'scrollHorz',
		speed: 900,
		timeout: 5000,
		prev: '.png-bgr-prev',
		next: '.png-bgr-next'					
	});	

	j('#sliderVendasHome').cycle({
		fx: 'scrollHorz',
		speed: 900,
		timeout: 0,
		prev: '.prev-vendas',
		next: '.next-vendas'					
	});	

	j('#sliderAlugueisHome').cycle({
		fx: 'scrollHorz',
		speed: 900,
		timeout: 0,
		prev: '.prev-alugueis',
		next: '.next-alugueis'					
	});


	j('#formNews').validate({
		ignore: "",
		ignoreTitle: true,
    	rules:{
			txt_nome: {required: true, valordefault: true},
			txt_email: {required: true, email: true, valordefault: true }			
			},errorPlacement: function(error,element) {
            	            	
				return true;
            },submitHandler: function( form ){ 
				j(".aguarde").show();
				url=j(form).attr("action");
				var dados = j(form).serialize();               
				
					j.ajax({  
						type: "POST",  
	                    url: url,  
	                    timeout: 15000,
						data: dados, 
						dataType : "json",
						error: function(error){
							j(".aguarde").hide();
							j(".boxRetorno").html("Houve algum erro, tente em instantes.");
							j(".boxRetorno").addClass("boxErro");
							j(".boxRetorno").stop().animate({
	                    		bottom: "0px",
	                    	});	
	                    	setTimeout(function(){
	                    		j(".boxRetorno").stop().animate({
		                    		bottom: "-135px",
		                    	});
	                    	}, 3000);			
						},
	                    success: function(json)  
	                    {  
	                    	j(".aguarde").hide();
	                    	if(json.erro){
	                    		j(".aguarde").hide();
								j(".boxRetorno").html(json.retorno);
								j(".boxRetorno").addClass("boxErro");
								j(".boxRetorno").stop().animate({
		                    		bottom: "0px",
		                    	});	
		                    	setTimeout(function(){
		                    		j(".boxRetorno").stop().animate({
			                    		bottom: "-135px",
			                    	});
		                    	}, 3000);
	                    	}else {
	                    		j(".boxRetorno").removeClass("boxErro");
	                    		j(".boxRetorno").html(json.retorno);
		                    	j(".boxRetorno").stop().animate({
		                    		bottom: "0px",
		                    	});
		                    	setTimeout(function(){
		                    		j(".boxRetorno").stop().animate({
			                    		bottom: "-135px",
			                    	});
		                    	}, 3000);
								j(form)[0].reset();
							}
	                    }  
	                });  
				
                return false;  
			}  			
	});

	j("#mainMenu li, #mainMenuFooter li").mouseenter(function(){
		j(this).find(".subMenu").show();
	});
	j("#mainMenu li, #mainMenuFooter li").mouseleave(function(){
		j(this).find(".subMenu").hide();
	});
    		
});

