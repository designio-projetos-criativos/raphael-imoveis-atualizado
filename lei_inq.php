<?php require "head.php";?>
</head>
<body id="internas" class="juridico">
<h1 class="seo">Advogados Coligados</h1>
<?php require "header.php"; ?>
<div class="content">
	<div class="centro636">
		<div class="just">
			
			<p><strong>LEI N&deg; 8.245/91, DE 18 DE OUTUBRO DE 1991</strong></p>
			<p><strong>Disp&otilde;e sobre as Loca&ccedil;&otilde;es dos Im&oacute;veis Urbanos e os Procedimentos a Elas Pertinentes.</strong></p>

			<p class="center titulo"><strong>T&iacute;tulo I<br />Da Loca&ccedil;&atilde;o</strong></p>
			
			<p class="center capitulo"><strong>Cap&iacute;tulo I</strong> - Disposi&ccedil;&otilde;es Gerais</p>
			<p class="center"><strong>Se&ccedil;&atilde;o I<br />Da Loca&ccedil;&atilde;o em Geral</strong></p>
			
			<p>Art. 1&ordm; - A loca&ccedil;&atilde;o de im&oacute;vel urbano regula-se pelo disposto nesta Lei.<br />
			Par&aacute;grafo &uacute;nico. Continuam regulados pelo C&oacute;digo Civil e pelas leis especiais:<br />
			a) as loca&ccedil;&otilde;es:<br />
			1. de im&oacute;veis de propriedade da Uni&atilde;o, dos Estados e dos Munic&iacute;pios, de suas autarquias e funda&ccedil;&otilde;es p&uacute;blicas;<br />
			2. de vagas aut&ocirc;nomas de garagem ou de espa&ccedil;os para estacionamento de ve&iacute;culos;<br />
			3. de espa&ccedil;os destinados &agrave; publicidade;<br />
			4. em apart-hot&eacute;is, hot&eacute;is-resid&ecirc;ncia ou equiparados, assim considerados aqueles que prestam servi&ccedil;os regulares a seus usu&aacute;rios e como tais sejam autorizados a funcionar.<br />
			b) o arrendamento mercantil, em qualquer de suas modalidades.</p>
			<p>Art. 2&ordm; - Havendo mais de um locador ou mais de um locat&aacute;rio, entende se que s&atilde;o solid&aacute;rios se o contr&aacute;rio n&atilde;o se estipulou.<br />
			Par&aacute;grafo &uacute;nico. Os ocupantes de habita&ccedil;&otilde;es coletivas multifamiliares presumem-se locat&aacute;rios ou sublocat&aacute;rios.</p>
			<p>Art. 3&ordm; - O contrato de loca&ccedil;&atilde;o pode ser ajustado por qualquer prazo, dependendo de v&ecirc;nia conjugal, se igual ou superior a dez anos.<br />
			Par&aacute;grafo &uacute;nico. Ausente a v&ecirc;nia conjugal, o c&ocirc;njuge n&atilde;o estar&aacute; obrigado a observar o prazo excedente.</p>
			<p>Art. 4&ordm; - Durante o prazo estipulado para a dura&ccedil;&atilde;o do contrato, n&atilde;o poder&aacute; o locador reaver o im&oacute;vel alugado. O locat&aacute;rio, todavia, poder&aacute; devolv&ecirc;-lo, pagando a multa pactuada, segundo a propor&ccedil;&atilde;o prevista no Art. 924 do C&oacute;digo Civil e, na sua falta, a que for judicialmente estipulada.<br />
			Par&aacute;grafo &uacute;nico. O locat&aacute;rio ficar&aacute; dispensado da multa se a devolu&ccedil;&atilde;o do im&oacute;vel decorrer de transfer&ecirc;ncia, pelo seu empregador, privado ou p&uacute;blico, para prestar servi&ccedil;os em localidades diversas daquela do in&iacute;cio do contrato, e se notificar, por escrito, o locador com prazo de, no m&iacute;nimo, trinta dias de anteced&ecirc;ncia.</p>
			<p>Art. 5&ordm; - Seja qual for o fundamento do t&eacute;rmino da loca&ccedil;&atilde;o, a a&ccedil;&atilde;o do locador para reaver o im&oacute;vel &eacute; a de despejo.<br />
			Par&aacute;grafo &uacute;nico. O disposto neste artigo n&atilde;o se aplica se a loca&ccedil;&atilde;o termina em decorr&ecirc;ncia de desapropria&ccedil;&atilde;o, com a imiss&atilde;o do expropriante na posse do im&oacute;vel.</p>
			<p>Art. 6&ordm; - O locat&aacute;rio poder&aacute; denunciar a loca&ccedil;&atilde;o por prazo indeterminado mediante aviso por escrito ao locador, com anteced&ecirc;ncia m&iacute;nima de trinta dias.<br />
			Par&aacute;grafo &uacute;nico. Na aus&ecirc;ncia do aviso, o locador poder&aacute; exigir quantia correspondente a um m&ecirc;s de aluguel e encargos, vigentes quando da resili&ccedil;&atilde;o.</p>
			<p>Art. 7&ordm; - Nos casos de extin&ccedil;&atilde;o de usufruto ou de fideicomisso, a loca&ccedil;&atilde;o celebrada pelo usufrutu&aacute;rio ou fiduci&aacute;rio poder&aacute; ser denunciada, com o prazo de trinta dias para a desocupa&ccedil;&atilde;o, salvo se tiver havido aquiesc&ecirc;ncia escrita do nu propriet&aacute;rio ou do fideicomiss&aacute;rio, ou se a propriedade estiver consolidada em m&atilde;os do usufrutu&aacute;rio ou do fiduci&aacute;rio.<br />
			Par&aacute;grafo &uacute;nico. A den&uacute;ncia dever&aacute; ser exercitada no prazo de noventa dias contados da extin&ccedil;&atilde;o do fideicomisso ou da averba&ccedil;&atilde;o da extin&ccedil;&atilde;o do usufruto, presumindo-se, ap&oacute;s esse prazo, a concord&acirc;ncia na manuten&ccedil;&atilde;o da loca&ccedil;&atilde;o.</p>
			<p>Art. 8&ordm; - Se o im&oacute;vel for alienado durante a loca&ccedil;&atilde;o, o adquirente poder&aacute; denunciar o contrato, com o prazo de noventa dias para a desocupa&ccedil;&atilde;o, salvo se a loca&ccedil;&atilde;o for por tempo determinado e o contrato contiver cl&aacute;usula de vig&ecirc;ncia em caso de aliena&ccedil;&atilde;o e estiver averbado junto &agrave; matr&iacute;cula do im&oacute;vel.<br />
			&sect; 1&ordm; - Id&ecirc;ntico direito ter&aacute; o promiss&aacute;rio-comprador e o promiss&aacute;rio-cession&aacute;rio, em car&aacute;ter irrevog&aacute;vel, com imiss&atilde;o na posse do im&oacute;vel e t&iacute;tulo registrado junto &agrave; matr&iacute;cula do mesmo.<br />
			&sect; 2&ordm; - A den&uacute;ncia dever&aacute; ser exercitada no prazo de noventa dias contados do registro da venda ou do compromisso, presumindo-se, ap&oacute;s esse prazo, a concord&acirc;ncia na manuten&ccedil;&atilde;o da loca&ccedil;&atilde;o.</p>
			<p>Art. 9&ordm; - A loca&ccedil;&atilde;o tamb&eacute;m poder&aacute; ser desfeita:<br />
			I - por m&uacute;tuo acordo;<br />
			II - em decorr&ecirc;ncia da pr&aacute;tica de infra&ccedil;&atilde;o legal ou contratual;<br />
			III - em decorr&ecirc;ncia da falta de pagamento do aluguel e demais encargos;<br />
			IV - para a realiza&ccedil;&atilde;o de repara&ccedil;&otilde;es urgentes determinadas pelo Poder P&uacute;blico, que n&atilde;o possam ser normalmente executadas com a perman&ecirc;ncia do locat&aacute;rio no im&oacute;vel ou, podendo, ele se recuse a consenti-las.</p>
			<p>Art. 10 - Morrendo o locador, a loca&ccedil;&atilde;o transmite-se aos herdeiros.</p>
			<p>Art. 11 - Morrendo o locat&aacute;rio, ficar&atilde;o sub-rogados nos seus direitos e obriga&ccedil;&otilde;es:<br />
			I - nas loca&ccedil;&otilde;es com finalidade residencial, o c&ocirc;njuge sobrevivente ou o companheiro e, sucessivamente, os herdeiros necess&aacute;rios e as pessoas que viviam na depend&ecirc;ncia econ&ocirc;mica do &quot;de cujus&quot;, desde que residentes no im&oacute;vel;<br />
			II - nas loca&ccedil;&otilde;es com finalidade n&atilde;o residencial, o esp&oacute;lio e, se for o caso, seu sucessor no neg&oacute;cio.</p>
			<p>Art. 12 - Em casos de separa&ccedil;&atilde;o de fato, separa&ccedil;&atilde;o judicial, div&oacute;rcio ou dissolu&ccedil;&atilde;o da sociedade concubin&aacute;ria, a loca&ccedil;&atilde;o prosseguir&aacute; automaticamente com o c&ocirc;njuge ou companheiro que permanecer no im&oacute;vel.<br />
			Par&aacute;grafo &uacute;nico. Nas hip&oacute;teses previstas neste artigo, a sub roga&ccedil;&atilde;o ser&aacute; comunicada por escrito ao locador, o qual ter&aacute; o direito de exigir, no prazo de trinta dias, a substitui&ccedil;&atilde;o do fiador ou o oferecimento de qualquer das garantias previstas nesta Lei.</p>
			<p>Art. 13 - A cess&atilde;o da loca&ccedil;&atilde;o, a subloca&ccedil;&atilde;o e o empr&eacute;stimo do im&oacute;vel, total ou parcialmente, dependem do consentimento pr&eacute;vio e escrito do locador.<br />
			&sect; 1&ordm; - N&atilde;o se presume o consentimento pela simples demora do locador em manifestar formalmente a sua oposi&ccedil;&atilde;o.<br />
			&sect; 2&ordm; - Desde que notificado por escrito pelo locat&aacute;rio, de ocorr&ecirc;ncia de uma das hip&oacute;teses deste artigo, o locador ter&aacute; o prazo de trinta dias para manifestar formalmente a sua oposi&ccedil;&atilde;o.</p>
			
			
			<p class="center"><strong>Se&ccedil;&atilde;o II<br />Das Subloca&ccedil;&otilde;es</strong></p>
			
			<p>Art. 14 - Aplicam-se &agrave;s subloca&ccedil;&otilde;es, no que couber, as disposi&ccedil;&otilde;es relativas &agrave;s loca&ccedil;&otilde;es.</p>
			<p>Art. 15 - Rescindida ou finda a loca&ccedil;&atilde;o, qualquer que seja sua causa, resolvem-se as subloca&ccedil;&otilde;es, assegurado o direito de indeniza&ccedil;&atilde;o do sublocat&aacute;rio contra o sublocador.</p>
			<p>Art. 16 - O sublocat&aacute;rio responde subsidiariamente ao locador pela import&acirc;ncia que dever ao sublocador, quando este for demandado e, ainda, pelos alugu&eacute;is que se vencerem durante a lide.<br />
			</p>
			
			
			<p class="center"><strong>Se&ccedil;&atilde;o III<br />Do Aluguel</strong></p>
			
			<p>Art. 17 - &Eacute; livre a conven&ccedil;&atilde;o do aluguel, vedada a sua estipula&ccedil;&atilde;o em moeda estrangeira e a sua vincula&ccedil;&atilde;o &agrave; varia&ccedil;&atilde;o cambial ou ao sal&aacute;rio m&iacute;nimo.<br />
			Par&aacute;grafo &uacute;nico. Nas loca&ccedil;&otilde;es residenciais ser&atilde;o observados os crit&eacute;rios de reajustes previstos na legisla&ccedil;&atilde;o espec&iacute;fica.</p>
			<p>Art. 18 - &Eacute; l&iacute;cito &agrave;s partes fixar, de comum acordo, novo valor para o aluguel, bem como inserir ou modificar cl&aacute;usula de reajuste.</p>
			<p>Art. 19 - N&atilde;o havendo acordo, o locador ou o locat&aacute;rio, ap&oacute;s tr&ecirc;s anos de vig&ecirc;ncia do contrato ou do acordo anteriormente realizado, poder&atilde;o pedir revis&atilde;o judicial do aluguel, a fim de ajust&aacute;-lo ao pre&ccedil;o do mercado.</p>
			<p>Art. 20 - Salvo as hip&oacute;teses do Art. 42 e da loca&ccedil;&atilde;o para temporada, o locador n&atilde;o poder&aacute; exigir o pagamento antecipado do aluguel.</p>
			<p>Art. 21 - O aluguel da subloca&ccedil;&atilde;o n&atilde;o poder&aacute; exceder o da loca&ccedil;&atilde;o, nas habita&ccedil;&otilde;es coletivas multifamiliares, a soma dos alugu&eacute;is n&atilde;o poder&aacute; ser superior ao dobro do valor da loca&ccedil;&atilde;o.<br />
			Par&aacute;grafo &uacute;nico. O descumprimento deste artigo autoriza o sublocat&aacute;rio a reduzir o aluguel at&eacute; os limites nele estabelecidos.<br />
			</p>
			
			
			<p class="center"><strong>Se&ccedil;&atilde;o IV<br />Dos Deveres do Locador e do Locat&aacute;rio</strong></p>
						
			<p>Art. 22 - O locador &eacute; obrigado a:<br />
			I - entregar ao locat&aacute;rio o im&oacute;vel alugado em estado de servir ao uso a que se destina;<br />
			II - garantir, durante o tempo da loca&ccedil;&atilde;o, o uso pac&iacute;fico do im&oacute;vel locado;<br />
			III - manter, durante a loca&ccedil;&atilde;o, a forma e o destino do im&oacute;vel;<br />
			IV - responder pelos v&iacute;cios ou defeitos anteriores &agrave; loca&ccedil;&atilde;o;<br />
			V - fornecer ao locat&aacute;rio, caso este solicite, descri&ccedil;&atilde;o minuciosa do estado do im&oacute;vel, quando de sua entrega, com expressa refer&ecirc;ncia aos eventuais defeitos existentes;<br />
			VI - fornecer ao locat&aacute;rio recibo discriminado das import&acirc;ncias por este pagas, vedada a quita&ccedil;&atilde;o gen&eacute;rica;<br />
			VII - pagar as taxas de administra&ccedil;&atilde;o imobili&aacute;ria, se houver, e de intermedia&ccedil;&otilde;es, nestas compreendidas as despesas necess&aacute;rias &agrave; aferi&ccedil;&atilde;o da idoneidade do pretendente ou de seu fiador;<br />
			VIII - pagar os impostos e taxas, e ainda o pr&ecirc;mio de seguro complementar contra fogo, que incidam ou venham a incidir sobre o im&oacute;vel, salvo disposi&ccedil;&atilde;o expressa em contr&aacute;rio no contrato;<br />
			IX - exibir ao locat&aacute;rio, quando solicitado, os comprovantes relativos &agrave;s parcelas que estejam sendo exigidas;<br />
			X - pagar as despesas extraordin&aacute;rias de condom&iacute;nio.<br />
			Par&aacute;grafo &uacute;nico. Por despesas extraordin&aacute;rias de condom&iacute;nio se entendem aquelas que n&atilde;o se refiram aos gastos rotineiros de manuten&ccedil;&atilde;o do edif&iacute;cio, especialmente:<br />
			a) obras de reformas ou acr&eacute;scimos que interessem &agrave; estrutura integral do im&oacute;vel;<br />
			b) pintura das fachadas, empenas, po&ccedil;os de aera&ccedil;&atilde;o e ilumina&ccedil;&atilde;o, bem como das esquadrias externas;<br />
			c) obras destinadas a repor as condi&ccedil;&otilde;es de habitabilidade do edif&iacute;cio;<br />
			d) indeniza&ccedil;&otilde;es trabalhistas e previdenci&aacute;rias pela dispensa de empregados, ocorridas em data anterior ao in&iacute;cio da loca&ccedil;&atilde;o;<br />
			e) instala&ccedil;&atilde;o de equipamentos de seguran&ccedil;a e de inc&ecirc;ndio, de telefonia, de intercomunica&ccedil;&atilde;o, de esporte e de lazer;<br />
			f) despesas de decora&ccedil;&atilde;o e paisagismo nas partes de uso comum;<br />
			g) constitui&ccedil;&atilde;o de fundo de reserva.</p>
			<p>Art. 23 - O locat&aacute;rio &eacute; obrigado a:<br />
			I - pagar pontualmente o aluguel e os encargos da loca&ccedil;&atilde;o, legal ou contratualmente exig&iacute;veis, no prazo estipulado ou, em sua falta, at&eacute; o sexto dia &uacute;til do m&ecirc;s seguinte ao vencido, no im&oacute;vel locado, quando outro local n&atilde;o tiver sido indicado no contrato;<br />
			II - servir-se do im&oacute;vel para uso convencionado ou presumido, compat&iacute;vel com a natureza deste e com o fim a que se destina, devendo trat&aacute;-lo com o mesmo cuidado como se fosse seu;<br />
			III - restituir o im&oacute;vel, finda a loca&ccedil;&atilde;o, no estado em que o recebeu, salvo as deteriora&ccedil;&otilde;es decorrentes do seu uso normal;<br />
			IV - levar imediatamente ao conhecimento do locador o surgimento de qualquer dano ou defeito cuja repara&ccedil;&atilde;o a este incumba, bem como as eventuais turba&ccedil;&otilde;es de terceiros;<br />
			V - realizar a imediata repara&ccedil;&atilde;o dos danos verificados no im&oacute;vel, ou nas suas instala&ccedil;&otilde;es, provocados por si, seus dependentes, familiares, visitantes ou prepostos;<br />
			VI - n&atilde;o modificar a forma interna ou externa do im&oacute;vel sem o consentimento pr&eacute;vio ou por escrito do locador;<br />
			VII - entregar imediatamente ao locador os documentos de cobran&ccedil;a de tributos e encargos condominiais, bem como qualquer intima&ccedil;&atilde;o, multa ou exig&ecirc;ncia de autoridade p&uacute;blica, ainda que dirigida a ele, locat&aacute;rio;<br />
			VIII - pagar as despesas de telefone e de consumo de for&ccedil;a, luz e g&aacute;s, &aacute;gua e esgoto;<br />
			IX - permitir a vistoria do im&oacute;vel pelo locador ou por seu mandat&aacute;rio, mediante combina&ccedil;&atilde;o pr&eacute;via, de dia e hora bem como admitir que seja o mesmo visitado e examinado por terceiros, na hip&oacute;tese prevista no Art. 27;<br />
			X - cumprir integralmente a conven&ccedil;&atilde;o de condom&iacute;nio e os regulamentos internos;<br />
			XI - pagar o pr&ecirc;mio do seguro de fian&ccedil;a;<br />
			XII - pagar as despesas ordin&aacute;rias de condom&iacute;nio.<br />
			&sect; 1&ordm; - Por despesas ordin&aacute;rias de condom&iacute;nio se entendem as necess&aacute;rias &agrave; administra&ccedil;&atilde;o respectiva, especialmente:<br />
			a) sal&aacute;rios, encargos trabalhistas, contribui&ccedil;&otilde;es previdenci&aacute;rias e sociais dos empregados do condom&iacute;nio;<br />
			b) consumo de &aacute;gua e esgoto, g&aacute;s, luz e for&ccedil;a das &aacute;reas de uso comum;<br />
			c) limpeza, conserva&ccedil;&atilde;o e pintura das instala&ccedil;&otilde;es e depend&ecirc;ncias de uso comum;<br />
			d) manuten&ccedil;&atilde;o e conserva&ccedil;&atilde;o das instala&ccedil;&otilde;es e equipamentos hidr&aacute;ulicos, el&eacute;tricos, mec&acirc;nicos e de seguran&ccedil;a, de uso comum;<br />
			e) manuten&ccedil;&atilde;o e conserva&ccedil;&atilde;o das instala&ccedil;&otilde;es e equipamentos de uso comum destinados &agrave; pr&aacute;tica de esportes e lazer;<br />
			f) manuten&ccedil;&atilde;o e conserva&ccedil;&atilde;o de elevadores, porteiro eletr&ocirc;nico e antenas coletivas;<br />
			g) pequenos reparos nas depend&ecirc;ncias e instala&ccedil;&otilde;es el&eacute;tricas e hidr&aacute;ulicas de uso comum;<br />
			h) rateios de saldo devedor, salvo se referentes a per&iacute;odo anterior ao in&iacute;cio da loca&ccedil;&atilde;o;<br />
			i) reposi&ccedil;&atilde;o do fundo de reserva, total ou parcialmente utilizado no custeio ou complementa&ccedil;&atilde;o das despesas referidas nas al&iacute;neas anteriores, salvo se referentes a per&iacute;odo anterior ao in&iacute;cio da loca&ccedil;&atilde;o.<br />
			&sect; 2&ordm; - O locat&aacute;rio fica obrigado ao pagamento das despesas referidas no par&aacute;grafo anterior, desde que comprovados a previs&atilde;o or&ccedil;ament&aacute;ria e o rateio mensal, podendo exigir a qualquer tempo a comprova&ccedil;&atilde;o das mesmas.<br />
			&sect; 3&ordm; - No edif&iacute;cio constitu&iacute;do por unidades imobili&aacute;rias aut&ocirc;nomas, de propriedade da mesma pessoa, os locat&aacute;rios ficam obrigados ao pagamento das despesas referidas no &sect; 1&ordm; deste artigo, desde que comprovadas.</p>
			<p>Art. 24 - Nos im&oacute;veis utilizados como habita&ccedil;&atilde;o coletiva multifamiliar, os locat&aacute;rios ou sublocat&aacute;rios poder&atilde;o depositar judicialmente o aluguel e encargos se a constru&ccedil;&atilde;o for considerada em condi&ccedil;&otilde;es prec&aacute;rias pelo Poder P&uacute;blico.<br />
			&sect; 1&ordm; - O levantamento dos dep&oacute;sitos somente ser&aacute; deferido com a comunica&ccedil;&atilde;o, pela autoridade p&uacute;blica, da regulariza&ccedil;&atilde;o do im&oacute;vel.<br />
			&sect; 2&ordm; - Os locat&aacute;rios ou sublocat&aacute;rios que deixarem o im&oacute;vel estar&atilde;o desobrigados do aluguel durante a execu&ccedil;&atilde;o das obras necess&aacute;rias &agrave; regulariza&ccedil;&atilde;o.<br />
			&sect; 3&ordm; - Os dep&oacute;sitos efetuados em ju&iacute;zo pelos locat&aacute;rios ou sublocat&aacute;rios poder&atilde;o ser levantados, mediante ordem judicial, para realiza&ccedil;&atilde;o das obras ou servi&ccedil;os necess&aacute;rios &agrave; regulariza&ccedil;&atilde;o do im&oacute;vel.</p>
			<p>Art. 25 - Atribu&iacute;da ao locat&aacute;rio a responsabilidade pelo pagamento dos tributos, encargos e despesas ordin&aacute;rias de condom&iacute;nio, o locador poder&aacute; cobrar tais verbas juntamente com o aluguel do m&ecirc;s a que se refiram.<br />
			Par&aacute;grafo &uacute;nico. Se o locador antecipar os pagamentos, a ele pertencer&atilde;o as vantagens da&iacute; advindas, salvo se o locat&aacute;rio reembols&aacute;-lo integralmente.</p>
			<p>Art. 26 - Necessitando o im&oacute;vel de reparos urgentes, cuja realiza&ccedil;&atilde;o incumba ao locador, o locat&aacute;rio &eacute; obrigado a consenti-los.<br />
			Par&aacute;grafo &uacute;nico. Se os reparos durarem mais de dez dias, o locat&aacute;rio ter&aacute; direito ao abatimento do aluguel, proporcional ao per&iacute;odo excedente; se mais de trinta dias, poder&aacute; resilir o contrato.<br />
			</p>
			
			
			<p class="center"><strong>Se&ccedil;&atilde;o V<br />Do Direito de Prefer&ecirc;ncia</strong></p>
			
			<p>Art. 27 - No caso de venda, promessa de venda, cess&atilde;o ou promessa de cess&atilde;o de direitos ou da&ccedil;&atilde;o em pagamento, o locat&aacute;rio tem prefer&ecirc;ncia para adquirir o im&oacute;vel locado em igualdade de condi&ccedil;&otilde;es com terceiros, devendo o locador dar-lhe conhecimento do neg&oacute;cio mediante notifica&ccedil;&atilde;o judicial, extrajudicial ou outro meio de ci&ecirc;ncia inequ&iacute;voca.<br />
			Par&aacute;grafo &uacute;nico. A comunica&ccedil;&atilde;o dever&aacute; conter todas as condi&ccedil;&otilde;es do neg&oacute;cio e, em especial, o pre&ccedil;o, a forma de pagamento, a exist&ecirc;ncia de &ocirc;nus reais, bem como o local e hor&aacute;rio em que pode ser examinada a documenta&ccedil;&atilde;o pertinente.</p>
			<p>Art. 28 - O direito de prefer&ecirc;ncia do locat&aacute;rio caducar&aacute; se n&atilde;o manifestada, de maneira inequ&iacute;voca, sua aceita&ccedil;&atilde;o integral &agrave; proposta, no prazo de trinta dias.</p>
			<p>Art. 29 - Ocorrendo aceita&ccedil;&atilde;o da proposta, pelo locat&aacute;rio, a posterior desist&ecirc;ncia do neg&oacute;cio pelo locador acarreta, a este, responsabilidade pelos preju&iacute;zos ocasionados, inclusive lucros cessantes.</p>
			<p>Art. 30 - Estando o im&oacute;vel sublocado em sua totalidade, caber&aacute; a prefer&ecirc;ncia ao sublocat&aacute;rio e, em seguida, ao locat&aacute;rio. Se forem v&aacute;rios os sublocat&aacute;rios, a prefer&ecirc;ncia caber&aacute; a todos, em comum, ou a qualquer deles, se um s&oacute; for o interessado.<br />
			Par&aacute;grafo &uacute;nico. Havendo pluralidade de pretendentes, caber&aacute; a prefer&ecirc;ncia ao locat&aacute;rio mais antigo, e, se da mesma data, ao mais idoso.</p>
			<p>Art. 31 - Em se tratando de aliena&ccedil;&atilde;o de mais de uma unidade imobili&aacute;ria, o direito de prefer&ecirc;ncia incidir&aacute; sobre a totalidade dos bens objeto da aliena&ccedil;&atilde;o.</p>
			<p>Art. 32 - O direito de prefer&ecirc;ncia n&atilde;o alcan&ccedil;a os casos de perda da propriedade ou venda por decis&atilde;o judicial, permuta, doa&ccedil;&atilde;o, integraliza&ccedil;&atilde;o de capital, cis&atilde;o, fus&atilde;o e incorpora&ccedil;&atilde;o.</p>
			<p>Art. 33 - O locat&aacute;rio preterido no seu direito de prefer&ecirc;ncia poder&aacute; reclamar do alienante as perdas e danos ou, depositando o pre&ccedil;o e demais despesas do ato de transfer&ecirc;ncia, haver para si o im&oacute;vel locado, se o requerer no prazo de seis meses, a contar do registro do ato no Cart&oacute;rio de Im&oacute;veis, desde que o contrato de loca&ccedil;&atilde;o esteja averbado pelo menos trinta dias antes da aliena&ccedil;&atilde;o junto &agrave; matr&iacute;cula do im&oacute;vel.<br />
			Par&aacute;grafo &uacute;nico. A averba&ccedil;&atilde;o far-se-&aacute; &agrave; vista de qualquer das vias do contrato de loca&ccedil;&atilde;o, desde que subscrito tamb&eacute;m por duas testemunhas.</p>
			<p>Art. 34 - Havendo condom&iacute;nio no im&oacute;vel, a prefer&ecirc;ncia do cond&ocirc;mino ter&aacute; prioridade sobre a do locat&aacute;rio.</p>
			
			
			<p class="center"><strong>Se&ccedil;&atilde;o VI<br />Das Benfeitorias</strong></p>
			
			<p>Art. 35 - Salvo expressa disposi&ccedil;&atilde;o contratual em contr&aacute;rio, as benfeitorias necess&aacute;rias introduzidas pelo locat&aacute;rio, ainda que n&atilde;o autorizadas pelo locador, bem como as &uacute;teis, desde que autorizadas, ser&atilde;o indeniz&aacute;veis e permitem o exerc&iacute;cio do direito de reten&ccedil;&atilde;o.</p>
			<p>Art. 36 - As benfeitorias voluptu&aacute;rias n&atilde;o ser&atilde;o indeniz&aacute;veis, podendo ser levantadas pelo locat&aacute;rio, finda a loca&ccedil;&atilde;o, desde que sua retirada n&atilde;o afete a estrutura e a subst&acirc;ncia do im&oacute;vel.</p>
			
			
			<p class="center"><strong>Se&ccedil;&atilde;o VII<br />Das Garantias Locat&iacute;cias</strong></p>
			
			<p>Art. 37 - No contrato de loca&ccedil;&atilde;o, pode o locador exigir do locat&aacute;rio as seguintes modalidades de garantia:<br />
			I - cau&ccedil;&atilde;o;<br />
			II - fian&ccedil;a;<br />
			III - seguro de fian&ccedil;a locat&iacute;cia.<br />
			Par&aacute;grafo &uacute;nico. &Eacute; vedada, sob pena de nulidade, mais de uma das modalidades de garantia num mesmo contrato de loca&ccedil;&atilde;o.</p>
			<p>Art. 38 - A cau&ccedil;&atilde;o poder&aacute; ser em bens m&oacute;veis ou im&oacute;veis.<br />
			&sect; 1&ordm; - A cau&ccedil;&atilde;o em bens m&oacute;veis dever&aacute; ser registrada em Cart&oacute;rio de T&iacute;tulos e Documentos; a em bens im&oacute;veis dever&aacute; ser averbada &agrave; margem da respectiva matr&iacute;cula.<br />
			&sect; 2&ordm; - A cau&ccedil;&atilde;o em dinheiro, que n&atilde;o poder&aacute; exceder o equivalente a tr&ecirc;s meses de aluguel, ser&aacute; depositada em caderneta de poupan&ccedil;a, autorizada pelo Poder P&uacute;blico e por ele regulamentada, revertendo em benef&iacute;cio do locat&aacute;rio todas as vantagens dela decorrentes por ocasi&atilde;o do levantamento da soma respectiva.<br />
			&sect; 3&ordm; - A cau&ccedil;&atilde;o em t&iacute;tulos e a&ccedil;&otilde;es dever&aacute; ser substitu&iacute;da, no prazo de trinta dias, em caso de concordata, fal&ecirc;ncia ou liquida&ccedil;&atilde;o das sociedades emissoras.</p>
			<p>Art. 39 - Salvo disposi&ccedil;&atilde;o contratual em contr&aacute;rio, qualquer das garantias da loca&ccedil;&atilde;o se estende at&eacute; a efetiva devolu&ccedil;&atilde;o do im&oacute;vel.</p>
			<p>Art. 40 - O locador poder&aacute; exigir novo fiador ou a substitui&ccedil;&atilde;o da modalidade de garantia, nos seguintes casos:<br />
			I - morte do fiador;<br />
			II - aus&ecirc;ncia, interdi&ccedil;&atilde;o, fal&ecirc;ncia ou insolv&ecirc;ncia do fiador, declaradas judicialmente;<br />
			III - aliena&ccedil;&atilde;o ou grava&ccedil;&atilde;o de todos os bens im&oacute;veis do fiador ou sua mudan&ccedil;a de resid&ecirc;ncia sem comunica&ccedil;&atilde;o ao locador;<br />
			IV - exonera&ccedil;&atilde;o do fiador;<br />
			V - prorroga&ccedil;&atilde;o da loca&ccedil;&atilde;o por prazo indeterminado, sendo a fian&ccedil;a ajustada por prazo certo;<br />
			VI - desaparecimento dos bens m&oacute;veis;<br />
			VII - desapropria&ccedil;&atilde;o ou aliena&ccedil;&atilde;o do im&oacute;vel.</p>
			<p>Art. 41 - O seguro de fian&ccedil;a locat&iacute;cia abranger&aacute; a totalidade das obriga&ccedil;&otilde;es do locat&aacute;rio.</p>
			<p>Art. 42 - N&atilde;o estando a loca&ccedil;&atilde;o garantida por qualquer das modalidades, o locador poder&aacute; exigir do locat&aacute;rio o pagamento do aluguel e encargos at&eacute; o sexto dia &uacute;til do m&ecirc;s vincendo.<br />
			</p>
			
			
			<p class="center"><strong>Se&ccedil;&atilde;o VIII<br />Das Penalidades Criminais e Civis</strong></p>
			
			<p>Art. 43 - Constitui contraven&ccedil;&atilde;o penal, pun&iacute;vel com pris&atilde;o simples de cinco dias a seis meses ou multa de tr&ecirc;s a doze meses do valor do &uacute;ltimo aluguel atualizado, revertida em favor do locat&aacute;rio:<br />
			I - exigir, por motivo de loca&ccedil;&atilde;o ou subloca&ccedil;&atilde;o, quantia ou valor al&eacute;m do aluguel e encargos permitidos;<br />
			II - exigir, por motivo de loca&ccedil;&atilde;o ou subloca&ccedil;&atilde;o, mais de uma modalidade de garantia num mesmo contrato de loca&ccedil;&atilde;o;<br />
			III - cobrar antecipadamente o aluguel, salvo a hip&oacute;tese do Art. 42 e da loca&ccedil;&atilde;o para temporada.</p>
			<p>Art. 44 - Constitui crime de a&ccedil;&atilde;o p&uacute;blica, pun&iacute;vel com deten&ccedil;&atilde;o de tr&ecirc;s meses a um ano, que poder&aacute; ser substitu&iacute;da pela presta&ccedil;&atilde;o de servi&ccedil;os &agrave; comunidade:<br />
			I - recusar-se o locador ou sublocador, nas habita&ccedil;&otilde;es coletivas multifamiliares, a fornecer recibo discriminado do aluguel e encargos;<br />
			II - deixar o retomante, dentro de cento e oitenta dias ap&oacute;s a entrega do im&oacute;vel, no caso do inciso III do Art. 47, de us&aacute;-lo para o fim declarado ou, usando-o, n&atilde;o o fizer pelo prazo m&iacute;nimo de um ano;<br />
			III - n&atilde;o iniciar o propriet&aacute;rio, promiss&aacute;rio-comprador ou promiss&aacute;rio-cession&aacute;rio, nos casos do inciso IV do Art. 9, inciso IV do Art. 47, inciso I do Art. 52 e inciso II do Art.53, a demoli&ccedil;&atilde;o ou a repara&ccedil;&atilde;o do im&oacute;vel, dentro de sessenta dias contados de sua entrega;<br />
			IV - executar o despejo com inobserv&acirc;ncia do disposto no &sect; 2&ordm; do Art. 65.<br />
			Par&aacute;grafo &uacute;nico. Ocorrendo qualquer das hip&oacute;teses previstas neste artigo, poder&aacute; o prejudicado reclamar em processo pr&oacute;prio, multa equivalente a um m&iacute;nimo de doze e um m&aacute;ximo de vinte e quatro meses do valor do &uacute;ltimo aluguel atualizado ou do que esteja sendo cobrado do novo locat&aacute;rio, se realugado o im&oacute;vel.<br />
			</p>
			
			
			<p class="center"><strong>Se&ccedil;&atilde;o IX<br />Das Nulidades</strong></p>
			
			<p>Art. 45 - S&atilde;o nulas de pleno direito as cl&aacute;usulas do contrato de loca&ccedil;&atilde;o que visem a elidir os objetivos da presente Lei, notadamente as que pro&iacute;bam a prorroga&ccedil;&atilde;o prevista no Art. 47, ou que afastem o direito &agrave; renova&ccedil;&atilde;o, na hip&oacute;tese do Art. 51, ou que imponham obriga&ccedil;&otilde;es pecuni&aacute;rias para tanto.</p>
			
			
			<p class="center capitulo"><strong>Cap&iacute;tulo II</strong> - Das Disposi&ccedil;&otilde;es Especiais</p>
			<p class="center"><strong>Se&ccedil;&atilde;o I<br />Da Loca&ccedil;&atilde;o Residencial</strong></p>
			
			<p>Art. 46 - Nas loca&ccedil;&otilde;es ajustadas por escrito e por prazo igual ou superior a trinta meses, a resolu&ccedil;&atilde;o do contrato ocorrer&aacute; findo o prazo estipulado, independentemente de notifica&ccedil;&atilde;o ou aviso.<br />
			&sect; 1&ordm; - Findo o prazo ajustado, se o locat&aacute;rio continuar na posse do im&oacute;vel alugado por mais de trinta dias sem oposi&ccedil;&atilde;o do locador, presumir-se-&aacute; prorrogada a loca&ccedil;&atilde;o por prazo indeterminado, mantidas as demais cl&aacute;usulas e condi&ccedil;&otilde;es do contrato.<br />
			&sect; 2&ordm; - Ocorrendo a prorroga&ccedil;&atilde;o, o locador poder&aacute; denunciar o contrato a qualquer tempo, concedido o prazo de trinta dias para desocupa&ccedil;&atilde;o.</p>
			<p>Art. 47 - Quando ajustada verbalmente ou por escrito e com prazo inferior a trinta meses, findo o prazo estabelecido, a loca&ccedil;&atilde;o prorroga-se automaticamente, por prazo indeterminado, somente podendo ser retomado o im&oacute;vel:<br />
			I - nos casos do Art. 9;<br />
			II - em decorr&ecirc;ncia de extin&ccedil;&atilde;o do contrato de trabalho, se a ocupa&ccedil;&atilde;o do im&oacute;vel pelo locat&aacute;rio estiver relacionada com o seu emprego;<br />
			III - se for pedido para uso pr&oacute;prio, de seu c&ocirc;njuge ou companheiro, ou para uso residencial de ascendente ou descendente que n&atilde;o disponha, assim como seu c&ocirc;njuge ou companheiro, de im&oacute;vel residencial pr&oacute;prio;<br />
			IV - se for pedido para demoli&ccedil;&atilde;o e edifica&ccedil;&atilde;o licenciada ou para a realiza&ccedil;&atilde;o de obras aprovadas pelo Poder P&uacute;blico, que aumentem a &aacute;rea constru&iacute;da em, no m&iacute;nimo, vinte por cento ou, se o im&oacute;vel for destinado a explora&ccedil;&atilde;o de hotel ou pens&atilde;o, em cinq&uuml;enta por cento;<br />
			V - se a vig&ecirc;ncia ininterrupta da loca&ccedil;&atilde;o ultrapassar cinco anos.<br />
			&sect; 1&ordm; - Na hip&oacute;tese do inciso III, a necessidade dever&aacute; ser judicialmente demonstrada, se:<br />
			a) o retomante, alegando necessidade de usar o im&oacute;vel estiver ocupando, com a mesma finalidade, outro de sua propriedade situado na mesma localidade ou, residindo ou utilizando im&oacute;vel alheio, j&aacute; tiver retomado o im&oacute;vel anteriormente;<br />
			b) o ascendente ou descendente, benefici&aacute;rio da retomada, residir em im&oacute;vel pr&oacute;prio.<br />
			&sect; 2&ordm; - Nas hip&oacute;teses dos incisos III e IV, o retomante dever&aacute; comprovar ser propriet&aacute;rio, promiss&aacute;rio-comprador ou promiss&aacute;rio cession&aacute;rio, em car&aacute;ter irrevog&aacute;vel, com imiss&atilde;o na posse do im&oacute;vel e t&iacute;tulo registrado junto &agrave; matr&iacute;cula do mesmo.<br />
			</p>
			
			
			<p class="center"><strong>Se&ccedil;&atilde;o II<br />Da Loca&ccedil;&atilde;o para Temporada</strong></p>
			
			<p>Art. 48 - Considera-se loca&ccedil;&atilde;o para temporada aquela destinada &agrave; resid&ecirc;ncia tempor&aacute;ria do locat&aacute;rio, para pr&aacute;tica de lazer, realiza&ccedil;&atilde;o de cursos, tratamento de sa&uacute;de, feitura de obras em seu im&oacute;vel, e outros fatos que decorram t&atilde;o somente de determinado tempo, e contratada por prazo n&atilde;o superior a noventa dias, esteja ou n&atilde;o mobiliado o im&oacute;vel.<br />
			Par&aacute;grafo &uacute;nico. No caso de a loca&ccedil;&atilde;o envolver im&oacute;vel mobiliado, constar&aacute; do Contrato, obrigatoriamente, a descri&ccedil;&atilde;o dos m&oacute;veis e utens&iacute;lios que o guarne&ccedil;am, bem como o estado em que se encontram.</p>
			<p>Art. 49 - O locador poder&aacute; receber de uma s&oacute; vez e antecipadamente os alugu&eacute;is e encargos, bem como exigir qualquer das modalidades de garantia previstas no Art. 37 para atender as demais obriga&ccedil;&otilde;es do contrato.</p>
			<p>Art. 50 - Findo o prazo ajustado, se o locat&aacute;rio permanecer no im&oacute;vel sem oposi&ccedil;&atilde;o do locador por mais de trinta dias, presumir-se-&aacute; prorrogada a loca&ccedil;&atilde;o por tempo indeterminado, n&atilde;o mais sendo exig&iacute;vel o pagamento antecipado do aluguel e dos encargos.<br />
			Par&aacute;grafo &uacute;nico. Ocorrendo a prorroga&ccedil;&atilde;o, o locador somente poder&aacute; denunciar o contrato ap&oacute;s trinta meses de seu in&iacute;cio ou nas hip&oacute;teses do Art. 47.<br />
			</p>
			
			
			<p class="center"><strong>Se&ccedil;&atilde;o III<br />Da Loca&ccedil;&atilde;o n&atilde;o Residencial</strong></p>
			
			<p>Art. 51 - Nas loca&ccedil;&otilde;es de im&oacute;veis destinados ao com&eacute;rcio, o locat&aacute;rio ter&aacute; direito a renova&ccedil;&atilde;o do contrato, por igual prazo, desde que, cumulativamente:<br />
			I - o contrato a renovar tenha sido celebrado por escrito e com prazo determinado;<br />
			II - o prazo m&iacute;nimo do contrato a renovar ou a soma dos prazos ininterruptos dos contratos escritos seja de cinco anos;<br />
			III - o locat&aacute;rio esteja explorando seu com&eacute;rcio, no mesmo ramo, pelo prazo m&iacute;nimo e ininterrupto de tr&ecirc;s anos.<br />
			&sect; 1&ordm; - O direito assegurado neste artigo poder&aacute; ser exercido pelos cession&aacute;rios ou sucessores da loca&ccedil;&atilde;o; no caso de subloca&ccedil;&atilde;o total do im&oacute;vel, o direito a renova&ccedil;&atilde;o somente poder&aacute; ser exercido pelo sublocat&aacute;rio.<br />
			&sect; 2&ordm; - Quando o contrato autorizar que o locat&aacute;rio utilize o im&oacute;vel para as atividades de sociedade de que fa&ccedil;a parte e que a esta passe a pertencer o fundo de com&eacute;rcio, o direito a renova&ccedil;&atilde;o poder&aacute; ser exercido pelo locat&aacute;rio ou pela sociedade.<br />
			&sect; 3&ordm; - Dissolvida a sociedade comercial por morte de um dos s&oacute;cios, o s&oacute;cio sobrevivente fica sub-rogado no direito a renova&ccedil;&atilde;o, desde que continue no mesmo ramo.<br />
			&sect; 4&ordm; - O direito a renova&ccedil;&atilde;o do contrato estende-se &agrave;s loca&ccedil;&otilde;es celebradas por ind&uacute;strias e sociedades civis com fim lucrativo, regularmente constitu&iacute;das, desde que ocorrentes os pressupostos previstos neste artigo.<br />
			&sect; 5&ordm; - Do direito a renova&ccedil;&atilde;o decai aquele que n&atilde;o propuser a a&ccedil;&atilde;o no interregno de um ano, no m&aacute;ximo, at&eacute; seis meses, no m&iacute;nimo, anteriores &agrave; data da finaliza&ccedil;&atilde;o do prazo do contrato em vigor.</p>
			<p>Art. 52 - O locador n&atilde;o estar&aacute; obrigado a renovar o contrato se:<br />
			I - por determina&ccedil;&atilde;o do Poder P&uacute;blico, tiver que realizar no im&oacute;vel obras que importarem na sua radical transforma&ccedil;&atilde;o; ou para fazer modifica&ccedil;&atilde;o de tal natureza que aumente o valor do neg&oacute;cio ou da propriedade;<br />
			II - o im&oacute;vel vier a ser utilizado por ele pr&oacute;prio ou para transfer&ecirc;ncia de fundo de com&eacute;rcio existente h&aacute; mais de um ano, sendo detentor da maioria do capital o locador, seu c&ocirc;njuge, ascendente ou descendente.<br />
			&sect; 1&ordm; - Na hip&oacute;tese do inciso II, o im&oacute;vel n&atilde;o poder&aacute; ser destinado ao uso do mesmo ramo do locat&aacute;rio, salvo se a loca&ccedil;&atilde;o tamb&eacute;m envolvia o fundo de com&eacute;rcio, com as instala&ccedil;&otilde;es e pertences.<br />
			&sect; 2&ordm; - Nas loca&ccedil;&otilde;es de espa&ccedil;os em &quot;shopping centers&quot;, o locador n&atilde;o poder&aacute; recusar a renova&ccedil;&atilde;o do contrato com fundamento no inciso II deste artigo.<br />
			&sect; 3&ordm; - O locat&aacute;rio ter&aacute; direito a indeniza&ccedil;&atilde;o para ressarcimento dos preju&iacute;zos e dos lucros cessantes que tiver que arcar com a mudan&ccedil;a, perda do lugar e desvaloriza&ccedil;&atilde;o do fundo de com&eacute;rcio, se a renova&ccedil;&atilde;o n&atilde;o ocorrer em raz&atilde;o de proposta de terceiro, em melhores condi&ccedil;&otilde;es, ou se o locador, no prazo de tr&ecirc;s meses da entrega do im&oacute;vel, n&atilde;o der o destino alegado ou n&atilde;o iniciar as obras determinadas pelo Poder P&uacute;blico ou que declarou pretender realizar.</p>
			<p>Art. 53 - Nas loca&ccedil;&otilde;es de im&oacute;veis utilizados por hospitais, unidades sanit&aacute;rias oficiais, asilos, estabelecimentos de sa&uacute;de e de ensino autorizados e fiscalizados pelo Poder P&uacute;blico, bem como por entidades religiosas devidamente registradas, o contrato somente poder&aacute; ser rescindido:<br />
			I - nas hip&oacute;teses do Art. 9;<br />
			II - se o propriet&aacute;rio, promiss&aacute;rio-comprador ou promiss&aacute;rio cession&aacute;rio, em car&aacute;ter irrevog&aacute;vel e imitido na posse, com t&iacute;tulo registrado, que haja quitado o pre&ccedil;o da promessa ou que, n&atilde;o o tendo feito, seja autorizado pelo propriet&aacute;rio, pedir o im&oacute;vel para demoli&ccedil;&atilde;o, edifica&ccedil;&atilde;o licenciada ou reforma que venha a resultar em aumento m&iacute;nimo de cinq&uuml;enta por cento da &aacute;rea &uacute;til.</p>
			<p>Art. 54 - Nas rela&ccedil;&otilde;es entre lojistas e empreendedores de &quot;shopping center&quot;, prevalecer&atilde;o as condi&ccedil;&otilde;es livremente pactuadas nos contratos de loca&ccedil;&atilde;o respectivos e as disposi&ccedil;&otilde;es procedimentais previstas nesta Lei.<br />
			&sect; 1&ordm; - O empreendedor n&atilde;o poder&aacute; cobrar do locat&aacute;rio em &quot;shopping center&quot;:<br />
			a) as despesas referidas nas al&iacute;neas &quot;a&quot;, &quot;b&quot; e &quot;d&quot; do par&aacute;grafo &uacute;nico do Art. 22; e<br />
			b) - as despesas com obras ou substitui&ccedil;&otilde;es de equipamentos, que impliquem modificar o projeto ou o memorial descritivo da data do habite-se e obras de paisagismo nas partes de uso comum.<br />
			&sect; 2&ordm; - As despesas cobradas do locat&aacute;rio devem ser previstas em or&ccedil;amento, salvo casos de urg&ecirc;ncia ou for&ccedil;a maior, devidamente demonstradas, podendo o locat&aacute;rio, a cada sessenta dias, por si ou entidade de classe exigir a comprova&ccedil;&atilde;o das mesmas.</p>
			<p>Art. 55 - Considera-se loca&ccedil;&atilde;o n&atilde;o residencial quando o locat&aacute;rio for pessoa jur&iacute;dica e o im&oacute;vel destinar-se ao uso de seus titulares, diretores, s&oacute;cios, gerentes, executivos ou empregados.</p>
			<p>Art. 56 - Nos demais casos de loca&ccedil;&atilde;o n&atilde;o residencial, o contrato por prazo determinado cessa, de pleno direito, findo o prazo estipulado, independentemente de notifica&ccedil;&atilde;o ou aviso.<br />
			Par&aacute;grafo &uacute;nico. Findo o prazo estipulado, se o locat&aacute;rio permanecer no im&oacute;vel por mais de trinta dias sem oposi&ccedil;&atilde;o do locador, presumir-se-&aacute; prorrogada a loca&ccedil;&atilde;o nas condi&ccedil;&otilde;es ajustadas, mas sem prazo determinado.</p>
			<p>Art. 57 - O contrato de loca&ccedil;&atilde;o por prazo indeterminado pode ser denunciado por escrito, pelo locador, concedidos ao locat&aacute;rio trinta dias para a desocupa&ccedil;&atilde;o.<br />
			</p>
			
			
			<p class="center titulo"><strong>T&iacute;tulo II</strong><br />Dos Procedimentos</p>
			
			<p class="center capitulo"><strong>Cap&iacute;tulo I</strong> - Das Disposi&ccedil;&otilde;es Gerais</p>
						
			<p>Art. 58 - Ressalvados os casos previstos no par&aacute;grafo &uacute;nico do Art. 1, nas a&ccedil;&otilde;es de despejo, consigna&ccedil;&atilde;o em pagamento de aluguel e acess&oacute;rio da loca&ccedil;&atilde;o, revisionais de aluguel e renovat&oacute;rias de loca&ccedil;&atilde;o, observar-se-&aacute; o seguinte:<br />
			I - os processos tramitam durante as f&eacute;rias forenses e n&atilde;o se suspendem pela superveni&ecirc;ncia delas;<br />
			II - &eacute; competente para conhecer e julgar tais a&ccedil;&otilde;es o foro do lugar da situa&ccedil;&atilde;o do im&oacute;vel, salvo se outro houver sido eleito no contrato;<br />
			III - o valor da causa corresponder&aacute; a doze meses de aluguel, ou, na hip&oacute;tese do inciso II do Art. 47, a tr&ecirc;s sal&aacute;rios vigentes por ocasi&atilde;o do ajuizamento;<br />
			IV - desde que autorizado no contrato, a cita&ccedil;&atilde;o, intima&ccedil;&atilde;o ou notifica&ccedil;&atilde;o far-se-&aacute; mediante correspond&ecirc;ncia com aviso de recebimento, ou, tratando-se de pessoa jur&iacute;dica ou firma individual, tamb&eacute;m mediante telex ou fac-s&iacute;mile, ou, ainda, sendo necess&aacute;rio, pelas demais formas previstas no C&oacute;digo de Processo Civil;<br />
			V - os recursos interpostos contra as senten&ccedil;as ter&atilde;o efeito somente devolutivo.<br />
			</p>
			
			<p class="center capitulo"><strong>Cap&iacute;tulo II</strong> - Das A&ccedil;&otilde;es de Despejo</p>
			
			<p>Art. 59 - Com as modifica&ccedil;&otilde;es constantes deste Cap&iacute;tulo, as a&ccedil;&otilde;es de despejo ter&atilde;o o rito ordin&aacute;rio.<br />
			&sect; 1&ordm; - Conceder-se-&aacute; liminar para desocupa&ccedil;&atilde;o em quinze dias, independentemente da audi&ecirc;ncia da parte contr&aacute;ria e desde que prestada a cau&ccedil;&atilde;o no valor equivalente a tr&ecirc;s meses de aluguel, nas a&ccedil;&otilde;es que tiverem por fundamento exclusivo:<br />
			I - o descumprimento do m&uacute;tuo acordo (Art. 9, inciso I), celebrado por escrito e assinado pelas partes e por duas testemunhas, no qual tenha sido ajustado o prazo m&iacute;nimo de seis meses para desocupa&ccedil;&atilde;o, contada da assinatura do instrumento;<br />
			II - o disposto no inciso II do Art. 47, havendo prova escrita da rescis&atilde;o do contrato de trabalho ou sendo ela demonstrada em audi&ecirc;ncia pr&eacute;via;<br />
			III - o t&eacute;rmino do prazo da loca&ccedil;&atilde;o para temporada, tendo sido proposta a a&ccedil;&atilde;o de despejo em at&eacute; trinta dias ap&oacute;s o vencimento do contrato;<br />
			IV - a morte do locat&aacute;rio sem deixar sucessor leg&iacute;timo na loca&ccedil;&atilde;o, de acordo com o referido no inciso I do Art. 11, permanecendo no im&oacute;vel pessoas n&atilde;o autorizadas por lei;<br />
			V - a perman&ecirc;ncia do sublocat&aacute;rio no im&oacute;vel, extinta a loca&ccedil;&atilde;o, celebrada com o locat&aacute;rio.<br />
			&sect; 2&ordm; - Qualquer que seja o fundamento de a&ccedil;&atilde;o far-se-&aacute; ci&ecirc;ncia do pedido aos sublocat&aacute;rios, que poder&atilde;o intervir no processo como assistentes.</p>
			<p>Art. 60 - Nas a&ccedil;&otilde;es de despejo fundadas no inciso IV do Art. 9, inciso IV do Art. 47 e inciso II do Art. 53, a peti&ccedil;&atilde;o inicial dever&aacute; ser instru&iacute;da com prova de propriedade do im&oacute;vel ou do compromisso registrado.</p>
			<p>Art. 61 - Nas a&ccedil;&otilde;es fundadas no &sect; 2&ordm; do Art. 46 e nos incisos III e IV do Art. 47, se o locat&aacute;rio, no prazo da contesta&ccedil;&atilde;o, manifestar sua concord&acirc;ncia com a desocupa&ccedil;&atilde;o do im&oacute;vel, o juiz acolher&aacute; o pedido fixando prazo de seis meses para a desocupa&ccedil;&atilde;o, contados da cita&ccedil;&atilde;o, impondo ao vencido a responsabilidade pelas custas e honor&aacute;rios advocat&iacute;cios de vinte por cento sobre o valor dado &agrave; causa. Se a desocupa&ccedil;&atilde;o ocorrer dentro do prazo fixado, o r&eacute;u ficar&aacute; isento dessa responsabilidade; caso contr&aacute;rio, ser&aacute; expedido mandado de despejo.</p>
			<p>Art. 62 - Nas a&ccedil;&otilde;es de despejo fundadas na falta de pagamento de aluguel e acess&oacute;rios da loca&ccedil;&atilde;o, observar-se-&aacute; o seguinte:<br />
			I - o pedido de rescis&atilde;o da loca&ccedil;&atilde;o poder&aacute; ser cumulado com o de cobran&ccedil;a dos alugu&eacute;is e acess&oacute;rios da loca&ccedil;&atilde;o, devendo ser apresentado, com a inicial, c&aacute;lculo discriminado do valor do d&eacute;bito;<br />
			II - o locat&aacute;rio poder&aacute; evitar a rescis&atilde;o da loca&ccedil;&atilde;o requerendo, no prazo da contesta&ccedil;&atilde;o, autoriza&ccedil;&atilde;o para o pagamento do d&eacute;bito atualizado, independentemente de c&aacute;lculo e mediante dep&oacute;sito judicial, inclu&iacute;dos:<br />
			a) os alugu&eacute;is e acess&oacute;rios da loca&ccedil;&atilde;o que vencerem at&eacute; a sua efetiva&ccedil;&atilde;o;<br />
			b) as multas ou penalidades contratuais, quando exig&iacute;veis;<br />
			c) os juros de mora;<br />
			d) as custas e os honor&aacute;rios do advogado do locador, fixadas em dez por cento sobre o montante devido, se do contrato n&atilde;o constar disposi&ccedil;&atilde;o diversa.<br />
			III - autorizada a emenda da mora e efetuado o dep&oacute;sito judicial at&eacute; quinze dias ap&oacute;s a intima&ccedil;&atilde;o do deferimento, se o locador alegar que a oferta n&atilde;o &eacute; integral, justificando a diferen&ccedil;a, o locat&aacute;rio poder&aacute; complementar o dep&oacute;sito no prazo de dez dias, contados da ci&ecirc;ncia dessa manifesta&ccedil;&atilde;o;<br />
			IV - n&atilde;o sendo complementado o dep&oacute;sito, o pedido de rescis&atilde;o prosseguir&aacute; pela diferen&ccedil;a, podendo o locador levantar a quantia depositada;<br />
			V - os alugu&eacute;is que forem vencendo at&eacute; a senten&ccedil;a dever&atilde;o ser depositados &agrave; disposi&ccedil;&atilde;o do ju&iacute;zo, nos respectivos vencimentos, podendo o locador levant&aacute;-los desde que incontroversos;<br />
			VI - havendo cumula&ccedil;&atilde;o dos pedidos de rescis&atilde;o da loca&ccedil;&atilde;o e cobran&ccedil;a dos alugu&eacute;is, a execu&ccedil;&atilde;o desta pode ter in&iacute;cio antes da desocupa&ccedil;&atilde;o do im&oacute;vel, caso ambos tenham sido acolhidos.<br />
			Par&aacute;grafo &uacute;nico. N&atilde;o se admitir&aacute; a emenda da mora se o locat&aacute;rio j&aacute; houver utilizado essa faculdade por duas vezes nos doze meses imediatamente anteriores &agrave; propositura da a&ccedil;&atilde;o.</p>
			<p>Art. 63 - Julgada procedente a a&ccedil;&atilde;o de despejo, o juiz fixar&aacute; prazo de trinta dias para a desocupa&ccedil;&atilde;o volunt&aacute;ria, ressalvado o disposto nos par&aacute;grafos seguintes.<br />
			&sect; 1&ordm; - O prazo ser&aacute; de quinze dias se:<br />
			a) entre a cita&ccedil;&atilde;o e a senten&ccedil;a de primeira inst&acirc;ncia houverem decorrido mais de quatro meses; ou b) o despejo houver sido decretado com fundamento nos incisos II e III do Art. 9&ordm; ou no &sect; 2&ordm; do Art. 46.<br />
			&sect; 2&ordm; - Tratando-se de estabelecimento de ensino autorizado e fiscalizado pelo Poder P&uacute;blico, respeitado o prazo m&iacute;nimo de seis meses e o m&aacute;ximo de um ano, o juiz dispor&aacute;, de modo que a desocupa&ccedil;&atilde;o coincida com o per&iacute;odo de f&eacute;rias escolares.<br />
			&sect; 3&ordm; - Tratando-se de hospitais, reparti&ccedil;&otilde;es p&uacute;blicas, unidades sanit&aacute;rias oficiais, asilos e estabelecimentos de sa&uacute;de e de ensino autorizados e fiscalizados pelo Poder P&uacute;blico, bem como por entidades religiosas devidamente registradas, e o despejo for decretado com fundamento no inciso IV do Art. 9&ordm; ou no inciso II do Art. 53, o prazo ser&aacute; de um ano, exceto no caso em que entre a cita&ccedil;&atilde;o e a senten&ccedil;a de primeira inst&acirc;ncia houver decorrido mais de um ano, hip&oacute;tese em que o prazo ser&aacute; de seis meses.<br />
			&sect; 4&ordm; - A senten&ccedil;a que decretar o despejo fixar&aacute; o valor da cau&ccedil;&atilde;o para o caso de ser executada provisoriamente.</p>
			<p>Art. 64 - Salvo nas hip&oacute;teses das a&ccedil;&otilde;es fundadas nos incisos I, II e IV do Art. 9, a execu&ccedil;&atilde;o provis&oacute;ria do despejo depender&aacute; de cau&ccedil;&atilde;o n&atilde;o inferior a doze meses e nem superior a dezoito meses do aluguel, atualizado at&eacute; a data do dep&oacute;sito da cau&ccedil;&atilde;o.<br />
			&sect; 1&ordm; - A cau&ccedil;&atilde;o poder&aacute; ser real ou fidejuss&oacute;ria e ser&aacute; prestada nos autos da execu&ccedil;&atilde;o provis&oacute;ria.<br />
			&sect; 2&ordm; - Ocorrendo a reforma da senten&ccedil;a ou da decis&atilde;o que concedeu liminarmente o despejo, o valor da cau&ccedil;&atilde;o reverter&aacute; em favor do r&eacute;u, como indeniza&ccedil;&atilde;o m&iacute;nima das perdas e danos, podendo este reclamar, em a&ccedil;&atilde;o pr&oacute;pria, a diferen&ccedil;a pelo que a exceder.</p>
			<p>Art. 65 - Findo o prazo assinado para a desocupa&ccedil;&atilde;o, contado da data da notifica&ccedil;&atilde;o, ser&aacute; efetuado o despejo, se necess&aacute;rio com emprego de for&ccedil;a, inclusive arrombamento.<br />
			&sect; 1&ordm; - Os m&oacute;veis e utens&iacute;lios ser&atilde;o entregues &agrave; guarda de deposit&aacute;rios, se n&atilde;o os quiser retirar o despejado.<br />
			&sect; 2&ordm; - O despejo n&atilde;o poder&aacute; ser executado at&eacute; o trig&eacute;simo dia seguinte ao do falecimento do c&ocirc;njuge, ascendente, descendente ou irm&atilde;o de qualquer das pessoas que habitem o im&oacute;vel.</p>
			<p>Art. 66 - Quando o im&oacute;vel for abandonado ap&oacute;s ajuizada a a&ccedil;&atilde;o, o locador poder&aacute; imitir-se na posse do im&oacute;vel.<br />
			</p>
			
			
			<p class="center capitulo"><strong>Cap&iacute;tulo III</strong> - Da A&ccedil;&atilde;o de Consigna&ccedil;&atilde;o de Aluguel e Acess&oacute;rios da Loca&ccedil;&atilde;o</p>
			
			<p>Art. 67 - Na a&ccedil;&atilde;o que objetivar o pagamento dos alugu&eacute;is e acess&oacute;rios da loca&ccedil;&atilde;o mediante consigna&ccedil;&atilde;o, ser&aacute; observado o seguinte:<br />
			I - a peti&ccedil;&atilde;o inicial, al&eacute;m dos requisitos exigidos pelo Art. 282 do C&oacute;digo de Processo Civil, dever&aacute; especificar os alugu&eacute;is e acess&oacute;rios da loca&ccedil;&atilde;o com indica&ccedil;&atilde;o dos respectivos valores;<br />
			II - determinada a cita&ccedil;&atilde;o do r&eacute;u, o autor ser&aacute; intimado a, no prazo de vinte e quatro horas, efetuar o dep&oacute;sito judicial da import&acirc;ncia indicada na peti&ccedil;&atilde;o inicial, sob pena deve ser extinto o processo;<br />
			III - o pedido envolver&aacute; a quita&ccedil;&atilde;o das obriga&ccedil;&otilde;es que vencerem durante a tramita&ccedil;&atilde;o do feito e at&eacute; ser prolatada a senten&ccedil;a de primeira inst&acirc;ncia, devendo o autor promover os dep&oacute;sitos nos respectivos vencimentos;<br />
			IV - n&atilde;o sendo oferecida a contesta&ccedil;&atilde;o, ou se o locador receber os valores depositados, o juiz acolher&aacute; o pedido, declarando quitadas as obriga&ccedil;&otilde;es, condenando o r&eacute;u ao pagamento das custas e honor&aacute;rios de vinte por cento do valor dos dep&oacute;sitos;<br />
			V - a contesta&ccedil;&atilde;o do locador, al&eacute;m da defesa de direito que possa caber, ficar&aacute; adstrita, quanto &agrave; mat&eacute;ria de fato, a:<br />
			a) n&atilde;o ter havido recusa ou mora em receber a quantia devida;<br />
			b) ter sido justa a recusa;<br />
			c) n&atilde;o ter sido efetuado o dep&oacute;sito no prazo ou no lugar do pagamento;<br />
			d) n&atilde;o ter sido o dep&oacute;sito integral.<br />
			VI - al&eacute;m de contestar, o r&eacute;u poder&aacute;, em reconven&ccedil;&atilde;o, pedir o despejo e a cobran&ccedil;a dos valores objeto da consignat&oacute;ria ou da diferen&ccedil;a do dep&oacute;sito inicial, na hip&oacute;tese de ter sido alegado n&atilde;o ser o mesmo integral;<br />
			VII - o autor poder&aacute; complementar o dep&oacute;sito inicial, no prazo de cinco dias contados da ci&ecirc;ncia do oferecimento da resposta, com acr&eacute;scimo de dez por cento sobre o valor da diferen&ccedil;a. Se tal ocorrer, o juiz declarar&aacute; quitadas as obriga&ccedil;&otilde;es, elidindo a rescis&atilde;o da loca&ccedil;&atilde;o, mas impor&aacute; ao autor-reconvindo a responsabilidade pelas custas e honor&aacute;rios advocat&iacute;cios de vinte por cento sobre o valor dos dep&oacute;sitos;<br />
			VIII - havendo, na reconven&ccedil;&atilde;o, cumula&ccedil;&atilde;o dos pedidos de rescis&atilde;o da loca&ccedil;&atilde;o e cobran&ccedil;a dos valores objeto da consignat&oacute;ria, a execu&ccedil;&atilde;o desta somente poder&aacute; ter in&iacute;cio ap&oacute;s obtida a desocupa&ccedil;&atilde;o do im&oacute;vel, caso ambos tenham sido acolhidos.<br />
			Par&aacute;grafo &uacute;nico. O r&eacute;u poder&aacute; levantar a qualquer momento as import&acirc;ncias depositadas sobre as quais n&atilde;o penda controv&eacute;rsia.<br />
			</p>
			
			<p class="center capitulo"><strong>Cap&iacute;tulo IV</strong> - Da A&ccedil;&atilde;o Revisional de Aluguel</p>
			
			<p>Art. 68 - Na a&ccedil;&atilde;o revisional de aluguel, que ter&aacute; o rito sumar&iacute;ssimo, observar-se-&aacute; o seguinte:<br />
			I - al&eacute;m dos requisitos exigidos pelos artigos 276 e 282 do C&oacute;digo de Processo Civil, a peti&ccedil;&atilde;o inicial dever&aacute; indicar o valor do aluguel cuja fixa&ccedil;&atilde;o &eacute; pretendida;<br />
			II - ao designar a audi&ecirc;ncia de instru&ccedil;&atilde;o e julgamento, o juiz, se houver pedido e com base nos elementos fornecidos pelo autor ou nos que indicar, fixar&aacute; o aluguel provis&oacute;rio, n&atilde;o excedente a oitenta por cento do pedido, que ser&aacute; devido desde a cita&ccedil;&atilde;o;<br />
			III - sem preju&iacute;zo da contesta&ccedil;&atilde;o e at&eacute; a audi&ecirc;ncia, o r&eacute;u poder&aacute; pedir seja revisto o aluguel provis&oacute;rio, fornecendo os elementos para tanto;<br />
			IV - na audi&ecirc;ncia de instru&ccedil;&atilde;o e julgamento, apresentada a contesta&ccedil;&atilde;o, que dever&aacute; conter contraproposta se houver discord&acirc;ncia quanto ao valor pretendido, o juiz tentar&aacute; a concilia&ccedil;&atilde;o e, n&atilde;o sendo esta poss&iacute;vel, suspender&aacute; o ato para a realiza&ccedil;&atilde;o de per&iacute;cia, se necess&aacute;ria, designando, desde logo, audi&ecirc;ncia em continua&ccedil;&atilde;o.<br />
			&sect; 1&ordm; - N&atilde;o caber&aacute; a&ccedil;&atilde;o revisional na pend&ecirc;ncia de prazo para desocupa&ccedil;&atilde;o do im&oacute;vel (artigos 46, &sect; 2&ordm; e 57), ou quando tenha sido este estipulado amig&aacute;vel ou judicialmente.<br />
			&sect; 2&ordm; - No curso da a&ccedil;&atilde;o de revis&atilde;o, o aluguel provis&oacute;rio ser&aacute; reajustado na periodicidade pactuada ou na fixada em lei.</p>
			<p>Art. 69 - O aluguel fixado na senten&ccedil;a retroage &agrave; cita&ccedil;&atilde;o, e as diferen&ccedil;as devidas durante a a&ccedil;&atilde;o de revis&atilde;o, descontados os alugueres provis&oacute;rios satisfeitos, ser&atilde;o pagas corrigidas, exig&iacute;veis a partir do tr&acirc;nsito em julgado da decis&atilde;o que fixar o novo aluguel.<br />
			&sect; 1&ordm; - Se pedido pelo locador, ou sublocador, a senten&ccedil;a poder&aacute; estabelecer periodicidade de reajustamento do aluguel diversa daquela prevista no contrato revisando, bem como adotar outro indexador para reajustamento do aluguel.<br />
			&sect; 2&ordm; - A execu&ccedil;&atilde;o das diferen&ccedil;as ser&aacute; feita nos autos da a&ccedil;&atilde;o de revis&atilde;o.</p>
			<p>Art. 70 - Na a&ccedil;&atilde;o de revis&atilde;o do aluguel, o juiz poder&aacute; homologar acordo de desocupa&ccedil;&atilde;o, que ser&aacute; executado mediante a expedi&ccedil;&atilde;o de mandado de despejo.<br />
			</p>
			<p class="center capitulo"><strong>Cap&iacute;tulo V</strong> - Da A&ccedil;&atilde;o Renovat&oacute;ria</p>
			<p>Art. 71 - Al&eacute;m dos demais requisitos exigidos no Art. 282 do C&oacute;digo de Processo Civil, a peti&ccedil;&atilde;o inicial da a&ccedil;&atilde;o renovat&oacute;ria dever&aacute; ser instru&iacute;da com:<br />
			I - prova do preenchimento dos requisitos dos incisos I, II e III do Art. 51;<br />
			II - prova do exato cumprimento do contrato em curso;<br />
			III - prova da quita&ccedil;&atilde;o dos impostos e taxas que incidiram sobre o im&oacute;vel cujo pagamento lhe incumbia;<br />
			IV - indica&ccedil;&atilde;o clara e precisa das condi&ccedil;&otilde;es oferecidas para a renova&ccedil;&atilde;o da loca&ccedil;&atilde;o;<br />
			V - indica&ccedil;&atilde;o de fiador quando houver no contrato a renovar e, quando n&atilde;o for o mesmo, com indica&ccedil;&atilde;o do nome ou denomina&ccedil;&atilde;o completa, n&uacute;mero de sua inscri&ccedil;&atilde;o no Minist&eacute;rio da Economia, Fazenda e Planejamento, endere&ccedil;o e, tratando-se de pessoa natural, a nacionalidade, o estado civil, a profiss&atilde;o e o n&uacute;mero da carteira de identidade, comprovando, em qualquer caso e desde logo, a idoneidade financeira;<br />
			VI - prova de que o fiador do contrato ou o que o substituir na renova&ccedil;&atilde;o aceita os encargos da fian&ccedil;a, autorizado por seu c&ocirc;njuge, se casado for;<br />
			VII - prova, quando for o caso, de ser cession&aacute;rio ou sucessor, em virtude de t&iacute;tulo opon&iacute;vel ao propriet&aacute;rio.<br />
			Par&aacute;grafo &uacute;nico. Proposta a a&ccedil;&atilde;o pelo sublocat&aacute;rio do im&oacute;vel ou de parte dele, ser&atilde;o citados o sublocador e o locador, como litisconsortes, salvo se, em virtude de loca&ccedil;&atilde;o origin&aacute;ria ou renovada, o sublocador dispuser de prazo que admita renovar a subloca&ccedil;&atilde;o; na primeira hip&oacute;tese, procedente a a&ccedil;&atilde;o, o propriet&aacute;rio ficar&aacute; diretamente obrigado &agrave; renova&ccedil;&atilde;o.</p>
			<p>Art. 72 - A contesta&ccedil;&atilde;o do locador, al&eacute;m da defesa de direito que possa caber, ficar&aacute; adstrita, quanto a mat&eacute;ria de fato, ao seguinte:<br />
			I - n&atilde;o preencher o autor os requisitos estabelecidos nesta Lei;<br />
			II - n&atilde;o atender, a proposta do locat&aacute;rio, o valor locativo real do im&oacute;vel na &eacute;poca da renova&ccedil;&atilde;o, exclu&iacute;da a valoriza&ccedil;&atilde;o trazida por aquele ao ponto ou lugar;<br />
			III - ter proposta de terceiro para a loca&ccedil;&atilde;o, em condi&ccedil;&otilde;es melhores;<br />
			IV - n&atilde;o estar obrigado a renovar a loca&ccedil;&atilde;o (incisos I e II do Art. 52).<br />
			&sect; 1&ordm; - No caso do inciso II, o locador dever&aacute; apresentar, em contraproposta, as condi&ccedil;&otilde;es de loca&ccedil;&atilde;o que repute compat&iacute;veis com o valor locativo real e atual do im&oacute;vel.<br />
			&sect; 2&ordm; - No caso do inciso III, o locador dever&aacute; juntar prova documental da proposta do terceiro, subscrita por este e por duas testemunhas, com clara indica&ccedil;&atilde;o do ramo a ser explorado, que n&atilde;o poder&aacute; ser o mesmo do locat&aacute;rio. Nessa hip&oacute;tese, o locat&aacute;rio poder&aacute;, em r&eacute;plica, aceitar tais condi&ccedil;&otilde;es para obter a renova&ccedil;&atilde;o pretendida.<br />
			&sect; 3&ordm; - No caso do inciso I do Art. 52, a contesta&ccedil;&atilde;o dever&aacute; trazer prova da determina&ccedil;&atilde;o do Poder P&uacute;blico ou relat&oacute;rio pormenorizado das obras a serem realizadas e da estimativa de valoriza&ccedil;&atilde;o que sofrer&aacute; o im&oacute;vel, assinado por engenheiro devidamente habilitado.<br />
			&sect; 4&ordm; - Na contesta&ccedil;&atilde;o, o locador, ou sublocador, poder&aacute; pedir, ainda, a fixa&ccedil;&atilde;o de aluguel provis&oacute;rio, para vigorar a partir do primeiro m&ecirc;s do prazo do contrato a ser renovado, n&atilde;o excedente a oitenta por cento do pedido, desde que apresentados elementos h&aacute;beis para aferi&ccedil;&atilde;o do justo valor do aluguel.<br />
			&sect; 5&ordm; - Se pedido pelo locador, ou sublocador, a senten&ccedil;a poder&aacute; estabelecer periodicidade de reajustamento do aluguel diversa daquela prevista no contrato renovando, bem como adotar outro indexador para reajustamento do aluguel.</p>
			<p>Art. 73 - Renovada a loca&ccedil;&atilde;o, as diferen&ccedil;as dos alugu&eacute;is vencidos ser&atilde;o executadas nos pr&oacute;prios autos da a&ccedil;&atilde;o e pagas de uma s&oacute; vez.</p>
			<p>Art. 74 - N&atilde;o sendo renovada a loca&ccedil;&atilde;o, o Juiz fixar&aacute; o prazo de at&eacute; seis meses ap&oacute;s o tr&acirc;nsito em julgado da senten&ccedil;a para desocupa&ccedil;&atilde;o, se houver pedido na contesta&ccedil;&atilde;o.</p>
			<p>Art. 75 - Na hip&oacute;tese do inciso III do Art. 72, a senten&ccedil;a fixar&aacute; desde logo a indeniza&ccedil;&atilde;o devida ao locat&aacute;rio em conseq&uuml;&ecirc;ncia da n&atilde;o prorroga&ccedil;&atilde;o da loca&ccedil;&atilde;o, solidariamente devida pelo locador e o proponente.<br />
			</p>
			
			
			<p class="center titulo"><strong>T&iacute;tulo III</strong><br />Das Disposi&ccedil;&otilde;es Finais e Transit&oacute;rias</p>
			
			<p>Art. 76 - N&atilde;o se aplicam as disposi&ccedil;&otilde;es desta Lei aos processos em curso.</p>
			<p>Art. 77 - Todas as loca&ccedil;&otilde;es residenciais que tenham sido celebradas anteriormente &agrave; vig&ecirc;ncia desta Lei ser&atilde;o automaticamente prorrogadas por tempo indeterminado, ao t&eacute;rmino do prazo ajustado no contrato.</p>
			<p>Art. 78 - As loca&ccedil;&otilde;es residenciais que tenham sido celebrados anteriormente &agrave; vig&ecirc;ncia desta Lei e que j&aacute; vigorem ou venham a vigorar por prazo indeterminado, poder&atilde;o ser denunciadas pelo locador, concedido o prazo de doze meses para a desocupa&ccedil;&atilde;o.<br />
			Par&aacute;grafo &uacute;nico. Na hip&oacute;tese de ter havido revis&atilde;o judicial ou amig&aacute;vel do aluguel, atingindo o pre&ccedil;o do mercado, a den&uacute;ncia somente poder&aacute; ser exercitada ap&oacute;s vinte e quatro meses da data da revis&atilde;o, se esta ocorreu nos doze meses anteriores &agrave; data da vig&ecirc;ncia desta Lei.</p>
			<p>Art. 79 - No que for omissa esta Lei aplicam-se as normas do C&oacute;digo Civil e do C&oacute;digo de Processo Civil.</p>
			<p>Art. 80 - Para os fins do inciso I do Art. 98 da Constitui&ccedil;&atilde;o Federal, as a&ccedil;&otilde;es de despejo poder&atilde;o ser consideradas como causas c&iacute;veis de menor complexidade.</p>
			<p>Art. 81 - O inciso II do Art. 167 e o Art.169 da Lei n&uacute;mero 6.015, de 31 de dezembro de 1973, passam a vigorar com as seguintes altera&ccedil;&otilde;es:</p>
			<p>Art. 82 - O Art. 3&ordm; da Lei n&uacute;mero 8.009, de 29 de mar&ccedil;o de 1990, passa a vigorar acrescido do seguinte inciso VII:</p>
			<p>Art. 83 - Ao Art. 24 da Lei n&uacute;mero 4.591, de 16 de dezembro de 1964, fica acrescido o seguinte &sect; 4:<br />
			&quot;Art. 24 ..........................................<br />
			&sect; 4&ordm; - Nas decis&otilde;es da Assembl&eacute;ia que envolvam despesas ordin&aacute;rias do condom&iacute;nio, o locat&aacute;rio poder&aacute; votar, caso o cond&ocirc;mino-locador a ela n&atilde;o compare&ccedil;a&quot;.</p>
			<p>Art. 84 - Reputam-se v&aacute;lidos os registros dos contratos de loca&ccedil;&atilde;o dos im&oacute;veis, realizados at&eacute; a data da vig&ecirc;ncia desta Lei.</p>
			<p>Art. 85 - Nas loca&ccedil;&otilde;es residenciais, &eacute; livre a conven&ccedil;&atilde;o do aluguel quanto a pre&ccedil;o, periodicidade e indexador de reajustamento, vedada a vincula&ccedil;&atilde;o &agrave; varia&ccedil;&atilde;o do sal&aacute;rio m&iacute;nimo, varia&ccedil;&atilde;o cambial e moeda estrangeira:<br />
			I - dos im&oacute;veis novos com habite-se concedido a partir da entrada em vigor desta Lei;<br />
			II - dos demais im&oacute;veis n&atilde;o enquadrados no inciso anterior, em rela&ccedil;&atilde;o aos contratos celebrados, ap&oacute;s cinco anos de entrada em vigor desta Lei.</p>
			<p>Art. 86 - O Art. 8&ordm; da Lei n&uacute;mero 4.380, de 21 de agosto de 1964 passa a vigorar com a seguinte reda&ccedil;&atilde;o:<br />
			&quot;Art. 8&ordm; - O sistema financeiro da habita&ccedil;&atilde;o, destinado a facilitar e promover a constru&ccedil;&atilde;o e a aquisi&ccedil;&atilde;o da casa pr&oacute;pria ou moradia, especialmente pelas classes de menor renda da popula&ccedil;&atilde;o, ser&aacute; integrado&quot;.</p>
			<p>Art. 87 - (Vetado).</p>
			<p>Art. 88 - (Vetado).</p>
			<p>Art. 89 - Esta Lei entrar&aacute; em vigor sessenta dias ap&oacute;s seu publica&ccedil;&atilde;o.</p>
			<p>Art. 90 - Revogam-se as disposi&ccedil;&otilde;es em contr&aacute;rio, especialmente:<br />
			I - o Decreto n&uacute;mero 24.150, de 20 de abril de 1934;<br />
			II - a Lei n&uacute;mero 6.239, de 19 de setembro de 1975;<br />
			III - a Lei n&uacute;mero 6.649, de 16 de maio de 1979;<br />
			IV - a Lei n&uacute;mero 6.698, de 15 de outubro de 1979;<br />
			V - a Lei n&uacute;mero 7.355, de 31 de agosto de 1985;<br />
			VI - a Lei n&uacute;mero 7.538, de 24 de setembro de 1986;<br />
			VII - a Lei n&uacute;mero 7.612, de 9 de julho de 1987; e<br />
			VIII - a Lei n&uacute;mero 8.157, de 3 de janeiro de 1991.</p>
			
			<p>DOU 21/10/1991</p>
			
			<p><strong>LEI N&deg; 12.112, DE 09 DE DEZEMBRO DE 2009.</strong></p>
			
			<p class="bordas"><strong>Altera a Lei no 8.245, de 18 de outubro de 1991, para aperfei&ccedil;oar as regras e procedimentos sobre loca&ccedil;&atilde;o de im&oacute;vel urbano.<br/><br/>
			O PRESIDENTE DA REP&Uacute;BLICA Fa&ccedil;o saber que o Congresso Nacional decreta e eu sanciono a seguinte Lei:</strong><br />
			</p>
			<p>Art. 1&deg; Esta Lei introduz altera&ccedil;&atilde;o na Lei no 8.245, de 18 de outubro de 1991, que disp&otilde;e sobre as loca&ccedil;&otilde;es de im&oacute;veis urbanos.</p>
			<p>Art. 2&deg; A Lei no 8.245, de 18 de outubro de 1991, passa a vigorar com as seguintes altera&ccedil;&otilde;es:</p>
			<p>&ldquo;Art. 4&deg; Durante o prazo estipulado para a dura&ccedil;&atilde;o do contrato, n&atilde;o poder&aacute; o locador reaver o im&oacute;vel alugado. O locat&aacute;rio, todavia, poder&aacute; devolv&ecirc;-lo, pagando a multa pactuada, proporcionalmente ao per&iacute;odo de cumprimento do contrato, ou, na sua falta, a que for judicialmente estipulada.<br />
			....................................................................................................&rdquo; (NR)</p>
			<p>&ldquo;Art. 12. Em casos de separa&ccedil;&atilde;o de fato, separa&ccedil;&atilde;o judicial, div&oacute;rcio ou dissolu&ccedil;&atilde;o da uni&atilde;o est&aacute;vel, a loca&ccedil;&atilde;o residencial prosseguir&aacute; automaticamente com o c&ocirc;njuge ou companheiro que permanecer no im&oacute;vel.</p>
			<p>&sect; 1&deg; Nas hip&oacute;teses previstas neste artigo e no art. 11, a sub-roga&ccedil;&atilde;o ser&aacute; comunicada por escrito ao locador e ao fiador, se esta for a modalidade de garantia locat&iacute;cia.</p>
			<p>&sect; 2&deg; O fiador poder&aacute; exonerar-se das suas responsabilidades no prazo de 30 (trinta) dias contado do recebimento da comunica&ccedil;&atilde;o oferecida pelo sub-rogado, ficando respons&aacute;vel pelos efeitos da fian&ccedil;a durante 120 (cento e vinte) dias ap&oacute;s a notifica&ccedil;&atilde;o ao locador.&rdquo; (NR)</p>
			<p>&ldquo;Art. 13. ..........................................................................................................</p>
			<p>&sect; 3&deg; (VETADO)&rdquo;</p>
			<p>&ldquo;Art. 39. Salvo disposi&ccedil;&atilde;o contratual em contr&aacute;rio, qualquer das garantias da loca&ccedil;&atilde;o se estende at&eacute; a efetiva devolu&ccedil;&atilde;o do im&oacute;vel, ainda que prorrogada a loca&ccedil;&atilde;o por prazo indeterminado, por for&ccedil;a desta Lei.&rdquo; (NR)</p>
			<p>&ldquo;Art. 40. ..........................................................................................................</p>
			<p>II &ndash; aus&ecirc;ncia, interdi&ccedil;&atilde;o, recupera&ccedil;&atilde;o judicial, fal&ecirc;ncia ou insolv&ecirc;ncia do fiador, declaradas judicialmente;</p>
			<p>..........................................................................................................</p>
			<p>X &ndash; prorroga&ccedil;&atilde;o da loca&ccedil;&atilde;o por prazo indeterminado uma vez notificado o locador pelo fiador de sua inten&ccedil;&atilde;o de desonera&ccedil;&atilde;o, ficando obrigado por todos os efeitos da fian&ccedil;a, durante 120 (cento e vinte) dias ap&oacute;s a notifica&ccedil;&atilde;o ao locador.</p>
			<p>Par&aacute;grafo &uacute;nico. O locador poder&aacute; notificar o locat&aacute;rio para apresentar nova garantia locat&iacute;cia no prazo de 30 (trinta) dias, sob pena de desfazimento da loca&ccedil;&atilde;o.&rdquo; (NR)</p>
			<p>&ldquo;Art. 52. ..........................................................................................................</p>
			<p>&sect; 3&deg; (VETADO)&rdquo;</p>
			<p>&ldquo;Art. 59. ..........................................................................................................</p>
			<p>&sect; 1&deg; ................................................................................................................</p>
			<p>VI &ndash; o disposto no inciso IV do art. 9o, havendo a necessidade de se produzir repara&ccedil;&otilde;es urgentes no im&oacute;vel, determinadas pelo poder p&uacute;blico, que n&atilde;o possam ser normalmente executadas com a perman&ecirc;ncia do locat&aacute;rio, ou, podendo, ele se recuse a consenti-las;</p>
			<p>VII &ndash; o t&eacute;rmino do prazo notificat&oacute;rio previsto no par&aacute;grafo &uacute;nico do art. 40, sem apresenta&ccedil;&atilde;o de nova garantia apta a manter a seguran&ccedil;a inaugural do contrato;</p>
			<p>VIII &ndash; o t&eacute;rmino do prazo da loca&ccedil;&atilde;o n&atilde;o residencial, tendo sido proposta a a&ccedil;&atilde;o em at&eacute; 30 (trinta) dias do termo ou do cumprimento de notifica&ccedil;&atilde;o comunicando o intento de retomada;</p>
			<p>IX &ndash; a falta de pagamento de aluguel e acess&oacute;rios da loca&ccedil;&atilde;o no vencimento, estando o contrato desprovido de qualquer das garantias previstas no art. 37, por n&atilde;o ter sido contratada ou em caso de extin&ccedil;&atilde;o ou pedido de exonera&ccedil;&atilde;o dela, independentemente de motivo.</p>
			<p>.....................................................................................................................</p>
			<p>&sect; 3&ordm; No caso do inciso IX do &sect; 1o deste artigo, poder&aacute; o locat&aacute;rio evitar a rescis&atilde;o da loca&ccedil;&atilde;o e elidir a liminar de desocupa&ccedil;&atilde;o se, dentro dos 15 (quinze) dias concedidos para a desocupa&ccedil;&atilde;o do im&oacute;vel e independentemente de c&aacute;lculo, efetuar dep&oacute;sito judicial que contemple a totalidade dos valores devidos, na forma prevista no inciso II do art. 62.&rdquo; (NR)</p>
			<p>&ldquo;Art. 62. Nas a&ccedil;&otilde;es de despejo fundadas na falta de pagamento de aluguel e acess&oacute;rios da loca&ccedil;&atilde;o, de aluguel provis&oacute;rio, de diferen&ccedil;as de alugu&eacute;is, ou somente de quaisquer dos acess&oacute;rios da loca&ccedil;&atilde;o, observar-se-&aacute; o seguinte:</p>
			<p>I &ndash; o pedido de rescis&atilde;o da loca&ccedil;&atilde;o poder&aacute; ser cumulado com o pedido de cobran&ccedil;a dos alugu&eacute;is e acess&oacute;rios da loca&ccedil;&atilde;o; nesta hip&oacute;tese, citar-se-&aacute; o locat&aacute;rio para responder ao pedido de rescis&atilde;o e o locat&aacute;rio e os fiadores para responderem ao pedido de cobran&ccedil;a, devendo ser apresentado, com a inicial, c&aacute;lculo discriminado do valor do d&eacute;bito;</p>
			<p>II &ndash; o locat&aacute;rio e o fiador poder&atilde;o evitar a rescis&atilde;o da loca&ccedil;&atilde;o efetuando, no prazo de 15 (quinze) dias, contado da cita&ccedil;&atilde;o, o pagamento do d&eacute;bito atualizado, independentemente de c&aacute;lculo e mediante dep&oacute;sito judicial, inclu&iacute;dos:</p>
			<p>.....................................................................................................................</p>
			<p>III &ndash; efetuada a purga da mora, se o locador alegar que a oferta n&atilde;o &eacute; integral, justificando a diferen&ccedil;a, o locat&aacute;rio poder&aacute; complementar o dep&oacute;sito no prazo de 10 (dez) dias, contado da intima&ccedil;&atilde;o, que poder&aacute; ser dirigida ao locat&aacute;rio ou diretamente ao patrono deste, por carta ou publica&ccedil;&atilde;o no &oacute;rg&atilde;o oficial, a requerimento do locador;</p>
			<p>IV &ndash; n&atilde;o sendo integralmente complementado o dep&oacute;sito, o pedido de rescis&atilde;o prosseguir&aacute; pela diferen&ccedil;a, podendo o locador levantar a quantia depositada;</p>
			<p>.....................................................................................................................</p>
			<p>Par&aacute;grafo &uacute;nico. N&atilde;o se admitir&aacute; a emenda da mora se o locat&aacute;rio j&aacute; houver utilizado essa faculdade nos 24 (vinte e quatro) meses imediatamente anteriores &agrave; propositura da a&ccedil;&atilde;o.&rdquo; (NR)</p>
			<p>&ldquo;Art. 63. Julgada procedente a a&ccedil;&atilde;o de despejo, o juiz determinar&aacute; a expedi&ccedil;&atilde;o de mandado de despejo, que conter&aacute; o prazo de 30 (trinta) dias para a desocupa&ccedil;&atilde;o volunt&aacute;ria, ressalvado o disposto nos par&aacute;grafos seguintes.</p>
			<p>&sect; 1&deg; ..........................................................................................................</p>
			<p>b) o despejo houver sido decretado com fundamento no art. 9o ou no &sect; 2o do art. 46.</p>
			<p>....................................................................................................&rdquo;(NR)</p>
			<p>&ldquo;Art. 64. Salvo nas hip&oacute;teses das a&ccedil;&otilde;es fundadas no art. 9o, a execu&ccedil;&atilde;o provis&oacute;ria do despejo depender&aacute; de cau&ccedil;&atilde;o n&atilde;o inferior a 6 (seis) meses nem superior a 12 (doze) meses do aluguel, atualizado at&eacute; a data da presta&ccedil;&atilde;o da cau&ccedil;&atilde;o.</p>
			<p>....................................................................................................&rdquo;(NR)</p>
			<p>&ldquo;Art. 68. Na a&ccedil;&atilde;o revisional de aluguel, que ter&aacute; o rito sum&aacute;rio, observar-se-&aacute; o seguinte:</p>
			<p>.....................................................................................................................</p>
			<p>II &ndash; ao designar a audi&ecirc;ncia de concilia&ccedil;&atilde;o, o juiz, se houver pedido e com base nos elementos fornecidos tanto pelo locador como pelo locat&aacute;rio, ou nos que indicar, fixar&aacute; aluguel provis&oacute;rio, que ser&aacute; devido desde a cita&ccedil;&atilde;o, nos seguintes moldes:</p>
			<p>a) em a&ccedil;&atilde;o proposta pelo locador, o aluguel provis&oacute;rio n&atilde;o poder&aacute; ser excedente a 80% (oitenta por cento) do pedido;</p>
			<p>b) em a&ccedil;&atilde;o proposta pelo locat&aacute;rio, o aluguel provis&oacute;rio n&atilde;o poder&aacute; ser inferior a 80% (oitenta por cento) do aluguel vigente;</p>
			<p>.....................................................................................................................</p>
			<p>IV &ndash; na audi&ecirc;ncia de concilia&ccedil;&atilde;o, apresentada a contesta&ccedil;&atilde;o, que dever&aacute; conter contraproposta se houver discord&acirc;ncia quanto ao valor pretendido, o juiz tentar&aacute; a concilia&ccedil;&atilde;o e, n&atilde;o sendo esta poss&iacute;vel, determinar&aacute; a realiza&ccedil;&atilde;o de per&iacute;cia, se necess&aacute;ria, designando, desde logo, audi&ecirc;ncia de instru&ccedil;&atilde;o e julgamento;</p>
			<p>V &ndash; o pedido de revis&atilde;o previsto no inciso III deste artigo interrompe o prazo para interposi&ccedil;&atilde;o de recurso contra a decis&atilde;o que fixar o aluguel provis&oacute;rio.</p>
			<p>....................................................................................................&rdquo;(NR)</p>
			<p>&ldquo;Art. 71. ..........................................................................................................</p>
			<p>V &ndash; indica&ccedil;&atilde;o do fiador quando houver no contrato a renovar e, quando n&atilde;o for o mesmo, com indica&ccedil;&atilde;o do nome ou denomina&ccedil;&atilde;o completa, n&uacute;mero de sua inscri&ccedil;&atilde;o no Minist&eacute;rio da Fazenda, endere&ccedil;o e, tratando-se de pessoa natural, a nacionalidade, o estado civil, a profiss&atilde;o e o n&uacute;mero da carteira de identidade, comprovando, desde logo, mesmo que n&atilde;o haja altera&ccedil;&atilde;o do fiador, a atual idoneidade financeira;</p>
			<p>....................................................................................................&rdquo;(NR)</p>
			<p>&ldquo;Art. 74. N&atilde;o sendo renovada a loca&ccedil;&atilde;o, o juiz determinar&aacute; a expedi&ccedil;&atilde;o de mandado de despejo, que conter&aacute; o prazo de 30 (trinta) dias para a desocupa&ccedil;&atilde;o volunt&aacute;ria, se houver pedido na contesta&ccedil;&atilde;o.</p>
			<p>&sect; 1&deg; (VETADO)</p>
			<p>&sect; 2&deg; (VETADO)</p>
			<p>&sect; 3&deg; (VETADO)&rdquo; (NR)</p>
			<p>&ldquo;Art. 75. (VETADO).&rdquo;</p>
			<p>Art. 3&deg; (VETADO)</p>
			<p>Bras&iacute;lia, 9 de dezembro de 2009; 188o da Independ&ecirc;ncia e 121o da Rep&uacute;blica.</p>
			<p>LUIZ IN&Aacute;CIO LULA DA SILVA<br />
			Tarso Genro<br />
			Guido Mantega<br />
			Miguel Jorge</p>
			<p>Este texto n&atilde;o substitui o publicado no DOU de 10.12.2009</p>
			
	  </div> 
	</div>
</div>
</div>
<?php require "footer.php"; ?>
</body>
</html>